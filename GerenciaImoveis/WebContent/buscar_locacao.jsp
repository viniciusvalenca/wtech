<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ABSI - Buscar Locação</title>

<jsp:include page="includes/links.jsp" />
<script type="text/javascript" src="js/locacao.js"></script>
	
	<style type="text/css">
		.is-disabled {
			opacity: .5;
			pointer-events: none;
		}
	</style>
	
<script>
$(document).ready(function() {
	//$("#blocoEditar").hide();

	$('.date').datepicker({
		language : "pt-BR"
	 });
	
	$('#dt_inicio').datepicker({
		startDate : "01/01/1930",
		language : "pt-BR"
	});
	
	$('#dt_inicio').change(function() {
		date = $('#dt_inicio').datepicker('getDate');

		if(moment(date, 'YYYY-MM-DD', true).isValid()){
			var inicio = moment(date);
		  	var termino = moment(date).subtract(1, "days");
		  	var reajuste = moment(date);
		  	
		  	termino.add(30,'months');
		  	reajuste.add(12,'months');
		  	
		  	console.log("término: ", termino.toDate());
			console.log("reajuste: ", reajuste.toDate());
			console.log("inicio: ", inicio.toDate());
		  	
		  	$('#dt_termino').datepicker({ 
		  		startDate : inicio.toDate(),
				language : "pt-BR"
			});	  	
		    $("#dt_termino").datepicker('setDate', termino.toDate());
		  	
		    $('#dt_reajuste').datepicker({ 
		    	startDate : inicio.toDate(),
				language : "pt-BR"
			});
		    $("#dt_reajuste").datepicker('setDate', reajuste.toDate());
		}
	});
	
	$("#btnAtualizar").click(function() {
		$("#form-locacao").submit();
	});
});

	window.actionEvents = {
		'click .edit' : function(e, value, row, index) {
			
			$("#item-editar").removeClass("is-disabled");
			//$("#blocoEditar").show();
			
			$("#tableLocacao").bootstrapTable('refresh');
			
			$("#form-locacao").each(function() {
				this.reset();
			});
			
			searchImoveisId(row);
			//popularCampos(row);
		}
	};
	

	function searchImoveisDisponiveis(row) {
		$.ajax({
			type : 'GET',
			url : 'listarImoveisDisponiveis',
			contentType : 'application/json; charset=utf-8',
			dataType : 'json',
			success : function(retornoJson) {
				
				$('#tableImoveis').bootstrapTable('append', retornoJson);
				popularCampos(row);
			}
		});
	}

	function searchImoveisId(row) {
		$.ajax({
			type : 'GET',
			url : 'listarImovelId',
			data: {id: row.imovel.id},
			contentType : 'application/json; charset=utf-8',
			dataType : 'json',
			success : function(data) {
				$table = $('#tableImoveis');
				$table.bootstrapTable('removeAll');
				/*
				rows = [];
				rows.push({
	                "state": true,
	                "endereco.enderecoCompleto": data.endereco.enderecoCompleto,
	                "locador.nome_completo": data.locador.nome_completo,
	                "valorString": data.valorString
	            });*/
				
				$table.bootstrapTable('append', data);
	            $('#tableImoveis').bootstrapTable('check', 0);
				
				searchImoveisDisponiveis(row);
			}
		});
	}
	
	function popularCampos(locacao) {
		
		$("#idLocacaoHidden").val(locacao.id);
		
		/*$.each($table.bootstrapTable('getData'), function(index, value) {
			if (value.id == locacao.imovel.id) {
				$table.bootstrapTable("check", index);
			}
		});*/
		
		$('select#locatario').val(locacao.locatario.id);
		$('select').trigger('change');

		$("#modalidade_loc").val(locacao.modalidade);

		changeModalidade(locacao.modalidade);

		if (locacao.modalidade == "Seguro Fianca") {
			$("#valor_seguro").val(locacao.valor_seguro);

		} else if (locacao.modalidade == "Deposito") {
			$("#valor_deposito").val(locacao.valor_deposito);

		} else {
			
			if(locacao.listaInfoFiador.length >= 1){
				$('select#fiador').val(locacao.listaInfoFiador[0].fk_id_fiador);
				$('select').trigger('change');

				$("#mat_imovel").val(locacao.listaInfoFiador[0].mat_imovel);
				$("#cartorio").val(locacao.listaInfoFiador[0].cartorio);
				$("#endereco_imovel").val(locacao.listaInfoFiador[0].endereco_imovel);
			}
			
			if(locacao.listaInfoFiador.length >= 2){
				$('select#fiador2').val(locacao.listaInfoFiador[1].fk_id_fiador);
				$('select').trigger('change');

				$("#mat_imovel2").val(locacao.listaInfoFiador[1].mat_imovel);
				$("#cartorio2").val(locacao.listaInfoFiador[1].cartorio);
				$("#endereco_imovel2").val(locacao.listaInfoFiador[1].endereco_imovel);
			}
			
			if(locacao.listaInfoFiador.length == 3){
				$('select#fiador3').val(locacao.listaInfoFiador[2].fk_id_fiador);
				$('select').trigger('change');

				$("#mat_imovel3").val(locacao.listaInfoFiador[2].mat_imovel);
				$("#cartorio3").val(locacao.listaInfoFiador[2].cartorio);
				$("#endereco_imovel3").val(locacao.listaInfoFiador[2].endereco_imovel);
			}
		}

		$("#dt_inicio").val(locacao.contrato.dt_inicioFormat);
		$("#dt_termino").val(locacao.contrato.dt_terminoFormat);
		$("#dia_vencimento").val(locacao.contrato.dia_vencimento);
		$("#dt_reajuste").val(locacao.contrato.dt_reajusteFormat);
		$("#dt_entrega_chave").val(locacao.contrato.dt_entrega_chaveFormat);
	}

function actionAtivoFormatter(value, row, index) {
	if(value == null){
		return "Sim";
	}else{
		return "Não";
	}
}

</script>

</head>
<body>

	<jsp:include page="includes/menu.jsp" />

	<div
		class="col-lg-10 col-lg-offset-2 col-md-9 col-md-offset-3 col-sm-9 col-sm-offset-3 main">

		<div class="container-fluid main">

			<ol class="breadcrumb">
				<li><i class="fa fa-home"></i> <a href="dashboard">Dashboard</a></li>
				<li><i class="fa fa-search"></i><a href="#"> Consultar Locação</a></li>
			</ol>

			<div class="conteudo">
				<div class="row">
					<div class="col-lg-12">

						<div class="panel panel-default">
							<a data-target="#item-table">
								<div class="panel-heading">Consultar </div>
							</a>
							<div id="item-table" class="panel-body panel-branco collapse in">

								<table id="tableLocacao" data-toggle="table"
									data-classes="table table-condensed"
									data-url="/GerenciaImoveis/listarLocacao" data-search="true"
									data-show-refresh="true" data-content-type="application/json"
									data-data-type="json" data-sort-order="desc"
									data-show-columns="true"
									data-sort-name="stargazers_count" data-sort-order="desc"
									data-pagination="true" data-page-size="5" data-page-list="[5, 10, 20, 50, 100, 200]"
									data-pagination-first-text="Primeiro"
									data-pagination-pre-text="<i class='glyphicon glyphicon glyphicon-chevron-left'></i>"
									data-pagination-next-text="<i class='glyphicon glyphicon glyphicon-chevron-right'></i>"
									data-pagination-last-text="Último" data-locale="pt-BR">
									<thead>
										<tr>
											<th data-field="id" data-halign="center" data-align="center" data-sortable="true">ID</th>
											<th data-field="imovel.endereco.enderecoCompleto" data-halign="center" data-align="center" data-sortable="true">Imóvel</th>
											<th data-field="locatario.nome_completo" data-halign="center" data-align="center" data-sortable="true">Locatário</th>
											<th data-field="modalidade" data-halign="center" data-align="center" data-sortable="true">Modalidade</th>
											<th data-field="contrato.dt_entrega_chave" data-halign="center" data-formatter="actionAtivoFormatter" data-align="center" data-sortable="true">Ativo</th>
											<th data-halign="center" data-align="center" data-width="80" data-formatter="actionFormatter" data-events="actionEvents">Editar</th>
										</tr>
									</thead>
								</table>

							</div>
						</div>
						<div id="blocoEditar">
							<div class="panel panel-default">
								<a data-target="#item-editar">
									<div class="panel-heading">Editar Locação </div>
								</a>
								<div id="item-editar"
									class="panel-body panel-branco collapse in is-disabled">
									<form id="form-locacao" class="form-horizontal" method="POST"
										action="atualizarLocacao">

										<input id="nom_tela" name="nom_tela" type="hidden" value="Editar > Locação" />
										
										<input id="idImovelHidden" name="idImovelHidden" type="hidden"/>
										<input id="idLocacaoHidden" name="idLocacaoHidden" type="hidden"/>

										<jsp:include page="includes/locacao/associar_imovel.jsp" />

										<jsp:include page="includes/locacao/associar_locatario.jsp" />

										<jsp:include page="includes/locacao/contrato.jsp" />

										<div class="panel">
											<div class="row">
												<div class="col-lg-12">
													<button type="button" id="btnAtualizar"
														class="btn btn-primary btn-block">Atualizar Locação</button>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</body>
</html>