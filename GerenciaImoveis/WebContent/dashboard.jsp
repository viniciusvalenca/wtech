<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ABSI - Dashboard</title>

<jsp:include page="includes/links.jsp" />

<style type="text/css">
@media screen and (max-width: 1199px) {
	.panel-status:first-of-type {
		border-bottom: 1px solid #D9DEE4;
		padding: 0 0 0 20px;
		position: relative;
	}
	.panel-status:first-of-type:before {
		content: "";
		position: absolute;
		left: 0;
		/*height: 65px;*/
		height: 55px;
		border-left: 2px solid #ADB2B5;
		margin-top: 5px;
	}
}

@media ( min-width : 768px) {
	.modal-dialog {
		width: 290px;
		margin: 10% auto;
	}
}
</style>

<script>
	$(document)
			.ready(
					function() {

						$(".btnRenovar")
								.click(
										function() {
											var idContrato = $(this).attr(
													"tabindex");

											$
													.ajax({
														type : 'GET',
														url : 'renovarContrato',
														data : {
															idContrato : idContrato
														},
														contentType : 'application/json; charset=utf-8',
														dataType : 'json',
														success : function(
																retorno) {
															if (retorno) {
																location
																		.reload();
															} else {
																mostrarAlerta(
																		"Error",
																		"Não foi possível agendar a renovação do contrato");
															}
														}
													});
										});

						$(".btnNaoRenovar").click(function() {
							var idContrato = $(this).attr("tabindex");
							$(".idContratoNãoRenovar").html(idContrato);
						});

						$("#btnNãoRenovarRsp")
								.click(
										function() {
											var idContrato = $(
													".idContratoNãoRenovar")
													.html();

											$
													.ajax({
														type : 'GET',
														url : 'naoRenovarContrato',
														data : {
															idContrato : idContrato
														},
														contentType : 'application/json; charset=utf-8',
														dataType : 'json',
														success : function(
																retorno) {
															if (retorno) {
																location
																		.reload();
															} else {
																mostrarAlerta(
																		"Error",
																		"Não foi possível desfazer a renovação do contrato");
															}
														}
													});
										});
					});
</script>

</head>
<body>

	<jsp:include page="includes/menu.jsp" />

	<div
		class="col-lg-10 col-lg-offset-2 col-md-9 col-md-offset-3 col-sm-9 col-sm-offset-3 main">

		<div class="container-fluid main">

			<ol class="breadcrumb">
				<li><i class="fa fa-home"></i> <a href="dashboard">Dashboard</a></li>
			</ol>

			<div class="conteudo">

				<!-- Status -->
				<div class="col-lg-12">
					<div class="row">
						<div class="col-xs-12" style="margin-bottom: 15px">
							<div class="col-lg-2 col-md-6 col-sm-6 col-xs-6 panel-status">
								<span class="count_top f-azul-claro"> <i
									class="fa fa-user f-azul-claro"></i> Locadores
								</span>
								<div class="count f-azul-claro">${countLocador}</div>
								<!-- <span class="count_bottom"><i class="green">4% </i> From
							last Week</span> -->
							</div>

							<div class="col-lg-2 col-md-6 col-sm-6 col-xs-6 panel-status">
								<span class="count_top f-azul-claro"> <i
									class="fa fa-user f-azul-claro"></i> Locatários
								</span>
								<div class="count f-azul-claro">${countLocatario}</div>
								<!-- <span class="count_bottom"><i class="green">4% </i> From
							last Week</span> -->
							</div>

							<div class="col-lg-2 col-md-6 col-sm-6 col-xs-6 panel-status">
								<span class="count_top f-azul-claro"> <i
									class="fa fa-user f-azul-claro"></i> Fiadores
								</span>
								<div class="count f-azul-claro">${countFiador}</div>
								<!-- <span class="count_bottom"><i class="green">4% </i> From
							last Week</span> -->
							</div>

							<div class="col-lg-2 col-md-6 col-sm-6 col-xs-6 panel-status">
								<span class="count_top f-azul-claro"> <i
									class="fa fa-building f-azul-claro"></i> Imóveis
								</span>
								<div class="count f-azul-claro">${countImoveis}</div>
								<!-- <span class="count_bottom"><i class="green">4% </i> From
							last Week</span> -->
							</div>

							<div class="col-lg-2 col-md-6 col-sm-6 col-xs-6 panel-status">
								<span class="count_top f-azul-claro"> <i
									class="fa fa-usd f-azul-claro"></i> Lucro do Mês
								</span> 
								<c:if test="${user.nome != 'Lucia'}">
									<div class="count f-azul-claro">${countlucroMes}</div>
								</c:if>
								<c:if test="${user.nome == 'Lucia'}">
									<div class="count f-azul-claro">R$---,--</div>
								</c:if>
							</div>

							<div class="col-lg-2 col-md-6 col-sm-6 col-xs-6 panel-status">
								<span class="count_top f-azul-claro"> <i
									class="fa fa-percent"></i> Índice
								</span>
								<div class="count f-azul-claro">${indice}</div>
								<!-- <span class="count_bottom"><i class="green">4% </i> From
							last Week</span> -->
							</div>
						</div>
					</div>
				</div>

				<!-- Aniversáriantes do mês  -->
				<div class="col-xs-12 col-md-6">
					<div class="panel-nivers">
						<ul class="list-group">
							<li class="list-group-item">Aniversáriantes do mês <i
								class="fa fa-birthday-cake fa-2x"></i>
							</li>
							<c:choose>
								<c:when test="${fn:length(aniversariantes) gt 0}">
									<c:forEach var="aniversariantes" items="${aniversariantes}">
										<li class="list-group-item clearfix">
											<div class="row">
												<div class="col-xs-12">
													<h5 class="float-left">${aniversariantes.nome_completo}</h5>
													<h5 class="float-right">${aniversariantes.dt_nascimentoFormat}</h5>
												</div>
												<div class="col-xs-12">
													<h5 class="float-left">${aniversariantes.email}</h5>
												</div>
												<div class="col-xs-12">
													<h5 class="float-left">${aniversariantes.telefones[0].num_telefoneFormat}</h5>
												</div>
												<div class="col-xs-12">
													<h5 class="float-left">${aniversariantes.celulares[0].num_celularFormat}</h5>
												</div>
											</div>
										</li>
									</c:forEach>
								</c:when>
								<c:otherwise>
									<li class="list-group-item clearfix">
										<div class="row">
											<div class="col-xs-12">
												<h5 class="float-left">Nenhuma aniversariante no mês</h5>
												<h5 class="float-right"></h5>
											</div>
											<div class="col-xs-12">
												<h5 class="float-left"></h5>
											</div>
										</div>
									</li>
								</c:otherwise>
							</c:choose>
						</ul>
					</div>
				</div>

				<!-- Contratos a Vencer -->
				<div class="col-xs-12  col-md-6">
					<div class="panel-nivers">
						<ul class="list-group">
							<li class="list-group-item">Contratos a Vencer<i
								class="fa fa-file-text-o fa-2x"></i>
							</li>
							<c:choose>
								<c:when test="${fn:length(aVencer) gt 0}">
									<c:forEach var="locacao" items="${aVencer}">
										<li class="list-group-item clearfix">
											<div class="row">
												<div class="col-xs-12">
													<h5 class="float-left">${locacao.locatario.nome_completo}</h5>
													<h5 class="float-right">${locacao.contrato.dt_terminoFormat}
														<c:choose>
															<c:when test="${locacao.contrato.flg_renovar == false}">
																<button tabindex="${locacao.contrato.id}"
																	class="btn btn-success btn-xs btnRenovar"
																	style="margin-left: 10px; font-size: 12px">Renovar</button>
															</c:when>
															<c:otherwise>
																<button tabindex="${locacao.contrato.id}"
																	class="btn btn-default btn-xs btnNaoRenovar"
																	style="margin-left: 10px; font-size: 12px"
																	data-toggle="modal" data-target="#modalNaoRenovar">Não
																	Renovar</button>
															</c:otherwise>
														</c:choose>
													</h5>
												</div>
											</div>
										</li>
									</c:forEach>
								</c:when>
								<c:otherwise>
									<li class="list-group-item clearfix">
										<div class="row">
											<div class="col-xs-12">
												<h5 class="float-left">Nenhum contrato a vencer</h5>
												<h5 class="float-right"></h5>
											</div>
											<div class="col-xs-12">
												<h5 class="float-left"></h5>
											</div>
										</div>
									</li>
								</c:otherwise>
							</c:choose>
						</ul>
					</div>
				</div>

				<!-- Últimas Modificações -->
				<div class="col-xs-12 col-md-7">
					<div class="panel-nivers">
						<ul class="list-group">
							<li class="list-group-item">Últimas Modificações <i
								class="fa fa-edit fa-2x"></i>
							</li>
							<c:choose>
								<c:when test="${fn:length(historico) gt 0}">
									<c:forEach var="historico" items="${historico}">
										<li class="list-group-item clearfix">
											<div class="row">
												<div class="col-xs-12">
													<h5 class="float-left">${historico.usuario.nome}</h5>
													<h5 class="float-right">${historico.dt_modificacaoFormat}</h5>
												</div>
												<div class="col-xs-12">
													<h5 class="float-left">${historico.nom_tela}</h5>
													<h5 class="float-left panel-margin">${historico.descricao}</h5>
												</div>
											</div>
										</li>
									</c:forEach>
								</c:when>
								<c:otherwise>
									<li class="list-group-item clearfix">
										<div class="row">
											<div class="col-xs-12">
												<h5 class="float-left">Nenhuma modificação</h5>
												<h5 class="float-right"></h5>
											</div>
											<div class="col-xs-12">
												<h5 class="float-left"></h5>
											</div>
										</div>
									</li>
								</c:otherwise>
							</c:choose>
							<li class="list-group-item clearfix"><a class=""
								href="historico_acesso.jsp">Ver Mais (+)</a></li>
						</ul>
					</div>
				</div>

				<!-- Contratos Vencidos -->
				<div class="col-xs-12 col-md-5">
					<div class="panel-nivers">
						<ul class="list-group">
							<li class="list-group-item">Contratos Vencidos<i
								class="fa fa-file-text-o fa-2x"></i>
							</li>
							<c:choose>
								<c:when test="${fn:length(ctVencido) gt 0}">
									<c:forEach var="locacao" items="${ctVencido}">
										<li class="list-group-item clearfix">
											<div class="row">
												<div class="col-xs-12">
													<h5 class="float-left">${locacao.locatario.nome_completo}</h5>
													<h5 class="float-right">${locacao.contrato.dt_terminoFormat}</h5>
												</div>
											</div>
										</li>
									</c:forEach>
								</c:when>
								<c:otherwise>
									<li class="list-group-item clearfix">
										<div class="row">
											<div class="col-xs-12">
												<h5 class="float-left">Nenhum contrato vencido</h5>
												<h5 class="float-right"></h5>
											</div>
											<div class="col-xs-12">
												<h5 class="float-left"></h5>
											</div>
										</div>
									</li>
								</c:otherwise>
							</c:choose>
							<li class="list-group-item clearfix"><a class=""
								href="historico_contrato.jsp">Ver Mais (+)</a></li>
						</ul>
					</div>
				</div>

			</div>

		</div>
	</div>


	<div class="modal fade" id="modalNaoRenovar" tabindex="-1"
		role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">Deseja cancelar a
						renovação do contrato?</h4>
					<div class="idContratoNãoRenovar hidden"></div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Não</button>
					<button type="button" class="btn btn-primary" id="btnNãoRenovarRsp">Sim</button>
				</div>
			</div>
		</div>
	</div>


</body>
</html>
