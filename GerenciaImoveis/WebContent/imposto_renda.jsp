<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ABSI - Imposto de Renda</title>

<jsp:include page="includes/links.jsp" />

<script>

var id_locador = "";
var mes_ref = "";

$(document).ready(function() {
	
	$('#mes_referencia').datepicker({
		format: "mm/yyyy",
	    startView: 1,
	    minViewMode: 1,
		language : "pt-BR"
	});
	
	$('.date').datepicker({
		language : "pt-BR"
	 });
	
	$('#tableLocador').on('check.bs.table', function (row, $element) {
		id_locador = ($element.id);
		$('#idLocadorHidden').val($element.id);	
	});
	
	$("#blocoEditar").hide();
	
	$("#btnBuscar").click(function() {
		mes_ref = $('#mes_referencia').val();
		
		if(id_locador != 0 && mes_ref != ""){
			$("#blocoEditar").show();

			retornarImpostoRenda();
			
		}else{
			mostrarAlerta("Aviso", "Selecione as opções acima");
		}
		
	});
	
	$("#btnAtualizar").click(function(){
		if($("#imposto_renda").val() != ""){
			$("#form-imposto-renda").submit();
		}else{
			mostrarAlerta("Aviso","Valor inválido");
		}
	});
});

function retornarImpostoRenda(){
	$.ajax({
        url: "recuperarIR",
        type: "POST",
        data: $("#form-imposto-renda").serialize(),
        success: function(retorno) {
			console.log(JSON.parse(retorno));
        	var data = JSON.parse(retorno);
        	
        	$('#imposto_renda').val("");
        	if(data != null){
        		$('#imposto_renda').val(data.valor_descontoString).trigger('mask.maskMoney');
        	}
        },
        error: function(retorno){
        	
        }            
    });
}


</script>

</head>
<body>

	<jsp:include page="includes/menu.jsp" />

	<div
		class="col-lg-10 col-lg-offset-2 col-md-9 col-md-offset-3 col-sm-9 col-sm-offset-3 main">

		<div class="container-fluid main">

			<ol class="breadcrumb">
				<li><i class="fa fa-home"></i> <a href="dashboard">Dashboard</a></li>
				<li><i class="fa fa-cubes"></i> <a href="#">Imposto de Renda</a></li>
			</ol>

			<div class="conteudo">
				<form id="form-imposto-renda" class="form-horizontal"
					action="atualizarIR" method="POST">
					<div class="row">
						<div class="col-lg-12">

							<div class="panel panel-default">
								<a data-target="#item-gerar-conta">
									<div class="panel-heading">
										Buscar Locador </div>
								</a>

								<div id="item-table" class="panel-body panel-branco collapse in">

									<table id="tableLocador" 
										data-toggle="table"
										data-classes="table table-condensed"
										data-click-to-select="true" 
										data-single-select="true"
										data-url="/GerenciaImoveis/listarLocadores" 
										data-search="true"
										data-show-refresh="true" 
										data-content-type="application/json"
										data-data-type="json" 
										data-sort-order="desc"
										data-show-columns="true"
										data-sort-name="stargazers_count"
										data-pagination="true" 
										data-page-size="5"
										data-page-list="[5, 10, 20, 50, 100, 200]"
										data-pagination-first-text="Primeiro"
										data-pagination-pre-text="<i class='glyphicon glyphicon glyphicon-chevron-left'></i>"
										data-pagination-next-text="<i class='glyphicon glyphicon glyphicon-chevron-right'></i>"
										data-pagination-last-text="Último" data-locale="pt-BR">
										<thead>
											<tr>
												<th data-field="state" data-checkbox="true"
													data-align="center" data-width="40"></th>
												<th data-field="id" data-halign="center" data-align="center">ID</th>
												<th data-field="nome_completo" data-halign="center"
													data-align="center" data-sortable="true">Nome</th>
												<th data-field="email" data-halign="center"
													data-align="center" data-sortable="true">Email</th>
												<th data-field="cpf" data-halign="center"
													data-align="center" data-width="200" data-sortable="true">CPF</th>
												<th data-field="identidade" data-halign="center"
													data-align="center" data-width="200" data-sortable="true">Identidade</th>
											</tr>
										</thead>
									</table>
								</div>

								<div class="panel-body panel-branco collapse in">

									<input id="nom_tela" name="nom_tela" type="hidden"
										value="Financeiro > Imposto de Renda" /> 
									<input id="idLocadorHidden" name="idLocadorHidden" type="hidden" />

									<div class="col-lg-12">
										<div class="row">
											<div class="col-xs-12 col-lg-2">
												<div class="form-group">
													<label>Mês de Referência</label>
													<div class="input-group date" 
													data-provide="datepicker"
													data-date-format="mm/yyyy" 
													data-date-start-view="months"
													data-date-min-view-mode="months" 
													data-date-autoclose="true">
														<input id="mes_referencia" name="mes_referencia"
															type="text" class="form-control format_data_mes_ref">
														<div class="input-group-addon">
															<span class="glyphicon glyphicon-th"></span>
														</div>
													</div>
												</div>
											</div>

											<div class="col-xs-12 col-lg-2">
												<div class="form-group">
													<label class=""></label>
													<button type="button" id="btnBuscar"
														class="btn btn-primary btn-sm btn-block">Buscar</button>
												</div>
											</div>
										</div>
									</div>


								</div>
							</div>

							<div id="blocoEditar">

								<div class="panel panel-default">
									<a data-target="#item-info">
										<div class="panel-heading">
											Editar Imposto de Renda </div>
									</a>
									<div id="item-info" class="panel-body panel-branco collapse in">

										<div class="col-lg-12">
											<div class="row">
												<div class="col-lg-2">
													<div class="form-group">
														<label>Imposto de Renda</label> <input id="imposto_renda"
															name="imposto_renda" type="text" class="valor form-control">
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>

								<div class="panel">
									<div class="row">
										<div class="col-lg-12">
											<button type="button" id="btnAtualizar"
												class="btn btn-primary btn-block">Atualizar Itens</button>
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>
				</form>

			</div>
		</div>
	</div>

</body>
</html>
