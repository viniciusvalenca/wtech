<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ABSI - Buscar Imóvel</title>

<jsp:include page="includes/links.jsp" />
<script src="js/buscar_imovel.js" type="text/javascript"></script>

</head>
<body>

	<jsp:include page="includes/menu.jsp" />

	<div
		class="col-lg-10 col-lg-offset-2 col-md-9 col-md-offset-3 col-sm-9 col-sm-offset-3 main">

		<div class="container-fluid main">

			<ol class="breadcrumb">
				<li><i class="fa fa-home"></i> <a href="dashboard">Dashboard</a></li>
				<li><i class="fa fa-search"></i><a href="#"> Consultar Imóvel</a></li>
			</ol>

			<div class="conteudo">
				<div class="row">
					<div class="col-lg-12">

						<div class="panel panel-default">
							<a data-target="#item-table">
								<div class="panel-heading"> Consultar </div>
							</a>
							<div id="item-table" class="panel-body panel-branco collapse in">

								<table id="tableImoveis" data-toggle="table"
									data-classes="table table-condensed"
									data-url="/GerenciaImoveis/listarImovel"
								    data-search="true"
								    data-show-refresh="true"
								    data-content-type="application/json"
								    data-data-type="json"
								    data-sort-order="desc"
								    data-sort-name="stargazers_count" 
									data-sort-order="desc"
									data-pagination="true"
									data-show-columns="true"
									data-page-size="5" data-page-list="[5, 10, 20, 50, 100, 200]"
									data-pagination-first-text="Primeiro"
									data-pagination-pre-text="<i class='glyphicon glyphicon glyphicon-chevron-left'></i>"
									data-pagination-next-text="<i class='glyphicon glyphicon glyphicon-chevron-right'></i>"
									data-pagination-last-text="Último" data-locale="pt-BR">
									<thead>
										<tr>
											<th data-field="id"data-halign="center"
												data-align="center" data-width="80" data-sortable="true">ID</th>
											<th data-field="endereco.enderecoCompleto" data-halign="center"
												data-align="center" data-sortable="true">Endereço</th>
											<th data-field="locador.nome_completo" data-halign="center"
												data-align="center" data-sortable="true">Locador</th>
											<th data-field="valorString" data-halign="center" data-align="center"
												data-width="160" data-sortable="true">Valor (R$)</th>
											<th data-field="flg_disponivel" data-halign="center"
												data-align="center" data-formatter="maskDisponivel" 
												data-width="80" data-sortable="true">Disponibilidade</th>
											<th data-halign="center" data-align="center"
												data-width="80" data-formatter="actionFormatter"
												data-events="actionEvents">Editar</th>
										</tr>
									</thead>
								</table>

							</div>
						</div>
						<div id="blocoEditar">
							<div class="panel panel-default">
								<a data-target="#item-editar">
									<div class="panel-heading">
										Editar Imóvel </div>
								</a>
								<div id="item-editar" class="panel-body panel-branco collapse in">
									<form id="form-imovel" class="form-horizontal" method="POST"
										action="atualizarImovel">
										
										<input id="nom_tela" name="nom_tela" type="hidden" value="Editar > Imóvel"/>

										<jsp:include page="includes/imovel/associar.jsp" />

										<jsp:include page="includes/endereco.jsp" />

										<jsp:include page="includes/imovel/infos_extras.jsp" />

										<jsp:include page="includes/imovel/documentacao.jsp" />


										<div class="panel">
											<div class="row">
												<div class="col-lg-12">
													<button type="button" id="btnAtualizar"
														class="btn btn-primary btn-block">Atualizar
														Imóvel</button>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>