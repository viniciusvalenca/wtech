<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ABSI - Relatório Controle de Aluguéis</title>

<jsp:include page="includes/links.jsp" />

<script type="text/javascript" src="bootstrap-table/extensions/editable/bootstrap-table-editable.js"></script>

<!-- link href="bootstrap-x-editable/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"-->
<!-- script src="bootstrap-x-editable/bootstrap3-editable/js/bootstrap-editable.js"></script-->

<link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
<script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>

<style type="text/css">
.editable-input>input {
	margin-left: 20px !important;
}
</style>

<script>
$(document).ready(function() {

		var _field = "";
		
		$("#editar").hide();
		
		$("#editar").click(function() {
			if(_field != ""){
				_field.css('background-color','white');
				_field.css('color','black');
				
            	$('#myModal').modal('show');
			}else{
				mostrarAlerta("Aviso","Selecione uma locacação");
			}
		});
		
		$('#table').on('click-row.bs.table', function (row, $element, field) {
			$("#idboleto").val($element.id);
			$("#dt_pag_to_locador").val($element.dt_pag_to_locadorFormat);
			
			var valorTMP=($element.valor_pag_to_locador.toFixed(2)).toString();
			valorTMP=valorTMP.replace(",","*").replace(".",",").replace("*",".");
			$("#valor_pag_to_locador").val(valorTMP).trigger('mask.maskMoney');
			
			$("#cheque").val($element.cheque);
			
			if(_field != ""){
				_field.css('background-color','white');
				_field.css('color','black');
			}
		
			_field = field;
			
			field.css('background-color','rgba(0, 128, 0, 0.5)');
			field.css('color','white');
		});
		
		$("#btnAtualizar").click(function() {
			_field.css('background-color','white');
			_field.css('color','black');
		});
		
		$('#form-atualizar-relatorio').validate({
			submitHandler: function(form) {
		        $.ajax({
		            url: form.action,
		            type: form.method,
		            data: $(form).serialize(),
		            success: function(retorno) {
		            	$('#myModal').modal('hide');
		            	
		            	if(retorno == "true"){
		            		_field = "";
		            		recuperarPagamentoMes();
		            		mostrarAlerta("Sucesso","Associação realizada com sucesso");
		            	}else{
		            		mostrarAlerta("Error","Associação não foi realizada");
		            	}
		            }            
		        });
		    }
		});
		
		/* ----------------------- */
		
		$('#mes_referencia').datepicker({
			format: "mm/yyyy",
		    startView: 1,
		    minViewMode: 1,
		  	language : "pt-BR"
		});
		
		$('#mes_referencia').change(function() {
			recuperarPagamentoMes();
		});
		
		//$('#mes_referencia').datepicker().on('changeDate', function (ev) {
			//
		//});
		
		$('.date').datepicker({
			language : "pt-BR"
		});
});

function recuperarPagamentoMes() {
	$.ajax({
		type : 'GET',
		url : 'recuperarPagamentoMes',
		data : { mes_ref : $("#mes_referencia").val() },
		contentType : 'application/json; charset=utf-8',
		dataType : 'json',
		success : function(retorno) {
			$("#editar").show();
			
			$('#table').bootstrapTable('load', retorno);
		}
	});
}
</script>
</head>
<body>

	<jsp:include page="includes/menu.jsp" />

	<div
		class="col-lg-10 col-lg-offset-2 col-md-9 col-md-offset-3 col-sm-9 col-sm-offset-3 main">

		<div class="container-fluid main">

			<ol class="breadcrumb">
				<li><i class="fa fa-home"></i> <a href="dashboard">Dashboard</a></li>
				<li><i class="fa fa-file"></i> <a href="#">Relatório Controle de Aluguéis</a></li>
			</ol>

			<div id="myModal" class="modal fade" tabindex="-1" role="dialog">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							<h4 class="modal-title">Editar</h4>
						</div>
						<form id="form-atualizar-relatorio" name="form-atualizar-relatorio" action="atualizarRelatorio" method="POST">
						<div class="modal-body">
								<div class="form-group">
									<label for="idboleto" class="control-label">ID:</label>
									<input type="text" class="form-control" id="idboleto" name="idboleto" readonly/>
								</div>
								<div class="form-group">
									<label for="dt_pag_to_locador" class="control-label">Data
										do pagamento para o locador:</label>
									<div class="input-group date" data-provide="datepicker"
										data-date-format="dd/mm/yyyy" data-date-autoclose="true">
										<input id="dt_pag_to_locador" name="dt_pag_to_locador" type="text"
											class="form-control format_data">
										<div class="input-group-addon">
											<span class="glyphicon glyphicon-th"></span>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label for="valor_pag_to_locador" class="control-label">Valor do pagamento para o locador:</label>
									<input class="form-control" id="valor_pag_to_locador" name="valor_pag_to_locador" data-thousands="." data-decimal=","/>
								</div>
								<div class="form-group">
									<label for="cheque" class="control-label">Cheque:</label>
									<input class="form-control" id="cheque" name="cheque" data-thousands="." data-decimal=","/>
								</div>
						</div>
						<div class="modal-footer">
							<button type="submit" class="btn btn-primary" id="btnAtualizar">Atualizar</button>
						</div>
						</form>
					</div>
				</div>
			</div>

			<div class="conteudo">
				<div class="row">
					<div class="col-lg-12">

						<div class="panel panel-default">
							<a data-target="#item-gerar-conta">
								<div class="panel-heading">Relatório Controle de Aluguéis</div>
							</a>
							<div class="panel-body panel-branco collapse in">
								<form>
									<div class="row">
										<div class="col-xs-12 col-lg-4">
											<div class="form-group">
												<label>Mês de Referência</label>
												<div class="input-group date" data-provide="datepicker"
													data-date-format="mm/yyyy" data-date-start-view="months"
													data-date-min-view-mode="months" data-date-autoclose="true">
													<input id="mes_referencia" name="mes_referencia" type="text"
														class="form-control">
													<div class="input-group-addon">
														<span class="glyphicon glyphicon-th"></span>
													</div>
												</div>
											</div>
										</div>
									</div>
								</form>
							
								<div class="toolbar">
									<button id="editar" type="button" class="btn btn-default">Editar</button>
								</div>
								<table id="table" 
									class="table"
									data-toggle="table" 
									data-toolbar=".toolbar"
									data-search="true" 
									data-pagination="true" 
									data-page-size="5"
									data-card-view="true"
									data-show-columns="true"
									data-sort-name="locacao.imovel.locador.nome_completo" 
									data-page-list="[5, 10, 20, 50, 100, 200]">
									<thead>
										<tr>
											<th data-field="id" data-visible="false">ID</th>
											<th data-field="locacao.imovel.locador.nome_completo">Locador</th>
											<th data-field="locacao.imovel.endereco.enderecoCompleto">Imóvel</th>
											<th data-field="locacao.locatario.nome_completo">Locatário</th>
											<th data-field="valor_aluguelString">Valor do Aluguel</th>
											<th data-field="dt_vencimentoFormat">Data de Vencimento do Aluguel</th>
											<th data-field="dt_pagamentoFormat">Data que o Dinheiro do Locatário Entrou na Conta</th>
											<th data-field="valor_pagoString">Valor que o Locatario Pagou</th>
											<th data-field="dt_pag_to_locadorFormat">Data de Pagamento para o Locador</th>
											<th data-field="valor_pag_to_locadorFormat">Valor do Pagamento para o Locador</th>
											<th data-field="cheque">Cheque</th>
										</tr>
									</thead>
								</table>
							</div>

						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
