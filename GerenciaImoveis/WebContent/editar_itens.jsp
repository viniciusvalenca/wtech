<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ABSI - Editar Itens</title>

<jsp:include page="includes/links.jsp" />


<script>

var id_locacao = "";
var mes_ref = "";
var valorAluguel = "";
var startDateFormat = "";
var startDateFormat2 = "";

$(document).ready(function() {
	$('.parcela').mask('00/00');
	$('.merRefBoleto').mask('00/0000');
	
	$('.btnDeletar').click(function(){
		if(id_locacao != null){
			tab = $("li a[aria-expanded=true]").attr('aria-controls');
			
			if($('input[name="' + tab + 'descricaoItem"]').val() != ""){
				$('input[name="' + tab + 'descricaoItem"]').val("");
			}
			$('input[name="' + tab + 'numParcela"]').val("");
			$( 'input[name="' + tab + 'itemValor"]').val("");
			
			$("#btnAtualizar").click();
			
		}else{
			mostrarAlerta("Aviso","Nenhuma locação selecionada!");
		}
	});
	
	startDateFormat = moment(moment().utc().valueOf()).format('MM/YYYY');
	startDateFormat2 = moment(moment().utc().valueOf()).format('YYYY-MM-DD');
	
	$('#tableLocacao').on('check.bs.table', function (row, $element) {
		$("#blocoEditar").hide();
		limparTabela();
		
		id_locacao = ($element.id);
		valorAluguel = $element.imovel.valorString;
		$('#idLocacaoHidden').val($element.id);	
		
		var temp = $element.contrato.dt_inicioFormat;
		var $data = (temp).split("/");
		var mes = $data[1];
		var ano = $data[2];
		var contrato_dt_inicio = moment().set({'year': ano, 'month': mes-1, 'date': 1}).format('YYYY-MM-DD');
		
		startDateFormat2 = moment(startDateFormat2).subtract(1, 'months').format('YYYY-MM-DD');
		
		if(moment(startDateFormat2).isSameOrAfter(contrato_dt_inicio)){
			$('#mes_referencia').datepicker('setStartDate', moment(startDateFormat2).format('MM/YYYY'));
		
		}else{
			$('#mes_referencia').datepicker('setStartDate', moment(contrato_dt_inicio).format('MM/YYYY'));
		}
	});
	
	$('#tableLocacao').on('uncheck.bs.table', function (row, $element) {
		$("#blocoEditar").hide();
		limparTabela();
		id_locacao = "";
		$('#idLocacaoHidden').val("");	
	});
	
	$('#mes_referencia').datepicker({
		format: "mm/yyyy",
	    startView: 1,
	    minViewMode: 1,
	    startDate: startDateFormat,
		language : "pt-BR"
	});
	
	$('.date').datepicker({
		language : "pt-BR"
	 });
	
	$("#blocoEditar").hide();
	
	$("#btnBuscar").click(function() {
		mes_ref = $('#mes_referencia').val();
		
		limparTabela();

		if(id_locacao != 0 && mes_ref != ""){
			searchItensLocacoes();
			$("#blocoEditar").show();
			$("#blocoMesRef").remove();
			
		}else{
			mostrarAlerta("Aviso", "Selecione as opções acima");
		}
		
		$("select").each(function(){
		    var name = $(this).attr("name");
		    $(this).children("option:first").each(function(){
		        if($(this).html() == "Receita"){
		        	$('select[name="'+name+'"]').val("R");
		        }else{
		        	$('select[name="'+name+'"]').val("D");
		        }
		    });
		});
	});
});

function limparItens(){
	
	var arr = [
		 'tabA','tabB','tabC','tabD',
		   'tabE','tabF','tabG','tabH',
		   'tabI','tabJ','tabK','tabL',
		   'tabM','tabN','tabO','tabP',
		   'tabQ','tabR','tabS','tabT','tabU','tabV'
		];
	$.each(arr, function (index, value) {
		$('input[name="'+ value + 'itemValor"]').val("");
		$('input[name="'+ value + 'numParcela"]').val("");
	});
}

function limparTabela(){
	$("#blocoTAB :input").each(function(){
		$(this).val("");
	});
	$(".nav-pills li a").css("background-color","#2e363f"); //AZUL ESCURO
}

function searchItensLocacoes() {
	$.ajax({
		type : 'GET',
		url : 'listitenslocacao',
		data: { id_locacao: id_locacao, mes_ref : mes_ref } ,
		contentType : 'application/json; charset=utf-8',
		dataType : 'json',
		success : function(retornoJson) {
			//$('#tableLocacao').bootstrapTable('load', retornoJson);
			$("li a[aria-controls=tabA]").click();
			if(retornoJson != 'null'){
				console.log("retornoJson", retornoJson);
				var lista= JSON.parse(retornoJson);	
				console.log("lista", lista);
				popularCampos(lista);
				
			}else{
				mostrarAlerta("Aviso", "Não existe itens para o mês selecionado");
				$(".nav-pills li a[aria-controls=tabA]").css("background-color","#5cb85c"); // VERDE
				
				$('input[name="tabAitemValor"]').val(valorAluguel).trigger('mask.maskMoney');
				$('input[name="tabAnumParcela"]').val("01/30");
				$('select[name="tabArdStatus"]').val("D");
				
				$('input[name="tabAmesRefBoleto"]').val(mes_ref);
				
				var arr = [
					 'tabA','tabB','tabC','tabD',
					   'tabE','tabF','tabG','tabH',
					   'tabI','tabJ','tabK','tabL',
					   'tabM','tabN','tabO','tabP',
					   'tabQ','tabR','tabS','tabT','tabU','tabV'
					];
				$.each(arr, function (index, value) {
					
					$('#' + value + 'ckSoma').prop('checked', true);
					
					$('#' + value + 'ckSoma').val("1");
					$('#' + value + 'ckDiminui').val("2");
					$('#' + value + 'ckFica').val("3");
					
					if(value == "tabB")
						$('#' + value + 'ckFica').val("4");
					
					$('input[name="' + value + 'mesRefBoleto"]').val(mes_ref);
				});
				
			}
		}
	});
}

function popularCampos(listaItens) {
	
	

	$('.merRefBoleto').val($('#mes_referencia').val());
	
	var arr = [
		   'tabA','tabB','tabC','tabD',
		   'tabE','tabF','tabG','tabH',
		   'tabI','tabJ','tabK','tabL',
		   'tabM','tabN','tabO','tabP',
		   'tabQ','tabR','tabS','tabT','tabU','tabV'
		];
	$.each(arr, function (index, value) {
		
		$('#' + value + 'ckSoma').prop('checked', true);
		
		$('#' + value + 'ckSoma').val("1");
		$('#' + value + 'ckDiminui').val("2");
		$('#' + value + 'ckFica').val("3");
		
		if(value == "tabB")
			$('#' + value + 'ckFica').val("4");
		
		/*
		if(value == '1')
			$('#' + tp + 'ckSoma').prop('checked', true);
			$('#' + tp + 'ckDiminui').prop('checked', true);
			$('#' + tp + 'ckFica').prop('checked', true);*/
	});
	
	
	if(jQuery.isEmptyObject(listaItens)){
		
		$(".nav-pills li a[aria-controls=tabA]").css("background-color","#5cb85c"); // VERDE
		
		$('input[name="tabAitemValor"]').val(valorAluguel).trigger('mask.maskMoney');
		$('input[name="tabAnumParcela"]').val("01/30");
		$('select[name="tabArdStatus"]').val("D");
		$('input[name="tabAmesRefBoleto"]').val(mes_ref);
		//alert("pagina editar itens jQuery.isEmptyObject(listaItens)");
		
	}else{
		var mes_refFormat = listaItens[0].mes_refFormat;
		
		var $data = (mes_refFormat).split("/");
		var mes = $data[0];
		var ano = $data[1];
		var dataControler = moment().set({'year': ano, 'month': mes-1, 'date': 1}).format('YYYY-MM-DD');
		
		var $dataPag = (mes_ref).split("/");
		var mesPag = $dataPag[0];
		var anoPag = $dataPag[1];
		var dataPag = moment().set({'year': anoPag, 'month': mesPag-1, 'date': 1}).format('YYYY-MM-DD');
		
		//alert("dataControler: " + dataControler + " dataPag: " + dataPag);
		//alert(moment(dataControler).isSame(dataPag));
		
		//alert("listaItens: " + listaItens);
		//alert("length: " + listaItens.length);
		//alert("length(): " + listaItens.length());
		//alert("size: " + listaItens.size);
		
		if(moment(dataControler).isSame(dataPag)){
		
			$.each(listaItens, function(index, value) {
				//alert(JSON.stringify(value))

				var valorTMP=(value.valor.toFixed(2)).toString();
				valorTMP=valorTMP.replace(",","*").replace(".",",").replace("*",".");
				
				var tp = value.tipo;
				$(".nav-pills li a[aria-controls=" + tp + "]").css("background-color","#27a9e3"); //AZUL CLARO
							
				if(tp== 'tabJ' || tp=='tabN' || tp=='tabO'|| tp == "tabR"|| tp == "tabS"|| tp == "tabL")
					$('input[name="' + tp + 'descricaoItem"]').val(value.descricao);
				
				$('input[name="' + tp + 'numParcela"]').val(value.parcela + "/" + value.tot_parcela);

				$('input[name="' + tp + 'itemValor"]').val(valorTMP).trigger('mask.maskMoney');
				
				$('select[name="' + tp + 'rdStatus"]').val(value.rd);
				
				if(value.mes_refBoleto != null && value.mes_refBoleto != ""){
					$('input[name="' + tp + 'mesRefBoleto"]').val(value.mes_refBoleto);
				}
				
				//alert("pagina editar itens moment(dataControler).isSame(dataPag)");
				
				if(value.status == '1')
					$('#' + tp + 'ckSoma').prop('checked', true);
				else if(value.status == '2')
					$('#' + tp + 'ckDiminui').prop('checked', true);
				else if(value.status == '3' || value.status == '4')
					$('#' + tp + 'ckFica').prop('checked', true);
			});
			
		}else{
			var diferenca = -1;
			
			do {
				dataControler = moment(dataControler).add(1, 'months');
				diferenca++;
			}
			while (moment(dataControler).isSameOrBefore(dataPag));
			
			$.each(listaItens, function(index, value) {
				var tp = value.tipo;
				var _parcela = parseInt(value.parcela);
				var _diferenca = parseInt(diferenca);
				
				
				if((_parcela + _diferenca) <= value.tot_parcela){	
					$(".nav-pills li a[aria-controls=" + tp + "]").css("background-color","#5cb85c"); // VERDE
					
					//alert("pagina editar itens if (_parcela + _diferenca) <= value.tot_parcela");
					
					if(value.status == '1')
						$('#' + tp + 'ckSoma').prop('checked', true);
					else if(value.status == '2')
						$('#' + tp + 'ckDiminui').prop('checked', true);
					else if(value.status == '3' || value.status == '4')
						$('#' + tp + 'ckFica').prop('checked', true);
					
					if(tp== 'tabJ' || tp=='tabN' || tp=='tabO'|| tp == "tabR"|| tp == "tabS"|| tp == "tabL")
						$('input[name="' + tp + 'descricaoItem"]').val(value.descricao);
					
					var valorTMP=(value.valor.toFixed(2)).toString();
					valorTMP=valorTMP.replace(",","*").replace(".",",").replace("*",".");
					
					if(tp == 'tabA')
						$('input[name="' + tp + 'itemValor"]').val(valorAluguel).trigger('mask.maskMoney');
					else
						$('input[name="' + tp + 'itemValor"]').val(valorTMP).trigger('mask.maskMoney');
					
					$('input[name="' + tp + 'numParcela"]').val((_parcela + _diferenca) + "/" + value.tot_parcela);
					$('select[name="' + tp + 'rdStatus"]').val(value.rd);
					
					if(value.mes_refBoleto != null && value.mes_refBoleto != ""){
						var $data2 = (value.mes_refBoleto).split("/");
						var mes2 = $data2[0];
						var ano2 = $data2[1];
						var dataControler2 = moment().set({'year': ano2, 'month': mes2-1, 'date': 1}).format('YYYY-MM-DD');
						var dtTMP=moment(dataControler2).add(_diferenca, 'months');
						var str = moment(dtTMP).format('MM/YYYY');
						
						$('input[name="' + tp + 'mesRefBoleto"]').val(str);
					}
				}else{
					//alert("pagina editar itens else (_parcela + _diferenca) <= value.tot_parcela");
				}
			});
		}
	}
}

</script>

</head>
<body>

	<jsp:include page="includes/menu.jsp" />

	<div class="col-lg-10 col-lg-offset-2 col-md-9 col-md-offset-3 col-sm-9 col-sm-offset-3 main">

		<div class="container-fluid main">

			<ol class="breadcrumb">
				<li><i class="fa fa-home"></i> <a href="dashboard">Dashboard</a></li>
				<li><i class="fa fa-list-ul"></i> <a href="#">Editar
						Itens</a></li>
			</ol>

			<div class="conteudo">
				<form id="form-itemLocacao" class="form-horizontal"
					action="atualizaritens" method="POST">
					<div class="row">
						<div class="col-lg-12">

							<div class="panel panel-default">
								<a data-target="#item-gerar-conta">
									<div class="panel-heading"> Buscar Itens </div>
								</a>

								<div id="item-table" class="panel-body panel-branco collapse in">

									<table id="tableLocacao" 
										data-toggle="table"
										data-classes="table table-condensed"
										data-single-select="true"
										data-url="/GerenciaImoveis/listarLocacao" 
										data-search="true"
										data-search-time-out="1500"
										data-content-type="application/json"
										data-data-type="json" 
										data-sort-order="desc"
										data-sort-name="stargazers_count" 
										data-pagination="true" 
										data-page-size="5" 
										data-page-list="[5, 10, 20, 50, 100, 200]"
										data-pagination-first-text="Primeiro"
										data-pagination-pre-text="<i class='glyphicon glyphicon glyphicon-chevron-left'></i>"
										data-pagination-next-text="<i class='glyphicon glyphicon glyphicon-chevron-right'></i>"
										data-pagination-last-text="Último" data-locale="pt-BR">
										<thead>
											<tr>
												<th data-field="state" data-checkbox="true"
													data-align="center" data-width="40"></th>
												<th data-field="id" data-halign="center" data-align="center" data-sortable="true">ID</th>
												<th data-field="imovel.endereco.enderecoCompleto"
													data-halign="center" data-align="center" data-sortable="true">Imóvel</th>
												<th data-field="imovel.locador.nome_completo"
													data-halign="center" data-align="center" data-sortable="true">Locador</th>
												<th data-field="locatario.nome_completo"
													data-halign="center" data-align="center" data-sortable="true">Locatário</th>
												<th data-field="imovel.valorString"
													data-halign="center" data-align="center" data-width="100" data-sortable="true">Aluguel</th>
												<th data-field="modalidade" data-halign="center"
													data-align="center" data-sortable="true">Modalidade</th>
											</tr>
										</thead>
									</table>
								</div>

								<div class="panel-body panel-branco collapse in">

									<input id="nom_tela" name="nom_tela"
										type="hidden" value="Editar > Itens de Locação" />
										
										<input id="idLocacaoHidden" name="idLocacaoHidden"
										type="hidden" />

									<div class="col-lg-12">
										<div class="row">
											<div class="col-xs-12 col-lg-4">
												<div class="form-group">
													<label>Mês de Referência</label>
													<div class="input-group date" data-provide="datepicker"
													data-date-format="mm/yyyy" data-date-start-view="months"
														data-date-min-view-mode="months" data-date-autoclose="true">
														<input id="mes_referencia" name="mes_referencia" 
															type="text" class="form-control format_data_mes_ref">
														<div class="input-group-addon">
															<span class="glyphicon glyphicon-th"></span>
														</div>
													</div>
												</div>
											</div>

											<div class="col-xs-12 col-lg-2">
												<div class="form-group">
													<label class=""></label>
													<button type="button" id="btnBuscar"
														class="btn btn-primary btn-sm btn-block">Buscar</button>
												</div>
											</div>
										</div>
									</div>


								</div>
							</div>

							<div id="blocoEditar">
								<jsp:include page="includes/locacao/locacao_item.jsp" />


								<div class="panel">
									<div class="row">
										<div class="col-lg-12">
											<button type="submit" id="btnAtualizar"
												class="btn btn-primary btn-block">Atualizar Itens</button>
										</div>
									</div>
								</div>
							</div>
							
						</div>
					</div>
				</form>
				
			</div>
		</div>
	</div>

</body>
</html>
