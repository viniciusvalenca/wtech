<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ABSI - Boletos</title>

<jsp:include page="includes/links.jsp" />

<script>
$(document).ready(function(){

	$btn = $("#btnBaixar");
	$btn2 = $("#btnBuscar");
	$table = $('#tableBoletos');
	
	$btn.addClass("disabled");
	
	$table.on('check.bs.table', function (row, $element) {
		var obj = $(this).bootstrapTable('getSelections');
		
		if(obj.length > 0){
			$btn.html('Baixar Todos Selecionados');
			$btn.removeClass("disabled");
		}
	});
	
	$table.on('check-all.bs.table', function (row, $element) {		
		var obj = $(this).bootstrapTable('getSelections');
		
		if(obj.length > 0){
			$btn.html('Baixar Todos');
			$btn.removeClass("disabled");
		}
	});
	
	$table.on('uncheck-all.bs.table', function (row, $element) {		
		$btn.addClass("disabled");
	});
	
	$btn.click(function(){
		fazerDownloadBoleto();
	});
	
	$('#mes_referencia').datepicker({
		format: "mm/yyyy",
	    startView: 1,
	    minViewMode: 1,
		language : "pt-BR"
	});
	
	$('.date').datepicker({
		language : "pt-BR"
	 });
	
	$btn2.click(function(){
		$table.bootstrapTable('removeAll');
		listarBoletosNovosPorMes();
	});
});

function actionEnviadoFormatter(value, row, index) {
	if(value){
		return "Sim";
	}else{
		return "Não";
	}
}

function actionDownloadFormatter(value, row, index) {
	return '<a class="baixar disabled" href="#" value="'+ row.id + '"><i class="fa fa-download" aria-hidden="true"></i></a>'
}

function actionEnviarFormatter(value, row, index) {
	if(row.locacao.locatario.email != ""){
		return '<a class="enviar ml10" href="#" value="'+ value + '"><i class="fa fa-paper-plane" aria-hidden="true"></i></a>'
	}else{
		return '<i class="fa fa-paper-plane f-vermelho" aria-hidden="true"></i>';
	}	
}

function fazerDownloadBoleto(){
	var obj = $table.bootstrapTable('getSelections');
	var listaID = [];
	$.each(obj, function(index, value) {
		listaID.push(value.id);
	});
	$("#idboleto").val(JSON.stringify(listaID));
	$("#form-gerar-boleto").submit();
}

function listarBoletosNovosPorMes() {
	$.ajax({
		type : 'GET',
		url : 'listarBoletosNovosPorMes',
		data : {mes_ref : $("#mes_referencia").val()},
		contentType : 'application/json; charset=utf-8',
		dataType : 'json',
		success : function(retorno) {
			$("#tableBoletos").bootstrapTable('load',retorno);
		}
	});
}

function enviarBoleto(id) {
	$("#mes_referencia").prop('disabled', true);
	$.ajax({
		type : 'GET',
		url : 'enviarBoleto',
		data : {id : id},
		contentType : 'application/json; charset=utf-8',
		dataType : 'json',
		success : function(retorno) {
			if(retorno){
				mostrarAlerta("Sucesso","Email enviado com sucesso");
				listarBoletosNovosPorMes();
			}else{
				mostrarAlerta("Error","Email não enviado");
			}
			$("#mes_referencia").prop('disabled', false);
		}
	});
}

window.actionEvents = {
	'click .baixar' : function(e, value, row, index) {
		$table = $('#tableBoletos');
		$table.bootstrapTable('check', index);
		
    	fazerDownloadBoleto();
	},
	'click .enviar' : function(e, value, row, index) {
		enviarBoleto(row.id);
	}
};
</script>

</head>
<body>

	<jsp:include page="includes/menu.jsp" />

	<div
		class="col-lg-10 col-lg-offset-2 col-md-9 col-md-offset-3 col-sm-9 col-sm-offset-3 main">

		<div class="container-fluid main">

			<ol class="breadcrumb">
				<li><i class="fa fa-home"></i> <a href="dashboard">Dashboard</a></li>
				<li><i class="fa fa-copy"></i> <a href="#">Boletos</a></li>
			</ol>

			<div class="conteudo">
				<form id="form-gerar-boleto" class="form-horizontal" action="download">
					<div class="row">
						<div class="col-lg-12">

							<input id="nom_tela" name="nom_tela" type="hidden"
								value="Financeiro > Boletos" />
								
								<input id="idboleto" name="idboleto" type="hidden" value="" />

							<div class="panel panel-default">
								<a data-target="#item-info">
									<div class="panel-heading"> Boletos</div>
								</a>
								<div id="item-info" class="panel-body panel-branco collapse in">

									<div class="col-lg-12">
										<div class="row">
										
											<div class="col-xs-12 col-lg-4">
												<div class="form-group">
													<label>Mês de Referência</label>
													<div class="input-group date" 
														data-provide="datepicker"
														data-date-format="mm/yyyy" 
														data-date-start-view="months"
														data-date-min-view-mode="months" 
														data-date-autoclose="true">
														<input id="mes_referencia" name="mes_referencia" 
															type="text" class="form-control format_data_mes_ref">
														<div class="input-group-addon">
															<span class="glyphicon glyphicon-th"></span>
														</div>
													</div>
												</div>
											</div>
											
											<div class="col-xs-12 col-lg-2">
												<div class="form-group">
													<label class=""></label>
													<button type="button" id="btnBuscar"
														class="btn btn-primary btn-sm btn-block">Buscar</button>
												</div>
											</div>

											<table id="tableBoletos" 
												data-toggle="table"
												data-classes="table table-condensed"
												data-click-to-select="true" 
												data-search="true"
												data-show-refresh="true"
												data-show-columns="true"
												data-content-type="application/json" 
												data-data-type="json"
												data-sort-order="desc" 
												data-sort-name="id"
												data-pagination="true"
												data-pagination-first-text="Primeiro"
												data-pagination-pre-text="<i class='glyphicon glyphicon glyphicon-chevron-left'></i>"
												data-pagination-next-text="<i class='glyphicon glyphicon glyphicon-chevron-right'></i>"
												data-pagination-last-text="Último" data-locale="pt-BR">
												<thead>
													<tr>
														<th id="ckAll" data-field="state" data-checkbox="true"
															data-align="center" data-width="40"></th>
														<th data-field="id" data-halign="center"
															data-align="center" data-width="50" data-sortable="true">Boleto</th>
														<th data-field="locacao.imovel.locador.nome_completo"
															data-halign="center" data-align="center" data-sortable="true">Locador</th>
														<th data-field="locacao.locatario.nome_completo"
															data-halign="center" data-align="center" data-sortable="true">Locatário</th>
														<th data-field="valorString" data-halign="center"
															data-align="center" data-width="100" data-sortable="true">Valor (R$)</th>
														<th data-field="dt_documentoFormat" data-halign="center"
															data-align="center" data-width="100" data-sortable="true">Data do
															Documento</th>
														<th data-field="dt_vencimentoFormat" data-halign="center"
															data-align="center" data-width="100" data-sortable="true">Vencimento</th>
														<th data-field="dt_pagamentoFormat" data-halign="center"
															data-align="center" data-width="100" data-sortable="true">Pagamento</th>
														<th data-field="flg_enviado" data-halign="center"
															data-align="center" data-formatter="actionEnviadoFormatter"
															data-width="70" data-sortable="true">Enviado</th>
														<th data-halign="center" data-align="center"
															data-formatter="actionEnviarFormatter"
															data-events="actionEvents" data-width="70">Enviar</th>
														<th data-halign="center" data-align="center"
															data-formatter="actionDownloadFormatter"
															data-events="actionEvents" data-width="80">Download</th>
													</tr>
												</thead>
											</table>
										</div>
									</div>

									<div class="col-lg-12">
										<div class="row">

											<div class="col-xs-12 col-lg-3">
												<div class="form-group">
													<label class=""></label>
													<button type="button" id="btnBaixar"
														class="btn btn-primary btn-sm btn-block">Baixar Todos</button>
												</div>
											</div>

										</div>
									</div>

								</div>
							</div>

						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

</body>
</html>
