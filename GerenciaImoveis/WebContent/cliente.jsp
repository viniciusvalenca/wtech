<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ABSI - Cliente</title>

<jsp:include page="includes/links.jsp" />
<script src="js/format_datapicker.js" type="text/javascript"></script>

</head>
<body>

	<jsp:include page="includes/menu.jsp" />

	<div
		class="col-lg-10 col-lg-offset-2 col-md-9 col-md-offset-3 col-sm-9 col-sm-offset-3 main">

		<div class="container-fluid main">

			<ol class="breadcrumb">
				<li><i class="fa fa-home"></i> <a href="dashboard">Dashboard</a></li>
				<li><i class="fa fa-user-plus"></i> <a href="#">Novo
						Cliente</a></li>
			</ol>

			<div class="conteudo">
				<form id="form-cliente" class="form-horizontal" action="cadastrar" method="POST">
					<div class="row">
						<div class="col-lg-12">

							<input id="nom_tela" name="nom_tela" type="hidden" value="Cadastrar > Cliente"/>

							<jsp:include page="includes/cliente/infos.jsp" />

							<jsp:include page="includes/cliente/numeros.jsp" />

							<jsp:include page="includes/endereco.jsp" />

							<div id="blocoDadosBancarios">
								<jsp:include page="includes/cliente/dadosbancarios.jsp" />
							</div>

							<div class="panel">
								<div class="row">
									<div class="col-sm-6 col-lg-6">
										<button type="reset" class="btn btn-default btn-block">Limpar
											Formulário</button>
									</div>
									<div class="col-sm-6 col-lg-6">
										<button type="submit" id="btnCadastrar"
											class="btn btn-primary btn-block">Cadastrar Cliente</button>
									</div>
								</div>
							</div>

						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

</body>
</html>
