<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ABSI - Gerar Remessa</title>

<jsp:include page="includes/links.jsp" />

<script>
$(document).ready(function(){
	$("form").find('input#mes_referencia').attr('autocomplete', 'off');
	
	$('#mes_referencia').datepicker({
	    format: "mm/yyyy",
	    startView: 1,
	    minViewMode: 1,
		language : "pt-BR"
	});
	
	$('.date').datepicker({
		language : "pt-BR"
	 });
	
	$table = $('#tableLocacao');
	$btnBuscar = $("#btnBuscar");
	$btnGerar = $("#btnGerar");
	
	$table.on('check.bs.table', function (row, $element) {			
		if($element.boleto != null){
			if($element.boleto.dt_documentoFormat != null){
				mostrarAlerta("Aviso","Existem remessa que já foram geradas");
			}
		}
	});
	
	$table.on('check-all.bs.table', function (row, $element) {		
		
		var obj = $(this).bootstrapTable('getSelections');
		var flag = false;
		
		$.each(obj, function(index, value) {
			if(value.boleto != null){
				if(value.boleto.dt_documentoFormat != null){
					flag = true;
				}
			}
		});

		if(flag == true){
			mostrarAlerta("Aviso","Existem remessa que já foram geradas");
		}
		
	});
	
	
	$btnBuscar.click(function(){
		$table.bootstrapTable('removeAll');
		
		if($("#mes_referencia").val() != ""){
			searchLocacoesPorMes();
		}
	});
	
	$btnGerar.click(function(){
		var obj = $table.bootstrapTable('getSelections');
		
		if(obj.length > 0){
			gerarRemessa();
		}else{
			mostrarAlerta("Aviso","Selecione pelo menos um imóvel");
		}
	});
});


function stateFormatter(value, row, index) {

	var resposta = false;
	
	if(row.imovel.locador.cpf != undefined){
		if(!valida_cpf(row.imovel.locador.cpf)){
			resposta = true;
		}
	}
	
	if(row.imovel.locador.cnpj != undefined){
		if(!valida_cnpj(row.imovel.locador.cnpj)){
			resposta = true;
		}
	}
	
	if(row.locatario.cpf != undefined){
		if(!valida_cpf(row.locatario.cpf)){
			resposta = true;
		}
	}
	
	if(row.locatario.cnpj != undefined){
		if(!valida_cnpj(row.locatario.cnpj)){
			resposta = true;
		}
	}

	 return { 
		 disabled: resposta
	 }
}

function gerarRemessa(){
	var obj = $table.bootstrapTable('getSelections');
	$("#lista").val(JSON.stringify(obj));
	$("#form-gerar-remessa").submit();
	$table.bootstrapTable('removeAll');
}

function searchLocacoesPorMes() {
	$.ajax({
		type : 'GET',
		url : 'listarLocacaoPorMes',
		data : { mes_ref : $("#mes_referencia").val() }, 
		contentType : 'application/json; charset=utf-8',
		dataType : 'json',
		success : function(retorno) {
			
			if(retorno.length > 0){
				
				$table = $('#tableLocacao');
				$table.bootstrapTable('removeAll');
				
				$.each(retorno, function( index, data ) {
				  
					if(data.boleto != null){
						
						//alert(JSON.stringify("id: " + data.boleto.id + " dt doc: " + data.boleto.dt_documento
								//+ " dt pag: " +data.boleto.dt_pagamento));
						
						if(data.boleto.dt_documento != null && data.boleto.dt_pagamento != null){
							
						}else{
							$table.bootstrapTable('append', data);
						}
						
					}else{
						$table.bootstrapTable('append', data);
					}
				});
				
				var tam = $table.bootstrapTable('getData').length;
				
				if(tam == 0){
					mostrarAlerta("Aviso","Não existe nenhuma remessa para ser gerada");
				}
				
			}else{
				mostrarAlerta("Aviso","Não existe nenhuma remessa para ser gerada")
			}
		}
	});
}
</script>
</head>
<body>

	<jsp:include page="includes/menu.jsp" />

	<div
		class="col-lg-10 col-lg-offset-2 col-md-9 col-md-offset-3 col-sm-9 col-sm-offset-3 main">

		<div class="container-fluid main">

			<ol class="breadcrumb">
				<li><i class="fa fa-home"></i> <a href="dashboard">Dashboard</a></li>
				<li><i class="fa fa-clipboard"></i> <a href="#">Gerar
						Remessa</a></li>
			</ol>

			<div class="conteudo">
				<form id="form-gerar-remessa" class="form-horizontal"
					action="gerarRemessa" method="POST">
					<div class="row">
						<div class="col-lg-12">

							<input id="nom_tela" name="nom_tela" type="hidden"
								value="Financeiro > Gerar Remessa" />
								
							<textarea id="lista" name="lista" class="hidden"></textarea>

							<div class="panel panel-default">
								<a data-target="#item-gerar-conta">
									<div class="panel-heading"> Gerar Cobrança  </div>
								</a>
								<div id="item-gerar-conta"
									class="panel-body panel-branco collapse in">


									<div class="col-lg-12">
										<div class="col-xs-12 col-lg-4">
											<div class="form-group">
												<label>Mês de Referência</label>
												<div class="input-group date" 
													data-provide="datepicker" 
													data-date-format="mm/yyyy" 
													data-date-start-view="months"
													data-date-min-view-mode="months" 
													data-date-autoclose="true">
													<input id="mes_referencia" name="mes_referencia"
														type="text" class="form-control format_data_mes_ref">
													<div class="input-group-addon">
														<span class="glyphicon glyphicon-th"></span>
													</div>
												</div>
											</div>
										</div>

										<div class="col-xs-12 col-lg-2">
											<div class="form-group">
												<label class=""></label>
												<button type="button" id="btnBuscar"
													class="btn btn-primary btn-sm btn-block">Buscar</button>
											</div>
										</div>
									</div>

								</div>
							</div>

							<div class="panel panel-default">
								<a data-target="#item-gerar-conta-locacao">
									<div class="panel-heading">
										Imóveis </div>
								</a>
								
								<div id="item-gerar-conta-locacao"
									class="panel-body panel-branco collapse in">
									
									<h5 class="f-vermelho"><input type="checkbox" disabled /> <b>Os checkboxs desativados ocorre quando CPF ou CNPJ do locatário ou 
									locador da locação está com problema !</b></h5>									
								
									<div id="item-table"
										class="panel-body panel-branco collapse in">

										<table id="tableLocacao" data-toggle="table"
											data-classes="table table-condensed"
											data-click-to-select="true"
											data-search="true"
											data-content-type="application/json"
											data-data-type="json" 
											data-show-columns="true"
											data-sort-order="desc"
											data-sort-name="stargazers_count" 
											data-sort-order="desc"
											data-pagination="true" data-page-size="5"
											data-pagination-first-text="Primeiro"
											data-pagination-pre-text="<i class='glyphicon glyphicon glyphicon-chevron-left'></i>"
											data-pagination-next-text="<i class='glyphicon glyphicon glyphicon-chevron-right'></i>"
											data-pagination-last-text="Último" data-locale="pt-BR">
											<thead>
												<tr>
													<th data-field="state" data-checkbox="true"
														data-align="center" data-width="40" data-formatter="stateFormatter"></th>
													<th data-field="id" data-halign="center"
														data-align="center" data-sortable="true">ID</th>
													<th data-field="imovel.endereco.enderecoCompleto"
														data-halign="center" data-align="center" data-sortable="true">Imóvel</th>
													<th data-field="imovel.locador.nome_completo"
														data-halign="center" data-align="center" data-sortable="true">Locador</th>
													<th data-field="locatario.nome_completo"
														data-halign="center" data-align="center" data-sortable="true">Locatário</th>
													<th data-field="modalidade" data-halign="center"
														data-align="center" data-sortable="true">Modalidade</th>
												</tr>
											</thead>
										</table>
									</div>


								</div>
							</div>

							<div class="panel">
								<div class="row">
									<div class="col-lg-12">
										<button type="button" id="btnGerar"
											class="btn btn-primary btn-block">Gerar</button>
									</div>
								</div>
							</div>

						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

</body>
</html>
