<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<div class="panel panel-default">
	<a data-target="#item-telefones">
		<div class="panel-heading"> Telefones </div>
	</a>
	<div id="item-telefones" class="panel-body panel-branco collapse in">
		<div class="col-lg-3">
			<div class="form-group">
				<button id="add1" type="button" class="btn btn-success btn-xs">Adicionar
					Telefone</button>
			</div>
			<div class="row">
				<div id="itens1">
					<div class="col-lg-12">
						<div class="form-group">
							<label>1 - Telefone</label>
							<div class="input-group">
								<input id="num_tel_1" name="num_tel" type="text"
									class="telefone form-control"><span
									class="input-group-btn">
									<button class="btn btn-danger delete" type="button">-</button>
								</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="col-lg-3">
			<div class="form-group">
				<button id="add2" type="button" class="btn btn-success btn-xs">Adicionar
					Celular</button>
			</div>
			<div class="row">
				<div id="itens2">
					<div class="col-lg-12">
						<div class="form-group">
							<label>1 - Celular</label>
							<div class="input-group">
								<input id="num_cel_1" name="num_cel" type="text"
									class="celular form-control"><span
									class="input-group-btn">
									<button class="btn btn-danger delete" type="button">-</button>
								</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="col-lg-3">
			<div class="form-group">
			<label>Telefones Extras</label>
				<textarea id="telefones_extras" name="telefones_extras" class="form-control" rows="3">
Telefone (Residencial): (00) 0000-0000
Telefone (Comercial): (00) 0000-0000
</textarea>
			</div>
		</div>
	</div>
</div>