<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="panel panel-default">
	<a data-target="#item-dadosbancarios">
		<div class="panel-heading"> Dados Bancários </div>
	</a>
	<div id="item-dadosbancarios"
		class="panel-body panel-branco collapse in">

		<div class="col-lg-12">
			<div class="row">
				<div class="col-lg-5">
					<div class="form-group">
						<label>Banco</label> <select class="bank form-control"
							name="nome_banco" id="nome_banco">
							<option value="0" selected>Selecione um banco</option>

							<option value="001 - Banco do Brasil S.A.">001 - Banco
								do Brasil S.A.</option>
							<option value="237 - Banco Bradesco S.A.">237 - Banco
								Bradesco S.A.</option>
							<option value="104 - Caixa Econômica Federal">104 -
								Caixa Econômica Federal</option>
							<option value="033 - Banco Santander (Brasil) S.A.">033
								- Banco Santander (Brasil) S.A.</option>
							<option value="341 - Itaú Unibanco S.A.">341 - Itaú
								Unibanco S.A.</option>
							<option value="745 - Banco Citibank S.A.">745 - Banco
								Citibank S.A.</option>

						</select>
					</div>
				</div>

				<div class="col-lg-3">
					<div class="form-group">
						<label>Agência</label> <input id="agencia" name="agencia"
							type="text" class="agencia form-control">
					</div>
				</div>

				<div class="col-lg-3">
					<div class="form-group">
						<label>Conta</label> <input id="conta" name="conta" type="text"
							class="form-control">
					</div>
				</div>
			</div>
		</div>
		
		<div class="col-lg-12">
			<div class="row">
				<div class="col-lg-3">
					<div class="form-group">
						<label>Tp. Conta</label> <select class="form-control"
							name="tp_conta" id="tp_conta">
							<option value="CC">Conta Corrente</option>
							<option value="CP">Conta Poupança</option>
						</select>
					</div>
				</div>

				<div class="col-lg-4">
					<div class="form-group">
						<label>Titular da Conta</label> <input id="titular" name="titular"
							type="text" class="form-control">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>