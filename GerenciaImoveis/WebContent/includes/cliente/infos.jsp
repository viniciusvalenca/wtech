<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="panel panel-default">
	<a data-target="#item-info">
		<div class="panel-heading"> Informações Pessoais </div>
	</a>
	<div id="item-info" class="panel-body panel-branco collapse in">

		<div class="col-lg-12">
			<div class="row">
				<div class="col-lg-6">
					<div class="form-group">
						<label>Tipo de Cliente</label>
						<div class="form-inline">
							<label class="checkbox-inline"> <input type="checkbox"
								id="ckLocador" name="ckLocador" value="true" checked="checked"> 
								Locador
							</label> <label class="checkbox-inline"> <input type="checkbox"
								id="ckLocatario" name="ckLocatario" value="false">
								Locatário
							</label> <label class="checkbox-inline"> <input type="checkbox"
								id="ckFiador" name="ckFiador" value="false"> 
								Fiador
							</label> 
							<!-- 
							<label class="checkbox-inline"> <input type="checkbox"
								id="ckEmpresa" name="ckEmpresa" value="false"> 
								Empresa
							</label> -->
						</div>
					</div>
				</div>

				<div class="col-lg-offset-3 col-lg-3">
					<div class="form-group">
						<label>ID</label> <input id="id" name="id" type="text"
							class="form-control" value="${id}" readonly>
					</div>
				</div>
			</div>
		</div>

		<div class="col-lg-12">
			<div class="row">
				<div class="col-lg-6">
					<div class="form-group">
						<label>Nome Completo</label> <input id="nome_completo" name="nome_completo"
							type="text" class="form-control">
					</div>
				</div>

				<div class="col-lg-4">
					<div class="form-group">
						<label>Dt. Nascimento</label>
						<div class="input-group date" data-provide="datepicker"
							data-date-format="dd/mm/yyyy" data-date-autoclose="true">
							<input id="dt_nascimento" name="dt_nascimento" type="text"
								class="format_data form-control">
							<div class="input-group-addon">
								<span class="glyphicon glyphicon-th"></span>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>

		<div class="col-lg-12">
			<div class="row">
				<div class="col-lg-4">
					<div class="form-group">
						<label>Email</label> <input id="email" name="email" type="email"
							class="form-control" placeholder="cliente@gmail.com">
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group">
						<label>Email Secundário (Opcional)</label> <input
							id="email_opcional" name="email_opcional" type="email"
							class="form-control" placeholder="cliente@gmail.com">
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-12">
			<div class="row">
				<div class="col-lg-3">
					<div class="form-group">
						<label>Identidade</label> <input name="identidade" id="identidade"
							type="text" class="identidade form-control">
					</div>
				</div>

				<div class="col-lg-3">
					<div class="form-group">
						<label>Emissor</label> <input id="emissor" name="emissor"
							type="text" class="form-control">
					</div>
				</div>
 
				<div class="col-lg-3">
					<div class="form-group">
						<label>Dt. Emissão</label>
						<div class="input-group date" data-provide="datepicker"
							data-date-format="dd/mm/yyyy" data-date-autoclose="true">
							<input id="dt_emissao" name="dt_emissao" type="text"
								class="format_data form-control">
							<div class="input-group-addon">
								<span class="glyphicon glyphicon-th"></span>
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-3">
					<div class="form-group">
						<label>CPF</label> <input id="cpf" name="cpf" type="text"
							class="cpf form-control cpfcnpj">
					</div>
				</div>
				
				<div class="col-lg-3">
					<div class="form-group">
						<label>CNPJ</label> <input id="cnpj" name="cnpj" type="text"
							class="cnpj form-control cpfcnpj">
					</div>
				</div>

			</div>
		</div>

		<div class="col-lg-12">
			<div class="row">
				<div class="col-lg-2">
					<div class="form-group">
						<label>Estado Civil</label> <select class="form-control"
							name="estado_civil" id="estado_civil">
							<option value="Solteiro(a)">Solteiro(a)</option>
							<option value="Casado(a)">Casado(a)</option>
							<option value="Viúvo(a)">Viúvo(a)</option>
							<option value="Divórciado(a)">Divorciado(a)</option>
						</select>
					</div>
				</div>
				<div class="col-lg-3">
					<div class="form-group">
						<label>Nacionalidade</label> <input name="nacionalidade"
							id="nacionalidade" type="text" class="form-control">
					</div>
				</div>

				<div class="col-lg-3">
					<div class="form-group">
						<label>Naturalidade</label> <input id="naturalidade"
							name="naturalidade" type="text" class="form-control">
					</div>
				</div>

				<div class="col-lg-4">
					<div class="form-group">
						<label>Profissão</label> <input id="profissao" name="profissao"
							type="text" class="form-control">
					</div>
				</div>

			</div>
		</div>
	</div>
</div>