<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="panel panel-default">
	<a data-target="#item-assoc-locatario">
		<div class="panel-heading"> Associar ao Locador </div>
	</a>
	<div id="item-assoc-locatario"
		class="panel-body panel-branco collapse in">

		<div class="col-lg-12">
			<div class="row">
				<div class="col-lg-6">
					<div class="form-group">
						<label>Locador</label> <select class="form-control selectpicker" data-live-search="true" 
							id="locador" name="locador">
							<option value="0">Selecione um locador</option>
							<c:forEach var="lista" items="${lista}">
								<option value="${lista.id}">${lista.id} -
									${lista.nome_completo}</option>
							</c:forEach>
						</select>
					</div>
				</div>

				<div class="col-lg-offset-3 col-lg-3">
					<div class="form-group">
						<label>ID</label> <input id="id" name="id" type="text"
							class="form-control" value="${id}" readonly>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>