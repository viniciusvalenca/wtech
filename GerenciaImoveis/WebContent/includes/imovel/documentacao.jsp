<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<div class="col-lg-6">
	<div class="row">
		<div class="panel panel-default">
			<a data-target="#item-documentacao">
				<div class="panel-heading"> Documentação </div>
			</a>
			<div id="item-documentacao"
				class="panel-body panel-branco collapse in">

				<div class="col-lg-12">
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
								<label>Insc. IPTU</label> <input id="insc_iptu" name="insc_iptu"
									type="text" class="insc_iptu form-control">
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label>Cod. Logradouro</label> <input id="cod_logradouro"
									name="cod_logradouro" type="text"
									class="cod_logradouro form-control">
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label>Mat. CEDAE</label> <input id="mat_cedae" name="mat_cedae"
									type="text" class="mat_cedae form-control">
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label>CBMERJ</label> <input id="cbmerj" name="cbmerj"
									type="text" class="cbmerj form-control">
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label>Mat. LIGHT</label> <input id="mat_light" name="mat_light"
									type="text" class="mat_light form-control">
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label>Mat. CEG - Nº Cliente</label> <input id="mat_ceg"
									name="mat_ceg" type="text" class="mat_ceg form-control">
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>