<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="col-lg-6">
	<div class="row">
		<div class="panel panel-default">
			<a data-target="#item-info-extras">
				<div class="panel-heading"> Informações Extras </div>
			</a>
			<div id="item-info-extras"
				class="panel-body panel-branco collapse in">

				<div class="col-lg-12">
					<div class="row">
						<div class="col-lg-4">
							<div class="form-group iradiobuttons">
								<label>Disponível</label>
								<div class="form-inline">
									<label class="radio-inline"> <input type="radio"
										name="flg_disponivel" id="rbFlgSim" value="true" checked="checked"> Imóvel vazio
									</label> <label class="radio-inline" style="margin-left:0;"> <input type="radio"
										name="flg_disponivel" id="rbFlgNao" value="false"> Imóvel Alugado
									</label>
								</div>
							</div>
						</div>

						<div class="col-lg-4">
							<div class="form-group">
								<label id="lbValor">Valor do Aluguel</label>
								<div class="input-group">
									<label class="sr-only"></label>
									<div class="input-group">
										<div class="input-group-addon">
											R<i class="fa fa-usd" aria-hidden="true"></i>
										</div>
										<input id="valor" name="valor" type="text" data-thousands="."
											data-decimal="," data-allow-zero="true" class="form-control text-left" required>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-4">
							<div class="form-group">
								<label>Comissão</label>
								<div class="input-group">
									<label class="sr-only"></label>
									<div class="input-group">
										<input id="comissao" name="comissao" type="text"
											class="form-control text-right" value="10">
										<div class="input-group-addon">&#37;</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-4">
							<div class="form-group">
								<label>Nº Quartos</label> <input id="qtd_quartos"
									name="qtd_quartos" type="text" class="form-control">
							</div>
						</div>
						<div class="col-lg-4">
							<div class="form-group">
								<label>Área Edificada</label>
								<div class="col-lg-12 input-group">
									<label class="sr-only"></label>
									<div class="input-group">
										<input id="area" name="area" type="text"
											class="form-control text-right">
										<div class="input-group-addon">m²</div>
									</div>
								</div>
							</div>
						</div>
						
					</div>
				</div>

				<div class="col-lg-12">
					<div class="row">
						<div class="col-lg-4">
							<div class="form-group">
								<label>Suítes</label> <input id="qtd_suites" name="qtd_suites"
									type="text" class="form-control">
							</div>
						</div>
						<div class="col-lg-4">
							<div class="form-group">
								<label>Tipo</label> <select id="tipo" name="tipo"
									class="form-control">
									<option value="Apartamento">Apartamento</option>
									<option value="Casa">Casa</option>
									<option value="Loja">Loja</option>
									<option value="Sobrado">Sobrado</option>
									<option value="Lote">Lote</option>
									<option value="Kitnet">Kitnet</option>
									<option value="Sala">Sala</option>
									<option value="Comercial">Comercial</option>
									<option value="Galpão">Galpão</option>
									<option value="Terreno">Terreno</option>
									<option value="Outro">Outro</option>
								</select>
							</div>
						</div>
						<div class="col-lg-4">
							<div class="form-group">
								<label>Nº Modalidade</label> <select id="modalidade"
									name="modalidade" class="form-control">
									<option value="0">Aluguel</option>
									<option value="1">Venda</option>
								</select>
							</div>
						</div>
						
					</div>
				</div>

			</div>
		</div>

	</div>
</div>