<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<head>

<script type="text/javascript" src="js/jquery-2.1.1.min.js"></script>

<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="css/sb-admin.css">
<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">

<link rel="stylesheet" href="bootstrap-datepicker/css/bootstrap-datepicker.min.css">
<link rel="stylesheet" href="bootstrap-datepicker/css/bootstrap-datepicker3.min.css">
<link rel="stylesheet" href="bootstrap-select/css/bootstrap-select.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.1/bootstrap3-editable/css/bootstrap-editable.css">
<link rel="stylesheet" href="bootstrap-table/css/bootstrap-table.min.css">
<link rel="stylesheet" href="bootstrap-table/extensions/group-by-v2/bootstrap-table-group-by.css">



<link rel="stylesheet" href="bootstrap-fileinput/css/fileinput.min.css" media="all"/>

<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="jquery-mask-plugin/jquery.mask.min.js"></script>
<script type="text/javascript" src="jquery-validation/jquery.validate.min.js"></script>
<script type="text/javascript" src="jquery-validation/additional-methods.min.js"></script>
<script type="text/javascript" src="jquery-blockui-master/jquery.blockUI.js"></script>
<script type="text/javascript" src="jquery-maskmoney/jquery.maskMoney.min.js"></script>
<script type="text/javascript" src="bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="bootstrap-datepicker/locales/bootstrap-datepicker.pt-BR.min.js"></script>

<script type="text/javascript" src="js/moment.js" type="text/javascript"></script>

<script type="text/javascript" src="bootstrap-select/js/bootstrap-select.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.1/bootstrap3-editable/js/bootstrap-editable.min.js"></script>

<script type="text/javascript" src="bootstrap-table/js/bootstrap-table.js"></script>
<script type="text/javascript" src="bootstrap-table/locale/bootstrap-table-pt-BR.min.js"></script>
<script type="text/javascript" src="bootstrap-table/extensions/group-by-v2/bootstrap-table-group-by.js"></script>
<script type="text/javascript" src="bootstrap-table/extensions/editable/bootstrap-table-editable.js"></script>

<script type="text/javascript" src="bootstrap-fileinput/js/plugins/canvas-to-blob.min.js"></script>
<script type="text/javascript" src="bootstrap-fileinput/js/plugins/sortable.min.js"></script>
<script type="text/javascript" src="bootstrap-fileinput/js/plugins/purify.min.js"></script>
<script type="text/javascript" src="bootstrap-fileinput/js/fileinput.js"></script>

<script src="js/funcao.js" type="text/javascript"></script>
<script src="js/buscar-cep.js" type="text/javascript"></script>
<script src="js/mascara.js" type="text/javascript"></script>
<script src="js/validador.js" type="text/javascript"></script>
<script src="js/alert.js" type="text/javascript"></script>
<script src="js/select.js" type="text/javascript"></script>

</head>