<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<script>
$(document).ready(function() {
	$("li a[aria-controls=tabA]").click();
	
	//$(".descricao-item").attr('maxlength','30');
	//$(".descricao-item").attr('placeholder','máximo 30 letras');
	$('#tableImoveis').on('check.bs.table',function(row, $element) {
		$('input[name="tabAitemValor"]').val(parseFloat($element.valorString)).trigger('mask.maskMoney');
	});

	$("#blocoTAB").focusout(function() {
		tab = $("li a[aria-expanded=true]").attr('aria-controls');
		
		_descricaoItem = $('input[name="' + tab + 'descricaoItem"]').val();
		_numParcela = $('input[name="' + tab + 'numParcela"]').val();
		_itemValor = $( 'input[name="' + tab + 'itemValor"]').val();
		
		if(tab == "tabB")
			$('#' + tab + 'ckFica').val("4");
		
		if(tab== 'tabJ' || tab=='tabN' || tab=='tabO'|| tab == "tabR"|| tab == "tabS"|| tab == "tabL") {
			if (parseInt(_itemValor) > 0 && _numParcela != "" && _descricaoItem != "") {
				$(".nav-pills li a[aria-controls="+ tab + "]").css("background-color","#5cb85c");
			}
		} else {
			if (parseInt(_itemValor) > 0 && _numParcela != "") {
				$(".nav-pills li a[aria-controls="+ tab + "]").css("background-color","#5cb85c");
			}
		}
	});
	
    //resgatou do banco = azul
	//editou algo = verde
});
</script>

<div class="panel panel-default">
	<a data-target="#item-itens">
		<div class="panel-heading">Itens</div>
	</a>
	<div id="item-itens" class="panel-body panel-branco collapse in">

		<div id="blocoMesRef">
			<div class="col-xs-12">
				<div class="col-xs-3 col-lg-2">
					<div class="form-group">
						<label>Mês Referência</label>
						<jsp:useBean id="now" class="java.util.Date" />
						<fmt:formatDate var="year" value="${now}" pattern="MM/yyyy" />
						<input class="mes_ref form-control" name="mes_ref" id="mes_ref"
							type="text" value="${year}">
					</div>
				</div>
			</div>
		</div>

		<div id="tab-locacao-item">
			<div class="col-xs-6 col-lg-3">
				<ul class="nav nav-pills nav-stacked" role="tablist">

					<li role="presentation"><a href="#tabA" aria-controls="tabA"
						role="tab" data-toggle="tab">Aluguel</a></li>
					<li role="presentation"><a href="#tabB" aria-controls="tabB"
						role="tab" data-toggle="tab">Despesa Bancária</a></li>
					<li role="presentation"><a href="#tabC" aria-controls="tabC"
						role="tab" data-toggle="tab">Cedae</a></li>
					<li role="presentation"><a href="#tabD" aria-controls="tabD"
						role="tab" data-toggle="tab">Luz de serviço</a></li>
					<li role="presentation"><a href="#tabE" aria-controls="tabE"
						role="tab" data-toggle="tab">IPTU</a></li>
					<li role="presentation"><a href="#tabF" aria-controls="tabF"
						role="tab" data-toggle="tab">Condomínio</a></li>
					<li role="presentation"><a href="#tabG" aria-controls="tabG"
						role="tab" data-toggle="tab">Faxina</a></li>
					<li role="presentation"><a href="#tabH" aria-controls="tabH"
						role="tab" data-toggle="tab">Seguro Incêndio Anual</a></li>
					<li role="presentation"><a href="#tabI" aria-controls="tabI"
						role="tab" data-toggle="tab">Prevenção e Extinção de Incêndios</a></li>
					<li role="presentation"><a href="#tabJ" aria-controls="tabJ"
						role="tab" data-toggle="tab">Desconto</a></li>
					<li role="presentation"><a href="#tabK" aria-controls="tabK"
						role="tab" data-toggle="tab">Acordo Extrajudicial</a></li>
					<li role="presentation"><a href="#tabL" aria-controls="tabL"
						role="tab" data-toggle="tab">Cobrança 2</a></li>
					<li role="presentation"><a href="#tabM" aria-controls="tabM"
						role="tab" data-toggle="tab">Parcela de deposito da garantia
							da locação</a></li>
					<li role="presentation"><a href="#tabN" aria-controls="tabN"
						role="tab" data-toggle="tab">Devolução</a></li>
					<li role="presentation"><a href="#tabO" aria-controls="tabO"
						role="tab" data-toggle="tab">Cobrança</a></li>
					<li role="presentation"><a href="#tabP" aria-controls="tabP"
						role="tab" data-toggle="tab">Multa</a></li>
					<li role="presentation"><a href="#tabQ" aria-controls="tabQ"
						role="tab" data-toggle="tab">Juros</a></li>
						<li role="presentation"><a href="#tabR" aria-controls="tabR"
						role="tab" data-toggle="tab">Desconto 2</a></li>
						<li role="presentation"><a href="#tabS" aria-controls="tabS"
						role="tab" data-toggle="tab">Desconto 3</a></li>
						<li role="presentation"><a href="#tabT" aria-controls="tabT"
						role="tab" data-toggle="tab">Taxa de Administração Acordo Extrajudicial</a></li>
						<li role="presentation"><a href="#tabU" aria-controls="tabU"
						role="tab" data-toggle="tab">Rescisão Contratual</a></li>
						<li role="presentation"><a href="#tabV" aria-controls="tabV"
						role="tab" data-toggle="tab">Taxa de Administração Rescisão Contratual</a></li>
						
				</ul>
			</div>

			<div class="col-xs-6 col-lg-9" id="blocoTAB">
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active" id="tabA">
						<div class="col-lg-12">
							<div class="row">

								<div class="tabname page-header">Aluguel 
									<div class="pull-right"><button type="button" class="btn btn-danger btn-xs btnDeletar">Deletar Item</button></div>
								</div>
								
								<div class="col-xs-12">
									<div class="form-group">
										<label>Para Locatário:</label>
									</div>
								</div>
								
								<div class="col-xs-12">
									<div class="row">
										<div class="col-xs-12 col-lg-3">
											<div class="form-group">
												<label>Mês de Ref. para o item</label> <input name="tabAmesRefBoleto"
													type="text" class="merRefBoleto form-control text-right"
													placeholder="00/0000">
											</div>
										</div>
									</div>
								</div>
								
								<div class="col-xs-12 col-lg-2">
									<div class="form-group">
										<label>Nº de Parcelas</label> <input name="tabAnumParcela"
											type="text" class="parcela form-control text-right"
											placeholder="00/00">
									</div>
								</div>

								<div class="col-xs-12 col-lg-3">
									<div class="form-group">
										<label>Valor</label>
										<div class="input-group">
											<label class="sr-only"></label>
											<div class="input-group">
												<div class="input-group-addon">
													R<i class="fa fa-usd" aria-hidden="true"></i>
												</div>
												<input name="tabAitemValor" type="text" 
													 class="valor form-control text-left">
											</div>
											</span>
										</div>
									</div>
								</div>

								<div class="col-xs-12 col-lg-2">
									<div class="form-group">
										<label>R/D</label> <select class="form-control"
											name="tabArdStatus">
											<option value="D">Despesa</option>
											<option value="R">Receita</option>
										</select>
									</div>
								</div>
							</div>
						</div>

						<div class="col-lg-12">
							<div class="row">
								<div class="col-xs-12">
									<div class="form-group">
										<label>Para Locador (Relatório):</label>
										<div class="radio">
											<label class="radio-inline"> 
												<input type="radio" name="tabAck" id="tabAckSoma" value="1">Soma
											</label> 
											<label class="radio-inline"> 
												<input type="radio" name="tabAck" id="tabAckDiminui" value="2" >Diminui
											</label> 
											<label class="radio-inline"> 
												<input type="radio" name="tabAck" id="tabAckFica" value="3" >Fica no Escritório
											</label>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="tabB">
						<div class="col-lg-12">
							<div class="row">

								<div class="tabname page-header">Despesa Bancária
								<div class="pull-right"><button type="button" class="btn btn-danger btn-xs btnDeletar">Deletar Item</button></div>
								</div>
								
								<div class="col-xs-12">
									<div class="form-group">
										<label>Para Locatário:</label>
									</div>
								</div>
								
								<div class="col-xs-12">
									<div class="row">
										<div class="col-xs-12 col-lg-3">
											<div class="form-group">
												<label>Mês de Ref. para o item</label> <input name="tabBmesRefBoleto"
													type="text" class="merRefBoleto form-control text-right"
													placeholder="00/0000">
											</div>
										</div>
									</div>
								</div>

								<div class="col-xs-12 col-lg-2">
									<div class="form-group">
										<label>Nº de Parcelas</label> <input name="tabBnumParcela"
											type="text" class="parcela form-control text-right"
											placeholder="00/00">
									</div>
								</div>

								<div class="col-xs-12 col-lg-3">
									<div class="form-group">
										<label>Valor</label>
										<div class="input-group">
											<label class="sr-only"></label>
											<div class="input-group">
												<div class="input-group-addon">
													R<i class="fa fa-usd" aria-hidden="true"></i>
												</div>
												<input name="tabBitemValor" type="text" 
													 class="valor form-control text-left">
											</div>
											</span>
										</div>
									</div>
								</div>

								<div class="col-xs-12 col-lg-2">
									<div class="form-group">
										<label>R/D</label> <select class="form-control"
											name="tabBrdStatus">
											<option value="D">Despesa</option>
											<option value="R">Receita</option>
										</select>
									</div>
								</div>
							</div>
						</div>
						
						<div class="col-lg-12">
							<div class="row">
								<div class="col-xs-12">
									<div class="form-group">
										<label>Para Locador (Relatório):</label>
										<div class="radio">
											<label class="radio-inline"> 
												<input type="radio" name="tabBck" id="tabBckSoma" value="1">Soma
											</label> 
											<label class="radio-inline"> 
												<input type="radio" name="tabBck" id="tabBckDiminui" value="2" >Diminui
											</label> 
											<label class="radio-inline"> 
												<input type="radio" name="tabBck" id="tabBckFica" value="4" >Fica no Escritório
											</label>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="tabC">
						<div class="col-lg-12">
							<div class="row">

								<div class="tabname page-header">Cedae
								<div class="pull-right"><button type="button" class="btn btn-danger btn-xs btnDeletar">Deletar Item</button></div>
								</div>
								
								<div class="col-xs-12">
									<div class="form-group">
										<label>Para Locatário:</label>
									</div>
								</div>
								
								<div class="col-xs-12">
									<div class="row">
										<div class="col-xs-12 col-lg-3">
											<div class="form-group">
												<label>Mês de Ref. para o item</label> <input name="tabCmesRefBoleto"
													type="text" class="merRefBoleto form-control text-right"
													placeholder="00/0000">
											</div>
										</div>
									</div>
								</div>

								<div class="col-xs-12 col-lg-2">
									<div class="form-group">
										<label>Nº de Parcelas</label> <input name="tabCnumParcela"
											type="text" class="parcela form-control text-right"
											placeholder="00/00">
									</div>
								</div>

								<div class="col-xs-12 col-lg-3">
									<div class="form-group">
										<label>Valor</label>
										<div class="input-group">
											<label class="sr-only"></label>
											<div class="input-group">
												<div class="input-group-addon">
													R<i class="fa fa-usd" aria-hidden="true"></i>
												</div>
												<input name="tabCitemValor" type="text" 
													 class="valor form-control text-left">
											</div>
											</span>
										</div>
									</div>
								</div>

								<div class="col-xs-12 col-lg-2">
									<div class="form-group">
										<label>R/D</label> <select class="form-control"
											name="tabCrdStatus">
											<option value="D">Despesa</option>
											<option value="R">Receita</option>
										</select>
									</div>
								</div>
							</div>
						</div>
						
						<div class="col-lg-12">
							<div class="row">
								<div class="col-xs-12">
									<div class="form-group">
										<label>Para Locador (Relatório):</label>
										<div class="radio">
											<label class="radio-inline"> 
												<input type="radio" name="tabCck" id="tabCckSoma" value="1">Soma
											</label> 
											<label class="radio-inline"> 
												<input type="radio" name="tabCck" id="tabCckDiminui" value="2" >Diminui
											</label> 
											<label class="radio-inline"> 
												<input type="radio" name="tabCck" id="tabCckFica" value="3" >Fica no Escritório
											</label>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="tabD">
						<div class="col-lg-12">
							<div class="row">

								<div class="tabname page-header">Luz de Serviço
								<div class="pull-right"><button type="button" class="btn btn-danger btn-xs btnDeletar">Deletar Item</button></div></div>
								
								<div class="col-xs-12">
									<div class="form-group">
										<label>Para Locatário:</label>
									</div>
								</div>
								
								<div class="col-xs-12">
									<div class="row">
										<div class="col-xs-12 col-lg-3">
											<div class="form-group">
												<label>Mês de Ref. para o item</label> <input name="tabDmesRefBoleto"
													type="text" class="merRefBoleto form-control text-right"
													placeholder="00/0000">
											</div>
										</div>
									</div>
								</div>

								<div class="col-xs-12 col-lg-2">
									<div class="form-group">
										<label>Nº de Parcelas</label> <input name="tabDnumParcela"
											type="text" class="parcela form-control text-right"
											placeholder="00/00">
									</div>
								</div>

								<div class="col-xs-12 col-lg-3">
									<div class="form-group">
										<label>Valor</label>
										<div class="input-group">
											<label class="sr-only"></label>
											<div class="input-group">
												<div class="input-group-addon">
													R<i class="fa fa-usd" aria-hidden="true"></i>
												</div>
												<input name="tabDitemValor" type="text" 
													 class="valor form-control text-left">
											</div>
											</span>
										</div>
									</div>
								</div>

								<div class="col-xs-12 col-lg-2">
									<div class="form-group">
										<label>R/D</label> <select class="form-control"
											name="tabDrdStatus">
											<option value="D">Despesa</option>
											<option value="R">Receita</option>
										</select>
									</div>
								</div>
							</div>
						</div>
						
						<div class="col-lg-12">
							<div class="row">
								<div class="col-xs-12">
									<div class="form-group">
										<label>Para Locador (Relatório):</label>
										<div class="radio">
											<label class="radio-inline"> 
												<input type="radio" name="tabDck" id="tabDckSoma" value="1">Soma
											</label> 
											<label class="radio-inline"> 
												<input type="radio" name="tabDck" id="tabDckDiminui" value="2" >Diminui
											</label> 
											<label class="radio-inline"> 
												<input type="radio" name="tabDck" id="tabDckFica" value="3" >Fica no Escritório
											</label>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="tabE">
						<div class="col-lg-12">
							<div class="row">

								<div class="tabname page-header">IPTU<div class="pull-right"><button type="button" class="btn btn-danger btn-xs btnDeletar">Deletar Item</button></div></div>
								
								<div class="col-xs-12">
									<div class="form-group">
										<label>Para Locatário:</label>
									</div>
								</div>
								
								<div class="col-xs-12">
									<div class="row">
										<div class="col-xs-12 col-lg-3">
											<div class="form-group">
												<label>Mês de Ref. para o item</label> <input name="tabEmesRefBoleto"
													type="text" class="merRefBoleto form-control text-right"
													placeholder="00/0000">
											</div>
										</div>
									</div>
								</div>

								<div class="col-xs-12 col-lg-2">
									<div class="form-group">
										<label>Nº de Parcelas</label> <input name="tabEnumParcela"
											type="text" class="parcela form-control text-right"
											placeholder="00/00">
									</div>
								</div>

								<div class="col-xs-12 col-lg-3">
									<div class="form-group">
										<label>Valor</label>
										<div class="input-group">
											<label class="sr-only"></label>
											<div class="input-group">
												<div class="input-group-addon">
													R<i class="fa fa-usd" aria-hidden="true"></i>
												</div>
												<input name="tabEitemValor" type="text" 
													 class="valor form-control text-left">
											</div>
											</span>
										</div>
									</div>
								</div>

								<div class="col-xs-12 col-lg-2">
									<div class="form-group">
										<label>R/D</label> <select class="form-control"
											name="tabErdStatus">
											<option value="D">Despesa</option>
											<option value="R">Receita</option>
										</select>
									</div>
								</div>
							</div>
						</div>
						
						<div class="col-lg-12">
							<div class="row">
								<div class="col-xs-12">
									<div class="form-group">
										<label>Para Locador (Relatório):</label>
										<div class="radio">
											<label class="radio-inline"> 
												<input type="radio" name="tabEck" id="tabEckSoma" value="1">Soma
											</label> 
											<label class="radio-inline"> 
												<input type="radio" name="tabEck" id="tabEckDiminui" value="2" >Diminui
											</label> 
											<label class="radio-inline"> 
												<input type="radio" name="tabEck" id="tabEckFica" value="3" >Fica no Escritório
											</label>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="tabF">
						<div class="col-lg-12">
							<div class="row">

								<div class="tabname page-header">Condomínio
								<div class="pull-right"><button type="button" class="btn btn-danger btn-xs btnDeletar">Deletar Item</button></div></div>
								
								<div class="col-xs-12">
									<div class="form-group">
										<label>Para Locatário:</label>
									</div>
								</div>
								
								<div class="col-xs-12">
									<div class="row">
										<div class="col-xs-12 col-lg-3">
											<div class="form-group">
												<label>Mês de Ref. para o item</label> <input name="tabFmesRefBoleto"
													type="text" class="merRefBoleto form-control text-right"
													placeholder="00/0000">
											</div>
										</div>
									</div>
								</div>

								<div class="col-xs-12 col-lg-2">
									<div class="form-group">
										<label>Nº de Parcelas</label> <input name="tabFnumParcela"
											type="text" class="parcela form-control text-right"
											placeholder="00/00">
									</div>
								</div>

								<div class="col-xs-12 col-lg-3">
									<div class="form-group">
										<label>Valor</label>
										<div class="input-group">
											<label class="sr-only"></label>
											<div class="input-group">
												<div class="input-group-addon">
													R<i class="fa fa-usd" aria-hidden="true"></i>
												</div>
												<input name="tabFitemValor" type="text" 
													 class="valor form-control text-left">
											</div>
											</span>
										</div>
									</div>
								</div>

								<div class="col-xs-12 col-lg-2">
									<div class="form-group">
										<label>R/D</label> <select class="form-control"
											name="tabFrdStatus">
											<option value="D">Despesa</option>
											<option value="R">Receita</option>
										</select>
									</div>
								</div>
							</div>
						</div>
						
						<div class="col-lg-12">
							<div class="row">
								<div class="col-xs-12">
									<div class="form-group">
										<label>Para Locador (Relatório):</label>
										<div class="radio">
											<label class="radio-inline"> 
												<input type="radio" name="tabFck" id="tabFckSoma" value="1">Soma
											</label> 
											<label class="radio-inline"> 
												<input type="radio" name="tabFck" id="tabFckDiminui" value="2" >Diminui
											</label> 
											<label class="radio-inline"> 
												<input type="radio" name="tabFck" id="tabFckFica" value="3" >Fica no Escritório
											</label>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="tabG">
						<div class="col-lg-12">
							<div class="row">

								<div class="tabname page-header">Faxina
								<div class="pull-right"><button type="button" class="btn btn-danger btn-xs btnDeletar">Deletar Item</button></div></div>

								<div class="col-xs-12">
									<div class="form-group">
										<label>Para Locatário:</label>
									</div>
								</div>
								
								<div class="col-xs-12">
									<div class="row">
										<div class="col-xs-12 col-lg-3">
											<div class="form-group">
												<label>Mês de Ref. para o item</label> <input name="tabGmesRefBoleto"
													type="text" class="merRefBoleto form-control text-right"
													placeholder="00/0000">
											</div>
										</div>
									</div>
								</div>
								
								<div class="col-xs-12 col-lg-2">
									<div class="form-group">
										<label>Nº de Parcelas</label> <input name="tabGnumParcela"
											type="text" class="parcela form-control text-right"
											placeholder="00/00">
									</div>
								</div>

								<div class="col-xs-12 col-lg-3">
									<div class="form-group">
										<label>Valor</label>
										<div class="input-group">
											<label class="sr-only"></label>
											<div class="input-group">
												<div class="input-group-addon">
													R<i class="fa fa-usd" aria-hidden="true"></i>
												</div>
												<input name="tabGitemValor" type="text" 
													 class="valor form-control text-left">
											</div>
											</span>
										</div>
									</div>
								</div>

								<div class="col-xs-12 col-lg-2">
									<div class="form-group">
										<label>R/D</label> <select class="form-control"
											name="tabGrdStatus">
											<option value="D">Despesa</option>
											<option value="R">Receita</option>
										</select>
									</div>
								</div>
							</div>
						</div>
						
						<div class="col-lg-12">
							<div class="row">
								<div class="col-xs-12">
									<div class="form-group">
										<label>Para Locador (Relatório):</label>
										<div class="radio">
											<label class="radio-inline"> 
												<input type="radio" name="tabGck" id="tabGckSoma" value="1">Soma
											</label> 
											<label class="radio-inline"> 
												<input type="radio" name="tabGck" id="tabGckDiminui" value="2" >Diminui
											</label> 
											<label class="radio-inline"> 
												<input type="radio" name="tabGck" id="tabGckFica" value="3" >Fica no Escritório
											</label>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="tabH">
						<div class="col-lg-12">
							<div class="row">

								<div class="tabname page-header">Seguro Incêndio Anual
								<div class="pull-right"><button type="button" class="btn btn-danger btn-xs btnDeletar">Deletar Item</button></div></div>
								
								<div class="col-xs-12">
									<div class="form-group">
										<label>Para Locatário:</label>
									</div>
								</div>
								
								<div class="col-xs-12">
									<div class="row">
										<div class="col-xs-12 col-lg-3">
											<div class="form-group">
												<label>Mês de Ref. para o item</label> <input name="tabHmesRefBoleto"
													type="text" class="merRefBoleto form-control text-right"
													placeholder="00/0000">
											</div>
										</div>
									</div>
								</div>

								<div class="col-xs-12 col-lg-2">
									<div class="form-group">
										<label>Nº de Parcelas</label> <input name="tabHnumParcela"
											type="text" class="parcela form-control text-right"
											placeholder="00/00">
									</div>
								</div>

								<div class="col-xs-12 col-lg-3">
									<div class="form-group">
										<label>Valor</label>
										<div class="input-group">
											<label class="sr-only"></label>
											<div class="input-group">
												<div class="input-group-addon">
													R<i class="fa fa-usd" aria-hidden="true"></i>
												</div>
												<input name="tabHitemValor" type="text" 
													 class="valor form-control text-left">
											</div>
											</span>
										</div>
									</div>
								</div>

								<div class="col-xs-12 col-lg-2">
									<div class="form-group">
										<label>R/D</label> <select class="form-control"
											name="tabHrdStatus">
											<option value="D">Despesa</option>
											<option value="R">Receita</option>
										</select>
									</div>
								</div>
							</div>
						</div>
						
						<div class="col-lg-12">
							<div class="row">
								<div class="col-xs-12">
									<div class="form-group">
										<label>Para Locador (Relatório):</label>
										<div class="radio">
											<label class="radio-inline"> 
												<input type="radio" name="tabHck" id="tabHckSoma" value="1">Soma
											</label> 
											<label class="radio-inline"> 
												<input type="radio" name="tabHck" id="tabHckDiminui" value="2" >Diminui
											</label> 
											<label class="radio-inline"> 
												<input type="radio" name="tabHck" id="tabHckFica" value="3" >Fica no Escritório
											</label>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="tabI">
						<div class="col-lg-12">
							<div class="row">

								<div class="tabname page-header">Prevenção e Extinção de
									Incêndios
									<div class="pull-right"><button type="button" class="btn btn-danger btn-xs btnDeletar">Deletar Item</button></div></div>
									
								<div class="col-xs-12">
									<div class="form-group">
										<label>Para Locatário:</label>
									</div>
								</div>
								
								<div class="col-xs-12">
									<div class="row">
										<div class="col-xs-12 col-lg-3">
											<div class="form-group">
												<label>Mês de Ref. para o item</label> <input name="tabImesRefBoleto"
													type="text" class="merRefBoleto form-control text-right"
													placeholder="00/0000">
											</div>
										</div>
									</div>
								</div>

								<div class="col-xs-12 col-lg-2">
									<div class="form-group">
										<label>Nº de Parcelas</label> <input name="tabInumParcela"
											type="text" class="parcela form-control text-right"
											placeholder="00/00">
									</div>
								</div>

								<div class="col-xs-12 col-lg-3">
									<div class="form-group">
										<label>Valor</label>
										<div class="input-group">
											<label class="sr-only"></label>
											<div class="input-group">
												<div class="input-group-addon">
													R<i class="fa fa-usd" aria-hidden="true"></i>
												</div>
												<input name="tabIitemValor" type="text" 
													 class="valor form-control text-left">
											</div>
											</span>
										</div>
									</div>
								</div>

								<div class="col-xs-12 col-lg-2">
									<div class="form-group">
										<label>R/D</label> <select class="form-control"
											name="tabIrdStatus">
											<option value="D">Despesa</option>
											<option value="R">Receita</option>
										</select>
									</div>
								</div>
							</div>
						</div>
						
						<div class="col-lg-12">
							<div class="row">
								<div class="col-xs-12">
									<div class="form-group">
										<label>Para Locador (Relatório):</label>
										<div class="radio">
											<label class="radio-inline"> 
												<input type="radio" name="tabIck" id="tabIckSoma" value="1">Soma
											</label> 
											<label class="radio-inline"> 
												<input type="radio" name="tabIck" id="tabIckDiminui" value="2" >Diminui
											</label> 
											<label class="radio-inline"> 
												<input type="radio" name="tabIck" id="tabIckFica" value="3" >Fica no Escritório
											</label>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="tabJ">
						<div class="col-lg-12">
							<div class="row">

								<div class="tabname page-header">Desconto
								<div class="pull-right"><button type="button" class="btn btn-danger btn-xs btnDeletar">Deletar Item</button></div></div>
								
								<div class="col-xs-12">
									<div class="form-group">
										<label>Para Locatário:</label>
									</div>
								</div>
								
								<div class="col-xs-12">
									<div class="row">
										<div class="col-xs-12 col-lg-3">
											<div class="form-group">
												<label>Mês de Ref. para o item</label> <input name="tabJmesRefBoleto"
													type="text" class="merRefBoleto form-control text-right"
													placeholder="00/0000">
											</div>
										</div>
									</div>
								</div>

								<div class="col-xs-12 col-lg-6">
									<div class="form-group">
										<label>Descrição</label> <input name="tabJdescricaoItem"
											type="text" class="form-control text-right descricao-item">
									</div>
								</div>
							</div>
						</div>

						<div class="col-lg-12">
							<div class="row">
								<div class="col-xs-12 col-lg-2">
									<div class="form-group">
										<label>Nº de Parcelas</label> <input name="tabJnumParcela"
											type="text" class="parcela form-control text-right"
											placeholder="00/00">
									</div>
								</div>

								<div class="col-xs-12 col-lg-3">
									<div class="form-group">
										<label>Valor</label>
										<div class="input-group">
											<label class="sr-only"></label>
											<div class="input-group">
												<div class="input-group-addon">
													R<i class="fa fa-usd" aria-hidden="true"></i>
												</div>
												<input name="tabJitemValor" type="text" 
													 class="valor form-control text-left">
											</div>
											</span>
										</div>
									</div>
								</div>

								<div class="col-xs-12 col-lg-2">
									<div class="form-group">
										<label>R/D</label> <select class="form-control"
											name="tabJrdStatus">
											<option value="R">Receita</option>
											<option value="D">Despesa</option>
											<option value="N">Neutro</option>
										</select>
									</div>
								</div>
							</div>
						</div>
						
						<div class="col-lg-12">
							<div class="row">
								<div class="col-xs-12">
									<div class="form-group">
										<label>Para Locador (Relatório):</label>
										<div class="radio">
											<label class="radio-inline"> 
												<input type="radio" name="tabJck" id="tabJckSoma" value="1">Soma
											</label> 
											<label class="radio-inline"> 
												<input type="radio" name="tabJck" id="tabJckDiminui" value="2" >Diminui
											</label> 
											<label class="radio-inline"> 
												<input type="radio" name="tabJck" id="tabJckFica" value="3" >Fica no Escritório
											</label>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="tabK">
						<div class="col-lg-12">
							<div class="row">

								<div class="tabname page-header">Acordo Extrajudicial
								<div class="pull-right"><button type="button" class="btn btn-danger btn-xs btnDeletar">Deletar Item</button></div></div>
								
								<div class="col-xs-12">
									<div class="form-group">
										<label>Para Locatário:</label>
									</div>
								</div>
								
								<div class="col-xs-12">
									<div class="row">
										<div class="col-xs-12 col-lg-3">
											<div class="form-group">
												<label>Mês de Ref. para o item</label> <input name="tabKmesRefBoleto"
													type="text" class="merRefBoleto form-control text-right"
													placeholder="00/0000">
											</div>
										</div>
									</div>
								</div>

								<div class="col-xs-12 col-lg-2">
									<div class="form-group">
										<label>Nº de Parcelas</label> <input name="tabKnumParcela"
											type="text" class="parcela form-control text-right"
											placeholder="00/00">
									</div>
								</div>

								<div class="col-xs-12 col-lg-3">
									<div class="form-group">
										<label>Valor</label>
										<div class="input-group">
											<label class="sr-only"></label>
											<div class="input-group">
												<div class="input-group-addon">
													R<i class="fa fa-usd" aria-hidden="true"></i>
												</div>
												<input name="tabKitemValor" type="text" 
													 class="valor_negativo form-control text-left">
											</div>
											</span>
										</div>
									</div>
								</div>

								<div class="col-xs-12 col-lg-2">
									<div class="form-group">
										<label>R/D</label> <select class="form-control"
											name="tabKrdStatus">
											<option value="D">Despesa</option>
											<option value="R">Receita</option>
										</select>
									</div>
								</div>
							</div>
						</div>
						
						<div class="col-lg-12">
							<div class="row">
								<div class="col-xs-12">
									<div class="form-group">
										<label>Para Locador (Relatório):</label>
										<div class="radio">
											<label class="radio-inline"> 
												<input type="radio" name="tabKck" id="tabKckSoma" value="1">Soma
											</label> 
											<label class="radio-inline"> 
												<input type="radio" name="tabKck" id="tabKckDiminui" value="2" >Diminui
											</label> 
											<label class="radio-inline"> 
												<input type="radio" name="tabKck" id="tabKckFica" value="3" >Fica no Escritório
											</label>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="tabL">
						<div class="col-lg-12">
							<div class="row">

								<div class="tabname page-header">Cobrança 2
								<div class="pull-right"><button type="button" class="btn btn-danger btn-xs btnDeletar">Deletar Item</button></div></div>
								
								<div class="col-xs-12">
									<div class="form-group">
										<label>Para Locatário:</label>
									</div>
								</div>
								
								<div class="col-xs-12">
									<div class="row">
										<div class="col-xs-12 col-lg-3">
											<div class="form-group">
												<label>Mês de Ref. para o item</label> <input name="tabLmesRefBoleto"
													type="text" class="merRefBoleto form-control text-right"
													placeholder="00/0000">
											</div>
										</div>
									</div>
								</div>

								<div class="col-xs-12 col-lg-6">
									<div class="form-group">
										<label>Descrição</label> <input name="tabLdescricaoItem"
											type="text" class="form-control text-right descricao-item">
									</div>
								</div>
							</div>
						</div>

						<div class="col-lg-12">
							<div class="row">
								<div class="col-xs-12 col-lg-2">
									<div class="form-group">
										<label>Nº de Parcelas</label> <input name="tabLnumParcela"
											type="text" class="parcela form-control text-right"
											placeholder="00/00">
									</div>
								</div>

								<div class="col-xs-12 col-lg-3">
									<div class="form-group">
										<label>Valor</label>
										<div class="input-group">
											<label class="sr-only"></label>
											<div class="input-group">
												<div class="input-group-addon">
													R<i class="fa fa-usd" aria-hidden="true"></i>
												</div>
												<input name="tabLitemValor" type="text" 
													 class="valor form-control text-left">
											</div>
											</span>
										</div>
									</div>
								</div>

								<div class="col-xs-12 col-lg-2">
									<div class="form-group">
										<label>R/D</label> <select class="form-control"
											name="tabLrdStatus">
											<option value="R">Receita</option>
											<option value="D">Despesa</option>
											<option value="N">Neutro</option>
										</select>
									</div>
								</div>
							</div>
						</div>
						
						<div class="col-lg-12">
							<div class="row">
								<div class="col-xs-12">
									<div class="form-group">
										<label>Para Locador (Relatório):</label>
										<div class="radio">
											<label class="radio-inline"> 
												<input type="radio" name="tabLck" id="tabLckSoma" value="1">Soma
											</label> 
											<label class="radio-inline"> 
												<input type="radio" name="tabLck" id="tabLckDiminui" value="2" >Diminui
											</label> 
											<label class="radio-inline"> 
												<input type="radio" name="tabLck" id="tabLckFica" value="3" >Fica no Escritório
											</label>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="tabM">
						<div class="col-lg-12">
							<div class="row">

								<div class="tabname page-header">Parcela de Deposito da
									Garantia da Locação
									<div class="pull-right"><button type="button" class="btn btn-danger btn-xs btnDeletar">Deletar Item</button></div></div>
									
								<div class="col-xs-12">
									<div class="form-group">
										<label>Para Locatário:</label>
									</div>
								</div>
								
								<div class="col-xs-12">
									<div class="row">
										<div class="col-xs-12 col-lg-3">
											<div class="form-group">
												<label>Mês de Ref. para o item</label> <input name="tabMmesRefBoleto"
													type="text" class="merRefBoleto form-control text-right"
													placeholder="00/0000">
											</div>
										</div>
									</div>
								</div>

								<div class="col-xs-12 col-lg-2">
									<div class="form-group">
										<label>Nº de Parcelas</label> <input name="tabMnumParcela"
											type="text" class="parcela form-control text-right"
											placeholder="00/00">
									</div>
								</div>

								<div class="col-xs-12 col-lg-3">
									<div class="form-group">
										<label>Valor</label>
										<div class="input-group">
											<label class="sr-only"></label>
											<div class="input-group">
												<div class="input-group-addon">
													R<i class="fa fa-usd" aria-hidden="true"></i>
												</div>
												<input name="tabMitemValor" type="text" 
													 class="valor form-control text-left">
											</div>
											</span>
										</div>
									</div>
								</div>

								<div class="col-xs-12 col-lg-2">
									<div class="form-group">
										<label>R/D</label> <select class="form-control"
											name="tabMrdStatus">
											<option value="D">Despesa</option>
											<option value="R">Receita</option>
										</select>
									</div>
								</div>
							</div>
						</div>
						
						<div class="col-lg-12">
							<div class="row">
								<div class="col-xs-12">
									<div class="form-group">
										<label>Para Locador (Relatório):</label>
										<div class="radio">
											<label class="radio-inline"> 
												<input type="radio" name="tabMck" id="tabMckSoma" value="1">Soma
											</label> 
											<label class="radio-inline"> 
												<input type="radio" name="tabMck" id="tabMckDiminui" value="2" >Diminui
											</label> 
											<label class="radio-inline"> 
												<input type="radio" name="tabMck" id="tabMckFica" value="3" >Fica no Escritório
											</label>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="tabN">
						<div class="col-lg-12">
							<div class="row">

								<div class="tabname page-header">Devolução
								<div class="pull-right"><button type="button" class="btn btn-danger btn-xs btnDeletar">Deletar Item</button></div></div>
								
								<div class="col-xs-12">
									<div class="form-group">
										<label>Para Locatário:</label>
									</div>
								</div>
								
								<div class="col-xs-12">
									<div class="row">
										<div class="col-xs-12 col-lg-3">
											<div class="form-group">
												<label>Mês de Ref. para o item</label> <input name="tabNmesRefBoleto"
													type="text" class="merRefBoleto form-control text-right"
													placeholder="00/0000">
											</div>
										</div>
									</div>
								</div>

								<div class="col-xs-12 col-lg-6">
									<div class="form-group">
										<label>Descrição</label> <input name="tabNdescricaoItem"
											type="text" class="form-control text-right descricao-item">
									</div>
								</div>
							</div>
						</div>

						<div class="col-lg-12">
							<div class="row">
								<div class="col-xs-12 col-lg-2">
									<div class="form-group">
										<label>Nº de Parcelas</label> <input name="tabNnumParcela"
											type="text" class="parcela form-control text-right"
											placeholder="00/00">
									</div>
								</div>

								<div class="col-xs-12 col-lg-3">
									<div class="form-group">
										<label>Valor</label>
										<div class="input-group">
											<label class="sr-only"></label>
											<div class="input-group">
												<div class="input-group-addon">
													R<i class="fa fa-usd" aria-hidden="true"></i>
												</div>
												<input name="tabNitemValor" type="text" 
													 class="valor form-control text-left">
											</div>
											</span>
										</div>
									</div>
								</div>

								<div class="col-xs-12 col-lg-2">
									<div class="form-group">
										<label>R/D</label> <select class="form-control"
											name="tabNrdStatus">
											<option value="R">Receita</option>
											<option value="D">Despesa</option>
										</select>
									</div>
								</div>
							</div>
						</div>
						
						<div class="col-lg-12">
							<div class="row">
								<div class="col-xs-12">
									<div class="form-group">
										<label>Para Locador (Relatório):</label>
										<div class="radio">
											<label class="radio-inline"> 
												<input type="radio" name="tabNck" id="tabNckSoma" value="1">Soma
											</label> 
											<label class="radio-inline"> 
												<input type="radio" name="tabNck" id="tabNckDiminui" value="2" >Diminui
											</label> 
											<label class="radio-inline"> 
												<input type="radio" name="tabNck" id="tabNckFica" value="3" >Fica no Escritório
											</label>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="tabO">
						<div class="col-lg-12">
							<div class="row">

								<div class="tabname page-header">Cobrança
								<div class="pull-right"><button type="button" class="btn btn-danger btn-xs btnDeletar">Deletar Item</button></div></div>
								
								<div class="col-xs-12">
									<div class="form-group">
										<label>Para Locatário:</label>
									</div>
								</div>
								
								<div class="col-xs-12">
									<div class="row">
										<div class="col-xs-12 col-lg-3">
											<div class="form-group">
												<label>Mês de Ref. para o item</label> <input name="tabOmesRefBoleto"
													type="text" class="merRefBoleto form-control text-right"
													placeholder="00/0000">
											</div>
										</div>
									</div>
								</div>

								<div class="col-xs-12 col-lg-6">
									<div class="form-group">
										<label>Descrição</label> <input name="tabOdescricaoItem"
											type="text" class="form-control text-right descricao-item">
									</div>
								</div>
							</div>
						</div>

						<div class="col-lg-12">
							<div class="row">
								<div class="col-xs-12 col-lg-2">
									<div class="form-group">
										<label>Nº de Parcelas</label> <input name="tabOnumParcela"
											type="text" class="parcela form-control text-right"
											placeholder="00/00">
									</div>
								</div>

								<div class="col-xs-12 col-lg-3">
									<div class="form-group">
										<label>Valor</label>
										<div class="input-group">
											<label class="sr-only"></label>
											<div class="input-group">
												<div class="input-group-addon">
													R<i class="fa fa-usd" aria-hidden="true"></i>
												</div>
												<input name="tabOitemValor" type="text" 
													 class="valor form-control text-left">
											</div>
											</span>
										</div>
									</div>
								</div>

								<div class="col-xs-12 col-lg-2">
									<div class="form-group">
										<label>R/D</label> <select class="form-control"
											name="tabOrdStatus">
											<option value="D">Despesa</option>
											<option value="R">Receita</option>
										</select>
									</div>
								</div>
							</div>
						</div>
						
						<div class="col-lg-12">
							<div class="row">
								<div class="col-xs-12">
									<div class="form-group">
										<label>Para Locador (Relatório):</label>
										<div class="radio">
											<label class="radio-inline"> 
												<input type="radio" name="tabOck" id="tabOckSoma" value="1">Soma
											</label> 
											<label class="radio-inline"> 
												<input type="radio" name="tabOck" id="tabOckDiminui" value="2" >Diminui
											</label> 
											<label class="radio-inline"> 
												<input type="radio" name="tabOck" id="tabOckFica" value="3" >Fica no Escritório
											</label>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="tabP">
						<div class="col-lg-12">
							<div class="row">

								<div class="tabname page-header">Multa
								<div class="pull-right"><button type="button" class="btn btn-danger btn-xs btnDeletar">Deletar Item</button></div></div>
								
								<div class="col-xs-12">
									<div class="form-group">
										<label>Para Locatário:</label>
									</div>
								</div>
								
								<div class="col-xs-12">
									<div class="row">
										<div class="col-xs-12 col-lg-3">
											<div class="form-group">
												<label>Mês de Ref. para o item</label> <input name="tabPmesRefBoleto"
													type="text" class="merRefBoleto form-control text-right"
													placeholder="00/0000">
											</div>
										</div>
									</div>
								</div>

								<div class="col-xs-12 col-lg-2">
									<div class="form-group">
										<label>Nº de Parcelas</label> <input name="tabPnumParcela"
											type="text" class="parcela form-control text-right"
											placeholder="00/00">
									</div>
								</div>

								<div class="col-xs-12 col-lg-3">
									<div class="form-group">
										<label>Valor</label>
										<div class="input-group">
											<label class="sr-only"></label>
											<div class="input-group">
												<div class="input-group-addon">
													R<i class="fa fa-usd" aria-hidden="true"></i>
												</div>
												<input name="tabPitemValor" type="text" 
													 class="valor form-control text-left">
											</div>
											</span>
										</div>
									</div>
								</div>

								<div class="col-xs-12 col-lg-2">
									<div class="form-group">
										<label>R/D</label> <select class="form-control"
											name="tabPrdStatus">
											<option value="D">Despesa</option>
											<option value="R">Receita</option>
										</select>
									</div>
								</div>
							</div>
						</div>
						
						<div class="col-lg-12">
							<div class="row">
								<div class="col-xs-12">
									<div class="form-group">
										<label>Para Locador (Relatório):</label>
										<div class="radio">
											<label class="radio-inline"> 
												<input type="radio" name="tabPck" id="tabPckSoma" value="1">Soma
											</label> 
											<label class="radio-inline"> 
												<input type="radio" name="tabPck" id="tabPckDiminui" value="2" >Diminui
											</label> 
											<label class="radio-inline"> 
												<input type="radio" name="tabPck" id="tabPckFica" value="3" >Fica no Escritório
											</label>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="tabQ">
						<div class="col-lg-12">
							<div class="row">

								<div class="tabname page-header">Juros
								<div class="pull-right"><button type="button" class="btn btn-danger btn-xs btnDeletar">Deletar Item</button></div></div>
								
								<div class="col-xs-12">
									<div class="form-group">
										<label>Para Locatário:</label>
									</div>
								</div>
								
								<div class="col-xs-12">
									<div class="row">
										<div class="col-xs-12 col-lg-3">
											<div class="form-group">
												<label>Mês de Ref. para o item</label> <input name="tabQmesRefBoleto"
													type="text" class="merRefBoleto form-control text-right"
													placeholder="00/0000">
											</div>
										</div>
									</div>
								</div>

								<div class="col-xs-12 col-lg-2">
									<div class="form-group">
										<label>Nº de Parcelas</label> <input name="tabQnumParcela"
											type="text" class="parcela form-control text-right"
											placeholder="00/00">
									</div>
								</div>

								<div class="col-xs-12 col-lg-3">
									<div class="form-group">
										<label>Valor</label>
										<div class="input-group">
											<label class="sr-only"></label>
											<div class="input-group">
												<div class="input-group-addon">
													R<i class="fa fa-usd" aria-hidden="true"></i>
												</div>
												<input name="tabQitemValor" type="text" 
													 class="valor form-control text-left">
											</div>
											</span>
										</div>
									</div>
								</div>

								<div class="col-xs-12 col-lg-2">
									<div class="form-group">
										<label>R/D</label> <select class="form-control"
											name="tabQrdStatus">
											<option value="D">Despesa</option>
											<option value="R">Receita</option>
										</select>
									</div>
								</div>
							</div>
						</div>
						
						<div class="col-lg-12">
							<div class="row">
								<div class="col-xs-12">
									<div class="form-group">
										<label>Para Locador (Relatório):</label>
										<div class="radio">
											<label class="radio-inline"> 
												<input type="radio" name="tabQck" id="tabQckSoma" value="1">Soma
											</label> 
											<label class="radio-inline"> 
												<input type="radio" name="tabQck" id="tabQckDiminui" value="2" >Diminui
											</label> 
											<label class="radio-inline"> 
												<input type="radio" name="tabQck" id="tabQckFica" value="3" >Fica no Escritório
											</label>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="tabR">
						<div class="col-lg-12">
							<div class="row">

								<div class="tabname page-header">Desconto 2
								<div class="pull-right"><button type="button" class="btn btn-danger btn-xs btnDeletar">Deletar Item</button></div></div>
								
								<div class="col-xs-12">
									<div class="form-group">
										<label>Para Locatário:</label>
									</div>
								</div>
								
								<div class="col-xs-12">
									<div class="row">
										<div class="col-xs-12 col-lg-3">
											<div class="form-group">
												<label>Mês de Ref. para o item</label> <input name="tabRmesRefBoleto"
													type="text" class="merRefBoleto form-control text-right"
													placeholder="00/0000">
											</div>
										</div>
									</div>
								</div>

								<div class="col-xs-12 col-lg-6">
									<div class="form-group">
										<label>Descrição</label> <input name="tabRdescricaoItem"
											type="text" class="form-control text-right descricao-item">
									</div>
								</div>
							</div>
						</div>

						<div class="col-lg-12">
							<div class="row">
								<div class="col-xs-12 col-lg-2">
									<div class="form-group">
										<label>Nº de Parcelas</label> <input name="tabRnumParcela"
											type="text" class="parcela form-control text-right"
											placeholder="00/00">
									</div>
								</div>

								<div class="col-xs-12 col-lg-3">
									<div class="form-group">
										<label>Valor</label>
										<div class="input-group">
											<label class="sr-only"></label>
											<div class="input-group">
												<div class="input-group-addon">
													R<i class="fa fa-usd" aria-hidden="true"></i>
												</div>
												<input name="tabRitemValor" type="text" 
													 class="valor form-control text-left">
											</div>
											</span>
										</div>
									</div>
								</div>

								<div class="col-xs-12 col-lg-2">
									<div class="form-group">
										<label>R/D</label> <select class="form-control"
											name="tabRrdStatus">
											<option value="R">Receita</option>
											<option value="D">Despesa</option>
											<option value="N">Neutro</option>
										</select>
									</div>
								</div>
							</div>
						</div>
						
						<div class="col-lg-12">
							<div class="row">
								<div class="col-xs-12">
									<div class="form-group">
										<label>Para Locador (Relatório):</label>
										<div class="radio">
											<label class="radio-inline"> 
												<input type="radio" name="tabRck" id="tabRckSoma" value="1">Soma
											</label> 
											<label class="radio-inline"> 
												<input type="radio" name="tabRck" id="tabRckDiminui" value="2" >Diminui
											</label> 
											<label class="radio-inline"> 
												<input type="radio" name="tabRck" id="tabRckFica" value="3" >Fica no Escritório
											</label>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="tabS">
						<div class="col-lg-12">
							<div class="row">

								<div class="tabname page-header">Desconto 3
								<div class="pull-right"><button type="button" class="btn btn-danger btn-xs btnDeletar">Deletar Item</button></div></div>
								
								<div class="col-xs-12">
									<div class="form-group">
										<label>Para Locatário:</label>
									</div>
								</div>
								
								<div class="col-xs-12">
									<div class="row">
										<div class="col-xs-12 col-lg-3">
											<div class="form-group">
												<label>Mês de Ref. para o item</label> <input name="tabSmesRefBoleto"
													type="text" class="merRefBoleto form-control text-right"
													placeholder="00/0000">
											</div>
										</div>
									</div>
								</div>

								<div class="col-xs-12 col-lg-6">
									<div class="form-group">
										<label>Descrição</label> <input name="tabSdescricaoItem"
											type="text" class="form-control text-right descricao-item">
									</div>
								</div>
							</div>
						</div>

						<div class="col-lg-12">
							<div class="row">
								<div class="col-xs-12 col-lg-2">
									<div class="form-group">
										<label>Nº de Parcelas</label> <input name="tabSnumParcela"
											type="text" class="parcela form-control text-right"
											placeholder="00/00">
									</div>
								</div>

								<div class="col-xs-12 col-lg-3">
									<div class="form-group">
										<label>Valor</label>
										<div class="input-group">
											<label class="sr-only"></label>
											<div class="input-group">
												<div class="input-group-addon">
													R<i class="fa fa-usd" aria-hidden="true"></i>
												</div>
												<input name="tabSitemValor" type="text" 
													 class="valor form-control text-left">
											</div>
											</span>
										</div>
									</div>
								</div>

								<div class="col-xs-12 col-lg-2">
									<div class="form-group">
										<label>R/D</label> <select class="form-control"
											name="tabSrdStatus">
											<option value="R">Receita</option>
											<option value="D">Despesa</option>
											<option value="N">Neutro</option>
										</select>
									</div>
								</div>
							</div>
						</div>
						
						<div class="col-lg-12">
							<div class="row">
								<div class="col-xs-12">
									<div class="form-group">
										<label>Para Locador (Relatório):</label>
										<div class="radio">
											<label class="radio-inline"> 
												<input type="radio" name="tabSck" id="tabSckSoma" value="1">Soma
											</label> 
											<label class="radio-inline"> 
												<input type="radio" name="tabSck" id="tabSckDiminui" value="2" >Diminui
											</label> 
											<label class="radio-inline"> 
												<input type="radio" name="tabSck" id="tabSckFica" value="3" >Fica no Escritório
											</label>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					
					
					<div role="tabpanel" class="tab-pane" id="tabT">
						<div class="col-lg-12">
							<div class="row">

								<div class="tabname page-header">Taxa de Administração Acordo Extrajudicial
								<div class="pull-right"><button type="button" class="btn btn-danger btn-xs btnDeletar">Deletar Item</button></div></div>
								
								<div class="col-xs-12">
									<div class="form-group">
										<label>Para Locatário:</label>
									</div>
								</div>
								
								<div class="col-xs-12">
									<div class="row">
										<div class="col-xs-12 col-lg-3">
											<div class="form-group">
												<label>Mês de Ref. para o item</label> <input name="tabTmesRefBoleto"
													type="text" class="merRefBoleto form-control text-right"
													placeholder="00/0000">
											</div>
										</div>
									</div>
								</div>

							
							</div>
						</div>

						<div class="col-lg-12">
							<div class="row">
								<div class="col-xs-12 col-lg-2">
									<div class="form-group">
										<label>Nº de Parcelas</label> <input name="tabTnumParcela"
											type="text" class="parcela form-control text-right"
											placeholder="00/00">
									</div>
								</div>

								<div class="col-xs-12 col-lg-3">
									<div class="form-group">
										<label>Valor</label>
										<div class="input-group">
											<label class="sr-only"></label>
											<div class="input-group">
												<div class="input-group-addon">
													R<i class="fa fa-usd" aria-hidden="true"></i>
												</div>
												<input name="tabTitemValor" type="text" 
													 class="valor_negativo form-control text-left">
											</div>
											</span>
										</div>
									</div>
								</div>

								<div class="col-xs-12 col-lg-2">
									<div class="form-group">
										<label>R/D</label> <select class="form-control"
											name="tabTrdStatus">
											<option value="R">Receita</option>
											<option value="D">Despesa</option>
											<option value="N">Neutro</option>
										</select>
									</div>
								</div>
							</div>
						</div>
						
						<div class="col-lg-12">
							<div class="row">
								<div class="col-xs-12">
									<div class="form-group">
										<label>Para Locador (Relatório):</label>
										<div class="radio">
											<label class="radio-inline"> 
												<input type="radio" name="tabTck" id="tabTckSoma" value="1">Soma
											</label> 
											<label class="radio-inline"> 
												<input type="radio" name="tabTck" id="tabTckDiminui" value="2" >Diminui
											</label> 
											<label class="radio-inline"> 
												<input type="radio" name="tabTck" id="tabTckFica" value="3" >Fica no Escritório
											</label>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<div role="tabpanel" class="tab-pane" id="tabU">
						<div class="col-lg-12">
							<div class="row">

								<div class="tabname page-header">Rescisão Contratual
								<div class="pull-right"><button type="button" class="btn btn-danger btn-xs btnDeletar">Deletar Item</button></div></div>
								
								<div class="col-xs-12">
									<div class="form-group">
										<label>Para Locatário:</label>
									</div>
								</div>
								
								<div class="col-xs-12">
									<div class="row">
										<div class="col-xs-12 col-lg-3">
											<div class="form-group">
												<label>Mês de Ref. para o item</label> <input name="tabUmesRefBoleto"
													type="text" class="merRefBoleto form-control text-right"
													placeholder="00/0000">
											</div>
										</div>
									</div>
								</div>

							</div>
						</div>

						<div class="col-lg-12">
							<div class="row">
								<div class="col-xs-12 col-lg-2">
									<div class="form-group">
										<label>Nº de Parcelas</label> <input name="tabUnumParcela"
											type="text" class="parcela form-control text-right"
											placeholder="00/00">
									</div>
								</div>

								<div class="col-xs-12 col-lg-3">
									<div class="form-group">
										<label>Valor</label>
										<div class="input-group">
											<label class="sr-only"></label>
											<div class="input-group">
												<div class="input-group-addon">
													R<i class="fa fa-usd" aria-hidden="true"></i>
												</div>
												<input name="tabUitemValor" type="text" 
													 class="valor form-control text-left">
											</div>
											</span>
										</div>
									</div>
								</div>

								<div class="col-xs-12 col-lg-2">
									<div class="form-group">
										<label>R/D</label> <select class="form-control"
											name="tabUrdStatus">
											<option value="R">Receita</option>
											<option value="D">Despesa</option>
											<option value="N">Neutro</option>
										</select>
									</div>
								</div>
							</div>
						</div>
						
						<div class="col-lg-12">
							<div class="row">
								<div class="col-xs-12">
									<div class="form-group">
										<label>Para Locador (Relatório):</label>
										<div class="radio">
											<label class="radio-inline"> 
												<input type="radio" name="tabUck" id="tabUckSoma" value="1">Soma
											</label> 
											<label class="radio-inline"> 
												<input type="radio" name="tabUck" id="tabUckDiminui" value="2" >Diminui
											</label> 
											<label class="radio-inline"> 
												<input type="radio" name="tabUck" id="tabUckFica" value="3" >Fica no Escritório
											</label>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<div role="tabpanel" class="tab-pane" id="tabV">
						<div class="col-lg-12">
							<div class="row">

								<div class="tabname page-header">Taxa de Administração Rescisão Contratual
								<div class="pull-right"><button type="button" class="btn btn-danger btn-xs btnDeletar">Deletar Item</button></div></div>
								
								<div class="col-xs-12">
									<div class="form-group">
										<label>Para Locatário:</label>
									</div>
								</div>
								
								<div class="col-xs-12">
									<div class="row">
										<div class="col-xs-12 col-lg-3">
											<div class="form-group">
												<label>Mês de Ref. para o item</label> <input name="tabVmesRefBoleto"
													type="text" class="merRefBoleto form-control text-right"
													placeholder="00/0000">
											</div>
										</div>
									</div>
								</div>

								
							</div>
						</div>

						<div class="col-lg-12">
							<div class="row">
								<div class="col-xs-12 col-lg-2">
									<div class="form-group">
										<label>Nº de Parcelas</label> <input name="tabVnumParcela"
											type="text" class="parcela form-control text-right"
											placeholder="00/00">
									</div>
								</div>

								<div class="col-xs-12 col-lg-3">
									<div class="form-group">
										<label>Valor</label>
										<div class="input-group">
											<label class="sr-only"></label>
											<div class="input-group">
												<div class="input-group-addon">
													R<i class="fa fa-usd" aria-hidden="true"></i>
												</div>
												<input name="tabVitemValor" type="text" 
													 class="valor form-control text-left">
											</div>
											</span>
										</div>
									</div>
								</div>

								<div class="col-xs-12 col-lg-2">
									<div class="form-group">
										<label>R/D</label> <select class="form-control"
											name="tabVrdStatus">
											<option value="R">Receita</option>
											<option value="D">Despesa</option>
											<option value="N">Neutro</option>
										</select>
									</div>
								</div>
							</div>
						</div>
						
						<div class="col-lg-12">
							<div class="row">
								<div class="col-xs-12">
									<div class="form-group">
										<label>Para Locador (Relatório):</label>
										<div class="radio">
											<label class="radio-inline"> 
												<input type="radio" name="tabVck" id="tabVckSoma" value="1">Soma
											</label> 
											<label class="radio-inline"> 
												<input type="radio" name="tabVck" id="tabVckDiminui" value="2" >Diminui
											</label> 
											<label class="radio-inline"> 
												<input type="radio" name="tabVck" id="tabVckFica" value="3" >Fica no Escritório
											</label>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
</div>