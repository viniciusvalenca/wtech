<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="panel panel-default">
	<a data-target="#item-info">
		<div class="panel-heading">Associar Locatário</div>
	</a>
	<div id="item-info" class="panel-body panel-branco collapse in">

		<div class="col-lg-12">
			<div class="row">

				<div class="col-lg-6">
					<div class="form-group">
						<label>Locatário</label> <select class="form-control selectpicker"
							data-live-search="true" name="locatario" id="locatario"
							required="required">
							<option value="0">Selecione um Locatário</option>
							<c:forEach var="lista" items="${lista}">
								<option value="${lista.id}">${lista.id}-
									${lista.nome_completo}</option>
							</c:forEach>
						</select>
					</div>
				</div>

				<div class="col-lg-6">
					<div class="form-group">
						<label>Modalidade</label> <select class="form-control"
							id="modalidade_loc" name="modalidade">
							<option value="0">Selecione uma modalidade</option>
							<option value="Fiador">Fiador</option>
							<option value="Deposito">Depósito</option>
							<option value="Seguro Fianca">Seguro Fiança</option>
						</select>
					</div>
				</div>

				<div class="col-lg-12">
					<div class="row">
						<div id="bloco_seguro">
							<div class="col-xs-12 col-lg-2">
								<div class="form-group">
									<label>Valor do Seguro</label>
									<div class="input-group">
										<label class="sr-only"></label>
										<div class="input-group">
											<div class="input-group-addon">
												R<i class="fa fa-usd" aria-hidden="true"></i>
											</div>
											<input id="valor_seguro" name="valor_seguro" type="text"
												data-thousands="." data-decimal=","
												class="form-control text-left">
										</div>
									</div>
								</div>
							</div>
						</div>

						<div id="bloco_deposito">
							<div class="col-xs-12 col-lg-2">
								<div class="form-group">
									<label>Valor do Depósito</label>
									<div class="input-group">
										<label class="sr-only"></label>
										<div class="input-group">
											<div class="input-group-addon">
												R<i class="fa fa-usd" aria-hidden="true"></i>
											</div>
											<input id="valor_deposito" name="valor_deposito" type="text"
												data-thousands="." data-decimal=","
												class="form-control text-left">
										</div>
									</div>
								</div>
							</div>
						</div>

						<div id="bloco_fiador">
							<div class="col-xs-12 col-lg-3">
								<div class="form-group">
									<label>#1 Fiador</label> <select
										class="form-control selectpicker" data-live-search="true"
										name="fiador" id="fiador">
										<option value="0">Selecione um Fiador</option>
										<c:forEach var="lista2" items="${lista2}">
											<option value="${lista2.id}"
												data-end="${lista2.endereco.enderecoCompleto}">${lista2.id}
												- ${lista2.nome_completo}</option>
										</c:forEach>
									</select>
								</div>
							</div>

							<div class="col-xs-12 col-lg-2">
								<div class="form-group">
									<label>Matrícula do Imóvel</label> <input id="mat_imovel"
										name="mat_imovel" type="text" class="form-control">
								</div>
							</div>

							<div class="col-xs-12 col-lg-2">
								<div class="form-group">
									<label>Cartório do Imóvel</label> <input id="cartorio"
										name="cartorio" type="text" class="form-control">
								</div>
							</div>

							<div class="col-xs-12 col-lg-5">
								<div class="form-group">
									<label>Endereço do Imóvel</label> <input id="endereco_imovel"
										name="endereco_imovel" type="text" class="form-control">
								</div>
							</div>

							<div id="bloco_fiador2">
								<div class="col-xs-12 col-lg-3">
									<div class="form-group">
										<label>#2 Fiador</label> <select
											class="form-control selectpicker" data-live-search="true"
											name="fiador2" id="fiador2">
											<option value="0">Selecione um Fiador</option>
											<c:forEach var="lista2" items="${lista2}">
												<option value="${lista2.id}"
													data-end="${lista2.endereco.enderecoCompleto}">${lista2.id}
													- ${lista2.nome_completo}</option>
											</c:forEach>
										</select>
									</div>
								</div>

								<div class="col-xs-12 col-lg-2">
									<div class="form-group">
										<label>Matrícula do Imóvel</label> <input id="mat_imovel2"
											name="mat_imovel2" type="text" class="form-control">
									</div>
								</div>

								<div class="col-xs-12 col-lg-2">
									<div class="form-group">
										<label>Cartório do Imóvel</label> <input id="cartorio2"
											name="cartorio2" type="text" class="form-control">
									</div>
								</div>

								<div class="col-xs-12 col-lg-5">
									<div class="form-group">
										<label>Endereço do Imóvel</label> <input id="endereco_imovel2"
											name="endereco_imovel2" type="text" class="form-control">
									</div>
								</div>
							</div>

							<div id="bloco_fiador3">
								<div class="col-xs-12 col-lg-3">
									<div class="form-group">
										<label>#3 Fiador</label> <select
											class="form-control selectpicker" data-live-search="true"
											name="fiador3" id="fiador3">
											<option value="0">Selecione um Fiador</option>
											<c:forEach var="lista2" items="${lista2}">
												<option value="${lista2.id}"
													data-end="${lista2.endereco.enderecoCompleto}">${lista2.id}
													- ${lista2.nome_completo}</option>
											</c:forEach>
										</select>
									</div>
								</div>

								<div class="col-xs-12 col-lg-2">
									<div class="form-group">
										<label>Matrícula do Imóvel</label> <input id="mat_imovel3"
											name="mat_imovel3" type="text" class="form-control">
									</div>
								</div>

								<div class="col-xs-12 col-lg-2">
									<div class="form-group">
										<label>Cartório do Imóvel</label> <input id="cartorio3"
											name="cartorio3" type="text" class="form-control">
									</div>
								</div>

								<div class="col-xs-12 col-lg-5">
									<div class="form-group">
										<label>Endereço do Imóvel</label> <input id="endereco_imovel3"
											name="endereco_imovel3" type="text" class="form-control">
									</div>
								</div>
							</div>

						</div>

					</div>
				</div>

			</div>
		</div>
	</div>
</div>