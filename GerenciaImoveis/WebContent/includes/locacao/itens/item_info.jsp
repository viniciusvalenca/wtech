<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<div class="col-lg-12">
	<div class="row">
	
		<div class="tabname page-header">Nome do Item</div>
	
		<div class="col-xs-3 col-lg-2">
			<div class="form-group">
				<label>Nº de Parcelas</label> <input name="numParcela"
					type="text" class="parcela form-control text-right numParcela" 
					placeholder="00/00">
			</div>
		</div>

		<div class="col-xs-4 col-lg-3">
			<div class="form-group">
				<label>Valor</label>
				<div class="input-group">
					<label class="sr-only"></label>
					<div class="input-group">
						<div class="input-group-addon">
							R<i class="fa fa-usd" aria-hidden="true"></i>
						</div>
						<input name="itemValor" type="text" data-thousands="."
							data-decimal="," class="itemValor form-control text-left">
					</div>
					</span>
				</div>
			</div>
		</div>

		<div class="col-xs-4 col-lg-2">
			<div class="form-group">
				<label>R/D</label> <select class="rdStatus form-control" name="rdStatus">
					<option value="0">Selecione ...</option>
					<option value="R">Receita</option>
					<option value="D">Despesa</option>
				</select>
			</div>
		</div>
	</div>
</div>
