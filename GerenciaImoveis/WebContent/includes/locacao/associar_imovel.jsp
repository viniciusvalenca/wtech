<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="panel panel-default">
	<a data-target="#item-info">
		<div class="panel-heading"> Associar Imóvel </div>
	</a>
	<div id="item-info" class="panel-body panel-branco collapse in">

		<div class="col-lg-12">
			<div class="row">

				<table id="tableImoveis" data-toggle="table" name="tableImoveis"
					data-classes="table table-condensed"
					data-url="/GerenciaImoveis/listarImoveisDisponiveis"
					data-search="true"
					data-click-to-select="true"
					data-single-select="true" 
					data-content-type="application/json"
					data-data-type="json" 
					data-sort-order="desc"
					data-show-columns="true"
					data-sort-name="stargazers_count" 
					data-pagination="true"
					data-page-size="5" data-page-list="[5, 10, 20, 50, 100, 200]"
					data-pagination-first-text="Primeiro"
					data-pagination-pre-text="<i class='glyphicon glyphicon glyphicon-chevron-left'></i>"
					data-pagination-next-text="<i class='glyphicon glyphicon glyphicon-chevron-right'></i>"
					data-pagination-last-text="Último" data-locale="pt-BR">
					<thead>
						<tr>
							<th data-field="state" data-checkbox="true" data-align="center"
								data-width="40" data-sortable="true"></th>
							<th data-field="endereco.enderecoCompleto" data-halign="center"
								data-align="center" data-sortable="true">Endereço</th>
							<th data-field="locador.nome_completo" data-halign="center"
								data-align="center" data-sortable="true">Locador</th>
							<th data-field="valorString" data-halign="center" data-align="center" data-width="160" data-sortable="true">Valor (R$)</th>
							<th data-field="flg_disponivel" data-halign="center"
								data-align="center" data-formatter="maskDisponivel"
								data-width="80" data-sortable="true">Disponibilidade</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>