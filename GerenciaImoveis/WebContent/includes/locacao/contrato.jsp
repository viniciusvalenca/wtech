<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="panel panel-default">
	<a data-target="#item-info">
		<div class="panel-heading"> Contrato </div>
	</a>
	<div id="item-info" class="panel-body panel-branco collapse in">

		<div class="col-lg-12">
			<div class="row">
				
				<div class="col-xs-6 col-lg-2">
					<div class="form-group">
						<label>Dt. Início</label>
						<div class="input-group date" data-provide="datepicker"
							data-date-format="dd/mm/yyyy" data-date-autoclose="true">
							<input id="dt_inicio" name="dt_inicio" type="text"
								class="format_data form-control" required>
							<div class="input-group-addon">
								<span class="glyphicon glyphicon-th"></span>
							</div>
						</div>
					</div>
				</div>

				<div class="col-xs-6 col-lg-2">
					<div class="form-group">
						<label>Dt. Término</label>
						<div class="input-group date" data-provide="datepicker"
							data-date-format="dd/mm/yyyy" data-date-autoclose="true">
							<input id="dt_termino" name="dt_termino" type="text"
								class="format_data form-control" required>
							<div class="input-group-addon">
								<span class="glyphicon glyphicon-th"></span>
							</div>
						</div>
					</div>
				</div>

				<div class="col-xs-6 col-lg-2">
					<div class="form-group">
						<label>Dia de Vencimento</label> <input id="dia_vencimento"
							name="dia_vencimento" type="text"
							class="dia_vencimento form-control" maxlength="2" required>
					</div>
				</div>

				<div class="col-xs-6 col-lg-2">
					<div class="form-group">
						<label>Dt. de Reajuste</label>
						<div class="input-group date" data-provide="datepicker"
							data-date-format="dd/mm/yyyy" data-date-autoclose="true">
							<input id="dt_reajuste" name="dt_reajuste" type="text"
								class="format_data form-control" required>
							<div class="input-group-addon">
								<span class="glyphicon glyphicon-th"></span>
							</div>
						</div>
					</div>
				</div>

				<div class="col-xs-6 col-lg-2">
					<div class="form-group">
						<label>Dt. Entrega da Chave</label>
						<div class="input-group date" data-provide="datepicker"
							data-date-format="dd/mm/yyyy" data-date-autoclose="true">
							<input id="dt_entrega_chave" name="dt_entrega_chave" type="text"
								class="format_data form-control">
							<div class="input-group-addon">
								<span class="glyphicon glyphicon-th"></span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>