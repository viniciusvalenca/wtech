<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div id="side-nav-collapse"
	class="col-lg-2 col-md-3 col-sm-3 collapse side-nav">

	<div class="absi">
		<!--logo img-rounded img-responsive center-block-->
		<img src="img/logoNew.jpg" alt="absi logo" class="img-responsive">
		<!--span class="text-center">Absi Negócios Imobiliários</span-->
	</div>

	<ul class="nav">
		<li class="mPai"><a href="dashboard"> <span
				class="glyphicon glyphicon-home"></span>Dashboard
		</a></li>
		<li class="mPai" data-toggle="collapse" data-target="#sub-item-1">
			<a href="#"><span class="glyphicon glyphicon-folder-open"></span>Cadastrar
				<span class="arrow glyphicon glyphicon-triangle-bottom navbar-right"></span></a>
			<ul id="sub-item-1" class="collapse">
				<li><a href="cadcliente"><span
						class="glyphicon glyphicon-plus"></span>Novo Cliente</a></li>
				<li><a href="cadimovel"><span
						class="glyphicon glyphicon-plus"></span>Imóvel</a></li>
				<li><a href="cadlocacao"><span
						class="glyphicon glyphicon-plus"></span>Locação</a></li>
			</ul>
		</li>
		<li class="mPai" data-toggle="collapse" data-target="#sub-item-2">
			<a href="#"><span class="glyphicon glyphicon-search"></span>Consultar
				<span class="arrow glyphicon glyphicon-triangle-bottom navbar-right"></span></a>
			<ul id="sub-item-2" class="collapse">
				<li><a href="buscar_cliente.jsp"><span
						class="glyphicon glyphicon-search"></span>Cliente</a></li>
				<li><a href="buscarImovel"><span
						class="glyphicon glyphicon-search"></span>Imóvel</a></li>
				<li><a href="buscarLocacao"><span
						class="glyphicon glyphicon-search"></span>Locação</a></li>
			</ul>
		</li>

		<li class="mPai"><a href="itenslocacao"><span
				class="glyphicon glyphicon-th-list"></span>Editar Itens da Locação</a></li>

		<li class="mPai" data-toggle="collapse" data-target="#sub-item-3">
			<a href="#"><span class="glyphicon glyphicon-usd"></span>Financeiro
				<span class="arrow glyphicon glyphicon-triangle-bottom navbar-right"></span></a>
			<ul id="sub-item-3" class="collapse">
				<li><a href="gerar_remessa.jsp"><span
						class="glyphicon glyphicon-paste"></span>Gerar Remessa</a></li>
				<li><a href="gerar_boleto.jsp"><span
						class="glyphicon glyphicon-copy"></span>Boletos</a></li>
				<li><a href="editar_boleto.jsp"><span
						class="glyphicon glyphicon-copy"></span>Editar Boleto</a></li>
				<li class="nav-divider"></li>
				<li><a href="receber_conta_auto.jsp"><span
						class="glyphicon glyphicon-triangle-right"></span>Receber Conta
						Automático</a></li>
				<li><a href="receber_conta_manual.jsp"><span
						class="glyphicon glyphicon-triangle-right"></span>Receber Conta
						Manual</a></li>
				<li class="nav-divider"></li>
				<li><a href="imposto_renda.jsp"><span
						class="glyphicon glyphicon-triangle-right"></span>Imposto de Renda</a></li>
			</ul>
		</li>

		<li class="mPai" data-toggle="collapse" data-target="#sub-item-4">
			<a href="#"><span class="glyphicon glyphicon-file"></span>Relatórios
				<span class="arrow glyphicon glyphicon-triangle-bottom navbar-right"></span></a>
			<ul id="sub-item-4" class="collapse">
				<li class="mPai"><a href="relatorio_locador.jsp"><span
						class="glyphicon glyphicon-file"></span>Locador</a></li>
				<li class="mPai"><a href="relatorio_transferencias.jsp"><span
						class="glyphicon glyphicon-file"></span>Transferências</a></li>
				<li class="mPai"><a href="relatorio_imposto_renda.jsp"><span
						class="glyphicon glyphicon-file"></span>Imposto de Renda</a></li>
				<c:if test="${user.nome != 'Lucia'}">
					<li class="mPai"><a href="relatorio_taxa_administracao.jsp"><span
							class="glyphicon glyphicon-file"></span>Taxa de Administração</a></li>
				</c:if>

				<li class="mPai"><a href="relatorio_controle_alugueis.jsp"><span
						class="glyphicon glyphicon-file"></span>Controle de Aluguéis</a></li>
						<li class="mPai"><a href="relatorio_controle_ir.jsp"><span
						class="glyphicon glyphicon-file"></span>Controle de imposto de Renda</a></li>
			</ul>
		</li>

		<li class="mPai" data-toggle="collapse" data-target="#sub-item-5">
			<a href="#"><span class="glyphicon glyphicon-th-list"></span>Históricos
				<span class="arrow glyphicon glyphicon-triangle-bottom navbar-right"></span></a>
			<ul id="sub-item-5" class="collapse">
				<li class="mPai"><a href="historico_acesso.jsp"><span
						class="glyphicon glyphicon-th-list"></span>Histórico de Acessos</a></li>
				<li class="mPai"><a href="historico_contrato.jsp"><span
						class="glyphicon glyphicon-th-list"></span>Histórico de Contratos</a></li>
			</ul>
		</li>



		<li class="mPai"><a href="indice"><span
				class="glyphicon glyphicon-edit"></span>Índice de Reajuste</a></li>

		<!-- <li role="presentation"></li> -->
		<li class="mPai"><a href="logout"><span
				class="glyphicon glyphicon-log-out" aria-hidden="true"></span>Sair</a></li>

		<li class="text-center"><a href="#" data-toggle="modal"
			data-target="#modalAbout"><span></span>Sobre</a></li>
	</ul>
</div>

<div class="modal fade" id="modalAbout" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="media">
					<div class="media-left">
						
					</div>
					<div class="media-body">
					<img src="img/zt.png" alt="absi logo" class="img-responsive">
						<address>
							 <a
								href="http://www.zeituneinformatica.com.br" target="blank">www.zeituneinformatica.com.br</a><br>
							<abbr>Email:</abbr><a href="mailto:vinicius@zeituneinformatica.com.br"
								target="_top"> vinicius@zeituneinformatica.com.br</a><br>
						</address>
					</div>

					<div style="margin: 0 10px 0 10px; text-align: justify;">
						<p>
A solução perfeita para seus problemas de TI, agora ao seu alcance. Há 12 anos no mercado provendo soluções inovadoras para sua empresa, permitindo que o seu negócio prospere com a melhor performance..
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

