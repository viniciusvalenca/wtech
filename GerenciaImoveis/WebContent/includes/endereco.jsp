<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<div class="panel panel-default">
	<a data-target="#item-endereco">
		<div class="panel-heading"> Endereço </div>
	</a>
	<div id="item-endereco" class="panel-body panel-branco collapse in">
		<div class="col-lg-12">
			<div class="row">
				<div class="col-lg-3">
					<div class="form-group">
						<label>CEP</label>
						<div class="input-group">
							<input type="text" id="cep" name="cep" class="cep form-control"
								placeholder="Entre com o CEP" required> <span
								class="input-group-btn">
								<button class="btn btn-default" id="bcep" name="bcep"
									type="button">
									<span class="glyphicon glyphicon-search" aria-hidden="true"></span>
								</button>
							</span>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="col-lg-12">
			<div class="row">
				<div class="col-lg-7">
					<div class="form-group">
						<label>Endereço</label> <input id="endereco" name="endereco"
							type="text" class="form-control">
					</div>
				</div>

				<div class="col-lg-2">
					<div class="form-group">
						<label>Número</label> <input id="numero" name="numero" type="text"
							class="form-control">
					</div>
				</div>
			</div>
		</div>

		<div class="col-lg-12">
			<div class="row">
				<div class="col-lg-1">
					<div class="form-group">
						<label>Estado</label> <input name="estado" id="estado" type="text"
							class="form-control">
					</div>
				</div>
				
				<div class="col-lg-2">
					<div class="form-group">
						<label>Cidade</label> <input name="cidade" id="cidade" type="text"
							class="form-control">
					</div>
				</div>
				
				<div class="col-lg-2">
					<div class="form-group">
						<label>Bairro</label> <input id="bairro" name="bairro" type="text"
							class="form-control">
					</div>
				</div>
				
				<div class="col-lg-3">
					<div class="form-group">
						<label>Complemento</label> <input id="complemento" name="complemento" type="text"
							class="form-control">
					</div>
				</div>
				
				<div class="col-lg-3">
					<div class="form-group">
						<label>Nome do Condomínio</label> <input id="nom_condominio" name="nom_condominio" type="text"
							class="form-control">
					</div>
				</div>

			</div>
		</div>
	</div>
</div>