<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ABSI - Locação</title>

<jsp:include page="includes/links.jsp" />
<script src="js/locacao.js" type="text/javascript"></script>

<script>

$(document).ready(function(){
	
	$('.date').datepicker({
		language : "pt-BR"
	 });
	
	$('#dt_inicio').datepicker({
		startDate : "01/01/1930",
		language : "pt-BR"
	});
	
	$('#dt_inicio').change(function() {
		date = $('#dt_inicio').datepicker('getDate');

		if(moment(date, 'YYYY-MM-DD', true).isValid()){
			var inicio = moment(date);
		  	var termino = moment(date).subtract(1, "days");
		  	var reajuste = moment(date);
		  	
		  	termino.add(30,'months');
		  	reajuste.add(12,'months');
		  	
		  	console.log("término: ", termino.toDate());
			console.log("reajuste: ", reajuste.toDate());
			console.log("inicio: ", inicio.toDate());
		  	
		  	$('#dt_termino').datepicker({ 
		  		startDate : inicio.toDate(),
				language : "pt-BR"
			});	  	
		    $("#dt_termino").datepicker('setDate', termino.toDate());
		  	
		    $('#dt_reajuste').datepicker({ 
		    	startDate : inicio.toDate(),
				language : "pt-BR"
			});
		    $("#dt_reajuste").datepicker('setDate', reajuste.toDate());
		}
	});
	
	$("#btnCadastrar").click(function() {
		var $form = $("#form-locacao");
		$form.validate().form(); 
		
		var obj = $("#tableImoveis").bootstrapTable('getSelections');
		if(obj.length != 0){
			$form.submit();
		}else{
			mostrarAlerta("Aviso","Selecione um Imóvel");
		}
	});
});
</script>

</head>
<body>

	<jsp:include page="includes/menu.jsp" />

	<div
		class="col-lg-10 col-lg-offset-2 col-md-9 col-md-offset-3 col-sm-9 col-sm-offset-3 main">

		<div class="container-fluid main">

			<ol class="breadcrumb">
				<li><i class="fa fa-home"></i> <a href="dashboard">Dashboard</a></li>
				<li><i class="fa fa-user-plus"></i> <a href="#"> Nova Locação</a></li>
			</ol>

			<div class="conteudo">
				<form id="form-locacao" class="form-horizontal" action="cadastrarLocacao"
					method="POST" accept-charset="utf-8">
					<div class="row">
						<div class="col-lg-12">
							
							<input id="idImovelHidden" name="idImovelHidden" type="hidden"/>
							
							<input id="nom_tela" name="nom_tela" type="hidden" value="Cadastrar > Locação"/>

							<jsp:include page="includes/locacao/associar_imovel.jsp" />

							<jsp:include page="includes/locacao/associar_locatario.jsp" />

							<jsp:include page="includes/locacao/contrato.jsp" />

							<div class="panel">
								<div class="row">
									<div class="col-sm-6 col-lg-6">
										<button type="reset" id="btnResetLocacao" class="btn btn-default btn-block">Limpar
											Formulário</button>
									</div>
									<div class="col-sm-6 col-lg-6">
										<button type="button" id="btnCadastrar"
											class="btn btn-primary btn-block">Associar</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

</body>
</html>
