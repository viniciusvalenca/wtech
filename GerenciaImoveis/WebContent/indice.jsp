<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ABSI - Editar Indice</title>

<jsp:include page="includes/links.jsp" />
<script>
$(document).ready(function(){
	$("#btnAtualizar").click(function(){
		//if($("#indice_reajuste").val() != ""){
			$("#form-indice").submit();
		//}else{
			//mostrarAlerta("Aviso","Valor inválido");
		//}
	});
});
</script>
</head>
<body>

	<jsp:include page="includes/menu.jsp" />

	<div
		class="col-lg-10 col-lg-offset-2 col-md-9 col-md-offset-3 col-sm-9 col-sm-offset-3 main">

		<div class="container-fluid main">

			<ol class="breadcrumb">
				<li><i class="fa fa-home"></i> <a href="dashboard">Dashboard</a></li>
				<li><i class="fa fa-pencil-square-o"></i> <a href="#">Editar Indice</a></li>
			</ol>

			<div class="conteudo">
				<form id="form-indice" class="form-horizontal"
					action="atualizarindice" method="POST">
					<div class="row">
						<div class="col-lg-12">

							<div class="panel panel-default">
								<a data-target="#item-gerar-conta">
									<div class="panel-heading">
										Editar Indice </div>
								</a>
								<div class="panel-body panel-branco collapse in">

									<input id="nom_tela" name="nom_tela" type="hidden"
										value="Editar > Indice" />

									<div class="col-lg-12">
										<div class="row">
											
											<div class="col-xs-12 col-lg-3">
												<div class="form-group">
													<label>Indice Reajuste</label>
													<div class="input-group">
														<label class="sr-only"></label>
														<div class="input-group">
															<div class="input-group-addon">&#37;</div>
															<input id="indice_reajuste" name="indice" type="text" 
																 value="${indice}"
																class="indice form-control text-left">
														</div>
														</span>
													</div>
												</div>
											</div>
										</div>
									</div>

								</div>
							</div>

							<div id="blocoEditar">
								<div class="panel">
									<div class="row">
										<div class="col-lg-12">
											<button type="button" id="btnAtualizar"
												class="btn btn-primary btn-block">Atualizar Indice</button>
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>
				</form>

			</div>
		</div>
	</div>

</body>
</html>
