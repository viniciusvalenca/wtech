<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ABSI - Relatório Locador</title>

<jsp:include page="includes/links.jsp" />
<script>

var listaID = [];
$(document).ready(function() {
	
	$('#mes_referencia').datepicker({
		format: "mm/yyyy",
	    startView: 1,
	    minViewMode: 1,
	 	language : "pt-BR"
	 });
	
	$('.date').datepicker({
		language : "pt-BR"
	});
	
	$table = $('#tableLocacao');
	$btn = $("#btnBuscar");
	$("#groupBtns").hide();
	
	$table.on('check.bs.table', function (row, $element) {
		$('#idLocadorHidden').val($element.imovel.locador.id);
		$('#nomeLocadorHidden').val($element.imovel.locador.nome_completo);
		
		setObjLista();
		setListaID();
		
		$("#groupBtns").show();
	});
	
	$table.on('check-all.bs.table', function (row, $element) {
		var obj = $table.bootstrapTable('getSelections');
		
		if(obj.length > 0){
			setObjLista();
			setListaID();
			
			$("#groupBtns").show();
		}
	});
	
	$table.on('uncheck.bs.table', function (row, $element) {
		pullListaID($element.imovel.locador.id);
		hiddenButtons();
	});
	
	$table.on('uncheck-all.bs.table', function (row, $element) {
		var obj = $table.bootstrapTable('getSelections');
		
		if(obj.length > 0){
			listaID = [];
			setObjLista();
			setListaID();
			
			$("#groupBtns").show();
		}
		hiddenButtons();
	});
	
	$btn.click(function(){
		if($("#mes_referencia").val() != ""){
			searchLocacoesPorMes();
		}else{
			mostrarAlerta("Aviso","Selecione um mês de referência!");
		}
	});
	
	$("#btnEmail").click(function(){
		if($("#mes_referencia").val() != ""){
			setObjLista();
			
			if(listaID.length==1){
				setAction("locador?op=enviarRelatorio");
			}else
				setAction("locadores?op=enviarRelatorio");
			
			$("#form-relatorioLocador").attr("target", "");
			
			limparCheckboxs();
			
			$("#form-relatorioLocador").submit();
		}else{
			mostrarAlerta("Aviso","Selecione um mês de referência!");
		}
	});
	
	$("#btnDownload").click(function(){
		if($("#mes_referencia").val() != ""){
			setObjLista();
			
			if(listaID.length==1){
				setAction("locador?op=downloadRelatorio");
			}else
				setAction("locadores?op=downloadRelatorio");
			
			$("#form-relatorioLocador").attr("target", "");
			
			limparCheckboxs();
			
			$("#form-relatorioLocador").submit();
		}else{
			mostrarAlerta("Aviso","Selecione um mês de referência!");
		}
	});
	
	$("#btnGerar").click(function(){
		if($("#mes_referencia").val() != ""){
			
			if(listaID.length==1){
				setAction("locador");
			}else
				setAction("locadores");
			
			console.log("listaID: "+ listaID);
			var obj = $table.bootstrapTable('getSelections');
			console.log("lista: "+ obj.length);
			
			limparCheckboxs();
			
		}else{
			mostrarAlerta("Aviso","Selecione um mês de referência!");
		}
	});
	
	
	var mensagem = '<%=request.getParameter("msg")%>';
	var ids = '<%=request.getParameter("ids")%>';
	
	if(mensagem != ""){
		if(mensagem == "true" && ids == "null"){
			mostrarAlerta("Sucesso", "Email enviado com sucesso.");
			
		}else if(mensagem == "true" && ids != ""){
			mostrarAlerta("Aviso", "Não foi possível enviar os email's para os seguintes locadores: " + ids);
			
		}else if(mensagem == "false"){
			mostrarAlerta("Error", "Não foi possível enviar o email.");
		}
	}
});

function setObjLista(){
	var obj = $table.bootstrapTable('getSelections');
	$("#lista").val(JSON.stringify(obj));
}

function setListaID(){
	var obj = $table.bootstrapTable('getSelections');
	$.each(obj, function(index, value) {

		var found = jQuery.inArray(value.imovel.locador.id, listaID);
	
		if (found >= 0) {
		    // Element was found, remove it.
		    //filters.splice(found, 1);
		} else {
		    // Element was not found, add it.
			listaID.push(value.imovel.locador.id);
		}
	});
	//$("#lista").val(JSON.stringify(listaID));
}

function pullListaID(id){
	listaID = jQuery.grep(listaID, function(value) {
		return value != id;
	});
}

function setAction(action){
	$("#form-relatorioLocador").attr("action", action);
}

function limparCheckboxs(){
	$table.bootstrapTable('uncheckAll');
	$('.bs-checkbox input[name="btSelectGroup"]').prop('checked', false);
	
	listaID = [];
	
	hiddenButtons();
}

function hiddenButtons(){
	var obj = $table.bootstrapTable('getSelections');
	if(obj.length == 0){
		$("#groupBtns").hide();
	}
}

function searchLocacoesPorMes() {
	$.ajax({
		type : 'GET',
		url : 'listarLocacaoRelatorio',
		data : { mes_ref : $("#mes_referencia").val() },
		contentType : 'application/json; charset=utf-8',
		dataType : 'json',
		success : function(retorno) {
			console.log(retorno);
			
			$('#tableLocacao').bootstrapTable('removeAll');
			
			if(retorno){
				$('#tableLocacao').bootstrapTable('load', retorno);
			}else{
				mostrarAlerta("Aviso","Não exite nenhuma locação para ser exibida!");
			}
			
		}
	});
}
</script>
</head>
<body>

	<jsp:include page="includes/menu.jsp" />

	<div
		class="col-lg-10 col-lg-offset-2 col-md-9 col-md-offset-3 col-sm-9 col-sm-offset-3 main">

		<div class="container-fluid main">

			<ol class="breadcrumb">
				<li><i class="fa fa-home"></i> <a href="dashboard">Dashboard</a></li>
				<li><i class="fa fa-file"></i> <a href="#">Relatório
						Locador</a></li>
			</ol>

			<div class="conteudo">
				<form id="form-relatorioLocador" class="form-horizontal"
					action="locador" method="POST" target="_blank">
					<div class="row">
						<div class="col-lg-12">

							<div class="panel panel-default">
								<a data-target="#item-gerar-conta">
									<div class="panel-heading">Relatório Locador</div>
								</a>
								<div class="panel-body panel-branco collapse in">

									<input id="nom_tela" name="nom_tela" type="hidden" value="Gerar > Relatorio Locador" /> 
									<input id="idLocadorHidden" name="idLocadorHidden" type="hidden" /> 
									<input id="nomeLocadorHidden" name="nomeLocadorHidden" type="hidden" />

									<textarea id="lista" name="lista" class="hidden"></textarea>
									<textarea id="listaId" name="listaId" class="hidden"></textarea>

									<div class="col-lg-12">
										<div class="col-xs-12 col-lg-4">
											<div class="form-group">
												<label>Mês de Referência</label>
												<div class="input-group date" data-provide="datepicker"
													data-date-format="mm/yyyy" data-date-start-view="months"
													data-date-min-view-mode="months" data-date-autoclose="true">
													<input id="mes_referencia" name="mes_referencia"
														type="text" class="form-control format_data_mes_ref">
													<div class="input-group-addon">
														<span class="glyphicon glyphicon-th"></span>
													</div>
												</div>
											</div>
										</div>

										<div class="col-xs-12 col-lg-2">
											<div class="form-group">
												<label class=""></label>
												<button type="button" id="btnBuscar"
													class="btn btn-primary btn-sm btn-block">Buscar</button>
											</div>
										</div>
									</div>

									<div class="toolbar" id="groupBtns">
										<button id="btnGerar" type="submit" class="btn btn-default">Imprimir</button>
										<button id="btnDownload" type="button" class="btn btn-default">Download</button>
										<button id="btnEmail" type="button" class="btn btn-default">Email</button>
									</div>
									<div id="item-table"
										class="panel-body panel-branco collapse in">

										<!-- data-single-select="true" -->

										<table id="tableLocacao" 
											data-toolbar=".toolbar"
											data-toggle="table"
											data-classes="table table-condensed"
											data-click-to-select="true"
											data-toggle="table" 
											data-search="true" 
											data-group-by="true"
											data-group-by-field="imovel.locador.nome_completo"
											data-show-refresh="true" 
					       					data-show-columns="true"
											data-content-type="application/json"
											data-data-type="json" 
											data-sort-order="desc"
											data-sort-name="imovel.endereco.enderecoCompleto" 
											data-pagination="true" data-page-size="10"
											data-page-list="[5, 10, 20, 50, 100, 200]"
											data-pagination-first-text="Primeiro"
											data-pagination-pre-text="<i class='glyphicon glyphicon glyphicon-chevron-left'></i>"
											data-pagination-next-text="<i class='glyphicon glyphicon glyphicon-chevron-right'></i>"
											data-pagination-last-text="Último" 
											data-locale="pt-BR">
											<thead>
												<tr>
													<th data-field="state" data-checkbox="true" data-align="center" data-width="40"></th>
													<th data-field="id" data-halign="center" data-align="center">ID</th>
													<th data-field="imovel.endereco.enderecoCompleto" data-halign="center" data-align="center">Imóvel</th>
													<th data-field="locatario.nome_completo" data-halign="center" data-align="center">Locatário</th>
													<th data-field="imovel.locador.nome_completo" data-halign="center" data-align="center">Locador</th>
													<th data-field="modalidade" data-halign="center" data-align="center">Modalidade</th>
												</tr>
											</thead>
										</table>
									</div>

									

								</div>
							</div>

						</div>
					</div>
				</form>

			</div>
		</div>
	</div>

</body>
</html>
