<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ABSI - Receber Conta (Manual)</title>

<jsp:include page="includes/links.jsp" />

<script>

var $form = $("#form-gerar-retorno-manual");
var $table = $("#tableRetorno");
var valor_aluguel;

$(document).ready(function() {
	
	$('#mes_referencia').datepicker({
	    format: "mm/yyyy",
	    startView: 1,
	    minViewMode: 1,
		language : "pt-BR"
	});
	
	$('.date').datepicker({
		language : "pt-BR"
	 });
	
	$('#dt_vencimento').datepicker('remove');
    $('#dt_vencimento > .form-control').prop('disabled', true);
	
	$("#blocoEditar").hide();
	$("#blocoTable").hide();

	$("#btnBuscar").click(function() {
		mes_ref = $('#mes_referencia').val();
		
		if(mes_ref != ""){
			$("#blocoTable").show();
			$("#blocoEditar").hide();
			
			$table.bootstrapTable('removeAll');
			searchLocacoesNaoPagas();
		}else{
			mostrarAlerta("Aviso", "Selecione uma data");
		}
	});
	
	$('#dt_pagamento').datepicker({
		startDate : "01/01/1930",
		language : "pt-BR",
	});
	
	$('#dt_pagamento').change(function() {
		
		var dataVencimento = moment($('#dt_vencimento').datepicker('getDate')).format('DD/MM/YYYY');
		var dataPagamento = moment($('#dt_pagamento').datepicker('getDate'));
		var dif = Math.ceil(moment(dataPagamento.diff(dataVencimento, 'days'))/30);
	  	
		if(dif > 0){
			
			$('#multa').val((parseFloat(valor_aluguel) * .01).toFixed(2).replace(".", ",")).trigger('mask.maskMoney');
			$('#juros').val((parseFloat(valor_aluguel) * .001 * dif).toFixed(2).replace(".", ",")).trigger('mask.maskMoney');
						
			var tmp = (parseFloat($('#juros').val().replace('.', '').replace(',', '.'))
					+
					parseFloat($('#multa').val().replace('.', '').replace(',', '.'))
					+
					parseFloat($('#valor_original').val().replace('.', '').replace(',', '.'))
					);
			
			$('#total').val(parseFloat(tmp).toFixed(2).replace(".", ",")).trigger('mask.maskMoney');
		}else{
			
			
			$('#multa').val(0).trigger('mask.maskMoney');
			$('#juros').val(0).trigger('mask.maskMoney');
			
			var tmp = (parseFloat($('#juros').val().replace('.', '').replace(',', '.'))
					+
					parseFloat($('#multa').val().replace('.', '').replace(',', '.'))
					+
					parseFloat($('#valor_original').val().replace('.', '').replace(',', '.'))
					);
			
			$('#total').val(parseFloat(tmp).toFixed(2).replace(".", ",")).trigger('mask.maskMoney');
		}
	});
});

function searchLocacoesNaoPagas() {
	$.ajax({
		type : 'GET',
		url : 'listarLocacoesNaoPagas',
		data: { mes_ref : $("#mes_referencia").val() } ,
		contentType : 'application/json; charset=utf-8',
		dataType : 'json',
		success : function(retorno) {
			
				if(retorno.length > 0){
				
				$table = $("#tableRetorno");
				$table.bootstrapTable('removeAll');
				
				$.each(retorno, function( index, data ) {				  
					if(data.boleto != null){
						
						
							$table.bootstrapTable('append', data);
						
					}else{
						$table.bootstrapTable('append', data);
					}
				});
				
				var tam = $table.bootstrapTable('getData').length;
				
				if(tam == 0){
					mostrarAlerta("Aviso","Não existem contas a receber");
				}
				
			}else{
				mostrarAlerta("Aviso","Não existem contas a receber");
			}
		}
	});
}

function searchBoleto() {
	$.ajax({
		type : 'GET',
		url : 'boletoManual',
		data: { mes_ref : $("#mes_referencia").val(), locacao :  $("#locacao").val()} ,
		contentType : 'application/json; charset=utf-8',
		dataType : 'json',
		success : function(boleto) {
			if(boleto != false){
				$("#idBoletoHidden").val(boleto.id);
				$("#dt_vencimento").val(boleto.dt_vencimentoFormat);
				$("#dt_pagamento").datepicker('setStartDate', boleto.dt_vencimentoFormat);
				$("#dt_pagamento").datepicker("refresh");
				$("#valor_original").val(boleto.valorString).trigger('mask.maskMoney');
				valor_aluguel = boleto.valor_aluguelString;
			}else{
				mostrarAlerta("Error","Nenhuma informação");
			}
		}
	});
}

window.actionEvents = {
	'click .edit' : function(e, value, row, index) {
		$("#blocoEditar").show();
			
		$("#dt_pagamento").val('');
		$("#valor_original").val(0).trigger('mask.maskMoney');
		$('#multa').val(0).trigger('mask.maskMoney');
		$('#juros').val(0).trigger('mask.maskMoney');
		$('#total').val(0).trigger('mask.maskMoney');
		
		if(JSON.stringify(row.boleto) != null){
			$("#idBoletoHidden").val(row.boleto.id);
			$("#dt_vencimento").val(row.boleto.dt_vencimentoFormat);
			//$("#dt_pagamento").datepicker('setStartDate', row.boleto.dt_vencimentoFormat);
			if(row.boleto.dt_pagamentoFormat != null){
				$("#dt_pagamento").val(row.boleto.dt_pagamentoFormat);
			}
			$("#dt_pagamento").datepicker("refresh");
			$("#valor_original").val(row.boleto.valorString).trigger('mask.maskMoney');
			valor_aluguel = row.boleto.valor_aluguelString;
			
		}else{
			$("#locacao").val(JSON.stringify(row));
			searchBoleto();
		}	
	}
};

</script>

</head>
<body>

	<jsp:include page="includes/menu.jsp" />

	<div
		class="col-lg-10 col-lg-offset-2 col-md-9 col-md-offset-3 col-sm-9 col-sm-offset-3 main">

		<div class="container-fluid main">

			<ol class="breadcrumb">
				<li><i class="fa fa-home"></i> <a href="dashboard">Dashboard</a></li>
				<li><i class="fa fa-file"></i> <a href="#">Receber Conta
						(Manual)</a></li>
			</ol>

			<div class="conteudo">
				<form id="form-gerar-retorno-manual" class="form-horizontal"
					action="receberContaManual" method="POST">
					<div class="row">
						<div class="col-lg-12">

							<input id="nom_tela" name="nom_tela" type="hidden"
								value="Financeiro > Receber Conta (Manual)" /> <input
								id="idBoletoHidden" name="idBoletoHidden" type="hidden" />
								
							<textarea id="locacao" name="locacao" class="hidden"></textarea>

							<div class="panel panel-default">
								<a data-target="#item-gerar-conta">
									<div class="panel-heading">Receber Conta (Manual)</div>
								</a>
								<div id="item-gerar-conta"
									class="panel-body panel-branco collapse in">

									<div class="col-lg-12">
										<div class="row">

											<div class="col-xs-12 col-lg-4">
												<div class="form-group">
													<label>Mês de Referência</label>
													<div class="input-group date" data-provide="datepicker"
													data-date-format="mm/yyyy" data-date-start-view="months"
														data-date-min-view-mode="months" data-date-autoclose="true">
														<input id="mes_referencia" name="mes_referencia"
															type="text" class="form-control format_data_mes_ref">
														<div class="input-group-addon">
															<span class="glyphicon glyphicon-th"></span>
														</div>
													</div>
												</div>
											</div>

											<div class="col-xs-12 col-lg-2">
												<div class="form-group">
													<label class=""></label>
													<button type="button" id="btnBuscar"
														class="btn btn-primary btn-sm btn-block">Buscar</button>
												</div>
											</div>

											<div id="blocoTable">
												<div class="col-lg-12">
													<div class="row">
													
														<table id="tableRetorno" 
															data-toggle="table"
															data-classes="table table-condensed" 
															data-search="true"
															data-content-type="application/json"
															data-data-type="json" 
															data-sort-order="desc"
															data-sort-name="stargazers_count" 
															data-pagination="true"
															data-page-size="5"
															data-page-list="[5, 10, 20, 50, 100, 200]"
															data-pagination-first-text="Primeiro"
															data-pagination-pre-text="<i class='glyphicon glyphicon glyphicon-chevron-left'></i>"
															data-pagination-next-text="<i class='glyphicon glyphicon glyphicon-chevron-right'></i>"
															data-pagination-last-text="Último" data-locale="pt-BR">
															<thead>
																<tr>
																	<th
																		data-field="imovel.endereco.enderecoCompleto"
																		data-halign="center" data-align="center"
																		data-sortable="true">Imóvel</th>
																	<th data-field="imovel.locador.nome_completo"
																		data-halign="center" data-align="center"
																		data-sortable="true">Locador</th>
																	<th data-field="locatario.nome_completo"
																		data-halign="center" data-align="center"
																		data-sortable="true">Locatário</th>
																	<th data-field="boleto.dt_vencimentoFormat" data-halign="center"
																		data-align="center" data-sortable="true">Vencimento</th>
																		<th data-field="boleto.dt_pagamentoFormat" data-halign="center"
																		data-align="center" data-sortable="true">Pagamento</th>
																	<th data-field="boleto.valorString" data-halign="center"
																		data-align="center" data-sortable="true">Valor</th>
																	<th data-halign="center" data-align="center"
																		data-width="80" data-formatter="actionFormatter"
																		data-events="actionEvents">Editar</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</div>

										</div>
									</div>
								</div>
							</div>


							<div id="blocoEditar">

								<div class="col-lg-4">
									<div class="row">
										<div class="panel panel-default">
											<a data-target="#item-editar-Valores">
												<div class="panel-heading">Datas</div>
											</a>

											<div id="item-editar-Valores"
												class="panel-body panel-branco collapse in">

												<div class="col-xs-12 col-lg-6">
													<div class="form-group">
														<label>Data do Vencimento</label>
														<div class="input-group date" data-provide="datepicker"
															data-date-format="dd/mm/yyyy" data-date-autoclose="true">
															<input id="dt_vencimento" name="dt_vencimento"
																type="text" class="format_data form-control"
																readonly="readonly">
															<div class="input-group-addon">
																<span class="glyphicon glyphicon-th"></span>
															</div>
														</div>
													</div>
													
													<div class="form-group">
														<label>Data do Pagamento</label>
														<div class="input-group date" data-provide="datepicker"
															data-date-format="dd/mm/yyyy" data-date-autoclose="true">
															<input id="dt_pagamento" name="dt_pagamento" type="text"
																class="format_data form-control">
															<div class="input-group-addon">
																<span class="glyphicon glyphicon-th"></span>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>

									</div>
								</div>

								<div class="col-lg-8">
									<div class="row">
										<div class="panel panel-default">
											<a data-target="#item-editar-pagamentos">
												<div class="panel-heading">Valores</div>
											</a>
											<div id="item-editar-pagamentos"
												class="panel-body panel-branco collapse in">

												<div class="col-lg-4">
													<div class="form-group">
														<label>Valor Original</label>
														<div class="input-group">
															<label class="sr-only"></label>
															<div class="input-group">
																<div class="input-group-addon">
																	R<i class="fa fa-usd" aria-hidden="true"></i>
																</div>
																<input id="valor_original" name="valor_original"
																	type="text" data-thousands="." data-decimal=","
																	class="valor form-control text-left"
																	readonly="readonly">
															</div>
														</div>
													</div>
													<div class="form-group">
														<label>Multa</label>
														<div class="input-group">
															<label class="sr-only"></label>
															<div class="input-group">
																<div class="input-group-addon">
																	R<i class="fa fa-usd" aria-hidden="true"></i>
																</div>
																<input id="multa" name="multa" type="text"
																	data-thousands="." data-decimal=","
																	class="valor form-control text-left">
															</div>
														</div>
													</div>
												</div>
												<div class="col-lg-4">
													<div class="form-group">
														<label>Juros</label>
														<div class="input-group">
															<label class="sr-only"></label>
															<div class="input-group">
																<div class="input-group-addon">
																	R<i class="fa fa-usd" aria-hidden="true"></i>
																</div>
																<input id="juros" name="juros" type="text"
																	data-thousands="." data-decimal=","
																	class="valor form-control text-left">
															</div>
														</div>
													</div>

													<div class="form-group">
														<label>Total</label>
														<div class="input-group">
															<label class="sr-only"></label>
															<div class="input-group">
																<div class="input-group-addon">
																	R<i class="fa fa-usd" aria-hidden="true"></i>
																</div>
																<input id="total" name="total" type="text"
																	data-thousands="." data-decimal=","
																	class="valor form-control text-left">
															</div>
														</div>
													</div>
												</div>
											</div>

										</div>
									</div>
								</div>



								<div class="panel">
									<div class="row">
										<div class="col-lg-12">
											<button type="submit" id="btnSalvar"
												class="btn btn-primary btn-block">Salvar
												Informações</button>
										</div>
									</div>
								</div>

							</div>
						</div>

					</div>
				</form>
			</div>
		</div>
	</div>

</body>
</html>