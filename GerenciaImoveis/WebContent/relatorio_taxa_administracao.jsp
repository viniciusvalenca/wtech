<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ABSI - Relatório Taxa de Administração</title>

<jsp:include page="includes/links.jsp" />
<script>
	$(document).ready(function() {

	$("#mensagem").hide();

	$('#mes_referencia').datepicker({
		format: "mm/yyyy",
	    startView: 1,
	    minViewMode: 1,
	 	language : "pt-BR"
	});

	$('.date').datepicker({
		language : "pt-BR"
	});

	$table = $('#tableLocacao');
	$btn = $("#btnBuscar");

	$btn.click(function() {
		if ($("#mes_referencia").val() != "") {
			$("#mensagem").hide();
			checktxadm();
		} else {
			mostrarAlerta("Aviso","Selecione um mês de referência!");
		}
	});
});

	function hiddenButtons() {
		var obj = $('#tableLocacao').bootstrapTable('getSelections');
		if (obj.length == 0) {
			$("#btnGerar").hide();
			$("#btnDownload").hide();
		}
	}

	function checktxadm() {
		$.ajax({
			type : 'GET',
			url : 'checktxadm',
			data : {
				mes_referencia : $("#mes_referencia").val()
			},
			contentType : 'application/json; charset=utf-8',
			dataType : 'json',
			success : function(retorno) {
				if (retorno) {
					$("#form-relatoriotxadm").submit();
				} else {
					$("#mensagem").show();
					//mostrarAlerta("Aviso","Selecione um mês de referência!");
				}
			}
		});
	}
</script>
</head>
<body>

	<jsp:include page="includes/menu.jsp" />

	<div
		class="col-lg-10 col-lg-offset-2 col-md-9 col-md-offset-3 col-sm-9 col-sm-offset-3 main">

		<div class="container-fluid main">

			<ol class="breadcrumb">
				<li><i class="fa fa-home"></i> <a href="dashboard">Dashboard</a></li>
				<li><i class="fa fa-file"></i> <a href="#">Relatório Taxa
						de Administração</a></li>
			</ol>

			<div class="conteudo">
				<form id="form-relatoriotxadm" class="form-horizontal"
					action="txadm" method="POST" target="_blank">
					<div class="row">
						<div class="col-lg-12">

							<div class="panel panel-default">
								<a data-target="#item-gerar-conta">
									<div class="panel-heading">Relatório Taxa de
										Administração</div>
								</a>
								<div class="panel-body panel-branco collapse in">

									<input id="nom_tela" name="nom_tela" type="hidden"
										value="Gerar > Relatorio Taxa de Administração" /> <input
										id="idLocadorHidden" name="idLocadorHidden" type="hidden" />
									<input id="nomeLocadorHidden" name="nomeLocadorHidden"
										type="hidden" />

									<textarea id="lista" name="lista" class="hidden"></textarea>

									<div class="col-lg-12">
										<div class="row">

											<div class="col-xs-12 col-lg-4">
												<div class="form-group">
													<label>Mês de Referência</label>
													<jsp:useBean id="now" class="java.util.Date" />
													<div class="input-group date" data-provide="datepicker"
													data-date-format="mm/yyyy" data-date-start-view="months"
													data-date-min-view-mode="months" data-date-autoclose="true">
														<input id="mes_referencia" name="mes_referencia"
															type="text" class="form-control format_data_mes_ref">
														<div class="input-group-addon">
															<span class="glyphicon glyphicon-th"></span>
														</div>
													</div>
												</div>
											</div>

											<div class="col-xs-12 col-lg-2">
												<div class="form-group">
													<label class=""></label>
													<button type="button" id="btnBuscar"
														class="btn btn-primary btn-sm btn-block">Buscar</button>
												</div>
											</div>

											<div class="row" id="mensagem">
												<div class="col-xs-12">
													<div class="alert alert-danger" role="alert">
														<strong>Aviso!</strong> Não existe relatório para ser exibido.
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>
				</form>

			</div>
		</div>
	</div>

</body>
</html>
