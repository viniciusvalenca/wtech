<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ABSI - Cliente</title>

<jsp:include page="includes/links.jsp" />

<script type="text/javascript">
	$(document).ready(function(){
		var $lb = $("#lbValor");
		$("#modalidade").change(function(){
			var txt = $("#modalidade option:selected" ).text();
			$lb.html('Valor do ' + txt);
		});
		
		$("#valor").val(0).trigger('mask.maskMoney');
	});
</script>

</head>
<body>

	<jsp:include page="includes/menu.jsp" />

	<div
		class="col-lg-10 col-lg-offset-2 col-md-9 col-md-offset-3 col-sm-9 col-sm-offset-3 main">

		<div class="container-fluid main">

			<ol class="breadcrumb">
				<li><i class="fa fa-home"></i> <a href="dashboard">Dashboard</a></li>
				<li><i class="fa fa-home"></i> <a href="#">Novo Imóvel</a></li>
			</ol>

			<div class="conteudo">
				<form id="form-imovel" class="form-horizontal"
					action="cadastrarImovel" method="POST">
					<div class="row">
						<div class="col-lg-12">
							
							<input id="nom_tela" name="nom_tela" type="hidden" value="Cadastrar > Imóvel"/>

							<jsp:include page="includes/imovel/associar.jsp" />

							<jsp:include page="includes/endereco.jsp" />

							<jsp:include page="includes/imovel/infos_extras.jsp" />

							<jsp:include page="includes/imovel/documentacao.jsp" />

							<div class="panel">
								<div class="row">
									<div class="col-sm-6 col-lg-6">
										<button type="reset" id="btnResetImovel" class="btn btn-default btn-block">Limpar
											Formulário</button>
									</div>
									<div class="col-sm-6 col-lg-6">
										<button type="submit" class="btn btn-primary btn-block">Cadastrar
											Imóvel</button>
									</div>
								</div>
							</div>

						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

</body>
</html>
