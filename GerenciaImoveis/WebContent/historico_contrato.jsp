<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ABSI - Histórico de Contrato</title>

<jsp:include page="includes/links.jsp" />
<script src="js/locacao.js" type="text/javascript"></script>


</head>
<body>

	<jsp:include page="includes/menu.jsp" />

	<div
		class="col-lg-10 col-lg-offset-2 col-md-9 col-md-offset-3 col-sm-9 col-sm-offset-3 main">

		<div class="container-fluid main">

			<ol class="breadcrumb">
				<li><i class="fa fa-home"></i> <a href="dashboard">Dashboard</a></li>
				<li><i class="fa fa-list-ul"></i> <a href="#"> Histórico de
						Contrato</a></li>
			</ol>

			<div class="conteudo">
				<form id="form-historico" class="form-horizontal" accept-charset="utf-8">
					<div class="row">
						<div class="col-lg-12">

							<div class="panel panel-default">
								<a data-target="#item-info">
									<div class="panel-heading">
										Histórico </div>
								</a>
								<div id="item-info" class="panel-body panel-branco collapse in">

									<div class="col-lg-12">
										<div class="row">

											<table id="tableHistorico" name="tableHistorico" 
												data-toggle="table"
												data-classes="table table-condensed"
												data-url="/GerenciaImoveis/historicoContrato" 
												data-search="true"
												data-show-refresh="true" 
												data-click-to-select="true"
												data-single-select="true"
												data-content-type="application/json" 
												data-data-type="json"
												data-sort-order="desc"
					       						data-show-columns="true" 
												data-sort-name="stargazers_count"
												data-pagination="true" 
												data-page-size="15"
												data-pagination-first-text="Primeiro"
												data-pagination-pre-text="<i class='glyphicon glyphicon glyphicon-chevron-left'></i>"
												data-pagination-next-text="<i class='glyphicon glyphicon glyphicon-chevron-right'></i>"
												data-pagination-last-text="Último" 
												data-locale="pt-BR">
												<thead>
													<tr>
														<th data-field="id" data-halign="center" data-align="center" data-sortable="true">ID</th>
														<th data-field="imovel.endereco.enderecoCompleto" data-halign="center" data-align="center" data-sortable="true">Imóvel</th>
														<th data-field="imovel.locador.nome_completo" data-halign="center" data-align="center">Locador</th>
														<th data-field="locatario.nome_completo" data-halign="center" data-align="center" data-sortable="true">Locatário</th>
														<th data-field="contrato.dt_inicioFormat" data-halign="center" data-align="center" data-sortable="true" data-width="80">Data Início</th>
														<th data-field="contrato.dt_terminoFormat" data-halign="center" data-align="center" data-sortable="true" data-width="80">Data Término</th>
													</tr>
												</thead>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

</body>
</html>
