<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ABSI - Relatório Controle Imposto de Renda</title>

<jsp:include page="includes/links.jsp" />

<script>
function priceFormatter(data) {
	var f = data.toLocaleString('pt-br',{style: 'currency', currency: 'BRL'});
	  return f;
  }
  
function dateFormatter(date) {
	  return moment(date,"MMM DD, YYYY").format('DD/MM/YYYY')
	}


$(document).ready(function() {
	$("#blocoEditar").hide();
	$('#tableIR').on('editable-save.bs.table', function(e, field, row, oldValue, $el){
		
		//console.log(field)
		//console.log((parseFloat(row.resultado_aluguel.substring(3).replace(".", "").replace(",", ".")) - parseFloat(row.valor_aluguel)).toFixed(2))
		if(field==="resultado_aluguel"){
		row.ajuste_aluguel=(parseFloat(row.resultado_aluguel.substring(3).replace(".", "").replace(",", ".")) - parseFloat(row.valor_aluguel)).toFixed(2)
		console.log("resultado_aluguel")
		//row.valor_txadm=0;
		}
		if(field==="resultado_txadm"){
		row.ajuste_txadm=(parseFloat(row.resultado_txadm.substring(3).replace(".", "").replace(",", ".")) - parseFloat(row.valor_txadm)).toFixed(2)
		console.log("resultado_txadm")
		//row.valor_aluguel=0;
		}
		
		
	  console.log(row);	    
	    $.ajax({
	    	url: 'ajustarIR',
            type: 'POST',
            data: row,
            success: function(retorno) {
            	if(retorno == "true"){
            		mostrarAlerta("Sucesso","Edição realizada com sucesso");
            		$("#blocoEditar").hide();
            		//searchLocacoes();
            		//id_locacao = "";
            		//limparForm($("#form-itemLocacao"));
            		
            		
            		
            	}else{
            		mostrarAlerta("Error","Associação não foi realizada");
            	}
            }            
        });
	})
	
	
	window.actionEvents = {
			'click .edit' : function(e, value, row, index) {
				console.log(JSON.stringify(row.irs));
				
				$("#blocoEditar").show();
				
				$("#tableIR").bootstrapTable('refresh');

				
				$('#tableIR').bootstrapTable({ data: row.irs });
				$('#tableIR').bootstrapTable('load', row.irs);
				
				
				
			}
		};
	
	
	
	$('#mes_referencia').datepicker({
		format: "yyyy",
	    startView: 2,
	    minViewMode: 1,
	    language : "pt-BR"
	});
	
	$('.date').datepicker({
		language : "pt-BR"
	});
	
	$table = $('#tableLocacao');
	$btn = $("#btnBuscar");
	$("#groupBtns").hide();
	
	
	
	
	
	$btn.click(function(){
		if($("#mes_referencia").val() != ""){
			searchLocacoesPorMes();
		}else{
			mostrarAlerta("Aviso","Selecione um mês de referência!");
		}
	});
	
	
	
	
	
	
	
	var mensagem = '<%=request.getParameter("msg")%>';
	var ids = '<%=request.getParameter("ids")%>';
	
	if(mensagem != ""){
		if(mensagem == "true" && ids == "null"){
			mostrarAlerta("Sucesso", "Email enviado com sucesso.");
			
		}else if(mensagem == "true" && ids != ""){
			mostrarAlerta("Aviso", "Não foi possível enviar os email's para os seguintes locadores: " + ids);
			
		}else if(mensagem == "false"){
			mostrarAlerta("Error", "Não foi possível enviar o email.");
		}
	}
});



function setAction(action){
	$("#form-relatorio-imposto-renda").attr("action", action);
}

function limparCheckboxs(){
	$table.bootstrapTable('uncheckAll');
	$('.bs-checkbox input[name="btSelectGroup"]').prop('checked', false);
	
	listaID = [];
	
	hiddenButtons();
}

function hiddenButtons(){
	var obj = $table.bootstrapTable('getSelections');
	if(obj.length == 0){
		$("#groupBtns").hide();
	}
}

function searchLocacoesPorMes() {
	$.ajax({
		type : 'GET',
		url : 'listarLocacaoIR',
		data : { mes_ref : $("#mes_referencia").val() },
		contentType : 'application/json; charset=utf-8',
		dataType : 'json',
		success : function(retorno) {
			////////////////////////////////////
			//console.log(JSON.stringify(retorno))
			$('#tableLocacao').bootstrapTable('load', retorno);
		}
	});
}


</script>
</head>
<body>

	<jsp:include page="includes/menu.jsp" />

	<div
		class="col-lg-10 col-lg-offset-2 col-md-9 col-md-offset-3 col-sm-9 col-sm-offset-3 main">

		<div class="container-fluid main">

			<ol class="breadcrumb">
				<li><i class="fa fa-home"></i> <a href="dashboard">Dashboard</a></li>
				<li><i class="fa fa-file"></i> <a href="#">Relatório Controle Imposto de Renda</a></li>
			</ol>

			<div class="conteudo">
				
					<div class="row">
						<div class="col-lg-12">

							<div class="panel panel-default">
								<a data-target="#item-gerar-conta">
									<div class="panel-heading">Relatório Controle Imposto de Renda</div>
								</a>
								<div class="panel-body panel-branco collapse in">

									

									<textarea id="lista" name="lista" class="hidden"></textarea>
									<textarea id="listaId" name="listaId" class="hidden"></textarea>
									
									<div class="col-lg-12">
										<div class="col-xs-12 col-lg-4">
											<div class="form-group">
												<label>Ano de Referência</label>
												<div class="input-group date" data-provide="datepicker"
													data-date-format="yyyy" data-date-start-view="years"
													data-date-min-view-mode="years" data-date-autoclose="true">
													<input id="mes_referencia" name="mes_referencia"
														type="text" class="form-control format_data_ano">
													<div class="input-group-addon">
														<span class="glyphicon glyphicon-th"></span>
													</div>
												</div>
											</div>
										</div>

										<div class="col-xs-12 col-lg-2">
											<div class="form-group">
												<label class=""></label>
												<button type="button" id="btnBuscar"
													class="btn btn-primary btn-sm btn-block">Buscar</button>
											</div>
										</div>
									</div>


									
									<div id="item-table"
										class="panel-body panel-branco collapse in">

										<table id="tableLocacao" 
											data-toggle="table"
											data-toolbar=".toolbar"
											data-classes="table table-condensed"
											data-single-select="true"
											data-toggle="table" 
											data-search="true" 
					       					data-show-columns="true"
											data-group-by="true"
											data-group-by-field="imovel.locador.nome_completo"
											data-show-refresh="true" 
											data-content-type="application/json"
											data-data-type="json" 
											data-sort-order="desc"
											data-sort-name="stargazers_count" 
											data-pagination="true" 
											data-page-size="10"
											data-page-list="[5, 10, 20, 50, 100, 200]"
											data-pagination-first-text="Primeiro"
											data-pagination-pre-text="<i class='glyphicon glyphicon glyphicon-chevron-left'></i>"
											data-pagination-next-text="<i class='glyphicon glyphicon glyphicon-chevron-right'></i>"
											data-pagination-last-text="Último" 
											data-locale="pt-BR">
											<thead>
												<tr>
													
													<th data-field="id" data-halign="center"
														data-align="center">ID</th>
													<th data-field="imovel.endereco.enderecoCompleto"
														data-halign="center" data-align="center">Imóvel</th>
													<th data-field="locatario.nome_completo"
														data-halign="center" data-align="center">Locatário</th>
													<th data-field="imovel.locador.nome_completo"
														data-halign="center" data-align="center">Locador</th>
													<th data-halign="center" data-align="center"
														data-width="80" data-formatter="actionFormatter"
														data-events="actionEvents">Editar</th>
												</tr>
											</thead>
										</table>
									</div>

									

								</div>
							</div>
<div id="blocoEditar">
							<div class="panel panel-default">
								<a data-target="#item-editar">
									<div class="panel-heading">Editar Imposto de Renda</div>
								</a>
								<div id="item-editar"
									class="panel-body panel-branco collapse in">
									
<form id="form-editar-IR" class="form-horizontal" action="ajustarIR">
										<input id="nom_tela" name="nom_tela" type="hidden"
											value="Relatorios > Controle Imposto de Renda" />

									
									
</form>
										<div class="col-lg-12">
										<div class="row">

											<table id="tableIR" name="tableIR" 
												data-toggle="table"
												data-classes="table table-condensed"
												data-show-refresh="false" 
												data-content-type="application/json" 
												data-data-type="json"
					       						data-show-columns="true"
												data-sort-order="desc" 
												data-sort-name="stargazers_count"
												
												data-page-size="12"
												
												data-locale="pt-BR">
												<thead>
													<tr>
													<th data-field="fk_id_locacao" data-align="center" data-sortable="true" data-visible="false">type</th>
													<th data-field="fk_id_locador" data-align="center" data-sortable="true" data-visible="false">type</th>
													<th data-field="mes_ref" data-align="center" data-sortable="true" data-visible="false" data-formatter="dateFormatter">type</th>
													<th data-field="mes_refFormat" data-align="center" data-sortable="true" data-visible="true" >type</th>
													<th data-field="id" data-align="center" data-sortable="true" data-visible="false">type</th>
														<th data-field="mes" data-halign="center"
															data-align="center" data-width="300" data-sortable="true">Meses</th>
														<th data-field="resultado_aluguel" data-halign="center" data-width="300"
															data-align="center" data-sortable="true" data-editable="true" data-formatter="priceFormatter">Aluguel</th>
														<th data-field="resultado_txadm" data-halign="center" data-editable="true" data-width="300" data-align="center" data-sortable="true" data-formatter="priceFormatter">Taxa de administração</th>
														
													</tr>
												</thead>
											</table>
										</div>
									</div>

										

										
									
								</div>
							</div>
						</div>
						</div>
					</div>

			</div>
		</div>
	</div>

</body>
</html>
