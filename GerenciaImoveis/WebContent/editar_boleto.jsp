<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ABSI - Editar Boleto</title>

<jsp:include page="includes/links.jsp" />

<script>
$(document).ready(function(){
	
	$(".infoboleto").hide();

	$btn = $("#btnSalvar");
	$btn2 = $("#btnBuscar");
	$table = $('#tableBoletos');
	
	$btn.addClass("disabled");
	
	$btn.click(function(){
		var obj = $(this).bootstrapTable('getSelections');
		if(obj.length > 0){
			AtualizarBoleto();
		}else{
			mostrarAlerta("Aviso","Selecione um boleto para atualizar!");
		}
	});
	
	$table.on('check.bs.table', function (row, $element) {
		var obj = $(this).bootstrapTable('getSelections');
		console.log(obj);
		
		var boleto = obj[0];
		
		var valorTMP = (boleto.valor_juros.toFixed(2)).toString();
		valorTMP = valorTMP.replace(",","*").replace(".",",").replace("*",".");
		
		var $data = (boleto.dt_vencimentoFormat).split("/");
		var dia = $data[0];
		var mes = $data[1];
		var ano = $data[2];
		var dataMoment = moment().set({'year': ano, 'month': mes-1, 'date': dia}).format('YYYY-MM-DD');
		
		if(obj.length > 0){
			$btn.removeClass("disabled");
			
			$("#valor_multa_juros").val(valorTMP).trigger('mask.maskMoney');;
			$("#dt_vencimento").val(boleto.dt_vencimentoFormat);
			
			$(".infoboleto").show();
		}
	});
	
	$table.on('uncheck.bs.table', function (row, $element) {
		var obj = $(this).bootstrapTable('getSelections');
		
		if(obj.length <= 0){
			$btn.addClass("disabled");
			$(".infoboleto").hide();
			$("#valor_multa_juros").val("");
			$("#dt_vencimento").val("");
			$("#idboleto").val("");
		}
	});
	
	$('#mes_referencia').datepicker({
		format: "mm/yyyy",
	    startView: 1,
	    minViewMode: 1,
		language : "pt-BR"
	});
	
	$('.date').datepicker({
		language : "pt-BR"
	 });
	
	$btn2.click(function(){
		$table.bootstrapTable('removeAll');
		listarBoletosNovosPorMes();
	});
});

function AtualizarBoleto(){
	var obj = $table.bootstrapTable('getSelections');
	console.log(obj[0].id);
	$("#idboleto").val(obj[0].id);
	$("#form-editar-boleto").submit();
}

function actionEnviadoFormatter(value, row, index) {
	if(value){
		return "Sim";
	}else{
		return "Não";
	}
}

function listarBoletosNovosPorMes() {
	$.ajax({
		type : 'GET',
		url : 'listarBoletosNovosPorMes',
		data : {mes_ref : $("#mes_referencia").val()},
		contentType : 'application/json; charset=utf-8',
		dataType : 'json',
		success : function(retorno) {
			$("#tableBoletos").bootstrapTable('load',retorno);
		}
	});
}

</script>

</head>
<body>

	<jsp:include page="includes/menu.jsp" />

	<div
		class="col-lg-10 col-lg-offset-2 col-md-9 col-md-offset-3 col-sm-9 col-sm-offset-3 main">

		<div class="container-fluid main">

			<ol class="breadcrumb">
				<li><i class="fa fa-home"></i> <a href="dashboard">Dashboard</a></li>
				<li><i class="fa fa-copy"></i> <a href="#">Editar Boleto</a></li>
			</ol>

			<div class="conteudo">
				<form id="form-editar-boleto" class="form-horizontal" action="atualizarBoleto">
					<div class="row">
						<div class="col-lg-12">

							<input id="nom_tela" name="nom_tela" type="hidden"
								value="Financeiro > Editar Boleto" />
								
								<input id="idboleto" name="idboleto" type="hidden" value="" />

							<div class="panel panel-default">
								<a data-target="#item-info">
									<div class="panel-heading">Atualizando Boletos</div>
								</a>
								<div id="item-info" class="panel-body panel-branco collapse in">

									<div class="col-lg-12">
										<div class="row">
										
											<div class="col-xs-12 col-lg-4">
												<div class="form-group">
													<label>Mês de Referência</label>
													<div class="input-group date" 
														data-provide="datepicker"
														data-date-format="mm/yyyy" 
														data-date-start-view="months"
														data-date-min-view-mode="months" 
														data-date-autoclose="true">
														<input id="mes_referencia" name="mes_referencia" 
															type="text" class="form-control format_data_mes_ref">
														<div class="input-group-addon">
															<span class="glyphicon glyphicon-th"></span>
														</div>
													</div>
												</div>
											</div>
											
											<div class="col-xs-12 col-lg-2">
												<div class="form-group">
													<label class=""></label>
													<button type="button" id="btnBuscar"
														class="btn btn-primary btn-sm btn-block">Buscar</button>
												</div>
											</div>

											<table id="tableBoletos" 
												data-toggle="table"
												data-classes="table table-condensed"
												data-click-to-select="true" 
												data-single-select="true"
												data-search="true"
												data-show-refresh="true"
												data-show-columns="true"
												data-content-type="application/json" 
												data-data-type="json"
												data-sort-order="desc" 
												data-sort-name="id"
												data-pagination="true"
												data-pagination-first-text="Primeiro"
												data-pagination-pre-text="<i class='glyphicon glyphicon glyphicon-chevron-left'></i>"
												data-pagination-next-text="<i class='glyphicon glyphicon glyphicon-chevron-right'></i>"
												data-pagination-last-text="Último" data-locale="pt-BR">
												<thead>
													<tr>
														<th id="ckAll" data-field="state" data-checkbox="true"
															data-align="center" data-width="40"></th>
														<th data-field="id" data-halign="center"
															data-align="center" data-width="50" data-sortable="true">Boleto</th>
														<th data-field="locacao.imovel.locador.nome_completo"
															data-halign="center" data-align="center" data-sortable="true">Locador</th>
														<th data-field="locacao.locatario.nome_completo"
															data-halign="center" data-align="center" data-sortable="true">Locatário</th>
														<th data-field="valorString" data-halign="center"
															data-align="center" data-width="100" data-sortable="true">Valor (R$)</th>
														<th data-field="dt_documentoFormat" data-halign="center"
															data-align="center" data-width="100" data-sortable="true">Data do
															Documento</th>
														<th data-field="valor_jurosString" data-halign="center"
															data-align="center" data-width="50" data-sortable="true">Juros</th>
														<th data-field="dt_vencimentoFormat" data-halign="center"
															data-align="center" data-width="100" data-sortable="true">Vencimento</th>
														<th data-field="dt_pagamentoFormat" data-halign="center"
															data-align="center" data-width="100" data-sortable="true">Pagamento</th>
													</tr>
												</thead>
											</table>
										</div>
									</div>
									
									<div class="col-lg-12">
										<div class="row">
										
											<div class="infoboleto" style="margin-top: 20px;">
												<div class="col-lg-4">
													<div class="form-group">
														<label id="lbValor">Valor Multa/Juros</label>
														<div class="input-group">
															<label class="sr-only"></label>
															<div class="input-group">
																<div class="input-group-addon">
																	R<i class="fa fa-usd" aria-hidden="true"></i>
																</div>
																<input id="valor_multa_juros" name="valor_multa_juros" type="text" data-thousands="."
																	data-decimal="," class="valor form-control text-left">
															</div>
															</span>
														</div>
													</div>
												</div>
	
												<div class="col-xs-4">
													<div class="form-group">
														<label>Data de Vencimento</label>
														<div class="input-group date" data-provide="datepicker" data-date-format="dd/mm/yyyy">
															<input id="dt_vencimento" name="dt_vencimento" type="text" class="format_data form-control" required>
															<div class="input-group-addon">
																<span class="glyphicon glyphicon-th"></span>
															</div>
														</div>
													</div>
												</div>
											</div>

										</div>
									</div>

									<div class="col-lg-6">
										<div class="row">

											<div class="col-xs-12">
												<div class="form-group">
													<label class=""></label>
													<button type="button" id="btnSalvar"
														class="btn btn-primary btn-sm btn-block">Atualizar Boleto</button>
												</div>
											</div>

										</div>
									</div>
									
									<div class="col-lg-6">
										<div class="row">
											<div class="col-xs-12">
												<div class="form-group">
													<label class=""></label>
													<button type="button" onClick="location.href='gerar_boleto.jsp'"
														class="btn btn-success btn-sm btn-block">Visualizar Boletos</button>
												</div>
											</div>

										</div>
									</div>

								</div>
							</div>

						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

</body>
</html>
