var title = '';
var _message = '';
var alertClass = '';
var icon = '';

function mostrarAlerta(type, message){
		
		switch(type) {
	    case "Error":
	    	title = 'Mensagem de Error';
	    	alertClass = 'alertaError';
	    	_message = message;
	    	icon = 'fa-times';
	        break;
	        
	    case "Aviso":
	    	title = 'Mensagem de Aviso';
	    	alertClass = 'alertaAviso';
	    	_message = message;
	    	icon = 'fa-exclamation-triangle';
	        break;
	        
	    case "Sucesso":
	    	title = 'Mensagem de Sucesso';
	    	alertClass = 'alertaSucesso';
	    	_message = message;
	    	icon = 'fa-check';
	        break;
		}
		
		$.blockUI({
	    	message:  '<div class='+ alertClass +'><div class="col-lg-3 iconAlert"><i class="fa '+ icon +' fa-3x"></i></div><div class="col-lg-9"><h4>'+ title +'</h4><p>'+ _message +'</p></div></div>', 
	        fadeIn: 700, 
	        fadeOut: 700, 
	        timeout: 4000, 
	        showOverlay: false, 
	        centerY: false, 
	        css: { 
	            width: '320px', 
	            top: '20px', 
	            left: '', 
	            right: '10px',
	            border: 'none', 
	            padding: '5px', 
	            backgroundColor: '#000', 
	            '-webkit-border-radius': '10px', 
	            '-moz-border-radius': '10px', 
	            opacity: .8, 
	            color: '#fff' 
	        } 
	    }); 
	}