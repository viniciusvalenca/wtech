function changeModalidade(val){
	if(val == "Deposito"){
		$("#bloco_deposito").show();
		$("#bloco_fiador").hide();
		$("#bloco_seguro").hide();
		
	}else if(val == "Fiador"){
		$("#bloco_fiador").show();
		$("#bloco_deposito").hide();
		$("#bloco_seguro").hide();
		
	}else if(val == "Seguro Fianca"){
		$("#bloco_fiador").hide();
		$("#bloco_deposito").hide();
		$("#bloco_seguro").show();
		
	}else{
		$("#bloco_fiador").hide();
		$("#bloco_deposito").hide();
		$("#bloco_seguro").hide();
	}
}