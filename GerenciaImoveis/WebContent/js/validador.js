$(document).ready(function(){
	
	function troca( pattern, replacement, subject ){
		return subject.replace( pattern, replacement );
	}
	
	var regex = /[^a-z àèìòùáéíóúâêîôûãõ]+/gi;
	var regexNumber = /[^0-9.,]/g;
	
	/*
	$("#nome").keyup(function() {
		var $this = $(this);
		$this.val(troca(regex,'', $this.val()));
	});
	
	$("#sobrenome").keyup(function() {
		var $this = $(this);
		$this.val(troca(regex,'', $this.val()));
	});
	
	$("#nacionalidade").keyup(function() {
		var $this = $(this);
		$this.val(troca(regex,'', $this.val()));
	});
	
	$("#naturalidade").keyup(function() {
		var $this = $(this);
		$this.val(troca(regex,'', $this.val()));
	});
	
	$("#bairro").keyup(function() {
		var $this = $(this);
		$this.val(troca(regex,'', $this.val()));
	});*/
	
	$("#cidade").keyup(function() {
		var $this = $(this);
		$this.val(troca(regex,'', $this.val()));
	});
	
	$("#indice_reajuste").keyup(function() {
		var $this = $(this);
		$this.val(troca(regexNumber,'', $this.val()));
	});
	
	$("#valor_deposito").keyup(function() {
		var $this = $(this);
		$this.val(troca(regexNumber,'', $this.val()));
	});
	
	$("#valor_seguro").keyup(function() {
		var $this = $(this);
		$this.val(troca(regexNumber,'', $this.val()));
	});
	
	$(".valor").keyup(function() {
		var $this = $(this);
		$this.val(troca(regexNumber,'', $this.val()));
	});
	
	$("#valor").keyup(function() {
		var $this = $(this);
		$this.val(troca(regexNumber,'', $this.val()));
	}); 
	
	$("#cheque").keyup(function() {
		var $this = $(this);
		$this.val(troca(regexNumber,'', $this.val()));
	}); 
	
	jQuery.extend(jQuery.validator.messages, {
	    required: "Este campo nao foi preenchido.",
	    remote: "Por favor conserte esse campo.",
	    email: "Por favor digite um email valido.",
	    url: "Por favor digite a valid URL.",
	    date: "Por favor digite uma data valida.",
	    dateBR: "Por favor digite uma data valida",
	    dateISO: "Por favor digite uma data valida (ISO).",
	    number: "Por favor digite um numero valido.",
	    digits: "Por favor digite somente digitos.",
	    creditcard: "Por favor digite um numero de cartao valido.",
	    equalTo: "Por favor digite o mesmo valor do campo anterior.",
	    accept: "Por favor digite a value with a valid extension.",
	    maxlength: jQuery.validator.format("Por favor enter no more than {0} characteres."),
	    minlength: jQuery.validator.format("Por favor digite mais de {0} characteres."),
	    rangelength: jQuery.validator.format("Por favor enter a value between {0} and {1} characters long."),
	    range: jQuery.validator.format("Por favor enter a value between {0} and {1}."),
	    max: jQuery.validator.format("Por favor enter a value less than or equal to {0}."),
	    min: jQuery.validator.format("Por favor enter a value greater than or equal to {0}."),
	    require_from_group:"Um desses campos é origatório (CPF ou CNPJ).",
	});
	
	function limparForm(form){
		var $this = form;
		$this.each (function(){
			  this.reset();
		});
	}
	
	function getAction(str){
		 var res = str.split("/");
		 return res[res.length-1];
	}
	
	jQuery.validator.addClassRules("cpfcnpj", {
	    require_from_group: [1,".cpfcnpj"]
	});
	
	$.validator.addMethod("valueNotEquals", function(value, element, arg){
		return arg != value;
	}, "Value must not equal arg.");
	
	$("#cpf").blur(function(){
		$(".cpf-error").html("");
		var _cpf = $("#cpf").val();
		if(valida_cpf(_cpf)){
			mostrarAlerta("Sucesso","CPF válido!");
		}else{
			if(_cpf != "") mostrarAlerta("Aviso","CPF inválido!");
		}
	});
	
	$("#cnpj").blur(function(){
		$(".cnpj-error").html("");
		var _cnpj = $("#cnpj").val();
		if(valida_cnpj(_cnpj)){
			mostrarAlerta("Sucesso","CNPJ válido!");
		}else{
			if(_cnpj != "") mostrarAlerta("Aviso","CNPJ inválido!");
		}
	});
	
	 
	 $('#form-cliente').validate({
		onfocusout: function(element) {
		    $(element).valid();
		},
		/*errorPlacement: function(error, element) {
	    	var el = element.attr("name");
	    	if( el == "dt_nascimento") {
	    		element.parent("div.input-group.date").after(error);
	    	}else {
	    		error.insertAfter(element);
	    	}
	    },*/
	    rules : {
	    	//nome_banco: { valueNotEquals: "0" },
	    	//dt_nascimento: { required: true},
            cpf: {
            	remote : {
						url : "validarCpf", 
						async : false,
						data : { cpf : function() {  return $("#cpf").val() },
								  id : function() {  return $("#id").val() }
						       }
					}
			},
			cnpj: {
            	remote : {
						url : "validarCnpj", 
						async : false,
						data : { cnpj : function() {  return $("#cnpj").val() },
								  id : function() {  return $("#id").val() }
						       }
					}
			},
			identidade: {
				remote : {
					url : "validarRg", 
					async : false,
					data : { identidade : function() { return $("#identidade").val() },
							 id : function() {  return $("#id").val() }
					}
				}
			}
       },
		messages:{
			//nome_banco: { valueNotEquals: "Selecione um Banco" },
			dt_nascimento: { required: "Campo Obrigatório"},
			cpf:{ remote : "CPF já cadastrado",
            	minlength: "CPF inválido",
            },
            cnpj:{ remote : "CNPJ já cadastrado",
            	minlength: "CNPJ inválido",
            },
            identidade:{ remote : "Identidade já cadastrada",
            	minlength: "Identidade inválida"
            }
       },

	    submitHandler: function(form) {
	    	$.ajax({
    	            url: form.action,
    	            type: form.method,
    	            data: $(form).serialize(),
    	            success: function(retorno) {
    	            	if(retorno == "true"){
    	            		mostrarAlerta("Sucesso","Cliente salvo com sucesso");
    	            		limparForm($("#form-cliente"));
    	            		
    	            		if(getAction(form.action) == "atualizar"){
	    	            		$("#blocoEditar").hide();
	    	            		search();
    	            		}
    	            	}else{
    	            		mostrarAlerta("Error","Cliente não foi salvo");
    	            	}
    	            }            
    	        });
	    }
	});
	
	$('#form-imovel').validate({
		errorPlacement: function(error, element) {
	    	var el = element.attr("name");
	    	if( el == "valor") {
	    		element.parent("div.input-group").after(error);
	    	}else if( el == "cep") {
	    		element.parent("div.input-group").after(error);
	    	}else {
	    		error.insertAfter(element);
	    	}
	    },
	    rules: {
			locador: { valueNotEquals: "0" },
			valor: { required: true},
			cep: { required: true }
        }, 
		messages:{
			locador:{ valueNotEquals: "Selecione um Locador!" },
			valor: { required: "Valor do aluguel é necessário."},
			cep: { required: "Cep necessário." }
		},
		submitHandler: function(form) {
			$.ajax({
	            url: form.action,
	            type: form.method,
	            data: $(form).serialize(),
	            success: function(retorno) {
	            	if(retorno == "true"){
	            		mostrarAlerta("Sucesso","Imóvel salvo com sucesso");
	            		limparForm($("#form-imovel"));
	            		
	            		$('select').val(0);
	            		$('select').trigger('change');
	            		
	            		if(getAction(form.action) == "atualizarImovel"){
	            			$("#blocoEditar").hide();
		            		searchImoveis();
	            		}
	            		
	            	}else{
	            		mostrarAlerta("Error","Imóvel não foi salvo");
	            	}
	            }            
	        });
	    }
	});
	
	$('#form-locacao').validate({
		errorPlacement: function(error, element) {
	    	var el = element.attr("name");
	    	if( el == "dt_inicio") {
	    		element.parent("div.input-group").after(error);
	    	}else if( el == "dt_termino") {
	    		element.parent("div.input-group").after(error);
	    	}else if( el == "dt_reajuste") {
	    		element.parent("div.input-group").after(error);
	    	}else {
	    		error.insertAfter(element);
	    	}
	    },
		rules: {
			locatario: { valueNotEquals: "0" },
			fiador: {  valueNotEquals: "0" }
        }, 
		messages:{
			locatario:{ valueNotEquals: "Selecione um locatário!" },
			fiador:{ valueNotEquals: "Selecione um Fiador!" }
		},
		submitHandler: function(form) {
	    	var obj = $("#tableImoveis").bootstrapTable('getSelections');
	    	$("#idImovelHidden").val(obj[0].id);
	    	
	        $.ajax({
	            url: form.action,
	            type: form.method,
	            data: $(form).serialize(),
	            success: function(retorno) {
	            	if(retorno == "true"){
	            		mostrarAlerta("Sucesso","Associação realizada com sucesso");
	            		$("#tableImoveis").bootstrapTable('refresh');
	            		limparForm($("#form-locacao"));

	            		
	            		$('select#locatario').val(0);
	            		$('select').trigger('change');
	            		
	            		$('select#fiador').val(0);
	            		$('select').trigger('change');
	            		
	            		$('select#fiador2').val(0);
	            		$('select').trigger('change');
	            		
	            		$('select#fiador3').val(0);
	            		$('select').trigger('change');
	            		
	            		$(".nav-pills li a").css("background-color","#2e363f");
	            		
	            		if(getAction(form.action) == "atualizarLocacao"){
	            			//$("#blocoEditar").hide();
	            			$("#item-editar").addClass("is-disabled");
	            			searchLocacoes();
	            		}
	            		
	            	}else{
	            		mostrarAlerta("Error","Associação não foi realizada");
	            	}
	            }            
	        });
	    }
	});
	
	$('#form-itemLocacao').validate({
		submitHandler: function(form) {
	    	var obj = $("#tableLocacao").bootstrapTable('getSelections');
	    	$("#idLocacaoHidden").val(obj[0].id);
	    	
	        $.ajax({
	            url: form.action,
	            type: form.method,
	            data: $(form).serialize(),
	            success: function(retorno) {
	            	if(retorno == "true"){
	            		mostrarAlerta("Sucesso","Edição realizada com sucesso");
	            		$("#blocoEditar").hide();
	            		searchLocacoes();
	            		id_locacao = "";
	            		limparForm($("#form-itemLocacao"));
	            		
	            		$(".nav-pills li a").css("background-color","#2e363f");
	            		
	            	}else{
	            		mostrarAlerta("Error","Associação não foi realizada");
	            	}
	            }            
	        });
	    }
	});
	
	$('#form-indice').validate({
		submitHandler: function(form) {
	        $.ajax({
	            url: form.action,
	            type: form.method,
	            data: $(form).serialize(),
	            success: function(retorno) {
	            	if(retorno == "true"){
	            		mostrarAlerta("Sucesso","Edição realizada com sucesso");
	            		
	            	}else{
	            		mostrarAlerta("Error","Indice não foi cadastrado");
	            	}
	            }            
	        });
	    }
	});
	
	$('#form-gerar-retorno-manual').validate({
		submitHandler: function(form) {
	        $.ajax({
	            url: form.action,
	            type: form.method,
	            data: $(form).serialize(),
	            success: function(retorno) {
	            	if(retorno == "true"){
	            		mostrarAlerta("Sucesso","Recebimento salvo com sucesso");
	            		$("#blocoEditar").hide();
	            		$("#tableRetorno").bootstrapTable('removeAll');
	            		
	            		limparForm($("#form-gerar-retorno-manual"));
	            		
	            	}else{
	            		mostrarAlerta("Error","Salvamento não foi realizada");
	            	}
	            }            
	        });
	    }
	});
	
	//01/05/2018
	$('#form-imposto-renda').validate({
		submitHandler: function(form) {
	
	        $.ajax({
	            url: form.action,
	            type: form.method,
	            data: $(form).serialize(),
	            success: function(retorno) {
	            	
	            	if(retorno == "true"){
	            		mostrarAlerta("Sucesso","Edição realizada com sucesso");
	            		$("#blocoEditar").hide();
	            		//searchLocacoes();
	            		
	            		limparForm($("#form-imposto-renda"));
	            		
	            	}else{
	            		mostrarAlerta("Error","Ação não realizada");
	            	}
	            },
	            error: function(retorno){
	            	mostrarAlerta("Error","Ação não realizada");
	            }            
	        });
	    }
	});
	
	$('#form-editar-boleto').validate({
		submitHandler: function(form) {
	
	        $.ajax({
	            url: form.action,
	            type: form.method,
	            data: $(form).serialize(),
	            success: function(retorno) {
	            	
	            	if(retorno == "true"){
	            		mostrarAlerta("Sucesso","Edição realizada com sucesso");
	            		limparForm($("#form-editar-boleto"));
	            		$("#tableBoletos").bootstrapTable('removeAll');
	            		
	            	}else{
	            		mostrarAlerta("Error","Ação não realizada");
	            	}
	            },
	            error: function(retorno){
	            	mostrarAlerta("Error","Ação não realizada");
	            }            
	        });
	    }
	});
});

