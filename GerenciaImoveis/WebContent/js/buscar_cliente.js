var cbk1 = false;
var cbk2 = false;
var cbk3 = false;

$(document).ready(function() {
	$("#blocoEditar").hide();
	
	search();

	$("#btnAtualizar").click(function() {
		$("#form-cliente").submit();
	});

	$('#ckb1').click(function() {
		cbk1 = $(this).is(':checked');
		search();
	});

	$('#ckb2').click(function() {
		cbk2 = $(this).is(':checked');
		search();
	});

	$('#ckb3').click(function() {
		cbk3 = $(this).is(':checked');
		search();
	});

});

window.actionEvents = {
	'click .edit' : function(e, value, row, index) {
		//alert(JSON.stringify(row));

		$("#blocoEditar").show();
		
		$("#tableCliente").bootstrapTable('refresh');

		$("#form-cliente").each(function() {
			this.reset();
		});

		resetNumeros();
		popularCampos(row);
	}
};

function resetNumeros() {
	$("#itens1").html('');
	$("#itens2").html('');
}

function mostrarDadosBancarios(flg_locador) {
	if (flg_locador == true)
		$("#blocoDadosBancarios").show();
	else
		$("#blocoDadosBancarios").hide();
}

function mostrarTelCel(telefones, celulares) {

	for (var i = 0; i < telefones.length; i++) {
		var tel = telefones[i];
		var count = i + 1;

			$("#itens1")
			.append(
					'<div class="col-lg-12">'
							+ '<div class="form-group">'
							+ '<label>'
							+ count
							+ ' - Telefone</label>'
							+ '<div class="input-group">'
							+ '<input name="num_tel" type="text" class="telefone form-control" value="'
							+ tel.ddd
							+ tel.num_telefone
							+ '">'
							+ '<span class="input-group-btn">'
							+ '<button class="btn btn-danger delete" type="button">-</button>'
							+ '</span></div></div></div>');
		
	}

	for (var i = 0; i < celulares.length; i++) {
		var cel = celulares[i];
		var count = i + 1;
		
			$("#itens2")
			.append(
					'<div class="col-lg-12">'
							+ '<div class="form-group">'
							+ '<label>'
							+ count
							+ ' - Celular</label>'
							+ '<div class="input-group">'
							+ '<input name="num_cel" type="text" class="celular form-control" value="'
							+ cel.ddd
							+ cel.num_celular
							+ '">'
							+ '<span class="input-group-btn">'
							+ '<button class="btn btn-danger delete" type="button">-</button>'
							+ '</span></div></div></div>');
		
	}
}

function popularCampos(pessoa) {
	$("#ckLocador").val(pessoa.flg_locador);
	$("#ckLocatario").val(pessoa.flg_locatario);
	$("#ckFiador").val(pessoa.flg_fiador);

	mostrarDadosBancarios(pessoa.flg_locador);

	$("#ckLocador").prop('checked', pessoa.flg_locador);
	$("#ckLocatario").prop('checked', pessoa.flg_locatario);
	$("#ckFiador").prop('checked', pessoa.flg_fiador);

	$("#id").val(pessoa.id);
	$("#nome_completo").val(pessoa.nome_completo);
	$("#email").val(pessoa.email);
	$("#email_opcional").val(pessoa.email_opcional);
	$("#dt_nascimento").val(pessoa.dt_nascimentoFormat);
	$("#identidade").val(pessoa.identidade);
	$("#cpf").val(pessoa.cpf);
	$("#cnpj").val(pessoa.cnpj);
	
	mostrarTelCel(pessoa.telefones, pessoa.celulares);
	
	$("#telefones_extras").val(pessoa.telefones_extras);

	$("#dt_emissao").val(pessoa.dt_emissaoFormat);
	$("#emissor").val(pessoa.emissor);
	$("#estado_civil").val(pessoa.estado_civil);
	$("#nacionalidade").val(pessoa.nacionalidade);
	$("#naturalidade").val(pessoa.naturalidade);
	$("#profissao").val(pessoa.profissao);

	$("#cep").val(pessoa.endereco.cep);
	$("#numero").val(pessoa.endereco.numero);
	$("#endereco").val(pessoa.endereco.endereco);
	$("#estado").val(pessoa.endereco.estado);
	$("#cidade").val(pessoa.endereco.cidade);
	$("#bairro").val(pessoa.endereco.bairro);
	$("#complemento").val(pessoa.endereco.complemento);
	$("#nom_condominio").val(pessoa.endereco.nom_condominio);

	if (pessoa.flg_locador == true && pessoa.dadosBancarios != null) {
		$("#nome_banco").val(pessoa.dadosBancarios.nome_banco);
		$("#titular").val(pessoa.dadosBancarios.titular);
		$("#agencia").val(pessoa.dadosBancarios.agencia);
		$("#conta").val(pessoa.dadosBancarios.conta);
		$("#tp_conta").val(pessoa.dadosBancarios.tp_conta);
	}
}
