$(document).ready(function() {
	$("#blocoEditar").hide();

	$("#btnAtualizar").click(function() {
		$("#form-imovel").submit();
	});
});

	window.actionEvents = {
		'click .edit' : function(e, value, row, index) {
			$("#blocoEditar").show();
			
			$("#tableImovel").bootstrapTable('refresh');

			$("#form-imovel").each(function() {
				this.reset();
			});
			//alert(JSON.stringify(row));
			popularCampos(row);
		}
	};

	function popularCampos(imovel) {
		$('select').val(imovel.id_locador);
		$('select').trigger('change');

		$("#id").val(imovel.id);
		
		if(imovel.flg_disponivel){
			$('#rbFlgSim').prop('checked', true);
		}else{
			$('#rbFlgNao').prop('checked', true);
		}
		
		$("#valor").val(imovel.valorString);
		$("#comissao").val(imovel.comissao);
		$("#qtd_quartos").val(imovel.qtd_quartos);
		$("#area").val(imovel.area);
		$("#tipo").val(imovel.tipo);
		$("#qtd_suites").val(imovel.qtd_suites);
		$("#modalidade").val(imovel.modalidade);
		
		$("#insc_iptu").val(imovel.insc_iptu);
		$("#cod_logradouro").val(imovel.cod_logradouro);
		$("#mat_cedae").val(imovel.mat_cedae);
		$("#mat_light").val(imovel.mat_light);
		$("#cbmerj").val(imovel.cbmerj);
		$("#mat_ceg").val(imovel.mat_ceg);

		$("#cep").val(imovel.endereco.cep);
		$("#numero").val(imovel.endereco.numero);
		$("#endereco").val(imovel.endereco.endereco);
		$("#estado").val(imovel.endereco.estado);
		$("#cidade").val(imovel.endereco.cidade);
		$("#bairro").val(imovel.endereco.bairro);
		$("#complemento").val(imovel.endereco.complemento);
		$("#nom_condominio").val(imovel.endereco.nom_condominio);
	}