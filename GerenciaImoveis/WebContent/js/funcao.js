$(document).ready(function () {
	$("#add1").click(function(e) {
		var $bloco = $('#itens1');
		var ctel = 1;
		
		$bloco.each(function () {
			ctel += $(this).children('.col-lg-12').size();
        });
		
		$bloco.append('<div class="col-lg-12">\n\
                 <div class="form-group">\n\
                 <label>'+ctel+' - Telefone</label>\n\
                 <div class="input-group">\n\
                 <input name="num_tel" type="text" class="telefone form-control">\n\
                 <span class="input-group-btn">\n\
                 <button class="btn btn-danger delete" type="button">-</button>\n\
                 </span></div></div></div>');
    });
	
	$("#add2").click(function(e) {
		var $bloco = $('#itens2');
		var cCel = 1;
		
		$bloco.each(function () {
			cCel += $(this).children('.col-lg-12').size();
        });
		
		$bloco.append('<div class="col-lg-12">\n\
                 <div class="form-group">\n\
                 <label>'+cCel+' - Celular</label>\n\
                 <div class="input-group">\n\
                 <input name="num_cel" type="text" class="celular form-control">\n\
                 <span class="input-group-btn">\n\
                 <button class="btn btn-danger delete" type="button">-</button>\n\
                 </span></div></div></div>');
    });

    $("body").on("click", ".delete", function(e) {
        $(this).parent().closest('.col-lg-12').remove();
    });
    
    /*-----------------------------------------------------------------------*/
    
    $("#blocoDadosBancarios").show();
	
	$('#ckFiador').change(function(){
		if($(this).prop('checked')){
			if($('#ckLocador').prop('checked'))
				$("#blocoDadosBancarios").show();
			else
				$("#blocoDadosBancarios").hide();
		}
	});
	
	$('#ckLocador').change(function(){
		if($(this).prop('checked'))
			$("#blocoDadosBancarios").show();
		else
			$("#blocoDadosBancarios").hide();
	});
	
	 /*-----------------------------------------------------------------------*/
	
	$('#ckLocador').click(function() {
		if($(this).is(':checked')){
			$(this).val(true);
		}else{
			$(this).val(false);
		}
	});

	$('#ckLocatario').click(function() {
		if($(this).is(':checked')){
			$(this).val(true);
		}else{
			$(this).val(false);
		}
	});

	$('#ckFiador').click(function() {
		if($(this).is(':checked')){
			$(this).val(true);
		}else{
			$(this).val(false);
		}
	});
	/*
	$('#ckEmpresa').click(function() {
		if($(this).is(':checked')){
			$(this).val(true);
		}else{
			$(this).val(false);
		}
	});*/
	
	/*------------------------------------------*/
	
	$("#btnResetImovel").click(function(){
		$('select').val(0);
		$('select').trigger('change');
	});
});

/*--------------------------------------------*/

function mascaraDisponivel(value) {
	if (value) {
		return "Imóvel Vazio";
	} else {
		return "Imóvel Alugado";
	}
}

function maskDisponivel(value, row, index) {
	return mascaraDisponivel(value);
}

function actionFormatter(value, row, index) {
	return '<a class="edit ml10" href="#" value="'+ value + '"><i class="fa fa-pencil" aria-hidden="true"></i></a>'
}


/*
function enderecoFormatter(value, row, index) {
	return value + ", " 
	+ row.endereco.numero + ", " 
	+ row.endereco.bairro + ", "
	+ row.endereco.cidade + " - "
	+ row.endereco.estado
}
*/



/*
calc_digitos_posicoes

Multiplica dígitos vezes posições

@param string digitos Os digitos desejados
@param string posicoes A posição que vai iniciar a regressão
@param string soma_digitos A soma das multiplicações entre posições e dígitos
@return string Os dígitos enviados concatenados com o último dígito
*/
function calc_digitos_posicoes(digitos, posicoes = 10, soma_digitos = 0) {
	digitos = digitos.toString();// Garante que o valor é uma string

	// Faz a soma dos dígitos com a posição
	// Ex. para 10 posições:
	//   0    2    5    4    6    2    8    8   4
	// x10   x9   x8   x7   x6   x5   x4   x3  x2
	//   0 + 18 + 40 + 28 + 36 + 10 + 32 + 24 + 8 = 196
	for ( var i = 0; i < digitos.length; i++  ) {
	    soma_digitos = soma_digitos + (digitos[i] * posicoes); // Preenche a soma com o dígito vezes a posição
	    posicoes--;// Subtrai 1 da posição
	
	    // Parte específica para CNPJ
	    // Ex.: 5-4-3-2-9-8-7-6-5-4-3-2
	    if ( posicoes < 2 ) posicoes = 9;// Retorno a posição para 9
	}

	// Captura o resto da divisão entre soma_digitos dividido por 11
	// Ex.: 196 % 11 = 9
	soma_digitos = soma_digitos % 11;

	// Verifica se soma_digitos é menor que 2
	if ( soma_digitos < 2 )  soma_digitos = 0; // soma_digitos agora será zero
	else {
	    // Se for maior que 2, o resultado é 11 menos soma_digitos
	    // Ex.: 11 - 9 = 2
	    // Nosso dígito procurado é 2
	    soma_digitos = 11 - soma_digitos;
	}

	// Concatena mais um dígito aos primeiro nove dígitos
	// Ex.: 025462884 + 2 = 0254628842
	var cpf = digitos + soma_digitos;
	return cpf;
}

function valida_cpf(valor) {
	valor = valor.toString();// Garante que o valor é uma string
	valor = valor.replace(/[^0-9]/g, '');// Remove caracteres inválidos do valor
	
	// Captura os 9 primeiros dígitos do CPF
	// Ex.: 02546288423 = 025462884
	var digitos = valor.substr(0, 9);

	// Faz o cálculo dos 9 primeiros dígitos do CPF para obter o primeiro dígito
	var novo_cpf = calc_digitos_posicoes(digitos);
	
	// Faz o cálculo dos 10 dígitos do CPF para obter o último dígito
	var novo_cpf = calc_digitos_posicoes(novo_cpf, 11);

	// Verifica se o novo CPF gerado é idêntico ao CPF enviado
	if ( novo_cpf === valor ) return true;// CPF válido
	else return false;// CPF inválido
}

function valida_cnpj(valor) {
	valor = valor.toString();// Garante que o valor é uma string
	valor = valor.replace(/[^0-9]/g, '');// Remove caracteres inválidos do valor
	var cnpj_original = valor;// O valor original

	// Captura os primeiros 12 números do CNPJ
	var primeiros_numeros_cnpj = valor.substr(0, 12);
	
	// Faz o primeiro cálculo
	var primeiro_calculo = calc_digitos_posicoes(primeiros_numeros_cnpj, 5);

	// O segundo cálculo é a mesma coisa do primeiro, porém, começa na posição 6
	var segundo_calculo = calc_digitos_posicoes(primeiro_calculo, 6);
	
	// Concatena o segundo dígito ao CNPJ
	var cnpj = segundo_calculo;

	// Verifica se o CNPJ gerado é idêntico ao enviado
	if (cnpj == cnpj_original) {
	    return true;
	}

	return false;// Retorna falso por padrão
} 
