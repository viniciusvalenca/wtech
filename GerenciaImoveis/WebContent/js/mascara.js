	$(document).ready(function() {
		$('.cep').mask('00000-000');
		$('.telefone').mask('(00) 0000-0000');
		$('.celular').mask('(00) 00000-0000');
		$('.cpf').mask('000.000.000-00');
		$('.cnpj').mask('00.000.000/0000-00');
		//$('.identidade').mask('AA.AAA.AAA-A'); 
		$('.format_data').mask('00/00/0000');
		$('.format_data_mes_ref').mask('00/0000');
		$('.format_data_ano').mask('0000');
		//$('.agencia').mask('AAAA-A');
        //$('.conta').mask('00.00A-A');
        $('.mes_ref').mask('00/0000');
        $('.dia_vencimento').mask('00');
        //$('.indice_reajuste').mask('000');
      //$('.insc_iptu').mask('0.000.000-0');
        //$('.cod_logradouro').mask('00.000-0');
        //$('.mat_cedae').mask('0.000.000-0');
        //$('.mat_light').mask('00000000');
        //$('.mat_ceg').mask('0000000-0');
        //$('.cbmerj').mask('0000000-0');


/*$('.cbmerj').mask("0000000-0")
	    	.focusout(function (event) {  
	            var target, phone, element;  
	            target = (event.currentTarget) ? event.currentTarget : event.srcElement;  
	            phone = target.value.replace(/\D/g, '');
	            element = $(target);  
	            element.unmask();  
	            if(phone.length > 10) {  
	                element.mask("000000-0");  
	            } else {  
	                element.mask("0000000-0");  
	            }  
	    	});*/
        
        $("#valor").maskMoney();
        $(".valor").maskMoney({ thousands:'.', decimal:',', precision: 2,allowZero : true});
        $(".valor_negativo").maskMoney({ thousands:'.', decimal:',', precision: 2,allowZero : true,allowNegative:true});
        $("#indice_reajuste").maskMoney({ precision: 3 , thousands:'.', decimal:',',allowZero : true});
        $("#valor_deposito").maskMoney();
        $("#valor_seguro").maskMoney();
        $("#valor_pag_to_locador").maskMoney();
        
	});