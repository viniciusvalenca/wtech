$(document).ready(function(){
	$("#bloco_deposito").hide();
	$("#bloco_fiador").hide();
	$("#bloco_seguro").hide();
	
	$("#modalidade_loc").change(function(){
		var val = $(this).val();
		changeModalidade(val);
	});
	
	$("#fiador").change(function(){
		if($(this).val() != 0 && $(this).val() == $("#locatario").val()){
			mostrarAlerta("Error","O fiador selecionado é o mesmo que o locatário");
			$(this).val(0);
		}
		
		if($(this).val() != 0 && $(this).val() == $("#fiador2").val()){
			mostrarAlerta("Error","O fiador já foi selecionado");
			$(this).val(0);
		}
		if($(this).val() != 0 && $(this).val() == $("#fiador3").val()){
			mostrarAlerta("Error","O fiador já foi selecionado");
			$(this).val(0);
		}
		
		var selected = $(this).find('option:selected');
	    var extra = selected.data('end');
	    $("#endereco_imovel").val(extra);
	});
	
	$("#fiador2").change(function(){
		if($(this).val() != 0 && $(this).val() == $("#locatario").val()){
			mostrarAlerta("Error","O fiador selecionado é o mesmo que o locatário");
			$(this).val(0);
		}
		
		if($(this).val() != 0 && $(this).val() == $("#fiador").val()){
			mostrarAlerta("Error","O fiador já foi selecionado");
			$(this).val(0);
		}
		if($(this).val() != 0 && $(this).val() == $("#fiador3").val()){
			mostrarAlerta("Error","O fiador já foi selecionado");
			$(this).val(0);
		}
		
		var selected = $(this).find('option:selected');
	    var extra = selected.data('end');
	    $("#endereco_imovel2").val(extra);
	});
	
	$("#fiador3").change(function(){
		if($(this).val() != 0 && $(this).val() == $("#locatario").val()){
			mostrarAlerta("Error","O fiador selecionado é o mesmo que o locatário");
			$(this).val(0);
		}
		
		if($(this).val() != 0 && $(this).val() == $("#fiador2").val()){
			mostrarAlerta("Error","O fiador já foi selecionado");
			$(this).val(0);
		}
		if($(this).val() != 0 && $(this).val() == $("#fiador").val()){
			mostrarAlerta("Error","O fiador já foi selecionado");
			$(this).val(0);
		}
		
		var selected = $(this).find('option:selected');
	    var extra = selected.data('end');
	    $("#endereco_imovel3").val(extra);
	});
	
	$table = $('#tableImoveis');
	
	$("#locatario").change(function(){
		var obj = $table.bootstrapTable('getSelections');
		
		if($(this).val() != 0 && $(this).val() == $("#fiador").val()){
			mostrarAlerta("Error","O Locatário selecionado é o mesmo que o fiador");
			$(this).val(0);
		}
		
		if($(this).val() != 0 && $(this).val() == $("#fiador2").val()){
			mostrarAlerta("Error","O Locatário selecionado é o mesmo que o fiador");
			$(this).val(0);
		}
		
		if($(this).val() != 0 && $(this).val() == $("#fiador3").val()){
			mostrarAlerta("Error","O Locatário selecionado é o mesmo que o fiador");
			$(this).val(0);
		}
		
		if(obj.length > 0){
			if($(this).val() != 0 && $(this).val() == obj[0].locador.id){
				mostrarAlerta("Error","O Locatário selecionado é o mesmo que o Locador do Imóvel");
				$(this).val(0);
			}
		}
	});
	
	$table.on('check.bs.table', function (row, $element) {
		var obj = $(this).bootstrapTable('getSelections');
		
		if(obj[0].locador.id == $("#locatario").val()){
			mostrarAlerta("Error","O Locador do Imóvel selecionado é o mesmo que o Locatário");
			$("#locatario").val(0);
		}
	});
	
	$("#btnResetLocacao").click(function(){
		$('select#locatario').val(0);
		$('select').trigger('change');
		
		$('select#fiador').val(0);
		$('select').trigger('change');
	
		$('select#fiador2').val(0);
		$('select').trigger('change');
		
		$('select#fiador3').val(0);
		$('select').trigger('change');
		
		changeModalidade(0);
	});
	
});

function changeModalidade(val){
	if(val == "Deposito"){
		$("#bloco_deposito").show();
		$("#bloco_fiador").hide();
		$("#bloco_seguro").hide();
		
	}else if(val == "Fiador"){
		$("#bloco_fiador").show();
		$("#bloco_deposito").hide();
		$("#bloco_seguro").hide();
		
	}else if(val == "Seguro Fianca"){
		$("#bloco_fiador").hide();
		$("#bloco_deposito").hide();
		$("#bloco_seguro").show();
		
	}else{
		$("#bloco_fiador").hide();
		$("#bloco_deposito").hide();
		$("#bloco_seguro").hide();
	}
}

