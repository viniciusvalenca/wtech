function search() {
	$.ajax({
		type : 'GET',
		url : 'listarClientes',
		data : {
			cbk1 : cbk1,
			cbk2 : cbk2,
			cbk3 : cbk3
		},
		contentType : 'application/json; charset=utf-8',
		dataType : 'json',
		success : function(retornoJson) {
			$('#tableClientes').bootstrapTable('load', retornoJson);
		}
	});
}

function searchImoveis() {
	$.ajax({
		type : 'GET',
		url : 'listarImovel',
		contentType : 'application/json; charset=utf-8',
		dataType : 'json',
		success : function(retornoJson) {
			$('#tableImoveis').bootstrapTable('load', retornoJson);
		}
	});
}

function searchImoveisDisponiveis() {
	$.ajax({
		type : 'GET',
		url : 'listarImoveisDisponiveis',
		contentType : 'application/json; charset=utf-8',
		dataType : 'json',
		success : function(retornoJson) {
			$('#tableImoveis').bootstrapTable('append', retornoJson);
		}
	});
}

function searchImoveisId(row) {
	$.ajax({
		type : 'GET',
		url : 'listarImovelId',
		data: {id: row.imovel.id},
		contentType : 'application/json; charset=utf-8',
		dataType : 'json',
		success : function(data) {
			$table = $('#tableImoveis');
			$table.bootstrapTable('removeAll');
			
			rows = [];
			rows.push({
                "state": true,
                "endereco.enderecoCompleto": data.endereco.enderecoCompleto,
                "locador.nome_completo": data.locador.nome_completo,
                "valorString": data.valorString
            });
			
			$table.bootstrapTable('append', rows);
			
			searchImoveisDisponiveis();
		}
	});
}

function searchLocacoes() {
		$.ajax({
			type : 'GET',
			url : 'listarLocacao',
			contentType : 'application/json; charset=utf-8',
			dataType : 'json',
			success : function(retornoJson) {
				$('#tableLocacao').bootstrapTable('load', retornoJson);
			}
		});
}





