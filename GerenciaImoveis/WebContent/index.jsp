<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ABSI - Login</title>

<jsp:include page="includes/links.jsp" />
<link href="css/login.css" rel="stylesheet">

</head>
<body>
	<div class="col-xs-12">
		<div class="container">
			<div class="login-container">
				<div>
					<!-- <img src="img/pok.png" alt="" class='img-circle img-responsive' /> -->
				</div>
				<div class="Absi">
					<h3>Absi Negócios Imobiliários</h3>
				</div>
				<div class="form-box">
					<form action="acesso" method="post">
						<input name="login" type="text" placeholder="Login" required> 
						<input name="senha" type="password" placeholder="Senha" required>
						<button class="btn btn-default btn-block login" type="submit">ENTRAR</button>
						<!--${msg}-->
					</form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
