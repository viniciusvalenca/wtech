<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
 
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ABSI - Receber Conta (Automático)</title>

<jsp:include page="includes/links.jsp" />

<script type="text/javascript">
var rows = [];

$(document).ready(function () {
	$("#input-44").fileinput({
		uploadUrl: '/file-upload-batch/1',
		maxFilePreviewSize : 25000,
		maxFileCount: 1,
	    overwriteInitial: false,
	    uploadAsync: false,
		showUpload: false,
		allowedFileExtensions : ["ret"],
		layoutTemplates: {
            main1: "{preview}\n" +
            "<div class=\'input-group {class}\'>\n" +
            "   <div class=\'input-group-btn\'>\n" +
            "       {browse}\n" +
            "       {upload}\n" +
            "       {remove}\n" +
            "   </div>\n" +
            "   {caption}\n" +
            "</div>"
        }
	});

	$("#btnSalvar").click(function(){
		$('#tableRetorno').bootstrapTable('removeAll');
		
		if($("#input-44").val() == ''){
			mostrarAlerta("Aviso","Selecione um arquivo");
			
		}else if($(".file-caption-name").attr("title") == "Validation Error"){
			mostrarAlerta("Error","Mais de um arquivo selecionado");
			
		}else{
			$("#form-gerar-retorno-auto").submit();
		}
	});
	
	$("#form-gerar-retorno-auto").submit(function (event) {
		event.preventDefault();
		var formData = new FormData($(this)[0]);

		$.ajax({
	    	url: 'retornoAjax',
	        type: 'POST',
	        data: formData,
	        //async: false,
	        cache: false,
	        contentType: false,
	        processData: false,
	        success: function (resposta) {
	        	if(resposta == "false"){
	        		mostrarAlerta("Aviso","Não foi encontrado nenhum dos pagementos do retorno infomado");
	        	}else{
	        		popularTabela(resposta);
	        	}
	        }
	    });

	    return false;
	});
});	

function actionMoneyFormatter(value, row, index) {
	return 'R$ '+ value + ''
}

function popularTabela(listaID){
	$.ajax({
		type : 'GET',
		url : 'listarBoletosRetorno',
		data: { lista : listaID},
		contentType : 'application/json; charset=utf-8',
		dataType : 'json',
		success : function(data) {
			if(data == false){
				mostrarAlerta("Error","Não foi possível listar as informações do retorno");
			}else{
				$table = $('#tableRetorno');
		  		//$table.bootstrapTable('removeAll');
		  		$table.bootstrapTable('load', data);
			}
		}
	});
}
</script>

</head>
<body>

	<jsp:include page="includes/menu.jsp" />

	<div
		class="col-lg-10 col-lg-offset-2 col-md-9 col-md-offset-3 col-sm-9 col-sm-offset-3 main">

		<div class="container-fluid main">

			<ol class="breadcrumb">
				<li><i class="fa fa-home"></i> <a href="dashboard">Dashboard</a></li>
				<li><i class="fa fa-file"></i> <a href="#">Receber Conta
						(Automático)</a></li>
			</ol>

			<div class="conteudo">
				<form id="form-gerar-retorno-auto" class="form-horizontal"
					action="retorno" method="POST" enctype="multipart/form-data">
					<div class="row">
						<div class="col-lg-12">

							<input id="nom_tela" name="nom_tela" type="hidden"
								value="Financeiro > Receber Conta (Automático)" />

							<div class="panel panel-default">
								<a data-target="#item-gerar-conta">
									<div class="panel-heading">
										Receber Conta (Automático) </div>
								</a>
								<div id="item-gerar-conta"
									class="panel-body panel-branco collapse in">

									<div class="col-lg-12">
										<div class="row">
											<div class="col-xs-12 col-lg-2">
												<div class="form-group">
													<label>Data Atual</label>
													<jsp:useBean id="now" class="java.util.Date" />
													<fmt:formatDate var="year" value="${now}"
														pattern="dd/MM/yyyy" />
													<input class="form-control" name="dt_dia" id="dt_dia"
														type="text" value="${year}" readonly="readonly">
												</div>
											</div>

											<div class="col-lg-10">
												<div class="form-group">
													<label>Selecione o arquivo RET</label> <input
														id="input-44" name="input-44[]" class="file-loading"
														type="file" data-show-preview="false" multiple>
													<div id="errorBlock" class="help-block"></div>
												</div>
											</div>

											<div class="panel">
												<div class="row">
													<div class="col-xs-12 col-lg-4">
														<button type="button" id="btnSalvar"
															class="btn btn-primary btn-block">Salvar
															Informações</button>
													</div>
												</div>
											</div>

										</div>
									</div>
								</div>
							</div>

						</div>
					</div>
				</form>

				<div class="panel panel-default">
					<a data-target="#item-gerar-conta-auto-infos">
						<div class="panel-heading"> Informações </div>
					</a>
					<div id="item-gerar-conta-auto-infos"
						class="panel-body panel-branco collapse in">

						<div class="col-lg-12">
							<div class="row">

								<div class="col-lg-12">
									<div class="row">

										<table id="tableRetorno" data-toggle="table"
											data-classes="table table-condensed"
											data-content-type="application/json" 
											data-data-type="json"
											data-sort-order="desc" 
											data-sort-name="stargazers_count"
											data-pagination="true" 
											data-show-columns="true"
											data-page-size="5"
											data-page-list="[5, 10, 20, 50, 100, 200]"
											data-pagination-first-text="Primeiro"
											data-pagination-pre-text="<i class='glyphicon glyphicon glyphicon-chevron-left'></i>"
											data-pagination-next-text="<i class='glyphicon glyphicon glyphicon-chevron-right'></i>"
											data-pagination-last-text="Último" data-locale="pt-BR">
											<thead>
												<tr>
													<th data-field="locacao.imovel.endereco.enderecoCompleto" data-halign="center" data-align="center" data-sortable="true">Imóvel</th>
													<th data-field="locacao.imovel.locador.nome_completo" data-halign="center" data-align="center" data-sortable="true">Locador</th>
													<th data-field="locacao.locatario.nome_completo" data-halign="center" data-align="center" data-sortable="true">Locatário</th>
													<th data-field="seu_numero" data-halign="center" data-align="center" data-sortable="true" data-width="60">NN</th>
													<th data-field="valorString" data-halign="center" data-align="center" data-width="60" data-formatter="actionMoneyFormatter" data-sortable="true">Valor Devido</th>
													<th data-field="valor_pagoString" data-halign="center" data-align="center" data-width="60" data-sortable="true">Valor Recebido</th>
													<th data-field="valor_diferencaString" data-halign="center" data-align="center" data-width="60" data-sortable="true">Diferença</th>
													<th data-field="dt_vencimentoFormat" data-halign="center" data-align="center" data-width="90" data-sortable="true">Vencimento</th>
													<th data-field="dt_pagamentoFormat" data-halign="center" data-align="center" data-width="90" data-sortable="true">Pagamento</th>
													<!-- <th data-field="status" data-halign="center" data-align="center" data-width="120">Status</th> -->
												</tr>
											</thead>
										</table>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</body>
</html>
