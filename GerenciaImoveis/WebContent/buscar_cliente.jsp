<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ABSI - Buscar Cliente</title>

<jsp:include page="includes/links.jsp" />
<script src="js/buscar_cliente.js" type="text/javascript"></script>
<script>
	$(document).ready(function() {
		$('.date').datepicker({
			language : "pt-BR"
		});
	});
</script>
</head>
<body>

	<jsp:include page="includes/menu.jsp" />

	<div
		class="col-lg-10 col-lg-offset-2 col-md-9 col-md-offset-3 col-sm-9 col-sm-offset-3 main">

		<div class="container-fluid main">

			<ol class="breadcrumb">
				<li><i class="fa fa-home"></i> <a href="dashboard">Dashboard</a></li>
				<li><i class="fa fa-search"></i> <a href="#"> Consultar
						Cliente</a></li>
			</ol>

			<div class="conteudo">
				<div class="row">
					<div class="col-lg-12">

						<div class="panel panel-default">
							<a data-target="#item-table">
								<div class="panel-heading">Consultar</div>
							</a>
							<div id="item-table" class="panel-body panel-branco collapse in">
								<div class="col-lg-12">
									<div class="row">
									
										<div class="col-sm-6 col-md-5 col-lg-6">
											<div class="form-group">
												<label>Tipo de Cliente:</label>
												<div id="checkboxes" class="inner-group">
													<label class="checkbox-inline"> <input
														type="checkbox" name="tpCliente" id="ckb1" value="0">
														Locador
													</label> <label class="checkbox-inline"> <input
														type="checkbox" name="tpCliente" id="ckb2" value="1">
														Locatário
													</label> <label class="checkbox-inline"> <input
														type="checkbox" name="tpCliente" id="ckb3" value="2">
														Fiador
													</label>
												</div>
											</div>
										</div>

										<table id="tableClientes" data-toggle="table"
											data-classes="table table-condensed"
											data-sort-name="stargazers_count" data-sort-order="desc"
											data-pagination="true" data-search="true"
											data-show-columns="true" data-page-size="5"
											data-page-list="[5, 10, 20, 50, 100, 200]"
											data-pagination-first-text="Primeiro"
											data-pagination-pre-text="<i class='glyphicon glyphicon glyphicon-chevron-left'></i>"
											data-pagination-next-text="<i class='glyphicon glyphicon glyphicon-chevron-right'></i>"
											data-pagination-last-text="Último" data-locale="pt-BR">
											<thead>
												<tr>
													<th data-field="id" data-halign="center"
														data-align="center" data-width="80" data-sortable="true">ID</th>
													<th data-field="nome_completo" data-halign="center"
														data-align="center" data-sortable="true">Nome</th>
													<th data-field="email" data-halign="center"
														data-align="center" data-sortable="true">Email</th>
													<th data-field="cpf" data-halign="center"
														data-align="center" data-width="200" data-sortable="true">CPF</th>
													<th data-field="identidade" data-halign="center"
														data-align="center" data-width="200" data-sortable="true">Identidade</th>
													<th data-halign="center" data-align="center"
														data-width="80" data-formatter="actionFormatter"
														data-events="actionEvents">Editar</th>
												</tr>
											</thead>
										</table>
									</div>
								</div>
							</div>
						</div>
						<div id="blocoEditar">
							<div class="panel panel-default">
								<a data-target="#item-editar">
									<div class="panel-heading">Editar Cliente</div>
								</a>
								<div id="item-editar"
									class="panel-body panel-branco collapse in">
									<form id="form-cliente" class="form-horizontal" method="POST"
										action="atualizar">

										<input id="nom_tela" name="nom_tela" type="hidden"
											value="Editar > Cliente" />

										<jsp:include page="includes/cliente/infos.jsp" />

										<jsp:include page="includes/cliente/numeros.jsp" />

										<jsp:include page="includes/endereco.jsp" />

										<div id="blocoDadosBancarios">
											<jsp:include page="includes/cliente/dadosbancarios.jsp" />
										</div>

										<div class="panel">
											<div class="row">
												<div class="col-lg-12">
													<button type="button" id="btnAtualizar"
														class="btn btn-primary btn-block">Atualizar
														Cliente</button>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>