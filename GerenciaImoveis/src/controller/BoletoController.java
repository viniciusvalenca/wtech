package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.mail.MessagingException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.mail.EmailException;
import org.jrimum.bopepo.Boleto;
import org.jrimum.bopepo.view.BoletoViewer;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import dao.TransactionController;
import dominio.bean.entity.BoletoAbsi;
import dominio.bean.entity.ImpostoRenda;
import dominio.bean.entity.Locacao;
import dominio.bean.entity.Usuario;
import dominio.bean.list.ListaBoletoAbsi;
import dominio.factory.BoletoAbsiFactory;
import dominio.factory.BoletoFactory;
import dominio.factory.ImpostoRendaFactory;
import utilitarios.ConverterData;
import utilitarios.DownloadFile;
import utilitarios.Email;
import utilitarios.util;

@WebServlet({ "/listarBoletosNovosPorMes", "/download", "/enviarBoleto", "/getBoleto", "/listarBoletosNaoPagos",
		"/receberContaManual", "/listarBoletosRetorno", "/listarLocacaoPorMes2", "/boletoManual", "/recuperarPagamentoMes", "/atualizarBoleto"})
public class BoletoController extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private HttpSession session;
	private Usuario u;

	public BoletoController() {
		super();
	}

	protected void setHistorico(HttpServletRequest request) throws IOException {
		session = ((HttpServletRequest) request).getSession(true);
		u = (Usuario) session.getAttribute("user");
	}

	protected void returnToPage(HttpServletRequest request, HttpServletResponse response, Boolean resposta)
			throws IOException {
		PrintWriter out = response.getWriter();
		Gson gson = new Gson();

		out.print(gson.toJson(resposta));
		out.flush();
		out.close();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		option(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		option(request, response);

	}

	protected void option(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			String url = request.getServletPath();

			if (url.equalsIgnoreCase("/listarBoletosNovosPorMes")) {
				listarBoletosNovosPorMes(request, response);

			} else if (url.equalsIgnoreCase("/download")) {
				download(request, response);

			} else if (url.equalsIgnoreCase("/enviarBoleto")) {
				enviarBoleto(request, response);

			} else if (url.equalsIgnoreCase("/getBoleto")) {
				getBoleto(request, response);

			} else if (url.equalsIgnoreCase("/listarBoletosNaoPagos")) {
				listarBoletosNaoPagos(request, response);

			} else if (url.equalsIgnoreCase("/receberContaManual")) {
				receberContaManual(request, response);

			} else if (url.equalsIgnoreCase("/listarBoletosRetorno")) {
				listarBoletosRetorno(request, response);

			} else if (url.equalsIgnoreCase("/listarLocacaoPorMes2")) {
				listarLocacaoPorMes(request, response);

			} else if (url.equalsIgnoreCase("/boletoManual")) {
				boletoManual(request, response);

			} else if(url.equalsIgnoreCase("/recuperarPagamentoMes")){
				recuperarPagamentoMes(request, response);
				
			} else if(url.equalsIgnoreCase("/atualizarBoleto")){
				atualizarBoleto(request, response);
				
			}
			else {
				response.sendRedirect("/");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void atualizarBoleto(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, ParseException {

		request.setCharacterEncoding("UTF-8");

		try {

			String idBoleto = request.getParameter("idboleto");
			
			String valor_multa_jurosForm = request.getParameter("valor_multa_juros");
			String dt_vencimentoForm = request.getParameter("dt_vencimento");

			BoletoAbsi boleto = BoletoAbsiFactory.recuperarPorID(Integer.valueOf(idBoleto));
			
			boleto.getLocacao().setListaItensLocacao(boleto.getMes_ref());
			
			BigDecimal valor = boleto.getLocacao().getValorBoleto(); 
			Date dt_documento = ConverterData.getDateNow();
			//Date dt_vencimento = ConverterData.incrementDiaVencimento(dt_documento, l.getContrato().getDia_vencimento(), util.getMes(mes_ref));
			
			boleto.getLocacao().getListaItensLocacao().setProcessado();
			boleto.setValor_comissao(boleto.getLocacao().getImovel().getValorTxAdm());
			System.out.println("teste");
			System.out.println(boleto.getLocacao().getImovel().getValor());
			boleto.setvalor_aluguel(boleto.getLocacao().getImovel().getValor());
			boleto.setLucro_txbancaria(boleto.getLocacao().getListaItensLocacao().valorTxBancaria());
			boleto.setValor(valor);
			boleto.setDt_documento(dt_documento);
			
			if(valor_multa_jurosForm != ""){
				BigDecimal valorForm = new BigDecimal(util.stringToBigDecimal(valor_multa_jurosForm));
				BigDecimal juros = boleto.getValor_juros();
				BigDecimal diferenca = juros.subtract(valorForm);
				
				boleto.setValor_juros(valorForm);
				
				BigDecimal soma = boleto.getLucro_txbancaria().add(diferenca);
				boleto.setLucro_txbancaria(soma);
				
				String comissao = boleto.getLocacao().getImovel().getComissao();
				BigDecimal jurosAtualizado = boleto.getValor_juros();
				
				BigDecimal cem = new BigDecimal(".01");
				BigDecimal porcentagem = cem.multiply(new BigDecimal(comissao));
				BigDecimal txAdm = jurosAtualizado.multiply(porcentagem);
				
				BigDecimal soma2 = boleto.getValor_comissao().add(txAdm);
				boleto.setValor_comissao(soma2);
			}
			
			if(dt_vencimentoForm != ""){
				java.sql.Date dtForm = ConverterData.stringToDateSqlBD(util.converterData(dt_vencimentoForm));
				boleto.setDt_vencimento(dtForm);
			}
			
			TransactionController tc = new TransactionController();
			tc.add(boleto);				
			tc.salvar();
			
			if (boleto.isUpdated()) {
				setHistorico(request);
				u.atualizarAcesso(request.getParameter("nom_tela"), "Atualizar Boleto: " + boleto.getId());
				
				returnToPage(request, response, true);
				
			}else{
				returnToPage(request, response, false);
			}
			
		} catch (Exception ex) {
			returnToPage(request, response, false);
			ex.printStackTrace();
		}
	}
	
	
	
	

	private void recuperarPagamentoMes(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, ParseException {

		ListaBoletoAbsi lista;

		response.setContentType("text/plain; charset=UTF-8");
		PrintWriter out = response.getWriter();
		Gson gson = new Gson();
		
		String mes_ref = request.getParameter("mes_ref");
		java.sql.Date dt = ConverterData.createDateFromMesRef(mes_ref);
		
		try {
			lista = BoletoAbsiFactory.recuperarPagamentoMes(dt);
			
			out.print(gson.toJson(lista));
			out.flush();
			out.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	

	private void receberContaManual(HttpServletRequest request, HttpServletResponse response) throws NumberFormatException, SQLException, ParseException, IOException {
		request.setCharacterEncoding("UTF-8");

		BoletoAbsi bol;

		bol = BoletoAbsiFactory.recuperarPorID(Integer.valueOf(request.getParameter("idBoletoHidden")));
		ImpostoRenda ir = ImpostoRendaFactory.recuperarPorLocadorSemLocacao(bol.getLocacao().getImovel().getLocador().getId(), bol.getMes_ref());	
			
		bol.setValor_pago(new BigDecimal(util.stringToBigDecimal(request.getParameter("total"))));
		bol.setArq_retorno("Manual" + ConverterData.formatBancoRetorno(ConverterData.getDateNow()));
		BigDecimal _juros = new BigDecimal(util.stringToBigDecimal(request.getParameter("juros")));
		BigDecimal _multa = new BigDecimal(util.stringToBigDecimal(request.getParameter("multa")));
		bol.setValor_juros(_juros.add(_multa));
		bol.setDt_pagamento(ConverterData.stringToDateSqlBD(util.converterData(request.getParameter("dt_pagamento"))));
		
		if(!bol.getValor_juros().equals(
				new BigDecimal(0)
			)){
			
			String comissao = bol.getLocacao().getImovel().getComissao();
			BigDecimal juros = bol.getValor_juros();
			
			BigDecimal cem = new BigDecimal(".01");
			BigDecimal porcentagem = cem.multiply(new BigDecimal(comissao));
			BigDecimal txAdm = juros.multiply(porcentagem);
			
			
			BigDecimal soma = bol.getValor_comissao().add(txAdm);
			bol.setValor_comissao(soma);
		}

		TransactionController tc = new TransactionController();
		
		if(ir != null){
			ir.setFk_id_locacao(bol.getLocacao().getId());
			tc.add(ir);
		}
		
		tc.add(bol);
		tc.salvar();

		if (bol.isUpdated()) {
			setHistorico(request);
			u.atualizarAcesso(request.getParameter("nom_tela"), "ID: " + String.valueOf(bol.getId()));
		}

		returnToPage(request, response, bol.isUpdated());
	}
	
	private void listarLocacaoPorMes(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, ParseException {

		ListaBoletoAbsi lista;

		response.setContentType("text/plain; charset=UTF-8");
		PrintWriter out = response.getWriter();
		Gson gson = new Gson();
		
		String mes_ref = request.getParameter("mes_ref");
		java.sql.Date dt = ConverterData.createDateFromMesRef(mes_ref);
		
		try {
			lista = BoletoAbsiFactory.recuperarLocacaoRemessa(dt);
			
			out.print(gson.toJson(lista));
			out.flush();
			out.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void listarBoletosRetorno(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, ParseException, NumberFormatException {

		ListaBoletoAbsi lista = new ListaBoletoAbsi();

		response.setContentType("text/plain; charset=UTF-8");
		PrintWriter out = response.getWriter();
		Gson gson = new Gson();

		String temp = request.getParameter("lista");
		temp = temp.replace("[", "");
		temp = temp.replace("]", "");
		String[] vetor = temp.split(",");

		try {
			
			for (int i = 0; i < vetor.length; i++) {
				if(!vetor[i].equalsIgnoreCase("false")){
					BoletoAbsi bol = BoletoAbsiFactory.recuperarPorID(Integer.valueOf(vetor[i]));
					lista.add(bol);
				}
			}
			
			if(!lista.isEmpty()){
				out.print(gson.toJson(lista));
				out.flush();
				out.close();
				
			}else{
				returnToPage(request, response, false);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			returnToPage(request, response, false);
		}
	}

	private void listarBoletosNovosPorMes(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, ParseException {

		ListaBoletoAbsi lista;

		response.setContentType("text/plain; charset=UTF-8");
		PrintWriter out = response.getWriter();
		Gson gson = new Gson();

		try {
			
			String mes_ref = request.getParameter("mes_ref");
			java.sql.Date dt = ConverterData.createDateFromMesRef(mes_ref);
			
			lista = BoletoAbsiFactory.listarBoletosNovosPorMes(dt);
			//lista = BoletoAbsiFactory.recuperarBoletosNovos();

			out.print(gson.toJson(lista));
			out.flush();
			out.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void getBoleto(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		BoletoAbsi bol;

		response.setContentType("text/plain; charset=UTF-8");
		PrintWriter out = response.getWriter();
		Gson gson = new Gson();

		Integer id = Integer.valueOf(request.getParameter("idLocacao"));

		try {
			bol = BoletoAbsiFactory.recuperarPorIdLocacao(id);

			out.print(gson.toJson(bol));
			out.flush();
			out.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void enviarBoleto(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, ParseException, MessagingException, EmailException {

		response.setContentType("text/plain; charset=UTF-8");
		ServletContext context = getServletContext();
		String reportLocation = context.getRealPath("");

		try {
			Integer id = Integer.valueOf(request.getParameter("id"));

			BoletoAbsi bol = BoletoAbsiFactory.recuperarPorID(id);
			bol.getLocacao().setListaItensLocacao(bol.getMes_ref());

			BoletoFactory boletoFactory = new BoletoFactory();
			Boleto boleto = boletoFactory.createDefaultBoleto(bol);

			byte[] baos2 = new BoletoViewer(boleto,reportLocation + "B001_BoletoEstiloFatura.pdf").getPdfAsByteArray();

			Email.emailBoleto(baos2, bol.getLocacao().getLocatario().getEmail(),
					bol.getLocacao().getLocatario().getNome_completo(),
					bol.getLocacao().getLocatario().getEmail_opcional());

			bol.setFlg_enviado(true);

			TransactionController tc = new TransactionController();
			tc.add(bol);
			tc.salvar();

			returnToPage(request, response, bol.isUpdated());

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void download(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		ServletContext context = getServletContext();
		String reportLocation = context.getRealPath("");

		try {

			String ids = request.getParameter("idboleto");

			Type listType = new TypeToken<ArrayList<Integer>>() {
			}.getType();
			List<Integer> listaID = new Gson().fromJson(ids, listType);

			System.out.println("Lista ID: " + listaID);

			BoletoFactory boletoFactory = new BoletoFactory();

			List<Boleto> boletos = new ArrayList<Boleto>();

			String nameFile = "";

			for (Integer id : listaID) {
				BoletoAbsi bol = BoletoAbsiFactory.recuperarPorID(Integer.valueOf(id));
				bol.getLocacao().setListaItensLocacao(bol.getMes_ref());

				Boleto boleto = boletoFactory.createDefaultBoleto(bol);
				

				boletos.add(boleto);

				if (listaID.size() == 1) {
					nameFile = "boleto " + id + " " + bol.getNumero_documento() + ".pdf";
				}
			}

			if (listaID.size() > 1) {
				nameFile = "boletos " + ConverterData.getDateNow() + ".pdf";
			}

			byte[] b = BoletoViewer.groupInOnePdfWithTemplate(boletos, reportLocation + "B001_BoletoEstiloFatura.pdf");

			DownloadFile.downloadPDF(response, b, nameFile);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void listarBoletosNaoPagos(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, ParseException {

		ListaBoletoAbsi lb;

		response.setContentType("text/plain; charset=UTF-8");
		PrintWriter out = response.getWriter();
		Gson gson = new Gson();

		String mes_ref = request.getParameter("mes_ref");
		java.sql.Date dt = ConverterData.createDateFromMesRef(mes_ref);

		try {
			lb = BoletoAbsiFactory.recuperarBoletosNaoPagos(dt);

			out.print(gson.toJson(lb));
			out.flush();
			out.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private void boletoManual(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, ParseException {

		response.setContentType("text/plain; charset=UTF-8");
		PrintWriter out = response.getWriter();

		try {

			String jsonString = request.getParameter("locacao");
			String mes_ref = request.getParameter("mes_ref");
			
			Gson gson = new Gson();
			
			Integer seuNumeroTemp = BoletoAbsiFactory.proximoId();
			
			Locacao l = gson.fromJson(jsonString, Locacao.class);
			
			String numero_documento = "L" + l.getId() + " -I" + l.getImovel().getId() + " -L" + l.getLocatario().getId();
			BigDecimal valor = l.getValorBoleto(); 
			Date dt_documento = ConverterData.getDateNow();
			Date dt_vencimento = ConverterData.incrementDiaVencimento(
						util.getAno(mes_ref)
						, l.getContrato().getDia_vencimento()
						, util.getMes(mes_ref));
				
			String seu_numero = String.valueOf(seuNumeroTemp);
			
			l.getListaItensLocacao().setProcessado();
				
			BoletoAbsi bol = BoletoAbsiFactory.criar(numero_documento, valor, ConverterData.stringToDateSqlBD(util.converterMesRef(mes_ref)), 
						dt_documento, dt_vencimento, seu_numero, false, l);
				
			bol.setValor_comissao(l.getImovel().getValorTxAdm());
			bol.setvalor_aluguel(l.getImovel().getValor());
			bol.setLucro_txbancaria(l.getListaItensLocacao().get(1).getValor());
				
			bol.setArq_remessa("manual" + ConverterData.getDateStringRemessa());
			bol.setDt_vencimentoFormat();
			bol.getValorStringFormat();
			bol.getvalor_aluguelStringFormat();
	
			TransactionController tc = new TransactionController();
			tc.add(bol);				
			tc.salvar();
			
			if (bol.isPersistente()) {
				Gson gson2 = new Gson();

				out.print(gson2.toJson(bol));
				out.flush();
				out.close();
			}
			
		} catch (Exception ex) {
			returnToPage(request, response, false);
			ex.printStackTrace();
		}
	}

}
