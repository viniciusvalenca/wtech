package controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.IOUtils;

import com.google.gson.Gson;

import boletos.ArquivoRetorno;
import boletos.TransacaoTitulo;
import dao.TransactionController;
import dominio.bean.entity.BoletoAbsi;
import dominio.bean.entity.ImpostoRenda;
import dominio.bean.entity.Usuario;
import dominio.bean.list.ListaBoletoAbsi;
import dominio.bean.list.ListaImpostoRenda;
import dominio.factory.BoletoAbsiFactory;
import dominio.factory.ImpostoRendaFactory;
import utilitarios.ConverterData;
import utilitarios.util;

@WebServlet({ "/retornoAjax" })
public class RetornoController extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private HttpSession session;
	private Usuario u;

	public RetornoController() {
		super();
	}

	protected void setHistorico(HttpServletRequest request) throws IOException {
		session = ((HttpServletRequest) request).getSession(true);
		u = (Usuario) session.getAttribute("user");
	}

	protected void returnToPage(HttpServletRequest request, HttpServletResponse response, Boolean resposta)
			throws IOException {
		PrintWriter out = response.getWriter();
		Gson gson = new Gson();

		out.print(gson.toJson(resposta));
		out.flush();
		out.close();
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			String url = request.getServletPath();

			if (url.equalsIgnoreCase("/retornoAjax")) {
				retornoAjax(request, response);

			} else {
				response.sendRedirect("/");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void retornoAjax(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, SQLException {
		request.setCharacterEncoding("UTF-8");

		ListaBoletoAbsi lista = new ListaBoletoAbsi();
		ListaImpostoRenda listaIR = new ListaImpostoRenda();
		ArrayList<Integer> lista2 = new ArrayList<Integer>();
		String namePage = "";

		// Boolean isMultipart = ServletFileUpload.isMultipartContent(request);

		String appPath = request.getServletContext().getRealPath("");
		String savePath = appPath + File.separator + "temp";

		File fileSaveDir = new File(savePath);
		if (!fileSaveDir.exists()) {
			fileSaveDir.mkdirs();
		}

		DiskFileItemFactory itemFactory = new DiskFileItemFactory();
		itemFactory.setRepository(new File(savePath));
		ServletFileUpload upload = new ServletFileUpload(itemFactory);

		try {
			List<?> fileItems = upload.parseRequest(request);
			Iterator<?> i = fileItems.iterator();

			while (i.hasNext()) {
				FileItem fi = (FileItem) i.next();

				if (fi.isFormField()) {

					if(fi.getFieldName().equals("nom_tela")){
						namePage = new String ((fi.getString()).getBytes ("iso-8859-1"), "UTF-8");
					}
					
				} else {
					fi.getInputStream();

					File tempFile = File.createTempFile("retorno", ".RET");
					tempFile.deleteOnExit();

					FileOutputStream out = new FileOutputStream(tempFile);
					IOUtils.copy(fi.getInputStream(), out);

					ArquivoRetorno obb = new ArquivoRetorno(tempFile);
					String nomeArquivo = "retorno" + ConverterData.formatBancoRetorno(obb.getCabecalho().getDataGravacao()) + ".RET";
					
					for (TransacaoTitulo tt : obb.getTransacoes()) {
						BoletoAbsi bol = BoletoAbsiFactory.recuperarPorSeuNumero(tt.getNossoNumero());
						
						System.out.println("-------------------------- Código Ocorrencia");
						System.out.println(tt.getCodigoOcorrencia());
						System.out.println("-------------------------- Data Ocorrencia");
						System.out.println(tt.getDataOcorrencia());
						System.out.println("-------------------------- Data Crédito");
						System.out.println(tt.getDataCredito()); 
						
						if(bol != null && tt.getCodigoOcorrencia() == 6){
							bol.setArq_retorno(nomeArquivo);
							bol.setValor_despesa_cobranca(tt.getValorDespesaCobranca());
							bol.setValor_juros(tt.getValorJuros());
							bol.setValor_pago(tt.getValorPago());
							bol.setLucro_txbancaria(
									bol.getLucro_txbancaria().subtract(tt.getValorDespesaCobranca())
							);
							
							if(!bol.getValor_juros().equals(
									new BigDecimal(0)
								)){
								
								String comissao = bol.getLocacao().getImovel().getComissao();
								BigDecimal juros = bol.getValor_juros();
								
								BigDecimal cem = new BigDecimal(".01");
								BigDecimal porcentagem = cem.multiply(new BigDecimal(comissao));
								BigDecimal txAdm = juros.multiply(porcentagem);
								
								
								BigDecimal soma = bol.getValor_comissao().add(txAdm);
								bol.setValor_comissao(soma);
							}

							if (!tt.getDataCredito().contains(" ")) {
								bol.setDt_pagamento(ConverterData.stringToDateSqlBD(util.formatData(tt.getDataCredito())));
							}

							System.out.println("----------------------------------------------");
							System.out.println("--------------------BOLETO--------------------");
							System.out.println("----------------------------------------------");
							System.out.println(bol);
							
							ImpostoRenda ir = ImpostoRendaFactory.recuperarPorLocadorSemLocacao(
									bol.getLocacao().getImovel().getLocador().getId(), 
									bol.getMes_ref());	

							System.out.println("----------------------------------------------");
							System.out.println("----------------------IR----------------------");
							System.out.println("----------------------------------------------");
							System.out.println(ir);
							
							if(ir != null){
								ir.setFk_id_locacao(bol.getLocacao().getId());
								listaIR.add(ir);
							}
							
							lista2.add(bol.getId());
							lista.add(bol);
						}
					}
					
					System.out.println("lista: " + lista.isEmpty());
					
					if(!lista.isEmpty()){
						TransactionController tc = new TransactionController();
						tc.add(lista);
						tc.add(listaIR);
						tc.salvar();
						
						if (lista.isUpdated()) {
							setHistorico(request);
							u.atualizarAcesso(namePage , "Retorno: " + lista.get(0).getArq_retorno());
							
							PrintWriter outpage = response.getWriter();
							Gson gson = new Gson();

							outpage.print(gson.toJson(lista2));
							outpage.flush();
							outpage.close();
							
						}else{
							PrintWriter outpage = response.getWriter();
							Gson gson = new Gson();

							outpage.print(gson.toJson(false));
							outpage.flush();
							outpage.close();
						}
						
					}else{
						PrintWriter outpage = response.getWriter();
						Gson gson = new Gson();

						outpage.print(gson.toJson(false));
						outpage.flush();
						outpage.close();
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			returnToPage(request, response, false);
		}
	}

}
