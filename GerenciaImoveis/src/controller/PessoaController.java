package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.text.ParseException;
import java.sql.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import dao.TransactionController;
import dominio.bean.entity.Celular;
import dominio.bean.entity.DadosBancarios;
import dominio.bean.entity.Endereco;
import dominio.bean.entity.Pessoa;
import dominio.bean.entity.Telefone;
import dominio.bean.entity.Usuario;
import dominio.bean.list.ListaCelular;
import dominio.bean.list.ListaPessoa;
import dominio.bean.list.ListaTelefone;
import dominio.factory.CelularFactory;
import dominio.factory.DadosBancariosFactory;
import dominio.factory.EnderecoFactory;
import dominio.factory.PessoaFactory;
import dominio.factory.TelefoneFactory;
import utilitarios.ConverterData;
import utilitarios.ValidarCpfCnpj;
import utilitarios.util;

@WebServlet({ "/cadastrar", "/cadcliente", "/atualizar", "/validarRg", "/validarCpf", "/validarCnpj", "/listarClientes", "/listarLocadores" })
@SuppressWarnings("serial")
public class PessoaController extends HttpServlet {

	private Pessoa p;
	private Endereco e;
	private DadosBancarios d;
	private HttpSession session;
	private Usuario u;

	public PessoaController() {
		super();
	}
	
	protected void setHistorico(HttpServletRequest request) throws IOException {
		session = ((HttpServletRequest) request).getSession(true);
		u = (Usuario) session.getAttribute("user");
	}
	
	protected void returnToPage(HttpServletRequest request, HttpServletResponse response, Boolean resposta)
			throws IOException {
		PrintWriter out = response.getWriter();
		Gson gson = new Gson();

		out.print(gson.toJson(resposta));
		out.flush();
		out.close();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		option(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		option(request, response);
	}

	protected void option(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			String url = request.getServletPath();
			if (url.equalsIgnoreCase("/cadastrar")) {
				cadastrar(request, response);
			} else if (url.equalsIgnoreCase("/cadcliente")) {
				cadcliente(request, response);
			} else if (url.equalsIgnoreCase("/atualizar")) {
				atualizar(request, response);
			} else if (url.equalsIgnoreCase("/validarRg")) {
				validarRg(request, response);
			} else if (url.equalsIgnoreCase("/validarCpf")) {
				validarCpf(request, response);
			} else if (url.equalsIgnoreCase("/validarCnpj")) {
				validarCnpj(request, response);
			} else if (url.equalsIgnoreCase("/listarClientes")) {
				listarClientes(request, response);
			} else if (url.equalsIgnoreCase("/listarLocadores")) {
				listarLocadores(request, response);
			} else {
				response.sendRedirect("/");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void listarLocadores(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			ListaPessoa ps;

			response.setContentType("text/plain; charset=UTF-8");
			PrintWriter out = response.getWriter();
			Gson gson = new Gson();

			Boolean flg_locador = true;
			Boolean flg_locatario = false;
			Boolean flg_fiador = false;

			ps = PessoaFactory.recuperar(flg_locatario, flg_locador, flg_fiador);

			out.print(gson.toJson(ps));
			out.flush();
			out.close();

		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}
	
	private void listarClientes(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			ListaPessoa ps;

			response.setContentType("text/plain; charset=UTF-8");
			PrintWriter out = response.getWriter();
			Gson gson = new Gson();

			Boolean flg_locador = Boolean.valueOf(request.getParameter("cbk1"));
			Boolean flg_locatario = Boolean.valueOf(request.getParameter("cbk2"));
			Boolean flg_fiador = Boolean.valueOf(request.getParameter("cbk3"));

			if ((flg_locador == true && flg_locatario == true && flg_fiador == true)
					|| (flg_locador == false && flg_locatario == false && flg_fiador == false))
				ps = PessoaFactory.recuperarTodos();
			else
				ps = PessoaFactory.recuperar(flg_locatario, flg_locador, flg_fiador);

			out.print(gson.toJson(ps));
			out.flush();
			out.close();

		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	private void cadcliente(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8"); // this line solves the problem
		Integer id = 0;
		try {
			id = PessoaFactory.proximoId();

			request.setAttribute("id", id);

			request.getRequestDispatcher("cliente.jsp").forward(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	protected void cadastrar(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		try {

			String[] tels = request.getParameterValues("num_tel");
			String[] cels = request.getParameterValues("num_cel");

			ListaTelefone listaTels = new ListaTelefone();
			if (tels != null) {
				for (int i = 0; i < tels.length; i++) {
					if (tels[i] != "") {
						Integer ddd = util.getDDD(tels[i]);
						Integer telefone = util.getNumero(tels[i]);
						System.out.println("tels[i] = " + tels[i]);
						Telefone tel = TelefoneFactory.criar(ddd, telefone);
						listaTels.addPersistente(tel);
					}
				}
			}

			ListaCelular listaCels = new ListaCelular();
			if (cels != null) {
				for (int i = 0; i < cels.length; i++) {
					if (cels[i] != "") {
						Integer ddd = util.getDDD(cels[i]);
						Integer telefone = util.getNumero(cels[i]);
						System.out.println("cels[i] = " + cels[i]);
						Celular cel = CelularFactory.criar(ddd, telefone);
						listaCels.addPersistente(cel);
					}
				}
			}

			String nome_completo = request.getParameter("nome_completo");
			String email = request.getParameter("email").toLowerCase();
			String email_opcional = request.getParameter("email_opcional").toLowerCase();
			String identidade = request.getParameter("identidade");
			String cpf = request.getParameter("cpf");
			String cnpj = request.getParameter("cnpj");

			String emissor = request.getParameter("emissor");
			
			Date dt_emissao = null;
			Date dt_nascimento = null;
			
			if(request.getParameter("dt_emissao") != ""){
				dt_emissao = ConverterData.stringToDateSqlBD(util.converterData(request.getParameter("dt_emissao")));
			}
			if(request.getParameter("dt_nascimento") != ""){
				dt_nascimento = ConverterData.stringToDateSqlBD(util.converterData(request.getParameter("dt_nascimento")));
			}

			String estado_civil = request.getParameter("estado_civil");
			String nacionalidade = request.getParameter("nacionalidade");
			String naturalidade = request.getParameter("naturalidade");
			String profissao = request.getParameter("profissao");
			String telefones_extras = request.getParameter("telefones_extras");

			Boolean flg_locatario = false, flg_locador = false, flg_fiador = false;

			flg_locador = Boolean.valueOf(request.getParameter("ckLocador"));
			flg_locatario = Boolean.valueOf(request.getParameter("ckLocatario"));
			flg_fiador = Boolean.valueOf(request.getParameter("ckFiador"));

			String agencia = request.getParameter("agencia");
			String nome_banco = request.getParameter("nome_banco");
			String conta = request.getParameter("conta");
			String titular = request.getParameter("titular");
			String tp_conta = request.getParameter("tp_conta");

			String endereco = request.getParameter("endereco");
			String cep = request.getParameter("cep");
			String numero = request.getParameter("numero");
			String bairro = request.getParameter("bairro");
			String cidade = request.getParameter("cidade");
			String estado = request.getParameter("estado");
			String complemento = request.getParameter("complemento");
			String nom_condominio = request.getParameter("nom_condominio");

			p = PessoaFactory.criar(nome_completo, email, email_opcional, cpf, cnpj, identidade, dt_emissao, emissor,
					estado_civil, dt_nascimento, nacionalidade, naturalidade, profissao, telefones_extras, flg_locatario, flg_locador,
					flg_fiador);

			if (flg_locador == true && !nome_banco.equalsIgnoreCase("0"))
				d = DadosBancariosFactory.criar(nome_banco, agencia, conta, tp_conta, titular);
			else
				d = null;

			e = EnderecoFactory.criar(endereco, bairro, cep, numero, estado, cidade, complemento, nom_condominio);

			if (p != null) {
				p.setDadosBancarios(d);
				p.setEndereco(e);
				p.setCelulares(listaCels);
				p.setTelefones(listaTels);
				
				TransactionController tc = new TransactionController();
				tc.add(p);				
				tc.salvar();

				if (p.isPersistente()) {
					setHistorico(request);
					u.atualizarAcesso(request.getParameter("nom_tela"), "ID: " + String.valueOf(p.getId()));
				}
				
				returnToPage(request, response, p.isPersistente());

			} else {
				returnToPage(request, response, false);
			}

		} catch (SQLException ex) {
			returnToPage(request, response, false);
			ex.printStackTrace();

		} catch (ParseException ex) {
			ex.printStackTrace();
			// logger.error("Test Result : ", ex);
			returnToPage(request, response, false);
		}
	}

	protected void atualizar(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		try {

			p = PessoaFactory.recuperarPorID(Integer.valueOf(request.getParameter("id")));

			p.setNome_completo(request.getParameter("nome_completo"));
			p.setEmail(request.getParameter("email").toLowerCase());
			p.setEmail_opcional(request.getParameter("email_opcional").toLowerCase());
			p.setIdentidade(request.getParameter("identidade"));
			p.setCpf(request.getParameter("cpf"));
			p.setCnpj(request.getParameter("cnpj"));
			p.setEmissor(request.getParameter("emissor"));
			
			Date dt_emissao = null;
			Date dt_nascimento = null;
			
			if(request.getParameter("dt_emissao") != ""){
				dt_emissao = ConverterData.stringToDateSqlBD(util.converterData(request.getParameter("dt_emissao")));
			}
			if(request.getParameter("dt_nascimento") != ""){
				dt_nascimento = ConverterData.stringToDateSqlBD(util.converterData(request.getParameter("dt_nascimento")));
			}
			
			p.setDt_nascimento(dt_nascimento);
			p.setDt_emissao(dt_emissao);

			p.setEstado_civil(request.getParameter("estado_civil"));
			p.setNacionalidade(request.getParameter("nacionalidade"));
			p.setNaturalidade(request.getParameter("naturalidade"));
			p.setProfissao(request.getParameter("profissao"));
			p.setTelefones_extras(request.getParameter("telefones_extras"));

			p.setFlg_locador(Boolean.valueOf(request.getParameter("ckLocador")));
			p.setFlg_locatario(Boolean.valueOf(request.getParameter("ckLocatario")));
			p.setFlg_fiador(Boolean.valueOf(request.getParameter("ckFiador")));

			p.getEndereco().setEndereco(request.getParameter("endereco"));
			p.getEndereco().setCep(request.getParameter("cep"));
			p.getEndereco().setNumero(request.getParameter("numero"));
			p.getEndereco().setBairro(request.getParameter("bairro"));
			p.getEndereco().setCidade(request.getParameter("cidade"));
			p.getEndereco().setEstado(request.getParameter("estado"));
			p.getEndereco().setComplemento(request.getParameter("complemento"));
			p.getEndereco().setNom_condominio(request.getParameter("nom_condominio"));

			String[] tels = request.getParameterValues("num_tel");
			String[] cels = request.getParameterValues("num_cel");

			ListaTelefone listaTels = new ListaTelefone();
			if (tels != null) {

				for (int i = 0; i < tels.length; i++) {
					if (tels[i] != "") {
						Integer ddd = util.getDDD(tels[i]);
						Integer telefone = util.getNumero(tels[i]);
						System.out.println("tels[i] = " + tels[i]);
						Telefone telAtualizado = TelefoneFactory.criar(ddd, telefone);
						listaTels.addPersistente(telAtualizado);
					}
				}
			}

			ListaCelular listaCels = new ListaCelular();
			if (cels != null) {
				for (int i = 0; i < cels.length; i++) {
					if (cels[i] != "") {
						Integer ddd = util.getDDD(cels[i]);
						Integer celular = util.getNumero(cels[i]);
						System.out.println("cels[i] = " + cels[i]);
						Celular celAtualizado = CelularFactory.criar(ddd, celular);
						listaCels.addPersistente(celAtualizado);
					}
				}
			}


			if (p.getFlg_locador() == true) {

				String agencia = request.getParameter("agencia");
				String nome_banco = request.getParameter("nome_banco");
				String conta = request.getParameter("conta");
				String titular = request.getParameter("titular");
				String tp_conta = request.getParameter("tp_conta");

				if (p.getDadosBancarios() == null && !nome_banco.equalsIgnoreCase("0")) {
					d = DadosBancariosFactory.criar(nome_banco, agencia, conta, tp_conta, titular);
					p.setDadosBancarios(d);

				} else if(p.getDadosBancarios() != null){
					if (p.getDadosBancarios().isPersistente() && !nome_banco.equalsIgnoreCase("0")) {
						
						p.getDadosBancarios().setNome_banco(nome_banco);
						p.getDadosBancarios().setTitular(titular);
						p.getDadosBancarios().setConta(conta);
						p.getDadosBancarios().setAgencia(agencia);
						p.getDadosBancarios().setTp_conta(tp_conta);

					} else if (p.getDadosBancarios().isPersistente() && nome_banco.equalsIgnoreCase("0")){
						d = null;
						p.setDadosBancarios(d);
					}
				} 
			}

			p.setCelulares(listaCels);
			p.setTelefones(listaTels);

			TransactionController tc = new TransactionController();
			tc.add(p);
			
			tc.salvar();
			
			if(p.isUpdated()){
				setHistorico(request);
				u.atualizarAcesso(request.getParameter("nom_tela"), "ID: " + String.valueOf(p.getId()));
			}
			
			returnToPage(request, response, p.isUpdated());

		} catch (SQLException ex) {
			ex.printStackTrace();
			returnToPage(request, response, false);

		} catch (ParseException ex) {
			ex.printStackTrace();
			returnToPage(request, response, false);
		}
	}

	private void validarRg(HttpServletRequest request, HttpServletResponse response) throws IOException {
		Pessoa p;
		response.setContentType("text/plain; charset=UTF-8");
		String rg = request.getParameter("identidade");

		Integer id = Integer.valueOf(request.getParameter("id"));
		
		Boolean resultado = false;

		try {
			p = PessoaFactory.recuperarPorRg(rg);
			
			if (p == null || p.getId().intValue() == id.intValue()) {
				resultado = true;
			}
			
			System.out.println("RG: " + resultado);

			response.getOutputStream().print(resultado);

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	private void validarCpf(HttpServletRequest request, HttpServletResponse response) throws IOException {
		Pessoa p;
		response.setContentType("text/plain; charset=UTF-8");
		String cpf = request.getParameter("cpf");

		Integer id = Integer.valueOf(request.getParameter("id"));

		Boolean resultado = false;

		try {
			p = PessoaFactory.recuperarPorCpf(cpf);
			
			if (p == null || p.getId().intValue() == id.intValue()) {
				resultado = true;
			}
			
			System.out.println("CPF: " + resultado);

			response.getOutputStream().print(resultado);

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private void validarCnpj(HttpServletRequest request, HttpServletResponse response) throws IOException {

		response.setContentType("text/plain; charset=UTF-8");
		String cnpj = request.getParameter("cnpj");

		Integer id = Integer.valueOf(request.getParameter("id"));

		Boolean resultado = false;

		try {
			Pessoa p = PessoaFactory.recuperarPorCnpj(cnpj);
			
			if (p == null || p.getId().intValue() == id.intValue()) {
				resultado = true;
			}
			
			System.out.println("CNPJ: " + resultado);

			response.getOutputStream().print(resultado);

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}