package controller;

import dao.ConexaoBD;
import dominio.dao.FabricaDAO;
import utilitarios.util;

import java.util.Calendar;
import java.util.Date;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletContextEvent;  
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class Config implements ServletContextListener {

	private ScheduledExecutorService scheduler;
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
    	//Instacia a classe responsavel pelo banco    	
    	ConexaoBD.iniciar();
        
        // Registra a fábrica de classes DAO
    	new FabricaDAO();
        
        System.out.println("Iniciou");
        
        scheduler = Executors.newScheduledThreadPool(1);
        //scheduler.scheduleAtFixedRate(new Eventos.renovarContrato(), 0, 20, TimeUnit.SECONDS);
        //scheduler.scheduleAtFixedRate(new Eventos.aplicarIndice(), 0, 30, TimeUnit.SECONDS);
        
        int intDelayInHour = util.getHoursUntilTarget(0);
        int intDelayInHour2 = util.getHoursUntilTarget(1);

        System.out.println("Comuted Delay for next 00 AM: "+intDelayInHour);
        System.out.println("Comuted Delay for next 1 AM: "+intDelayInHour2);

        scheduler.scheduleAtFixedRate(new Eventos.renovarContrato(), intDelayInHour, 24, TimeUnit.HOURS);
        scheduler.scheduleAtFixedRate(new Eventos.aplicarIndice(), intDelayInHour2, 24, TimeUnit.HOURS);
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
    
        System.out.println("Shutting down!");
        scheduler.shutdownNow();
    }
}
