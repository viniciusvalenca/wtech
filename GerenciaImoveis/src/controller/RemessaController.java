package controller;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.sql.Date;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.jrimum.texgit.FlatFile;
import org.jrimum.texgit.Record;
import org.jrimum.utilix.ClassLoaders;

import com.google.gson.Gson;

import boletos.Remessa;
import dao.TransactionController;
import dominio.bean.entity.BoletoAbsi;
import dominio.bean.entity.Locacao;
import dominio.bean.entity.Usuario;
import dominio.bean.list.ListaBoletoAbsi;
import dominio.bean.list.ListaLocacao;
import dominio.factory.BoletoAbsiFactory;
import utilitarios.ConverterData;
import utilitarios.DownloadFile;
import utilitarios.util;

@WebServlet({ "/gerarRemessa" })
public class RemessaController extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private HttpSession session;
	private Usuario u;

	public RemessaController() {
		super();
	}

	protected void setHistorico(HttpServletRequest request) throws IOException {
		session = ((HttpServletRequest) request).getSession(true);
		u = (Usuario) session.getAttribute("user");
	}

	protected void returnToPage(HttpServletRequest request, HttpServletResponse response, Boolean resposta)
			throws IOException {
		PrintWriter out = response.getWriter();
		Gson gson = new Gson();

		out.print(gson.toJson(resposta));
		out.flush();
		out.close();
	}

	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			String url = request.getServletPath();

			if (url.equalsIgnoreCase("/gerarRemessa")) {
				gerar(request, response);

			} else {
				response.sendRedirect("/");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	private void gerar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		try {

			String jsonString = request.getParameter("lista");
			String mes_ref = request.getParameter("mes_referencia");
			
			Gson gson = new Gson();
			
			ListaBoletoAbsi lista = new ListaBoletoAbsi();
			
			Integer seuNumeroTemp = BoletoAbsiFactory.proximoId();
			
			for(Locacao l : gson.fromJson(jsonString, ListaLocacao.class)){
				
				String numero_documento = "L" + l.getId() + " -I" + l.getImovel().getId() + " -L" + l.getLocatario().getId();
				BigDecimal valor = l.getValorBoleto(); 
				Date dt_documento = ConverterData.getDateNow();
				Date dt_vencimento = ConverterData.incrementDiaVencimento(
						util.getAno(mes_ref)
						, l.getContrato().getDia_vencimento()
						, util.getMes(mes_ref));
				
				String seu_numero = String.valueOf(seuNumeroTemp);
				
				seuNumeroTemp++;
				
				l.getListaItensLocacao().setProcessado();
				
				BoletoAbsi bol = BoletoAbsiFactory.criar(numero_documento, valor, ConverterData.stringToDateSqlBD(util.converterMesRef(mes_ref)), 
						dt_documento, dt_vencimento, seu_numero, false, l);
				
				bol.setValor_comissao(l.getImovel().getValorTxAdm());
				bol.setvalor_aluguel(l.getImovel().getValor());
				bol.setLucro_txbancaria(l.getListaItensLocacao().valorTxBancaria());
				
				lista.add(bol);
			}
			
			ServletContext context = getServletContext();
			String reportLocation = context.getRealPath("");
			
			File layoutTemporario = new File("LayoutCNAB400Itau.xml");
			File layout = org.jrimum.bopepo.pdf.Files.bytesToFile(
					layoutTemporario, org.apache.commons.io.IOUtils.toByteArray(ClassLoaders.getResourceAsStream("/layouts/LayoutCNAB400Itau.xml")));
			
			new Remessa();
			FlatFile<Record> ff = Remessa.GerarRemessa(lista, layout);
			
			if(ff != null){
				for(BoletoAbsi bol : lista){
					bol.setArq_remessa("remessa-" + ConverterData.getDateStringRemessa() +".bnk");
				}
			}
			
			//SALVA O ARQUIVO NA PASTA DO SERVIDOR
			FileUtils.writeLines(new File(reportLocation + "banco_financeiro//remessa//remessa-" + ConverterData.getDateStringRemessa() + ".bnk"), ff.write(), "\r\n");
			
			TransactionController tc = new TransactionController();
			tc.add(lista);				
			tc.salvar();
			
			if (lista.isUpdated()) {
				setHistorico(request);
				u.atualizarAcesso(request.getParameter("nom_tela"), "Remessa: " + lista.get(0).getArq_remessa());
				
				//EXECUTA O DOWNLOAD DO ARQUIVO NA PÁGINA
				DownloadFile.downloadToPage(response, reportLocation, "banco_financeiro//remessa//", "remessa-" + ConverterData.getDateStringRemessa() + ".bnk");
			}
			
		} catch (Exception ex) {
			returnToPage(request, response, false);
			ex.printStackTrace();

		}
	}
}