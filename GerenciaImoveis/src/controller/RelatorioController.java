package controller;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.mail.EmailException;
import org.jrimum.bopepo.Boleto;
import org.jrimum.bopepo.view.BoletoViewer;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import dao.TransactionController;
import dominio.bean.entity.BoletoAbsi;
import dominio.bean.entity.ImpostoRenda;
import dominio.bean.entity.Locacao;
import dominio.bean.entity.Usuario;
import dominio.bean.list.ListaBoletoAbsi;
import dominio.bean.list.ListaLocacao;
import dominio.factory.BoletoAbsiFactory;
import dominio.factory.BoletoFactory;
import dominio.factory.ImovelFactory;
import dominio.factory.ImpostoRendaFactory;
import dominio.factory.LocacaoFactory;
import dominio.factory.PessoaFactory;
import dominio.factory.UsuarioFactory;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JRPrintPage;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import utilitarios.ConverterData;
import utilitarios.DownloadFile;
import utilitarios.Email;
import utilitarios.util;

@WebServlet({ "/relatorio", "/atualizarRelatorio", "/ir", "/irs", "/locador", "/locadores", "/transferencias",
		"/txadm" ,"/checktxadm", "/checktransf" })
public class RelatorioController extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			String url = request.getServletPath();

			if (url.equalsIgnoreCase("/atualizarRelatorio")) {
				atualizarRelatorio(request, response);

			} else if (url.equalsIgnoreCase("/locador")) {
				locador(request, response);

			} else if (url.equalsIgnoreCase("/locadores")) {
				locadores(request, response);

			} else if (url.equalsIgnoreCase("/transferencias")) {
				transferencias(request, response);

			} else if (url.equalsIgnoreCase("/txadm")) {
				txadm(request, response);

			} else if (url.equalsIgnoreCase("/checktxadm")) {
				checktxadm(request, response);

			}else if (url.equalsIgnoreCase("/checktransf")) {
				checktransf(request, response);

			}else if (url.equalsIgnoreCase("/ir")) {
				ir(request, response);

			} else if (url.equalsIgnoreCase("/irs")) {
				irs(request, response);

			} else {
				response.sendRedirect("/");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected void returnToPage(HttpServletRequest request, HttpServletResponse response, Boolean resposta)
			throws IOException {
		PrintWriter out = response.getWriter();
		Gson gson = new Gson();

		out.print(gson.toJson(resposta));
		out.flush();
		out.close();
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	@Override
	public String getServletInfo() {
		return "Reporting Servlet";
	}

	protected void atualizarRelatorio(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, NumberFormatException, SQLException, ParseException {

		response.setContentType("text/plain; charset=UTF-8");
		PrintWriter out = response.getWriter();
		Gson gson = new Gson();

		String idboleto = request.getParameter("idboleto");
		String dt_pag_to_locador = request.getParameter("dt_pag_to_locador");
		String valor_pag_to_locador = request.getParameter("valor_pag_to_locador");
		String cheque = request.getParameter("cheque");

		BoletoAbsi bol = BoletoAbsiFactory.recuperarPorID(Integer.valueOf(request.getParameter("idboleto")));

		if (dt_pag_to_locador != "") {
			bol.setDt_pag_to_locador(
					ConverterData.stringToDateSqlBD(util.converterData(request.getParameter("dt_pag_to_locador"))));
		}
		if (valor_pag_to_locador != "") {
			bol.setValor_pag_to_locador(
					new BigDecimal(util.stringToBigDecimal(request.getParameter("valor_pag_to_locador"))));
		}
		if (cheque != "") {
			bol.setCheque(Integer.valueOf(request.getParameter("cheque")));
		}

		TransactionController tc = new TransactionController();
		tc.add(bol);
		tc.salvar();

		out.print(gson.toJson(bol.isUpdated()));
		out.flush();
		out.close();

	}

	protected void txadm(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/plain; charset=UTF-8");
		response.setContentType("application/pdf");

		// set input and output stream
		ServletOutputStream servletOutputStream = response.getOutputStream();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		FileInputStream fis;
		BufferedInputStream bufferedInputStream;
		JRBeanCollectionDataSource jrbcds;
		JasperReport jasperReport;
		JasperPrint jasperPrint;

		HttpSession session = ((HttpServletRequest) request).getSession(true);
		Usuario u = (Usuario) session.getAttribute("user");

		try {
			// get report location
			ServletContext context = getServletContext();
			String reportLocation = context.getRealPath("");

			// img
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put("logo", new javax.swing.ImageIcon(reportLocation + "img\\logoNew.png").getImage());
			params.put("user", u);
			request.setCharacterEncoding("UTF-8");

			// get report
			fis = new FileInputStream(reportLocation + "relatorio\\txadm\\main.jasper");

			// params.put("SubReport", reportLocation +
			// "relatorio\\locatario\\sub.jasper");

			bufferedInputStream = new BufferedInputStream(fis);

			String mes_ref = request.getParameter("mes_referencia");
			java.sql.Date dt2 = ConverterData.createDateFromMesRef(mes_ref);
			
			ListaBoletoAbsi listabol2 = BoletoAbsiFactory.recuperarPagamentoMes(dt2);
			
			listabol2.sort();
			listabol2.atualizarValorTxAdm();
			jrbcds = new JRBeanCollectionDataSource(listabol2);
			jasperReport = (JasperReport) JRLoader.loadObject(bufferedInputStream);

			jasperPrint = JasperFillManager.fillReport(jasperReport, params, jrbcds);

			// export to pdf
			JasperExportManager.exportReportToPdfStream(jasperPrint, baos);

			response.setContentLength(baos.size());
			baos.writeTo(servletOutputStream);

			// close it
			fis.close();
			bufferedInputStream.close();

		} catch (Exception ex) {
			ex.printStackTrace();

		} finally {

			servletOutputStream.flush();
			servletOutputStream.close();
			baos.close();
		}

	}
	
	protected void checktxadm(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		Gson gson = new Gson();

		try {			

			String mes_ref = request.getParameter("mes_referencia");
			java.sql.Date dt2 = ConverterData.createDateFromMesRef(mes_ref);

			ListaBoletoAbsi listabol2 = BoletoAbsiFactory.recuperarPagamentoMes(dt2);
			
			out.print(gson.toJson(!listabol2.isEmpty()));
			out.flush();
			out.close();

		}catch (Exception ex) {
			ex.printStackTrace();
			out.print(gson.toJson(false));
			out.flush();
			out.close();
		}
	}
	
	protected void checktransf(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		Gson gson = new Gson();

		try {			

			String mes_ref = request.getParameter("mes_referencia");
			java.sql.Date dt1 = ConverterData
					.stringToDateSqlBD(util.converterData(request.getParameter("mes_referencia")));

			ListaBoletoAbsi listabol = BoletoAbsiFactory.recuperarPagamento(dt1);
			
			out.print(gson.toJson(!listabol.isEmpty()));
			out.flush();
			out.close();

		}catch (Exception ex) {
			ex.printStackTrace();
			out.print(gson.toJson(false));
			out.flush();
			out.close();
		}
	}

	@SuppressWarnings("deprecation")
	protected void transferencias(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/plain; charset=UTF-8");
		response.setContentType("application/pdf");

		// set input and output stream
		ServletOutputStream servletOutputStream = response.getOutputStream();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		FileInputStream fis;
		BufferedInputStream bufferedInputStream;
		JRBeanCollectionDataSource jrbcds;
		JasperReport jasperReport;
		JasperPrint jasperPrint;

		HttpSession session = ((HttpServletRequest) request).getSession(true);
		Usuario u = (Usuario) session.getAttribute("user");

		try {
			// get report location
			ServletContext context = getServletContext();
			String reportLocation = context.getRealPath("");

			// img
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put("logo", new javax.swing.ImageIcon(reportLocation + "img\\logoNew.png").getImage());

			request.setCharacterEncoding("UTF-8");

			// get report
			fis = new FileInputStream(reportLocation + "relatorio\\transferencias\\main.jasper");
			// params.put("SubReport", reportLocation +
			// "relatorio\\locatario\\sub.jasper");
			params.put("user", u);
			bufferedInputStream = new BufferedInputStream(fis);

			java.sql.Date dt1 = ConverterData
					.stringToDateSqlBD(util.converterData(request.getParameter("mes_referencia")));

			ListaBoletoAbsi listabol = BoletoAbsiFactory.recuperarPagamento(dt1);
			
			listabol.sort();

			if (!listabol.isEmpty()) {
				for (BoletoAbsi b : listabol) {
					
					java.sql.Date mes_RefConvertida = ConverterData.createDateFromMesRef(request.getParameter("mes_referencia"));
				
					
					ImpostoRenda ir = ImpostoRendaFactory.recuperarPorLocadorLocacao(b.getLocacao().getImovel().getLocador().getId(), b.getMes_ref(),b.getLocacao().getId());
					
					b.setIr(
							ir != null ? ir.getValor_desconto(): new BigDecimal(0)
							);
					System.out.println("IR2- " +b.getIr());
					System.out.println(request.getParameter("mes_referencia") + " - " +dt1);
					
					
					b.getLocacao().setListaItensLocacao(b.getMes_ref());
							//ConverterData.createDateFromMesRef(ConverterData.getMes_refString(dt1)));
					b.getLocacao().setBoleto(b.getMes_ref());
					System.out.println(b.getLocacao().getValorSoma() + " - " + b.getMes_ref());

				}

				jrbcds = new JRBeanCollectionDataSource(listabol);
				jasperReport = (JasperReport) JRLoader.loadObject(bufferedInputStream);

				jasperPrint = JasperFillManager.fillReport(jasperReport, params, jrbcds);

				// export to pdf
				JasperExportManager.exportReportToPdfStream(jasperPrint, baos);

				response.setContentLength(baos.size());
				baos.writeTo(servletOutputStream);

				// close it
				fis.close();
				bufferedInputStream.close();
				listabol.setId(dt1);
				TransactionController tc = new TransactionController();
				tc.add(listabol);
				tc.salvar();
			} else {

			}

		} catch (Exception ex) {
			ex.printStackTrace();

		} finally {

			servletOutputStream.flush();
			servletOutputStream.close();
			baos.close();
		}

	}

	protected void locador(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/plain; charset=UTF-8");
		request.setCharacterEncoding("UTF-8");
		String op = request.getParameter("op");
		
		response.setContentType("application/pdf");

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		FileInputStream fis;
		BufferedInputStream bufferedInputStream;
		JRBeanCollectionDataSource jrbcds;
		JasperReport jasperReport;
		JasperPrint jasperPrint;

		HttpSession session = ((HttpServletRequest) request).getSession(true);
		Usuario u = (Usuario) session.getAttribute("user");

		try {
			// get report location
			ServletContext context = getServletContext();
			String reportLocation = context.getRealPath("");

			// img
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put("logo", new javax.swing.ImageIcon(reportLocation + "img\\logoNew.png").getImage());

			String listaLocacao = request.getParameter("lista");
			ArrayList<Integer> array = new ArrayList<Integer>();

			Gson gson = new Gson();

			//System.out.println("GSON: " + listaLocacao);

			Integer idLocador = 0;

			for (Locacao l : gson.fromJson(listaLocacao, ListaLocacao.class)) {
				System.out.println("ID: " + l.getId());
				array.add(l.getId());
				idLocador = l.getImovel().getLocador().getId();
			}

			// get report
			fis = new FileInputStream(reportLocation + "relatorio\\locador\\main.jasper");
			params.put("Receitas", reportLocation + "relatorio\\locador\\receitas.jasper");
			params.put("Despesas", reportLocation + "relatorio\\locador\\despesas.jasper");
			params.put("Informacoes", reportLocation + "relatorio\\locador\\informacoes.jasper");
			params.put("user", u);

			bufferedInputStream = new BufferedInputStream(fis);

			// fetch data from database
			// Integer idLocador =
			// Integer.valueOf(request.getParameter("idLocadorHidden"));
			ListaLocacao lista2 = LocacaoFactory.recuperarTodosPorLocador(idLocador, array);
			java.sql.Date dt = ConverterData.createDateFromMesRef(request.getParameter("mes_referencia"));
			lista2.setItensLocacao(dt);
			
			BigDecimal valor_ir = new BigDecimal(0);
			ImpostoRenda ir;

			if (!lista2.isEmpty()) {
				for (Locacao lo : lista2) {
					lo.setBoleto(dt);
					
					ir = ImpostoRendaFactory.recuperarPorLocadorLocacao(lista2.get(0).getImovel().getLocador().getId(), dt, lo.getId());
					
					if(ir != null){
						System.out.println(ir);
						valor_ir = ir.getValor_desconto();
					}
				}
				
				params.put("Locador", lista2.get(0).getImovel().getLocador().getNome_completo());
				params.put("count_imovel", lista2.size());
				params.put("imposto_renda", valor_ir);
				
				jrbcds = new JRBeanCollectionDataSource(lista2);
				jasperReport = (JasperReport) JRLoader.loadObject(bufferedInputStream);
				jasperPrint = JasperFillManager.fillReport(jasperReport, params, jrbcds);

				// export to pdf
				if (op != null && op.equals("downloadRelatorio")) {
					String nameFile = "";

					nameFile = "relatorio-" + request.getParameter("nomeLocadorHidden") + ".pdf";

					DownloadFile.downloadPDF(response, JasperExportManager.exportReportToPdf(jasperPrint), nameFile);

				} else if (op != null && op.equals("enviarRelatorio")) {

					response.setContentType("text/html");

					if (!lista2.get(0).getImovel().getLocador().getEmail().equals("")) {

						byte[] baos2 = JasperExportManager.exportReportToPdf(jasperPrint);

						Email.emailLocador(baos2, lista2.get(0).getImovel().getLocador().getEmail(),
								lista2.get(0).getImovel().getLocador().getNome_completo(),
								lista2.get(0).getImovel().getLocador().getEmail_opcional());

						response.sendRedirect("relatorio_locador.jsp?msg=true");

					} else {
						response.sendRedirect("relatorio_locador.jsp?msg=false");
					}

				} else {
					JasperExportManager.exportReportToPdfStream(jasperPrint, baos);

					ServletOutputStream servletOutputStream = response.getOutputStream();
					response.setContentLength(baos.size());
					baos.writeTo(servletOutputStream);

					fis.close();
					servletOutputStream.flush();
					servletOutputStream.close();
					baos.close();
				}

			} else {

			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	protected void locadores(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, JRException, SQLException, ParseException, MessagingException, EmailException {

		String op = request.getParameter("op");
		response.setContentType("text/plain; charset=UTF-8");
		request.setCharacterEncoding("UTF-8");
		response.setContentType("application/pdf");

		HttpSession session = ((HttpServletRequest) request).getSession(true);
		Usuario u = (Usuario) session.getAttribute("user");

		ArrayList<String> ids = new ArrayList<String>();
		Boolean msg = true;

		ServletContext context = getServletContext();
		String reportLocation = context.getRealPath("");

		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("logo", new javax.swing.ImageIcon(reportLocation + "img\\logoNew.png").getImage());

		ServletOutputStream servletOutputStream = response.getOutputStream();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		FileInputStream fis = null;
		BufferedInputStream bufferedInputStream = null;
		JRBeanCollectionDataSource jrbcds;
		JasperReport jasperReport;
		JasperPrint jasperPrint = null;

		String listaLocacao = request.getParameter("lista");

		Gson gson = new Gson();
		Map<Integer, ArrayList<Integer>> example = new HashMap<Integer, ArrayList<Integer>>();

		int tmp = 0;
		for (Locacao l : gson.fromJson(listaLocacao, ListaLocacao.class)) {

			if (example.containsKey(l.getImovel().getId_locador())) {

			} else {
				example.put(l.getImovel().getId_locador(), new ArrayList<Integer>());
				// System.err.println("Chave não existe");
			}

			if (tmp == l.getImovel().getId_locador()) {
				example.get(l.getImovel().getId_locador()).add(l.getId());
			} else {
				// array.clear();
				example.get(l.getImovel().getId_locador()).add(l.getId());
			}
			tmp = l.getImovel().getId_locador();
		}

		List<JasperPrint> jasperPrintList = new ArrayList<JasperPrint>();
		int temp = 0;

		java.sql.Date dt = ConverterData.createDateFromMesRef(request.getParameter("mes_referencia"));
		
		for (Map.Entry<Integer, ArrayList<Integer>> entry : example.entrySet()) {
			//System.out.println(entry.getKey() + "/" + entry.getValue());

			temp++;

			fis = new FileInputStream(reportLocation + "relatorio\\locador\\main.jasper");
			params.put("Receitas", reportLocation + "relatorio\\locador\\receitas.jasper");
			params.put("Despesas", reportLocation + "relatorio\\locador\\despesas.jasper");
			params.put("Informacoes", reportLocation + "relatorio\\locador\\informacoes.jasper");
			params.put("user", u);

			bufferedInputStream = new BufferedInputStream(fis);

			ListaLocacao lista2 = LocacaoFactory.recuperarTodosPorLocador(entry.getKey(), entry.getValue());
			lista2.setItensLocacao(dt);

			BigDecimal valor_ir = new BigDecimal(0);
			ImpostoRenda ir;
			
			if (!lista2.isEmpty()) {
				for (Locacao lo : lista2) {
					lo.setBoleto(dt);
					ir = ImpostoRendaFactory.recuperarPorLocadorLocacao(lista2.get(0).getImovel().getLocador().getId(), dt, lo.getId());
				
					if(ir != null){
						System.out.println(ir);
						valor_ir = ir.getValor_desconto();
					}
				}
				
				params.put("imposto_renda", valor_ir);

				params.put("Locador", lista2.get(0).getImovel().getLocador().getNome_completo());
				params.put("count_imovel", lista2.size());
				
				
				jrbcds = new JRBeanCollectionDataSource(lista2);
				jasperReport = (JasperReport) JRLoader.loadObject(bufferedInputStream);
				jasperPrint = JasperFillManager.fillReport(jasperReport, params, jrbcds);

				if (op != null && op.equals("enviarRelatorio")) {

					response.setContentType("text/html");

					if (!lista2.get(0).getImovel().getLocador().getEmail().equals("")) {

						byte[] baos2 = JasperExportManager.exportReportToPdf(jasperPrint);

						Email.emailLocador(baos2, lista2.get(0).getImovel().getLocador().getEmail(),
								lista2.get(0).getImovel().getLocador().getNome_completo(),
								lista2.get(0).getImovel().getLocador().getEmail_opcional());

					} else {
						ids.add(lista2.get(0).getImovel().getLocador().getId().toString());
					}

				}

				jasperPrintList.add(jasperPrint);
			}
		}
		

		JasperPrint jasperPrint2 = null;
		JasperPrint pages = jasperPrintList.get(0);
		synchronized (pages) {
			if (jasperPrintList.size() > 1) {

				for (int i = 1; i < jasperPrintList.size(); i++) {
					jasperPrint2 = jasperPrintList.get(i);

					List<JRPrintPage> list = jasperPrint2.getPages();

					for (JRPrintPage jRPrintPage : list) {
						pages.addPage(jRPrintPage);
					}
				}
			}
		}

		if (op != null && op.equals("downloadRelatorio")) {
			String nameFile = "";

			nameFile = "relatorio-" + ConverterData.getDateNow() + ".pdf";

			DownloadFile.downloadPDF(response, JasperExportManager.exportReportToPdf(pages), nameFile);

		} else if (op != null && op.equals("enviarRelatorio")) {

			response.setContentType("text/html");
			if (ids.isEmpty()) {
				response.sendRedirect("relatorio_locador.jsp?msg=" + msg);
			} else {
				if (temp == ids.size()) {
					msg = false;
					response.sendRedirect("relatorio_locador.jsp?msg=" + msg);
				} else {
					response.sendRedirect("relatorio_locador.jsp?msg=" + msg + "&ids=" + ids);
				}

			}

		}
		JasperExportManager.exportReportToPdfStream(pages, baos);

		response.setContentLength(baos.size());
		baos.writeTo(servletOutputStream);

		// close it
		fis.close();
		bufferedInputStream.close();

		servletOutputStream.flush();
		servletOutputStream.close();
		baos.close();

	}

	protected void ir(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/plain; charset=UTF-8");
		request.setCharacterEncoding("UTF-8");
		response.setContentType("application/pdf");

		String op = request.getParameter("op");
		// set input and output stream
		ServletOutputStream servletOutputStream = response.getOutputStream();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		FileInputStream fis;
		BufferedInputStream bufferedInputStream;
		JRBeanCollectionDataSource jrbcds;
		JasperReport jasperReport;
		JasperPrint jasperPrint;

		HttpSession session = ((HttpServletRequest) request).getSession(true);
		Usuario u = (Usuario) session.getAttribute("user");

		try {
			// get report location
			ServletContext context = getServletContext();
			String reportLocation = context.getRealPath("");

			// img
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put("logo", new javax.swing.ImageIcon(reportLocation + "img\\logoNew.png").getImage());
			params.put("user", u);
			//request.setCharacterEncoding("UTF-8");

			String listaLocacao = request.getParameter("lista");
			ArrayList<Integer> array = new ArrayList<Integer>();

			Gson gson = new Gson();

			for (Locacao l : gson.fromJson(listaLocacao, ListaLocacao.class)) {
				array.add(l.getId());
			}

			// get report
			fis = new FileInputStream(reportLocation + "relatorio\\ir\\main.jasper");
			params.put("sub", reportLocation + "relatorio\\ir\\sub.jasper");

			bufferedInputStream = new BufferedInputStream(fis);

			// fetch data from database
			Integer idLocador = Integer.valueOf(request.getParameter("idLocadorHidden"));
			ListaLocacao lista2 = LocacaoFactory.recuperarTodosPorLocador(idLocador, array);
			java.sql.Date dt = ConverterData.createDateFromMesRef("01/"+request.getParameter("mes_referencia"));

			Calendar cal = Calendar.getInstance();
			cal.setTime(dt);
			String ano = String.valueOf(cal.get(Calendar.YEAR)) + "/" + String.valueOf(cal.get(Calendar.YEAR) + 1) ;

			params.put("ano", ano);

			if (!lista2.isEmpty()) {
				
				for (Locacao lo : lista2) {
					lo.setLboleto(dt);
					System.out.println(dt);
					System.out.println(lo.getIrs());
				
				}				
				params.put("Locador", request.getParameter("nomeLocadorHidden"));
				params.put("LocadorCPFCNPJ", request.getParameter("cpfcnpjLocadorHidden"));
				params.put("count_imovel", lista2.size());
				jrbcds = new JRBeanCollectionDataSource(lista2);
				jasperReport = (JasperReport) JRLoader.loadObject(bufferedInputStream);
				jasperPrint = JasperFillManager.fillReport(jasperReport, params, jrbcds);

				if (op != null && op.equals("downloadRelatorio")) {
					System.out.println(op.equals("downloadRelatorio") +  " "+ "downloadRelatorio");
					
					String nameFile = "";

					nameFile = "relatorio-" + request.getParameter("nomeLocadorHidden") + ".pdf";

					DownloadFile.downloadPDF(response, JasperExportManager.exportReportToPdf(jasperPrint), nameFile);

				} else if (op != null && op.equals("enviarRelatorio")) {

					response.setContentType("text/html");

					if (!lista2.get(0).getImovel().getLocador().getEmail().equals("")) {

						byte[] baos2 = JasperExportManager.exportReportToPdf(jasperPrint);

						Email.emailIr(baos2, lista2.get(0).getImovel().getLocador().getEmail(),
								lista2.get(0).getImovel().getLocador().getNome_completo(),
								lista2.get(0).getImovel().getLocador().getEmail_opcional());

						response.sendRedirect("relatorio_imposto_renda.jsp?msg=true");

					} else {
						response.sendRedirect("relatorio_imposto_renda.jsp?msg=false");
					}

				} else {
					JasperExportManager.exportReportToPdfStream(jasperPrint, baos);

					response.setContentLength(baos.size());
					baos.writeTo(servletOutputStream);

					fis.close();
					servletOutputStream.flush();
					servletOutputStream.close();
					baos.close();
				}

				
				
				

			} else {

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		finally {

			servletOutputStream.flush();
			servletOutputStream.close();
			baos.close();
		}

	}

	protected void irs(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, JRException, SQLException, ParseException, MessagingException, EmailException {
		response.setContentType("text/plain; charset=UTF-8");
		request.setCharacterEncoding("UTF-8");
		response.setContentType("application/pdf");

		String op = request.getParameter("op");
		HttpSession session = ((HttpServletRequest) request).getSession(true);
		Usuario u = (Usuario) session.getAttribute("user");
		
		ArrayList<String> ids = new ArrayList<String>();
		Boolean msg = true;

		// get report location
		ServletContext context = getServletContext();
		String reportLocation = context.getRealPath("");

		// img
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("logo", new javax.swing.ImageIcon(reportLocation + "img\\logoNew.png").getImage());

		// set input and output stream
		ServletOutputStream servletOutputStream = response.getOutputStream();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		FileInputStream fis = null;
		BufferedInputStream bufferedInputStream = null;
		JRBeanCollectionDataSource jrbcds;
		JasperReport jasperReport;
		JasperPrint jasperPrint = null;
		// JasperPrint jasperPrint = null;

		String listaLocacao = request.getParameter("lista");

		Gson gson = new Gson();
		Map<Integer, ArrayList<Integer>> example = new HashMap<Integer, ArrayList<Integer>>();

		int tmp = 0;
		for (Locacao l : gson.fromJson(listaLocacao, ListaLocacao.class)) {

			if (example.containsKey(l.getImovel().getId_locador())) {

			} else {
				example.put(l.getImovel().getId_locador(), new ArrayList<Integer>());
				// System.err.println("Chave não existe");
			}

			if (tmp == l.getImovel().getId_locador()) {
				example.get(l.getImovel().getId_locador()).add(l.getId());
			} else {
				// array.clear();
				example.get(l.getImovel().getId_locador()).add(l.getId());
			}
			tmp = l.getImovel().getId_locador();
		}

		List<JasperPrint> jasperPrintList = new ArrayList<JasperPrint>();
		int temp = 0;
		for (Map.Entry<Integer, ArrayList<Integer>> entry : example.entrySet()) {
			temp++;
			// get report
			fis = new FileInputStream(reportLocation + "relatorio\\ir\\main.jasper");
			params.put("sub", reportLocation + "relatorio\\ir\\sub.jasper");

			bufferedInputStream = new BufferedInputStream(fis);

			// fetch data from database
			ListaLocacao lista2 = LocacaoFactory.recuperarTodosPorLocador(entry.getKey(), entry.getValue());
			java.sql.Date dt = ConverterData.createDateFromMesRef("01/"+request.getParameter("mes_referencia"));

			Calendar cal = Calendar.getInstance();
			cal.setTime(dt);
			String ano = String.valueOf(cal.get(Calendar.YEAR) - 1) + "/" + String.valueOf(cal.get(Calendar.YEAR));

			params.put("ano", ano);

			if (!lista2.isEmpty()) {
				for (Locacao lo : lista2) {
					lo.setLboleto(dt);
				}

				params.put("Locador", lista2.get(0).getImovel().getLocador().getNome_completo());
				params.put("LocadorCPFCNPJ", lista2.get(0).getImovel().getLocador().getCpfcnpj());
				params.put("count_imovel", lista2.size());
				params.put("user", u);
				jrbcds = new JRBeanCollectionDataSource(lista2);
				jasperReport = (JasperReport) JRLoader.loadObject(bufferedInputStream);
				jasperPrint = JasperFillManager.fillReport(jasperReport, params, jrbcds);
				jasperPrintList.add(jasperPrint);
				// export to pdf
			}
			
			if (op != null && op.equals("enviarRelatorio")) {

				response.setContentType("text/html");

				if (!lista2.get(0).getImovel().getLocador().getEmail().equals("")) {

					byte[] baos2 = JasperExportManager.exportReportToPdf(jasperPrint);

					Email.emailIr(baos2, lista2.get(0).getImovel().getLocador().getEmail(),
							lista2.get(0).getImovel().getLocador().getNome_completo(),
							lista2.get(0).getImovel().getLocador().getEmail_opcional());

				} else {
					ids.add(lista2.get(0).getImovel().getLocador().getId().toString());
				}

			}
			
			
			
		}
		
		
		JasperPrint jasperPrint2 = null;
		JasperPrint pages = jasperPrintList.get(0);
		synchronized (pages) {
			if (jasperPrintList.size() > 1) {

				for (int i = 1; i < jasperPrintList.size(); i++) {
					jasperPrint2 = jasperPrintList.get(i);

					List<JRPrintPage> list = jasperPrint2.getPages();

					for (JRPrintPage jRPrintPage : list) {
						pages.addPage(jRPrintPage);

						System.out.println("Adicionou página");
					}
				}
			}
		}

		if (op != null && op.equals("downloadRelatorio")) {
			String nameFile = "";

			nameFile = "relatorio-" + ConverterData.getDateNow() + ".pdf";

			DownloadFile.downloadPDF(response, JasperExportManager.exportReportToPdf(pages), nameFile);

		}else if (op != null && op.equals("enviarRelatorio")) {

			response.setContentType("text/html");
			if (ids.isEmpty()) {
				response.sendRedirect("relatorio_imposto_renda.jsp?msg=" + msg);
			} else {
				if (temp == ids.size()) {
					msg = false;
					response.sendRedirect("relatorio_imposto_renda.jsp?msg=" + msg);
				} else {
					response.sendRedirect("relatorio_imposto_renda.jsp?msg=" + msg + "&ids=" + ids);
				}

			}

		}
		JasperExportManager.exportReportToPdfStream(pages, baos);

		response.setContentLength(baos.size());
		baos.writeTo(servletOutputStream);

		// close it
		fis.close();
		bufferedInputStream.close();

		servletOutputStream.flush();
		servletOutputStream.close();
		baos.close();

	}

}
