package controller;

import javax.servlet.http.HttpServlet;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import dao.TransactionController;

import dominio.bean.entity.Indice;

import dominio.bean.entity.Usuario;

import dominio.factory.IndiceFactory;

import utilitarios.util;

@WebServlet({ "/indice", "/atualizarindice" })

public class IndiceController extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private HttpSession session;
	private Usuario u;

	public IndiceController() {
		super();
	}

	protected void setHistorico(HttpServletRequest request) throws IOException {
		session = ((HttpServletRequest) request).getSession(true);
		u = (Usuario) session.getAttribute("user");
	}

	protected void returnToPage(HttpServletRequest request, HttpServletResponse response, Boolean resposta)
			throws IOException {
		PrintWriter out = response.getWriter();
		Gson gson = new Gson();

		out.print(gson.toJson(resposta));
		out.flush();
		out.close();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		option(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		option(request, response);
	}

	protected void option(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			String url = request.getServletPath();

			if (url.equalsIgnoreCase("/indice")) {
				cad_indice(request, response);

			} else if (url.equalsIgnoreCase("/atualizarindice")) {
				atualizarindice(request, response);

			} else {
				response.sendRedirect("/");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void cad_indice(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			Indice ind = IndiceFactory.recuperarPorId(1);
			
			System.out.println("POST BACK = " +ind.getIndice());
			
			request.setAttribute("indice", ind.getIndice());
			request.getRequestDispatcher("indice.jsp").forward(request, response);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	protected void atualizarindice(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		try {
			Indice ind = IndiceFactory.recuperarPorId(1);
			System.out.println("IndiceFactory.recuperarPorId(1) = " + ind);
			
			BigDecimal indice = new BigDecimal(0);
			indice = indice.setScale(3, RoundingMode.CEILING);
			
			System.out.println("request.getParameter = " + request.getParameter("indice"));
			
			if(request.getParameter("indice") != ""){
				 indice = new BigDecimal(util.stringToBigDecimal(request.getParameter("indice")));
			}
			//BigDecimal indice = util.bigdecimalToBD(request.getParameter("indice"));
			
			System.out.println("BigDecimal = " +indice);
			
			ind.setIndice(indice);
			TransactionController tc = new TransactionController();
			tc.add(ind);

			tc.salvar();

			if (ind.isUpdated()) {
				setHistorico(request);
				u.atualizarAcesso(request.getParameter("nom_tela"), "Valor Atualizado: % " + IndiceFactory.recuperarPorId(1));
			}

			request.setAttribute("indice", util.bigdecimalToPageINDICE(indice));
			returnToPage(request, response, ind.isUpdated());

		} catch (Exception ex) {
			returnToPage(request, response, false);
			ex.printStackTrace();

		}
	}

}