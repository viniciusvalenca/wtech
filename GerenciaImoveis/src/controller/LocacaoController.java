package controller;

import javax.servlet.http.HttpServlet;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.sql.SQLException;
import java.text.ParseException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import dao.TransactionController;
import dominio.bean.entity.Contrato;
import dominio.bean.entity.Imovel;
import dominio.bean.entity.InfoFiador;
import dominio.bean.entity.Locacao;
import dominio.bean.entity.Usuario;
import dominio.bean.list.ListaBoletoAbsi;
import dominio.bean.list.ListaInfoFiador;
import dominio.bean.list.ListaLocacao;
import dominio.bean.list.ListaPessoa;
import dominio.factory.BoletoAbsiFactory;
import dominio.factory.ContratoFactory;
import dominio.factory.ImovelFactory;
import dominio.factory.InfoFiadorFactory;
import dominio.factory.LocacaoFactory;
import dominio.factory.PessoaFactory;
import utilitarios.ConverterData;
import utilitarios.util;

@WebServlet({ "/cadlocacao", "/cadastrarLocacao", "/atualizarLocacao", "/listarLocacao", "/listarLocacaoRelatorio", 
	"/buscarLocacao", "/listarLocacoesNaoPagas", "/listarLocacaoPorMes", "/listarLocacaoIR"  })

public class LocacaoController extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private HttpSession session;
	private Usuario u;
	private Locacao l;
	private Contrato c;

	public LocacaoController() {
		super();
	}

	protected void setHistorico(HttpServletRequest request) throws IOException {
		session = ((HttpServletRequest) request).getSession(true);
		u = (Usuario) session.getAttribute("user");
	}

	protected void returnToPage(HttpServletRequest request, HttpServletResponse response, Boolean resposta)
			throws IOException {
		PrintWriter out = response.getWriter();
		Gson gson = new Gson();

		out.print(gson.toJson(resposta));
		out.flush();
		out.close();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		option(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		option(request, response);
	}

	protected void option(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			String url = request.getServletPath();

			if (url.equalsIgnoreCase("/cadlocacao")) {
				cad_locacao(request, response);

			} else if (url.equalsIgnoreCase("/cadastrarLocacao")) {
				cadastrar(request, response);

			} else if (url.equalsIgnoreCase("/atualizarLocacao")) {
				atualizar(request, response);

			} else if (url.equalsIgnoreCase("/listarLocacao")) {
				listarLocacao(request, response);

			} else if (url.equalsIgnoreCase("/listarLocacaoRelatorio")) {
				listarLocacaoRelatorio(request, response);

			} else if (url.equalsIgnoreCase("/listarLocacaoIR")) {
				listarLocacaoIR(request, response);

			}else if (url.equalsIgnoreCase("/listarLocacoesNaoPagas")) {
				listarLocacoesNaoPagas(request, response);

			} else if (url.equalsIgnoreCase("/buscarLocacao")) {
				buscarLocacao(request, response);

			} else if (url.equalsIgnoreCase("/listarLocacaoPorMes")) {
				listarLocacaoPorMes(request, response);

			} else {
				response.sendRedirect("/");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void cad_locacao(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		ListaPessoa pLocatario, pFiador;
		// ListaItensLocacao iLocacao;

		try {
			// String jsonItens;

			pLocatario = PessoaFactory.recuperarTodosLocatario();
			pFiador = PessoaFactory.recuperarTodosFiador();
			// iLocacao = ItensLocacaoFactory.recuperarProximoMes("07/2017");

			// Gson gson = new Gson();

			// jsonProprietatio = gson.toJson(pLocatario);

			// jsonItens = gson.toJson(iLocacao);

			request.setAttribute("lista", pLocatario);
			request.setAttribute("lista2", pFiador);

			// request.setAttribute("itens", jsonItens);

			request.getRequestDispatcher("locacao.jsp").forward(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	private void buscarLocacao(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ListaPessoa pLocatario, pFiador;

		try {
			// String jsonFiador, jsonProprietatio;

			pLocatario = PessoaFactory.recuperarTodosLocatario();
			pFiador = PessoaFactory.recuperarTodosFiador();

			// Gson gson = new Gson();

			// jsonProprietatio = gson.toJson(pLocatario);
			// jsonFiador = gson.toJson(pFiador);

			request.setAttribute("lista", pLocatario);
			request.setAttribute("lista2", pFiador);

			request.getRequestDispatcher("buscar_locacao.jsp").forward(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void listarLocacao(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		ListaLocacao lo;

		response.setContentType("text/plain; charset=UTF-8");
		PrintWriter out = response.getWriter();
		Gson gson = new Gson();

		try {
			lo = LocacaoFactory.recuperarTodos();

			out.print(gson.toJson(lo));
			out.flush();
			out.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private void listarLocacaoRelatorio(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, ParseException {

		ListaLocacao lo;

		response.setContentType("text/plain; charset=UTF-8");
		PrintWriter out = response.getWriter();
		Gson gson = new Gson();

		try {
			
			String mes_ref = request.getParameter("mes_ref");
			java.sql.Date dt = ConverterData.createDateFromMesRef(mes_ref);
			
			lo = LocacaoFactory.recuperarTodosLocacaoPagasRelatorio(dt);
			
			System.out.println(lo);

			if(!lo.isEmpty()){
				out.print(gson.toJson(lo));
				out.flush();
				out.close();
				
			}else{
				returnToPage(request, response, false);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			returnToPage(request, response, false);
		}
	}
	
	private void listarLocacaoIR(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, ParseException {

		ListaLocacao lo;

		response.setContentType("text/plain; charset=UTF-8");
		PrintWriter out = response.getWriter();
		Gson gson = new Gson();

		try {
			
			String mes_ref = request.getParameter("mes_ref");
			java.sql.Date dt = ConverterData.createDateFromMesRef("01/"+mes_ref);
			
			lo = LocacaoFactory.recuperarTodosRelatorioAno(dt);
			lo.setBoletos(dt);
			//System.out.println(lo);

			if(!lo.isEmpty()){
				out.print(gson.toJson(lo));
				out.flush();
				out.close();
				
			}else{
				returnToPage(request, response, false);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			returnToPage(request, response, false);
		}
	}
	
	private void listarLocacaoPorMes(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, ParseException {

		ListaLocacao lo;

		response.setContentType("text/plain; charset=UTF-8");
		PrintWriter out = response.getWriter();
		Gson gson = new Gson();
		
		String mes_ref = request.getParameter("mes_ref");
		java.sql.Date dt = ConverterData.createDateFromMesRef(mes_ref);
		
		try {
			lo = LocacaoFactory.recuperarLocacaoRemessa(dt);
			lo.setBoleto(dt);
			
			out.print(gson.toJson(lo));
			out.flush();
			out.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
	
	private void listarLocacoesNaoPagas(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, ParseException {

		ListaLocacao lo;

		response.setContentType("text/plain; charset=UTF-8");
		PrintWriter out = response.getWriter();
		Gson gson = new Gson();
		
		String mes_ref = request.getParameter("mes_ref");
		java.sql.Date dt = ConverterData.createDateFromMesRef(mes_ref);
		
		try {
			lo = LocacaoFactory.recuperarLocacoesNaoPagas(dt);
			lo.setBoleto(dt);
		    System.out.println(gson.toJson(lo));
			
			out.print(gson.toJson(lo));
			out.flush();
			out.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	protected void cadastrar(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		try {
			String id_imovel = request.getParameter("idImovelHidden");
			String id_locatario = request.getParameter("locatario");
			String modalidade = request.getParameter("modalidade");

			String id_fiador = "", mat_imovel = "", cartorio = "", endereco_imovel = "";
			String id_fiador2 = "", mat_imovel2 = "", cartorio2 = "", endereco_imovel2 = "";
			String id_fiador3 = "", mat_imovel3 = "", cartorio3 = "", endereco_imovel3 = "";

			String valor_deposito = "";
			String valor_seguro = "";

			if (modalidade.equals("Deposito")) {
				valor_deposito = request.getParameter("valor_deposito");

			} else if (modalidade.equals("Seguro Fianca")) {
				valor_seguro = request.getParameter("valor_seguro");
			}

			//String mes_ref = request.getParameter("mes_ref");

			Date dt_inicio = null;
			Date dt_termino = null;
			Date dt_entrega_chave = null;
			Date dt_reajuste = null;

			dt_inicio = ConverterData.stringToDateSqlBD(util.converterData(request.getParameter("dt_inicio")));
			dt_termino = ConverterData.stringToDateSqlBD(util.converterData(request.getParameter("dt_termino")));
			dt_reajuste = ConverterData.stringToDateSqlBD(util.converterData(request.getParameter("dt_reajuste")));

			String dia_vencimento = request.getParameter("dia_vencimento");

			if (request.getParameter("dt_entrega_chave") != "") {
				dt_entrega_chave = ConverterData
						.stringToDateSqlBD(util.converterData(request.getParameter("dt_entrega_chave")));
			}

			l = LocacaoFactory.criar(modalidade, valor_deposito, valor_seguro);
			c = ContratoFactory.criar(dt_inicio, dt_termino, dt_reajuste, dt_entrega_chave,
					Integer.valueOf(dia_vencimento));

			l.setImovel(ImovelFactory.recuperarPorId(Integer.valueOf(id_imovel)));
			l.setLocatario(PessoaFactory.recuperarPorID(Integer.valueOf(id_locatario)));
			l.setContrato(c);

			ListaInfoFiador lif = new ListaInfoFiador();

			if (modalidade.equals("Fiador")) {
				if (!request.getParameter("fiador").equalsIgnoreCase("0")) {
					id_fiador = request.getParameter("fiador");
					mat_imovel = request.getParameter("mat_imovel");
					cartorio = request.getParameter("cartorio");
					endereco_imovel = request.getParameter("endereco_imovel");

					InfoFiador inf = InfoFiadorFactory.criar(mat_imovel, cartorio, endereco_imovel,
							Integer.valueOf(id_fiador));

					System.out.println(inf);

					lif.add(inf);
				}

				if (!request.getParameter("fiador2").equalsIgnoreCase("0")) {
					id_fiador2 = request.getParameter("fiador2");
					mat_imovel2 = request.getParameter("mat_imovel2");
					cartorio2 = request.getParameter("cartorio2");
					endereco_imovel2 = request.getParameter("endereco_imovel2");

					InfoFiador inf2 = InfoFiadorFactory.criar(mat_imovel2, cartorio2, endereco_imovel2,
							Integer.valueOf(id_fiador2));

					//System.out.println(inf2);

					lif.add(inf2);
				}

				if (!request.getParameter("fiador3").equalsIgnoreCase("0")) {
					id_fiador3 = request.getParameter("fiador3");
					mat_imovel3 = request.getParameter("mat_imovel3");
					cartorio3 = request.getParameter("cartorio3");
					endereco_imovel3 = request.getParameter("endereco_imovel3");

					InfoFiador inf3 = InfoFiadorFactory.criar(mat_imovel3, cartorio3, endereco_imovel3,
							Integer.valueOf(id_fiador3));

					//System.out.println(inf3);

					lif.add(inf3);
				}

				l.setListaInfoFiador(lif);
			}

			/*ListaItensLocacao lil = new ListaItensLocacao();

			for (char letra = 'A'; letra <= 'Q'; letra++) {

				String parc_tot = request.getParameter("tab" + letra + "numParcela");

				if (!parc_tot.isEmpty()) {

					String[] parcela = parc_tot.split("/");
					BigDecimal valor = new BigDecimal(
							utilitarios.util.stringToBigDecimal(request.getParameter("tab" + letra + "itemValor")));
					String rd = request.getParameter("tab" + letra + "rdStatus");
					String descricao = null;
					if (letra == 'J' || letra == 'N' || letra == 'O') {
						descricao = request.getParameter("tab" + letra + "descricaoItem");
					}

					ItensLocacao i = ItensLocacaoFactory.criar("tab" + letra, Integer.valueOf(letra)-64, descricao, ConverterData.getDateNow(),
							Integer.valueOf(parcela[0]), Integer.valueOf(parcela[1]), valor, rd, false);
					//foi editado ConverterData.getDateNowItens() e a flg_processado = false
					
					System.out.println(i);
					lil.add(i);
				}
			}*/
			l.getImovel().setFlg_disponivel(false);

			//l.setListaItensLocacao(lil);

			TransactionController tc = new TransactionController();
			tc.add(l);
			tc.salvar();

			if (l.isPersistente()) {
				setHistorico(request);
				u.atualizarAcesso(request.getParameter("nom_tela"), "ID: " + String.valueOf(l.getId()));
			}

			returnToPage(request, response, l.isPersistente());

		} catch (Exception ex) {
			returnToPage(request, response, false);
			ex.printStackTrace();

		}
	}

	protected void atualizar(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		try {
			Integer fk_id_locacao = Integer.valueOf(request.getParameter("idLocacaoHidden"));

			l = LocacaoFactory.recuperarPorId(fk_id_locacao);

			String id_imovel = request.getParameter("idImovelHidden");
			String id_locatario = request.getParameter("locatario");

			String id_fiador = "", mat_imovel = "", cartorio = "", endereco_imovel = "";
			String id_fiador2 = "", mat_imovel2 = "", cartorio2 = "", endereco_imovel2 = "";
			String id_fiador3 = "", mat_imovel3 = "", cartorio3 = "", endereco_imovel3 = "";

			Date dt_inicio = null;
			Date dt_termino = null;
			Date dt_entrega_chave = null;
			Date dt_reajuste = null;

			dt_inicio = ConverterData.stringToDateSqlBD(util.converterData(request.getParameter("dt_inicio")));
			dt_termino = ConverterData.stringToDateSqlBD(util.converterData(request.getParameter("dt_termino")));
			dt_reajuste = ConverterData.stringToDateSqlBD(util.converterData(request.getParameter("dt_reajuste")));

			String dia_vencimento = request.getParameter("dia_vencimento");

			if (request.getParameter("dt_entrega_chave") != "") {
				dt_entrega_chave = ConverterData
						.stringToDateSqlBD(util.converterData(request.getParameter("dt_entrega_chave")));

				l.getImovel().setFlg_disponivel(true);
			}

			l.getContrato().setDt_inicio(dt_inicio);
			l.getContrato().setDt_termino(dt_termino);
			l.getContrato().setDt_reajuste(dt_reajuste);
			l.getContrato().setDt_entrega_chave(dt_entrega_chave);
			l.getContrato().setDia_vencimento(Integer.valueOf(dia_vencimento));
			
			Imovel imovel_old = new Imovel();
			if (!l.getImovel().getId().equals(id_imovel)) {
				imovel_old = ImovelFactory.recuperarPorId(l.getImovel().getId());
				imovel_old.setFlg_disponivel(true);
				
				l.setImovel(ImovelFactory.recuperarPorId(Integer.valueOf(id_imovel)));
			}
			
			l.setLocatario(PessoaFactory.recuperarPorID(Integer.valueOf(id_locatario)));

			String modalidade = request.getParameter("modalidade");

			ListaInfoFiador lif = new ListaInfoFiador();

			l.setModalidade(request.getParameter("modalidade"));

			if (modalidade.equals("Deposito")) {
				l.setValor_deposito(request.getParameter("valor_deposito"));

			} else if (modalidade.equals("Seguro Fianca")) {
				l.setValor_seguro(request.getParameter("valor_seguro"));

			} else if (modalidade.equals("Fiador")) {
				if (!request.getParameter("fiador").equalsIgnoreCase("0")) {
					id_fiador = request.getParameter("fiador");
					mat_imovel = request.getParameter("mat_imovel");
					cartorio = request.getParameter("cartorio");
					endereco_imovel = request.getParameter("endereco_imovel");

					InfoFiador inf = InfoFiadorFactory.criar(mat_imovel, cartorio, endereco_imovel,
							Integer.valueOf(id_fiador));

					lif.add(inf);
				}

				if (!request.getParameter("fiador2").equalsIgnoreCase("0")) {
					id_fiador2 = request.getParameter("fiador2");
					mat_imovel2 = request.getParameter("mat_imovel2");
					cartorio2 = request.getParameter("cartorio2");
					endereco_imovel2 = request.getParameter("endereco_imovel2");

					InfoFiador inf2 = InfoFiadorFactory.criar(mat_imovel2, cartorio2, endereco_imovel2,
							Integer.valueOf(id_fiador2));
					lif.add(inf2);
				}

				if (!request.getParameter("fiador3").equalsIgnoreCase("0")) {
					id_fiador3 = request.getParameter("fiador3");
					mat_imovel3 = request.getParameter("mat_imovel3");
					cartorio3 = request.getParameter("cartorio3");
					endereco_imovel3 = request.getParameter("endereco_imovel3");

					InfoFiador inf3 = InfoFiadorFactory.criar(mat_imovel3, cartorio3, endereco_imovel3,
							Integer.valueOf(id_fiador3));

					lif.add(inf3);
				}

				lif.setId(fk_id_locacao);

				l.setListaInfoFiador(lif);
			}

			TransactionController tc = new TransactionController();
			tc.add(l);
			if(imovel_old!=null)
				tc.add(imovel_old);
			tc.salvar();

			if (l.isUpdated()) {
				setHistorico(request);
				u.atualizarAcesso(request.getParameter("nom_tela"), "ID: " + String.valueOf(l.getId()));
			}

			returnToPage(request, response, l.isUpdated());

		} catch (Exception ex) {
			returnToPage(request, response, false);
			ex.printStackTrace();

		}
	}
	
	

}