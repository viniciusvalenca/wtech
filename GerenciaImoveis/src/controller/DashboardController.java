package controller;

import javax.servlet.http.HttpServlet;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import dao.TransactionController;
import dominio.bean.entity.Contrato;
import dominio.bean.entity.Indice;
import dominio.bean.list.ListaHistoricoAcesso;
import dominio.bean.list.ListaLocacao;
import dominio.bean.list.ListaPessoa;
import dominio.factory.BoletoAbsiFactory;
import dominio.factory.ContratoFactory;
import dominio.factory.HistoricoAcessoFactory;
import dominio.factory.ImovelFactory;
import dominio.factory.IndiceFactory;
import dominio.factory.LocacaoFactory;
import dominio.factory.PessoaFactory;

@WebServlet({ "/dashboard", "/historicoAcesso", "/historicoContrato", "/renovarContrato", "/naoRenovarContrato"})

public class DashboardController extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	public DashboardController() {
		super();
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		option(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		option(request, response);
	}
	
	protected void option(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			String url = request.getServletPath();
			if (url.equalsIgnoreCase("/dashboard")) {
				informations(request, response);
			} else if (url.equalsIgnoreCase("/historicoAcesso")) {
				historicoAcesso(request, response);
			} else if (url.equalsIgnoreCase("/historicoContrato")) {
				historicoContrato(request, response);
			} else if (url.equalsIgnoreCase("/renovarContrato")) {
				renovarContrato(request, response);
			}else if (url.equalsIgnoreCase("/naoRenovarContrato")) {
				naoRenovarContrato(request, response);
			}
			else {
				response.sendRedirect("/");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	protected void informations(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		request.setCharacterEncoding("UTF-8");
		
		try {
			ListaPessoa ps = PessoaFactory.recuperarAniversariantes();
			ListaHistoricoAcesso had = HistoricoAcessoFactory.recuperarTodosDash();
			ListaLocacao listaLocacaoAvencer = LocacaoFactory.aVencer();
			ListaLocacao listaLocacaoVencido = LocacaoFactory.ctVencidoDash();
			
			Integer countLocador = PessoaFactory.countLocador();
			Integer countLocatario = PessoaFactory.countLocatario();
			Integer countFiador = PessoaFactory.countFiador();
			Integer countImoveis = ImovelFactory.countTodos();
			Indice indice = IndiceFactory.recuperarPorId(1);
			
			String lucroMes = BoletoAbsiFactory.recuperarLucroMes();

			if (ps != null) 
				request.setAttribute("aniversariantes", ps);
			
			if (had != null) 
				request.setAttribute("historico", had);
			
			if (listaLocacaoAvencer != null)
				request.setAttribute("aVencer", listaLocacaoAvencer);
				request.setAttribute("ctVencido", listaLocacaoVencido);
				request.setAttribute("countLocador", countLocador);
				request.setAttribute("countLocatario", countLocatario);
				request.setAttribute("countFiador", countFiador);
				request.setAttribute("countImoveis", countImoveis);
				request.setAttribute("countlucroMes", lucroMes);
				request.setAttribute("indice", indice.getIndice());
				
			request.getRequestDispatcher("dashboard.jsp").forward(request, response);

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	protected void historicoAcesso(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		response.setContentType("text/plain; charset=UTF-8");
		PrintWriter out = response.getWriter();
		Gson gson = new Gson();

		try {
			ListaHistoricoAcesso ha = HistoricoAcessoFactory.recuperarTodos();

			out.print(gson.toJson(ha));
			out.flush();
			out.close();

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	protected void historicoContrato(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		response.setContentType("text/plain; charset=UTF-8");
		PrintWriter out = response.getWriter();
		Gson gson = new Gson();

		try {
			ListaLocacao listaLocacaoVencido = LocacaoFactory.ctVencido();

			out.print(gson.toJson(listaLocacaoVencido));
			out.flush();
			out.close();

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	protected void renovarContrato(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		response.setContentType("text/plain; charset=UTF-8");
		PrintWriter out = response.getWriter();
		Gson gson = new Gson();

		try {
			
			System.out.println(request.getParameter("idContrato"));
			Integer idContrato = Integer.valueOf(request.getParameter("idContrato"));
			
			Contrato c = ContratoFactory.recuperarPorId(idContrato);
			c.setFlg_renovar(true);
			
			TransactionController tc = new TransactionController();
			tc.add(c);				
			tc.salvar();
			
			out.print(gson.toJson(c.isUpdated()));
			out.flush();
			out.close();

		} catch (Exception ex) {
			ex.printStackTrace();
			
			out.print(gson.toJson(false));
			out.flush();
			out.close();
		}
	}
	
	protected void naoRenovarContrato(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		response.setContentType("text/plain; charset=UTF-8");
		PrintWriter out = response.getWriter();
		Gson gson = new Gson();

		try {
			
			System.out.println(request.getParameter("idContrato"));
			Integer idContrato = Integer.valueOf(request.getParameter("idContrato"));
			
			Contrato c = ContratoFactory.recuperarPorId(idContrato);
			c.setFlg_renovar(false);
			
			TransactionController tc = new TransactionController();
			tc.add(c);
			tc.salvar();
			
			out.print(gson.toJson(c.isUpdated()));
			out.flush();
			out.close();

		} catch (Exception ex) {
			ex.printStackTrace();
			
			out.print(gson.toJson(false));
			out.flush();
			out.close();
		}
	}
	
}