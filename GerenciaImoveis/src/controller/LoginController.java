package controller;

import javax.servlet.http.HttpServlet;
import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.TransactionController;
import dominio.bean.entity.Usuario;
import dominio.factory.UsuarioFactory;

@WebServlet({ "/acesso", "/logout" })

public class LoginController extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public LoginController() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		option(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		option(request, response);
	}

	protected void option(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			String url = request.getServletPath();
			if (url.equalsIgnoreCase("/acesso")) {
				acesso(request, response);

			} else if (url.equalsIgnoreCase("/logout")) {
				logout(request, response);
			} else {
				response.sendRedirect("/");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	protected void logout(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setHeader("Cache-Control", "no-cache, no-store");
		response.setHeader("Pragma", "no-cache");

		request.getSession().invalidate();
		request.getRequestDispatcher("index.jsp").forward(request, response);
	}

	protected void acesso(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession(true);

		try {

			String login = request.getParameter("login");
			String senha = request.getParameter("senha");

			Usuario u;
			u = UsuarioFactory.recuperarPorLogin(login);

			if (u != null) {
				if (u.getSenha().equals(senha)) {

					session.setAttribute("user", u);

					u.setDt_acesso();
					TransactionController tc = new TransactionController();
					tc.add(u);
					tc.salvar();

					request.getRequestDispatcher("/dashboard").forward(request, response);
				} else {
					// senha errada
					request.getRequestDispatcher("index.jsp").forward(request, response);
				}

			} else {
				// usuario não encontrado
				request.getRequestDispatcher("index.jsp").forward(request, response);

			}

		} catch (SQLException ex) {
			ex.printStackTrace();

		}
	}

}