package controller;

import javax.servlet.http.HttpServlet;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.SQLException;
import java.text.ParseException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import dao.TransactionController;
import dominio.bean.entity.ImpostoRenda;
import dominio.bean.entity.Pessoa;
import dominio.bean.entity.Usuario;
import dominio.bean.list.ListaItensLocacao;
import dominio.factory.ImpostoRendaFactory;
import dominio.factory.ItensLocacaoFactory;
import dominio.factory.PessoaFactory;
import utilitarios.ConverterData;
import utilitarios.util;

@WebServlet({ "/atualizarIR", "/recuperarIR","/ajustarIR" })

public class ImpostoRendaController extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private HttpSession session;
	private Usuario u;

	public ImpostoRendaController() {
		super();
	}

	protected void setHistorico(HttpServletRequest request) throws IOException {
		session = ((HttpServletRequest) request).getSession(true);
		u = (Usuario) session.getAttribute("user");
	}

	protected void returnToPage(HttpServletRequest request, HttpServletResponse response, Boolean resposta)
			throws IOException {
		PrintWriter out = response.getWriter();
		Gson gson = new Gson();

		out.print(gson.toJson(resposta));
		out.flush();
		out.close();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		option(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		option(request, response);
	}

	protected void option(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			String url = request.getServletPath();

			if (url.equalsIgnoreCase("/atualizarIR")) {
				atualizarIR(request, response);

			} else if (url.equalsIgnoreCase("/recuperarIR")) {
				recuperarIR(request, response);

			} else if (url.equalsIgnoreCase("/ajustarIR")) {
				ajustarIR(request, response);

			}else {
				response.sendRedirect("/");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void atualizarIR(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {

			String mes_ref = request.getParameter("mes_referencia");
			java.sql.Date mes_RefConvertida = ConverterData.createDateFromMesRef(mes_ref);
			System.out.println(mes_RefConvertida);
			
			Integer idlocador = Integer.valueOf(request.getParameter("idLocadorHidden"));
			
			String strvalorir = request.getParameter("imposto_renda");
			
			BigDecimal valorIR = new BigDecimal(
					utilitarios.util.stringToBigDecimal(strvalorir));
			
			ImpostoRenda ir = ImpostoRendaFactory.recuperarPorLocador(idlocador, mes_RefConvertida);
			ir = ir != null ? ir : (new ImpostoRenda());
			
			Pessoa locador = PessoaFactory.recuperarPorID(idlocador);
			ir.setValor_desconto(valorIR);
			ir.setLocador(locador);
			ir.setMes_ref(mes_RefConvertida);
			
			boolean persit = ir.isPersistente();
					
			TransactionController tc = new TransactionController();
			tc.add(ir);
			tc.salvar();
			
			if(persit){
				if (ir.isUpdated()) {
					setHistorico(request);
					u.atualizarAcesso(request.getParameter("nom_tela"),
							"Locador ID: " + String.valueOf(idlocador) + " Mês de Referência: " + mes_ref + ", Valor: " + strvalorir);
				}
			}else{
				if (ir.isPersistente()) {
					persit = true;
					setHistorico(request);
					u.atualizarAcesso(request.getParameter("nom_tela"),
							"Locador ID: " + String.valueOf(idlocador) + " Mês de Referência: " + mes_ref + ", Valor: " + strvalorir);
				}
			}
			
			returnToPage(request, response, persit);
			
		} catch (SQLException | NumberFormatException | ParseException e) {
			e.printStackTrace();
		}
	}
	
	private void ajustarIR(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {		
			
			System.out.println(request.getParameter("mes_refFormat"));
			System.out.println(request.getParameter("valor_aluguel"));
			System.out.println(request.getParameter("valor_txadm"));
			BigDecimal ajusteAluguel;
			BigDecimal ajusteTxadm;			
			if(!request.getParameter("ajuste_aluguel").equals("0")) {
				 ajusteAluguel = new BigDecimal(request.getParameter("ajuste_aluguel"));
			}else {
				 ajusteAluguel = new BigDecimal(0);
			}
			if(!request.getParameter("ajuste_txadm").equals("0")) {
				 ajusteTxadm = new BigDecimal(request.getParameter("ajuste_txadm"));
			}else {
				 ajusteTxadm = new BigDecimal(0);
			}
			
			
			String idLocacao = request.getParameter("fk_id_locacao");
			String idLocador = request.getParameter("fk_id_locacador");
			
			String mes_ref = request.getParameter("mes_refFormat");
			
			java.sql.Date mes_RefConvertida = ConverterData.createDateFromMesRef2(mes_ref);
			
			System.out.println(mes_RefConvertida);
			System.out.println(idLocacao);
			System.out.println(idLocador);
			
			//String strvalorir = request.getParameter("imposto_renda");
			
			//BigDecimal valorIR = new BigDecimal(
					//utilitarios.util.stringToBigDecimal(strvalorir));
			
			ImpostoRenda ir = ImpostoRendaFactory.recuperarPorLocadorLocacao(Integer.valueOf(idLocador), mes_RefConvertida, Integer.valueOf(idLocacao));
			ir = ir != null ? ir : (new ImpostoRenda());
			
			Pessoa locador = PessoaFactory.recuperarPorID(Integer.valueOf(idLocador));			
			ir.setLocador(locador);
			ir.setMes_ref(mes_RefConvertida);
			ir.setFk_id_locacao(Integer.valueOf(idLocacao));
			ir.setAjuste_aluguel(ajusteAluguel);
			ir.setAjuste_txadm(ajusteTxadm);
			
			boolean persit = ir.isPersistente();
			
			 System.out.println(ir.toString());
		
			TransactionController tc = new TransactionController();
			tc.add(ir);
			tc.salvar();
			
			if(persit){
				if (ir.isUpdated()) {
					setHistorico(request);
					u.atualizarAcesso(request.getParameter("nom_tela"),
							"Locador ID: " + String.valueOf(Integer.valueOf(idLocador)) + " Mês de Referência: " + mes_ref + ", Valor: "  );
				}
			}else{
				if (ir.isPersistente()) {
					persit = true;
					setHistorico(request);
					u.atualizarAcesso(request.getParameter("nom_tela"),
							"Locador ID: " + String.valueOf(Integer.valueOf(idLocador)) + " Mês de Referência: " + mes_ref + ", Valor: "  );
				}
			}
			
			returnToPage(request, response, persit);
			
		} catch (SQLException | NumberFormatException | ParseException e) {
			e.printStackTrace();
		}
	}
	
	private void recuperarIR(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, ParseException {

		String mes_ref = request.getParameter("mes_referencia");
		java.sql.Date mes_RefConvertida = ConverterData.createDateFromMesRef(mes_ref);
		Integer idlocador = Integer.valueOf(request.getParameter("idLocadorHidden"));
		
		response.setContentType("text/plain; charset=UTF-8");
		PrintWriter out = response.getWriter();
		Gson gson = new Gson();
		
		try {
			
			ImpostoRenda ir = ImpostoRendaFactory.recuperarPorLocador(idlocador, mes_RefConvertida);
		
			out.print(gson.toJson(ir));
			out.flush();
			out.close();

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	
	

}