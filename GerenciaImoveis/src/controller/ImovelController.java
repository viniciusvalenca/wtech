package controller;

import javax.servlet.http.HttpServlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import dao.TransactionController;
import dominio.bean.entity.Endereco;
import dominio.bean.entity.Imovel;
import dominio.bean.entity.Usuario;
import dominio.bean.list.ListaImovel;
import dominio.bean.list.ListaPessoa;
import dominio.factory.EnderecoFactory;
import dominio.factory.ImovelFactory;
import dominio.factory.PessoaFactory;
import utilitarios.util;

@WebServlet({ "/cadastrarImovel", "/cadimovel", "/atualizarImovel", "/listarImovel", "/buscarImovel", "/listarImoveisDisponiveis", "/listarImovelId" })

public class ImovelController extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private Imovel i;
	private Endereco e;
	private HttpSession session;
	private Usuario u;

	public ImovelController() {
		super();
	}

	protected void setHistorico(HttpServletRequest request) throws IOException {
		session = ((HttpServletRequest) request).getSession(true);
		u = (Usuario) session.getAttribute("user");
	}

	protected void returnToPage(HttpServletRequest request, HttpServletResponse response, Boolean resposta)
			throws IOException {
		PrintWriter out = response.getWriter();
		Gson gson = new Gson();

		out.print(gson.toJson(resposta));
		out.flush();
		out.close();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		option(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		option(request, response);

	}

	protected void option(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			String url = request.getServletPath();
			if (url.equalsIgnoreCase("/cadimovel")) {
				cad_imovel(request, response);

			} else if (url.equalsIgnoreCase("/cadastrarImovel")) {
				cadastrar(request, response);

			} else if (url.equalsIgnoreCase("/atualizarImovel")) {
				atualizar(request, response);

			} else if (url.equalsIgnoreCase("/listarImovel")) {
				listarImoveis(request, response);

			} else if (url.equalsIgnoreCase("/listarImoveisDisponiveis")) {
				listarImoveisDisponiveis(request, response);

			}  else if (url.equalsIgnoreCase("/listarImovelId")) {
				listarImovelId(request, response);

			}  else if (url.equalsIgnoreCase("/buscarImovel")) {
				buscarImovel(request, response);

			} else {
				response.sendRedirect("/");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void listarImoveis(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		ListaImovel li;

		response.setContentType("text/plain; charset=UTF-8");
		PrintWriter out = response.getWriter();
		Gson gson = new Gson();

		try {
			li = ImovelFactory.recuperarTodos();

			out.print(gson.toJson(li));
			out.flush();
			out.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
	
	private void listarImoveisDisponiveis(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		ListaImovel li;

		response.setContentType("text/plain; charset=UTF-8");
		PrintWriter out = response.getWriter();
		Gson gson = new Gson();

		try {
			li = ImovelFactory.recuperarTodosDisponiveis(true);
	
			out.print(gson.toJson(li));
			out.flush();
			out.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
	
	private void listarImovelId(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		Imovel i;

		response.setContentType("text/plain; charset=UTF-8");
		PrintWriter out = response.getWriter();
		Gson gson = new Gson();

		try {
			i = ImovelFactory.recuperarPorId(Integer.valueOf(request.getParameter("id")));
		
			out.print(gson.toJson(i));
			out.flush();
			out.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	private void cad_imovel(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ListaPessoa p;
		Integer id = 0;

		try {
			p = PessoaFactory.recuperarTodosLocador();
			id = ImovelFactory.proximoId();

			request.setAttribute("lista", p);
			request.setAttribute("id", id);

			request.getRequestDispatcher("imovel.jsp").forward(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	private void buscarImovel(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ListaPessoa p;

		try {
			p = PessoaFactory.recuperarTodosLocador();

			request.setAttribute("lista", p);

			request.getRequestDispatcher("buscar_imovel.jsp").forward(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	protected void cadastrar(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		try {

			Integer id_locador = Integer.valueOf(request.getParameter("locador"));

			String endereco = request.getParameter("endereco");
			String cep = request.getParameter("cep");
			String numero = request.getParameter("numero");
			String bairro = request.getParameter("bairro");
			String cidade = request.getParameter("cidade");
			String estado = request.getParameter("estado");
			String complemento = request.getParameter("complemento");
			String nom_condominio = request.getParameter("nom_condominio");

			Boolean flg_disponivel = Boolean.valueOf(request.getParameter("flg_disponivel"));

			String valor = request.getParameter("valor");
			String comissao = request.getParameter("comissao");
			String qtd_quartos = request.getParameter("qtd_quartos");
			String area = request.getParameter("area");

			String tipo = request.getParameter("tipo");
			String modalidade = request.getParameter("modalidade");
			String qtd_suites = request.getParameter("qtd_suites");

			String insc_iptu = request.getParameter("insc_iptu");
			String cod_logradouro = request.getParameter("cod_logradouro");
			String mat_cedae = request.getParameter("mat_cedae");
			String mat_light = request.getParameter("mat_light");
			String mat_ceg = request.getParameter("mat_ceg");
			String cbmerj = request.getParameter("cbmerj");

			i = ImovelFactory.criar(new BigDecimal(util.stringToBigDecimal(valor)), comissao, tipo, modalidade, qtd_quartos, qtd_suites, area, insc_iptu,
					cod_logradouro, mat_cedae, mat_light, mat_ceg, cbmerj, id_locador, flg_disponivel);

			e = EnderecoFactory.criar(endereco, bairro, cep, numero, estado, cidade, complemento, nom_condominio);

			if (i != null) {
				i.setEndereco(e);

				TransactionController tc = new TransactionController();
				tc.add(i);				
				tc.salvar();
				
				if (i.isPersistente()) {
					setHistorico(request);
					u.atualizarAcesso(request.getParameter("nom_tela"), "ID: " + String.valueOf(i.getId()));	
				}
				
				returnToPage(request, response, i.isPersistente());
				

			} else {
				returnToPage(request, response, false);
			}

		} catch (SQLException ex) {
			ex.printStackTrace();
			returnToPage(request, response, false);
		}
	}

	protected void atualizar(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		try {
			i = ImovelFactory.recuperarPorId(Integer.valueOf(request.getParameter("id")));

			i.setId_locador(Integer.valueOf(request.getParameter("locador")));

			i.getEndereco().setEndereco(request.getParameter("endereco"));
			i.getEndereco().setCep(request.getParameter("cep"));
			i.getEndereco().setNumero(request.getParameter("numero"));
			i.getEndereco().setBairro(request.getParameter("bairro"));
			i.getEndereco().setCidade(request.getParameter("cidade"));
			i.getEndereco().setEstado(request.getParameter("estado"));
			i.getEndereco().setComplemento(request.getParameter("complemento"));
			i.getEndereco().setNom_condominio(request.getParameter("nom_condominio"));

			i.setFlg_disponivel(Boolean.valueOf(request.getParameter("flg_disponivel")));

			i.setValor(new BigDecimal(util.stringToBigDecimal(request.getParameter("valor"))));
			i.setComissao(request.getParameter("comissao"));
			i.setQtd_quartos(request.getParameter("qtd_quartos"));
			i.setArea(request.getParameter("area"));

			i.setTipo(request.getParameter("tipo"));
			i.setModalidade(request.getParameter("modalidade"));
			i.setQtd_suites(request.getParameter("qtd_suites"));

			i.setInsc_iptu(request.getParameter("insc_iptu"));
			i.setCod_logradouro(request.getParameter("cod_logradouro"));
			i.setMat_cedae(request.getParameter("mat_cedae"));
			i.setMat_ligth(request.getParameter("mat_light"));
			i.setMat_ceg(request.getParameter("mat_ceg"));
			i.setCbmerj(request.getParameter("cbmerj"));

			TransactionController tc = new TransactionController();
			tc.add(i);				
			tc.salvar();
			
			if( i.isUpdated()){
				setHistorico(request);
				u.atualizarAcesso(request.getParameter("nom_tela"), "ID: " + String.valueOf(i.getId()));	
			}
			
			returnToPage(request, response, i.isUpdated());

		} catch (SQLException ex) {
			returnToPage(request, response, false);
			ex.printStackTrace();
		}
	}

}