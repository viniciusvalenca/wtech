package controller;

import javax.servlet.http.HttpServlet;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.ParseException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import dao.TransactionController;
import dominio.bean.entity.ItensLocacao;
import dominio.bean.entity.Usuario;
import dominio.bean.list.ListaItensLocacao;
import dominio.factory.ItensLocacaoFactory;
import utilitarios.ConverterData;

@WebServlet({ "/itenslocacao", "/atualizaritens", "/listitenslocacao" })

public class ItensLocacaoController extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private HttpSession session;
	private Usuario u;

	public ItensLocacaoController() {
		super();
	}

	protected void setHistorico(HttpServletRequest request) throws IOException {
		session = ((HttpServletRequest) request).getSession(true);
		u = (Usuario) session.getAttribute("user");
	}

	protected void returnToPage(HttpServletRequest request, HttpServletResponse response, Boolean resposta)
			throws IOException {
		PrintWriter out = response.getWriter();
		Gson gson = new Gson();

		out.print(gson.toJson(resposta));
		out.flush();
		out.close();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		option(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		option(request, response);
	}

	protected void option(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			String url = request.getServletPath();

			if (url.equalsIgnoreCase("/itenslocacao")) {
				request.getRequestDispatcher("editar_itens.jsp").forward(request, response);
			} else if (url.equalsIgnoreCase("/listitenslocacao")) {
				itensLocacao(request, response);
			} else if (url.equalsIgnoreCase("/atualizaritens")) {
				atualizaritens(request, response);
			} else {
				response.sendRedirect("/");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void atualizaritens(HttpServletRequest request, HttpServletResponse response) throws IOException {
		ListaItensLocacao iLocacao;

		try {
			ListaItensLocacao lil = new ListaItensLocacao();
			String mes_ref = request.getParameter("mes_referencia");
			
			java.sql.Date  mes_RefConvertida = ConverterData.createDateFromMesRef(mes_ref);
			Integer idLocacao = Integer.valueOf(request.getParameter("idLocacaoHidden"));
			
			//System.out.println(mes_ref);
			
			iLocacao = ItensLocacaoFactory.recuperarDoMes(mes_RefConvertida, idLocacao);

			//System.out.println(mes_RefConvertida);
			
			for (char letra = 'A'; letra <= 'V'; letra++) {
				
				String parc_tot = request.getParameter("tab" + letra + "numParcela");
				
				//RECUPERA O NUMERO 1, 2 OU 3 RESPECTIVAMENTE A SOMA, DIMINUI E FICA NO ESCRITÓRIO
				//VERIFICAR QNDO FOR NULL, O QUE IRÁ FAZER?
				//JSP NÃO TA COM OPÇÃO DEFAULT
				
				System.out.println(letra + " - " + request.getParameter("tab" + letra + "ck") + " parc_tot: " + parc_tot);
				
				if (!parc_tot.isEmpty()) {
					String mes_refBoleto = request.getParameter("tab" + letra + "mesRefBoleto");
					System.out.println(mes_refBoleto);
					String[] parcela = parc_tot.split("/");
					BigDecimal valor = new BigDecimal(
							utilitarios.util.stringToBigDecimal(request.getParameter("tab" + letra + "itemValor")));
					String rd = request.getParameter("tab" + letra + "rdStatus");
					String descricao = null;
					String status = request.getParameter("tab" + letra + "ck");
					if (letra == 'J' || letra == 'N' || letra == 'O' || letra == 'R' || letra == 'S' || letra == 'L') {
						descricao = request.getParameter("tab" + letra + "descricaoItem");
					}
					
					System.out.println("----- rd: " + rd + " status: " + status);

					ItensLocacao i = ItensLocacaoFactory.criar("tab" + letra, Integer.valueOf(letra) - 64, descricao, 
							mes_RefConvertida, Integer.valueOf(parcela[0]), Integer.valueOf(parcela[1]), valor, rd,status, false,mes_refBoleto);
					//foi editado ConverterData.getDateNowItens() e a flg_processado = false
					
					lil.add(i);
					System.out.println(i);
					//System.out.println("i.getValorRelatorio(): " + i.getValorRelatorio());
				}
			}
			System.out.println(lil);
			//Setando id locaçao e fk itens na locaçao não precisa no update. TRATAR!
			lil.setId(idLocacao);

			iLocacao.setItensLocacao(lil);
			TransactionController tc = new TransactionController();
			tc.add(iLocacao);
			tc.salvar();

			if (iLocacao.isUpdated()) {

				setHistorico(request);
				u.atualizarAcesso(request.getParameter("nom_tela"),
						"Locação ID: " + String.valueOf(idLocacao) + " Mês de Referência: " + mes_ref);
			}

			returnToPage(request, response, iLocacao.isUpdated());

		} catch (SQLException | NumberFormatException | ParseException e) {
			e.printStackTrace();
		}
	}

	private void itensLocacao(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException, ParseException {

		ListaItensLocacao iLocacao = null;

		Integer id_locacao = Integer.valueOf(request.getParameter("id_locacao"));
		String mes_ref = request.getParameter("mes_ref");
		java.sql.Date data = ConverterData.createDateFromMesRef(mes_ref);

		response.setContentType("text/plain; charset=UTF-8");
		PrintWriter out = response.getWriter();
		Gson gson = new Gson();
		
		try {
			String jsonItens;
			
			if(ItensLocacaoFactory.temItensLocacao(id_locacao)) {
			do{
				
				iLocacao = ItensLocacaoFactory.recuperarDoMes(data, id_locacao);
				
				data = ConverterData.decrementMesRef(data);
				
			}
			while(iLocacao == null || iLocacao.isEmpty() );
			}

			jsonItens = gson.toJson(iLocacao);
			System.out.println(jsonItens);
			System.out.println(gson.toJson(jsonItens));
			out.print(gson.toJson(jsonItens));
			out.flush();
			out.close();

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}