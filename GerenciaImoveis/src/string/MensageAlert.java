package string;

public class MensageAlert {
	 
	private final static String i = "<div id='alert' class='alert-warning alert-dismissible btn-xs' role='alert' style='margin-top: 15px !important;'>";
	private final static String f = "</div>";
	//private final static String b = "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>";
	private final static String alertPermission = "Voce n�o tem permissao para acessar essa pagina.";

	public MensageAlert() {
	}

	public static String getMsgAlertPermission() {
		return i + alertPermission + f;
	}
}
