package string;

public class MensageError {

	private final static String il = "<div id='alert' class='alert-danger alert-dismissible btn-xs' role='alert' style='margin-top: 15px !important;'>";
	private final static String i = "<div id='alert' class='alert alert-danger btn-xs' role='alert'>";
	private final static String f = "</div>";
	private final static String b = "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>";
	private final static String errorLogin = "Senha ou Matr&iacute;cula n&atilde;o conferem!";
	private final static String errorMensagem = "Sua Mensagem n&atilde;o foi enviada";
	private final static String errorCategoria = "Nova Categoria n&atilde;o foi salva";
	private final static String errorSituacao = "Nova Situac&atilde;o n&atilde;o foi salva";
	
	public MensageError(){}
	
	public static String getMsgErrorLogin(){
		return il + b + errorLogin + f;
	}
	
	public static String getMsgErrorMensagem(){
		return i + b + errorMensagem + f;
	}
	
	public static String getMsgErrorCategoria(){
		return i + b + errorCategoria + f;
	}
	
	public static String getMsgErrorSituacao(){
		return i + b + errorSituacao + f;
	}

}
