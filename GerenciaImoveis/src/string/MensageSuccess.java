package string;

public class MensageSuccess {

	private final static String i = "<div id='alert' class='alert alert-success alert-dismissible btn-xs' role='alert'>";
	private final static String f = "</div>";
	private final static String b = "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>";
	private final static String successMensagem = "Mensagem Enviada com Sucesso";
	private final static String successCategoria = "Categoria Cadastrada com Sucesso";
	private final static String successSituacao = "Situac&atilde;o Cadastrada com Sucesso";
	
	public MensageSuccess(){}
	
	public static String getMsgSuccessMensagem(){
		return i + b + successMensagem + f;
	}
	
	public static String getMsgSuccessCategoria(){
		return i + b + successCategoria + f;
	}
	
	public static String getMsgSuccessSituacao(){
		return i + b + successSituacao + f;
	}

}
