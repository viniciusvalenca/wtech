package boletos;

import java.math.BigDecimal;
import java.util.Date;

import org.jrimum.texgit.Record;

public class TransacaoTitulo {

	private Record registro;
	
	public enum MotivoConfirmacaoDeProtesto {
		DESPREZADO, ACEITO, DESCONHECIDO
	}

	public TransacaoTitulo(Record registro) {
		if (registro != null) {
			this.registro = registro;
		} else {
			throw new IllegalArgumentException("Registro de transação não foi informado.");
		}
	}
	
	public MotivoConfirmacaoDeProtesto getMotivoConfirmacaoDeProtesto() {
		Character motivo = registro.getValue("MotivoConfirmacaoDeProtesto");
		
		if (motivo != null) {
			if (motivo.equals('D')) {
				return MotivoConfirmacaoDeProtesto.DESPREZADO;
			} else if (motivo.equals('A')) {
				return MotivoConfirmacaoDeProtesto.ACEITO;
			}
		}
		
		return MotivoConfirmacaoDeProtesto.DESCONHECIDO;
	}

	public String getCodigoRegistro() {
		return registro.getValue("CodigoRegistro");
	}

	public String getInscricaoEmpresa() {
		return registro.getValue("InscricaoEmpresa");
	}

	public String getNumeroInscricao() {
		return registro.getValue("NumeroInscricao");
	}
	
	public String getFiller1() {
		return registro.getValue("Filler1");
	}

	public String getFiller2() {
		return registro.getValue("Filler2");
	}

	public String getFiller3() {
		return registro.getValue("Filler3");
	}

	public String getFiller4() {
		return registro.getValue("Filler4");
	}
	
	public String getNFiller5() {
		return registro.getValue("Filler5");
	}

	public String getFiller() {
		return registro.getValue("Filler");
	}

	public Integer getNossoNumero() {
		return Integer.valueOf(registro.getValue("NossoNumero"));
	}
	
	public String getNFiller6() {
		return registro.getValue("Filler6");
	}

	public String getCarteira() {
		return registro.getValue("Carteira");
	}
	
	public String getFiller7() {
		return registro.getValue("Filler7");
	}

	public String getFiller8() {
		return registro.getValue("Filler8");
	}

	public String getFiller9() {
		return registro.getValue("Filler9");
	}

	public String getVariacaoCarteira() {
		return registro.getValue("VariacaoCarteira");
	}

	public Integer getCodigoOcorrencia() {
		return registro.getValue("CodigoOcorrencia");
	}

	public Date getDataOcorrencia() {
		return registro.getValue("DataOcorrencia");
	}

	public String getFillerx() {
		return registro.getValue("Filler");
	}

	public String getSeuNumero() {
		return registro.getValue("SeuNumero");
	}

	public String getFiller10() {
		return registro.getValue("Filler10");
	}

	public String getFiller11() {
		return registro.getValue("Filler11");
	}

	public Date getDataVencimento() {
		return registro.getValue("DataVencimento");
	}

	public BigDecimal getValorTitulo() {
		return registro.getValue("ValorTitulo");
	}

	public String getCodigoBanco() {
		return registro.getValue("CodigoBanco");
	}

	public String getFiller12() {
		return registro.getValue("Filler12");
	}

	public String getFiller13() {
		return registro.getValue("Filler13");
	}
	
	public String getFiller14() {
		return registro.getValue("Filler14");
	}

	public BigDecimal getValorDespesaCobranca() {
		return registro.getValue("ValorDespesaCobranca");
	}

	public String getFiller15() {
		return registro.getValue("Filler15");
	}

	public BigDecimal getValorIOF() {
		return registro.getValue("ValorIOF");
	}

	public BigDecimal getValorAbatimento() {
		return registro.getValue("ValorAbatimento");
	}

	public BigDecimal getValorDesconto() {
		return registro.getValue("ValorDesconto");
	}

	public BigDecimal getValorPago() {
		return registro.getValue("ValorPago");
	}

	public BigDecimal getValorJuros() {
		return registro.getValue("ValorJuros");
	}

	public BigDecimal getValorOutrosCreditos() {
		return registro.getValue("ValorOutrosCreditos");
	}
	
	
	public String getFiller16() {
		return registro.getValue("Filler16");
	}

	public String getFiller17() {
		return registro.getValue("Filler17");
	}
	
	public String getDataCredito() {
		return registro.getValue("DataCredito");
	}
	
	public String getCodigoInstrucaoCancelada() {
		return registro.getValue("CodigoInstrucaoCancelada");
	}
	
	public String getFiller18() {
		return registro.getValue("Filler18");
	}
	
	public String getFiller19() {
		return registro.getValue("Filler19");
	}
	
	public String getNomeSacado() {
		return registro.getValue("NomeSacado");
	}
	
	public String getRegistrosRejeitados() {
		return registro.getValue("RegistrosRejeitados");
	}
	
	public String getFiller21() {
		return registro.getValue("Filler21");
	}
	
	public String getCodigoLiquidacao() {
		return registro.getValue("CodigoLiquidacao");
	}
	
	public String getNumeroSequencialRegistro() {
		return registro.getValue("NumeroSequencialRegistro");
	}
}
