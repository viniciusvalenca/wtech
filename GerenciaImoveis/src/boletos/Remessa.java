package boletos;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;

import org.jrimum.texgit.FlatFile;
import org.jrimum.texgit.Record;
import org.jrimum.texgit.Texgit;

import dominio.bean.entity.BoletoAbsi;
import dominio.bean.list.ListaBoletoAbsi;
import utilitarios.ConverterData;

public class Remessa {

	private static int sequenciaLinha;
	
	public static FlatFile<Record> GerarRemessa(ListaBoletoAbsi boletos, File file) throws IOException {

		FlatFile<Record> ff = Texgit.createFlatFile(file);
		
		sequenciaLinha++;

		try{
			ff.addRecord(createHeader(ff));
			
			for (BoletoAbsi bol : boletos) {
				sequenciaLinha++;
				ff.addRecord(createTransacaoTitulos(ff, bol));
			}
			
			sequenciaLinha++;
			ff.addRecord(createTrailer(ff));
			
			sequenciaLinha = 0;
			
		}catch (Exception e) {
			e.printStackTrace();
		}

		return ff;
	}

	private static Record createHeader(FlatFile<Record> ff) throws ParseException {
		Record header = ff.createRecord("Header");

		//header.setValue("Filler", 0);
		//header.setValue("CodigoDoRegistro", 0);
		//header.setValue("IdentificacaoRemessa", 1);
		//header.setValue("LiteralRemessa", "REMESSA");
		//header.setValue("CodigoServico", 01);
		//header.setValue("LiteralServico", "COBRANCA");
		header.setValue("CodigoEmpresa", "30500132813"); //retirei o 0 do inicio da agencia 
		header.setValue("NomeEmpresa", BoletoAbsi.CEDENTE);
		//header.setValue("CodigoBanco", 341);
		//header.setValue("NomeBanco", "BANCO ITAU SA");
		header.setValue("DataGravacao", ConverterData.getDateNow());
		//header.setValue("Filler", 0);
		header.setValue("NumeroSequencialRegistro", sequenciaLinha); //aqui seria um contador de remessa caso fossemos gerar várias remessas do dia

		return header;
	}

	private static Record createTransacaoTitulos(FlatFile<Record> ff, BoletoAbsi bol) {
		Record transacaoTitulos = ff.createRecord("TransacaoTitulo");

		//transacaoTitulos.setValue("CodigoRegistro", 1);
		transacaoTitulos.setValue("InscricaoEmpresa", 02); // 01 para CPF e 02 para CNPJ
		transacaoTitulos.setValue("NumeroInscricao", BoletoAbsi.cpf_cnpg_string); // CPF ou CPNJ da empresa
		transacaoTitulos.setValue("Agencia", BoletoAbsi.agencia_string);
		//transacaoTitulos.setValue("Filler", 11);
		transacaoTitulos.setValue("Conta", BoletoAbsi.numero_conta_string);
		transacaoTitulos.setValue("DigitoConta", 3);
		
		//transacaoTitulos.setValue("Filler", 4);
		//transacaoTitulos.setValue("CodigoInstrucaoCancelada", 0);
		transacaoTitulos.setValue("UsoEmpresa", bol.getNumero_documento());
		
		transacaoTitulos.setValue("NossoNumero", bol.getSeu_numero());
		//transacaoTitulos.setValue("MoedaVariavel", 12);
		transacaoTitulos.setValue("Carteira", BoletoAbsi.CARTEIRA);
		//transacaoTitulos.setValue("UsoBanco", 123);
		//transacaoTitulos.setValue("VariacaoCarteira", "I");
		//transacaoTitulos.setValue("CodigoOcorrencia", 1);
		
		transacaoTitulos.setValue("SeuNumero", bol.getSeuNumero());
		
		transacaoTitulos.setValue("Vencimento", bol.getDt_vencimentoBoleto());
		//transacaoTitulos.setValue("Vencimento", bol.getDt_vencimento());
		
		transacaoTitulos.setValue("ValorTitulo", bol.getValor());
		//transacaoTitulos.setValue("CodigoBanco", 341);
		//transacaoTitulos.setValue("AgenciaCobradora", 12345);
		
		//VER ESPECIE
		transacaoTitulos.setValue("EspecieTitulo", 1);
		
		transacaoTitulos.setValue("Aceite", "N");
		transacaoTitulos.setValue("DataEmissao", bol.getDt_documento());
		
		//CONFERIR COM O OUTRO BNK
		transacaoTitulos.setValue("PrimeiraInstrucao", 10);
		transacaoTitulos.setValue("SegundaInstrucao", 39);
		
		transacaoTitulos.setValue("ValorJuros", 0);
		
		transacaoTitulos.setValue("DataDesconto", bol.getDt_vencimentoBoleto());
		//transacaoTitulos.setValue("DataDesconto", bol.getDt_vencimento());
		
		//transacaoTitulos.setValue("ValorDesconto", 0);
		//transacaoTitulos.setValue("ValorIOF", 0);
		//transacaoTitulos.setValue("ValorAbatimento", 0);
		transacaoTitulos.setValue("TipoSacado", bol.getTipoSacado()); // 01 para CPF e 02 para CNPJ
		transacaoTitulos.setValue("DocumentoSacado", bol.getDocumentoSacado());
		transacaoTitulos.setValue("NomeSacado", bol.getNomeSacado());//transacaoTitulos.setValue("Filler", 123456);
		transacaoTitulos.setValue("EnderecoSacado", bol.getEnderecoSacado());
		transacaoTitulos.setValue("BairroSacado", bol.getBairroSacado());
		transacaoTitulos.setValue("CepSacado", bol.getCepSacado());
		transacaoTitulos.setValue("CidadeSacado", bol.getCidadeSacado());
		transacaoTitulos.setValue("EstadoSacado", bol.getLocacao().getLocatario().getEndereco().getEstado().toUpperCase());
		transacaoTitulos.setValue("SacadorAvalista", bol.getSacadorAvalista());

		//transacaoTitulos.setValue("Filler", 1);
		
		transacaoTitulos.setValue("DataMora", bol.getDt_vencimento());
		
		transacaoTitulos.setValue("DiasProtesto", 00);
		
		//transacaoTitulos.setValue("Filler", 1);
		transacaoTitulos.setValue("NumeroSequencialRegistro", sequenciaLinha);

		return transacaoTitulos;
	}

	private static Record createTrailer(FlatFile<Record> ff) {
		Record trailer = ff.createRecord("Trailler");
		//trailer.setValue("CodigoDoRegistro",9);
		//trailer.setValue("Filler", 393);
		trailer.setValue("NumeroSequencialRegistro", sequenciaLinha);
		
		return trailer;
	}
}
