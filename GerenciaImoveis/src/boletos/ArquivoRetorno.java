package boletos;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.jrimum.texgit.FlatFile;
import org.jrimum.texgit.Record;
import org.jrimum.texgit.Texgit;
import org.jrimum.utilix.ClassLoaders;

public class ArquivoRetorno {
	private final String nomeArquivoLayout = "layouts/retorno_itau.xml";
	private Cabecalho cabecalho;
	private List<TransacaoTitulo> transacoes;
	private Sumario sumario;
	
	private FlatFile<Record> ff;
	
	public ArquivoRetorno(File arquivo) throws IOException {
		carregarLayout();
		carregarLinhas(arquivo);
		carregarInformacoes();
	}

	private void carregarInformacoes() {
		this.cabecalho = new Cabecalho(this.ff.getRecord("Header"));
		this.sumario = new Sumario(this.ff.getRecord("Trailler"));
		
		Collection<Record> registroDeTransacoes = this.ff.getRecords("TransacaoTitulo");
		this.transacoes = new ArrayList<>(registroDeTransacoes.size());
		
		for (Record registro : registroDeTransacoes) {
			TransacaoTitulo transacaoTitulo = new TransacaoTitulo(registro);
			transacoes.add(transacaoTitulo);
			
			System.out.print("---------- getCodigoRegistro: " + transacaoTitulo.getCodigoRegistro() + "------------\n");
			System.out.print("getInscricaoEmpresa: " + transacaoTitulo.getInscricaoEmpresa() + "\n");
			System.out.print("getNumeroInscricao: " + transacaoTitulo.getNumeroInscricao() + "\n");
			System.out.print("getFiller1: " + transacaoTitulo.getFiller1() + "\n");
			System.out.print("getFiller2: " + transacaoTitulo.getFiller2() + "\n");
			System.out.print("getFiller3: " + transacaoTitulo.getFiller3() + "\n");
			System.out.print("getFiller4: " + transacaoTitulo.getFiller4() + "\n");
			System.out.print("getNFiller5: " + transacaoTitulo.getNFiller5() + "\n");
			System.out.print("getFiller: " + transacaoTitulo.getFiller() + "\n");
			System.out.print("getNossoNumero: " + transacaoTitulo.getNossoNumero() + "\n");
			System.out.print("getNFiller6: " + transacaoTitulo.getNFiller6() + "\n");
			System.out.print("getCarteira: " + transacaoTitulo.getCarteira() + "\n");
			System.out.print("getFiller7: " + transacaoTitulo.getFiller7() + "\n");
			System.out.print("getFiller8: " + transacaoTitulo.getFiller8() + "\n");
			System.out.print("getFiller9: " + transacaoTitulo.getFiller9() + "\n");
			System.out.print("getVariacaoCarteira: " + transacaoTitulo.getVariacaoCarteira() + "\n");
			System.out.print("getCodigoOcorrencia: " + transacaoTitulo.getCodigoOcorrencia() + "\n");
			System.out.print("getDataOcorrencia: " + transacaoTitulo.getDataOcorrencia() + "\n");
			System.out.print("getFillerx: " + transacaoTitulo.getFillerx() + "\n");
			System.out.print("getSeuNumero: " + transacaoTitulo.getSeuNumero() + "\n");
			System.out.print("getFiller10: " + transacaoTitulo.getFiller10()+ "\n");
			System.out.print("getFiller11: " + transacaoTitulo.getFiller11()+ "\n");
			System.out.print("getDataVencimento: " + transacaoTitulo.getDataVencimento() + "\n");
			System.out.print("getValorTitulo: " + transacaoTitulo.getValorTitulo() + "\n"); 
			System.out.print("getCodigoBanco: " + transacaoTitulo.getCodigoBanco() + "\n");
			System.out.print("getFiller12: " + transacaoTitulo.getFiller12() + "\n");
			System.out.print("getFiller13: " + transacaoTitulo.getFiller13() + "\n");
			System.out.print("getFiller14: " + transacaoTitulo.getFiller14() + "\n");
			System.out.print("getValorDespesaCobranca: " + transacaoTitulo.getValorDespesaCobranca() + "\n");
			System.out.print("getFiller15: " + transacaoTitulo.getFiller15() + "\n");
			System.out.print("getValorIOF: " + transacaoTitulo.getValorIOF() + "\n");
			System.out.print("getValorAbatimento: " + transacaoTitulo.getValorAbatimento() + "\n");
			System.out.print("getValorDesconto: " + transacaoTitulo.getValorDesconto() + "\n");
			System.out.print("getValorPago: " + transacaoTitulo.getValorPago() + "\n"); 
			System.out.print("getValorJuros: " + transacaoTitulo.getValorJuros() + "\n");
			System.out.print("getValorOutrosCreditos: " + transacaoTitulo.getValorOutrosCreditos() + "\n"); 
			System.out.print("getFiller16: " + transacaoTitulo.getFiller16() + "\n");
			System.out.print("getFiller17: " + transacaoTitulo.getFiller17() + "\n");
			System.out.print("getDataCredito: " + transacaoTitulo.getDataCredito() + "\n");
			System.out.print("getCodigoInstrucaoCancelada: " + transacaoTitulo.getCodigoInstrucaoCancelada() + "\n");
			System.out.print("getFiller18: " + transacaoTitulo.getFiller18() + "\n");
			System.out.print("getFiller19: " + transacaoTitulo.getFiller19() + "\n");
			System.out.print("getNomeSacado: " + transacaoTitulo.getNomeSacado() + "\n");
			System.out.print("getRegistrosRejeitados: " + transacaoTitulo.getRegistrosRejeitados() + "\n");
			System.out.print("getFiller21: " + transacaoTitulo.getFiller21() + "\n");
			System.out.print("getCodigoLiquidacao: " + transacaoTitulo.getCodigoLiquidacao() + "\n");
			System.out.print("getNumeroSequencialRegistro: " + transacaoTitulo.getNumeroSequencialRegistro() + "\n");
		}
	}

	private void carregarLinhas(File arquivo) {
		List<String> linhas;
		try {
			//linhas = FileUtils.readLines(arquivo);
			linhas = FileUtils.readLines(arquivo, "utf-8");
		} catch (IOException e) {
			throw new RuntimeException("Erro lendo linhas do arquivo de retorno", e);
		}
		this.ff.read(linhas);
	}

	private void carregarLayout() {
		InputStream in = ClassLoaders.getResourceAsStream(nomeArquivoLayout, this.getClass());
		this.ff = Texgit.createFlatFile(in);
	}
	
	public Cabecalho getCabecalho() {
		return cabecalho;
	}

	public List<TransacaoTitulo> getTransacoes() {
		return transacoes;
	}

	public Sumario getSumario() {
		return sumario;
	}

}
