package boletos;

import java.math.BigDecimal;
import java.util.Date;

import org.jrimum.texgit.Record;

public class Sumario {

	private Record registro;

	public Sumario(Record registro) {
		if (registro != null) {
			this.registro = registro;
		} else {
			throw new IllegalArgumentException("Registro de sumário não informado.");
		}
	}

	public Integer getCodigoRegistro() {
		return Integer.valueOf(registro.getValue("CodigoRegistro"));
	}

	public String getCodigoRetorno() {
		return registro.getValue("CodigoRetorno");
	}

	public Integer getCodigoBanco() {
		return Integer.valueOf(registro.getValue("CodigoBanco"));
	}

	public BigDecimal getQuantidadeTitulosEmCobranca() {
		return registro.getValue("QuantidadeTitulosEmCobranca");
	}

	public BigDecimal getValorTotalTitulos() {
		return registro.getValue("ValorTotalTitulos");
	}
	
	public String getFiller1() {
		return registro.getValue("Filler1");
	}

	public String getFiller2() {
		return registro.getValue("Filler2");
	}

	public String getFiller3() {
		return registro.getValue("Filler3");
	}

	public String getFiller4() {
		return registro.getValue("Filler4");
	}
	
	public String getNFiller5() {
		return registro.getValue("Filler5");
	}

	public String getNFiller6() {
		return registro.getValue("Filler6");
	}

	public String getFiller7() {
		return registro.getValue("Filler7");
	}

	public String getFiller8() {
		return registro.getValue("Filler8");
	}

	public String getFiller9() {
		return registro.getValue("Filler9");
	}
	
	public String getFiller10() {
		return registro.getValue("Filler10");
	}
	
	public String getFiller11() {
		return registro.getValue("Filler11");
	}
	
	public String getFiller12() {
		return registro.getValue("Filler12");
	}
	
	public String getFiller13() {
		return registro.getValue("Filler13");
	}
	
	public String getFiller14() {
		return registro.getValue("Filler14");
	}

	public String getFiller15() {
		return registro.getValue("Filler15");
	}
	
	public String getFiller16() {
		return registro.getValue("Filler16");
	}

	@Override
	public String toString() {
		return "Sumario [getCodigoRegistro()=" + getCodigoRegistro() + ", getCodigoRetorno()=" + getCodigoRetorno()
				+ ", getCodigoBanco()=" + getCodigoBanco() + ", getQuantidadeTitulosEmCobranca()="
				+ getQuantidadeTitulosEmCobranca() + ", getValorTotalTitulos()=" + getValorTotalTitulos()
				+ ", getFiller1()=" + getFiller1() + ", getFiller2()=" + getFiller2() + ", getFiller3()=" + getFiller3()
				+ ", getFiller4()=" + getFiller4() + ", getNFiller5()=" + getNFiller5() + ", getNFiller6()="
				+ getNFiller6() + ", getFiller7()=" + getFiller7() + ", getFiller8()=" + getFiller8()
				+ ", getFiller9()=" + getFiller9() + ", getFiller10()=" + getFiller10() + ", getFiller11()="
				+ getFiller11() + ", getFiller12()=" + getFiller12() + ", getFiller13()=" + getFiller13()
				+ ", getFiller14()=" + getFiller14() + ", getFiller15()=" + getFiller15() + ", getFiller16()="
				+ getFiller16() + "]";
	}

}
