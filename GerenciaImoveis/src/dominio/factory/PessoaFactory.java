package dominio.factory;

import java.sql.SQLException;
import java.sql.Date;

import dao.DAOFactory;
import dominio.bean.entity.Pessoa;
import dominio.bean.list.ListaPessoa;
import dominio.dao.ListaPessoaDAO;
import dominio.dao.PessoaDAO;

public class PessoaFactory {

	public static Pessoa criar() {
		return new Pessoa();
	}

	public static Pessoa criar(String nome_completo, String email, String email_opcional, String cpf, String cnpj,
			String identidade, Date dt_emissao, String emissor, String estado_civil, Date dt_nascimento,
			String nacionalidade, String naturalidade, String profissao, String telefones_extras, Boolean flg_locatario, Boolean flg_locador,
			Boolean flg_fiador) throws SQLException {

		Pessoa ps = new Pessoa(nome_completo, email, email_opcional, cpf, cnpj, identidade, dt_emissao, emissor,
				estado_civil, dt_nascimento, nacionalidade, naturalidade, profissao, telefones_extras, flg_locatario, flg_locador,
				flg_fiador);
		return ps;
	}

	public static Pessoa recuperarPorID(int id) throws SQLException {
		Pessoa p = new Pessoa();
		PessoaDAO pessoaDao = (PessoaDAO) DAOFactory.getDAO(p);

		pessoaDao.recuperarPorID(id);

		if (p.isPersistente())
			return p;
		else
			return null;
	}

	public static Pessoa recuperarPorNome(String nome) throws SQLException {
		Pessoa p = new Pessoa();
		PessoaDAO pessoaDao = (PessoaDAO) DAOFactory.getDAO(p);

		pessoaDao.recuperarPorNome(nome);

		if (p.isPersistente())
			return p;
		else
			return null;
	}

	public static Pessoa recuperarPorRg(String rg) throws SQLException {
		Pessoa p = new Pessoa();
		PessoaDAO pessoaDao = (PessoaDAO) DAOFactory.getDAO(p);

		pessoaDao.recuperarPorRg(rg);

		if (p.isPersistente())
			return p;
		else
			return null;
	}

	public static Pessoa recuperarPorCpf(String cpf) throws SQLException {
		Pessoa p = new Pessoa();
		PessoaDAO pessoaDao = (PessoaDAO) DAOFactory.getDAO(p);

		pessoaDao.recuperarPorCpf(cpf);

		if (p.isPersistente())
			return p;
		else
			return null;
	}
	
	public static Pessoa recuperarPorCnpj(String cnpj) throws SQLException {
		Pessoa obj = new Pessoa();
		PessoaDAO pessoaDao = (PessoaDAO) DAOFactory.getDAO(obj);

		pessoaDao.recuperarPorCnpj(cnpj);

		if (obj.isPersistente())
			return obj;
		else
			return null;
	}

	public static Integer proximoId() throws SQLException {
		Pessoa p = new Pessoa();
		PessoaDAO pessoaDao = (PessoaDAO) DAOFactory.getDAO(p);

		return pessoaDao.proximoId();
	}

	public static ListaPessoa recuperarTodos() throws SQLException {		
		ListaPessoa lista = new ListaPessoa();
		ListaPessoaDAO dao = (ListaPessoaDAO) DAOFactory.getDAO(lista);

		dao.recuperarTodos();

		return lista;
	}

	public static ListaPessoa recuperarTodosLocador() throws SQLException {
		ListaPessoa lista = new ListaPessoa();
		ListaPessoaDAO dao = (ListaPessoaDAO) DAOFactory.getDAO(lista);

		dao.recuperarTodosLocador();

		return lista;
	}

	public static ListaPessoa recuperarTodosLocatario() throws SQLException {
		ListaPessoa lista = new ListaPessoa();
		ListaPessoaDAO dao = (ListaPessoaDAO) DAOFactory.getDAO(lista);

		dao.recuperarTodosLocatario();

		return lista;
	}

	public static ListaPessoa recuperarTodosFiador() throws SQLException {
		ListaPessoa lista = new ListaPessoa();
		ListaPessoaDAO dao = (ListaPessoaDAO) DAOFactory.getDAO(lista);

		dao.recuperarTodosFiador();

		return lista;
	}
	
	public static ListaPessoa recuperar(Boolean flg_locatario, Boolean flg_locador, Boolean flg_fiador) throws SQLException {
		ListaPessoa lista = new ListaPessoa();
		ListaPessoaDAO dao = (ListaPessoaDAO) DAOFactory.getDAO(lista);
		
		dao.recuperar(flg_locatario, flg_locador, flg_fiador);

		return lista;
	}
	
	public static ListaPessoa recuperarAniversariantes() throws SQLException {
		ListaPessoa lista = new ListaPessoa();
		ListaPessoaDAO dao = (ListaPessoaDAO) DAOFactory.getDAO(lista);
		
		dao.recuperarAniversariantes();

		return lista;
	}
	
	public static Integer countLocador() throws SQLException{
		Pessoa p = new Pessoa();
		PessoaDAO pessoaDao = (PessoaDAO) DAOFactory.getDAO(p);
		
		Integer c = pessoaDao.countLocador();
		
		return c;
		
	}
	
	public static Integer countLocatario() throws SQLException{
		Pessoa p = new Pessoa();
		PessoaDAO pessoaDao = (PessoaDAO) DAOFactory.getDAO(p);
		
		Integer c = pessoaDao.countLocatario();
		
		return c;
		
	}
	
	public static Integer countFiador() throws SQLException{
		Pessoa p = new Pessoa();
		PessoaDAO pessoaDao = (PessoaDAO) DAOFactory.getDAO(p);
		
		Integer c = pessoaDao.countFiador();
		
		return c;
		
	}
	
	public static Integer countTodos() throws SQLException{
		Pessoa p = new Pessoa();
		PessoaDAO pessoaDao = (PessoaDAO) DAOFactory.getDAO(p);
		
		Integer c = pessoaDao.countTodos();
		
		return c;
		
	}

}
