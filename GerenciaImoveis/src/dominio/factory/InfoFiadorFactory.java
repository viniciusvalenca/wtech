package dominio.factory;

import java.sql.SQLException;

import dao.DAOFactory;
import dominio.bean.entity.InfoFiador;
import dominio.bean.list.ListaInfoFiador;
import dominio.dao.InfoFiadorDAO;
import dominio.dao.ListaInfoFiadorDAO;

public class InfoFiadorFactory {

    public static InfoFiador criar() {
        return new InfoFiador();
    }

    public static InfoFiador criar(String mat_imovel, String cartorio, String endereco_imovel, 
    		Integer fk_id_fiador) throws SQLException {
    	
    	InfoFiador obj = new InfoFiador(mat_imovel, cartorio, endereco_imovel, 
    			fk_id_fiador);

        return obj;
    }

    public static InfoFiador recuperarPorId(int id) throws SQLException {
    	InfoFiador obj = new InfoFiador();
    	InfoFiadorDAO dao = (InfoFiadorDAO) DAOFactory.getDAO(obj);

        dao.recuperarPorId(id);

        if (obj.isPersistente())
            return obj;
        else
            return null;
    }
    
    public static ListaInfoFiador recuperarPorLocacaoId(int id) throws SQLException {
    	ListaInfoFiador lista = new ListaInfoFiador();
    	ListaInfoFiadorDAO dao = (ListaInfoFiadorDAO) DAOFactory.getDAO(lista);
        
        dao.recuperarPorLocacaoId(id);

        return lista;
    }

    public static ListaInfoFiador  recuperarTodos() throws SQLException {
    	ListaInfoFiador lista = new ListaInfoFiador();
    	ListaInfoFiadorDAO dao = (ListaInfoFiadorDAO) DAOFactory.getDAO(lista);
        
        dao.recuperarTodos();

        return lista;
    }
    
}
