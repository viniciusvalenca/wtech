package dominio.factory;

import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;

import dao.DAOFactory;
import dominio.bean.entity.Locacao;
import dominio.bean.list.ListaLocacao;
import dominio.dao.ListaLocacaoDAO;
import dominio.dao.LocacaoDAO;

public class LocacaoFactory {

    public static Locacao criar() {
        return new Locacao();
    }

    public static Locacao criar(String modalidade, String valor_deposito, String valor_seguro) throws SQLException {
    	Locacao l = new Locacao(modalidade, valor_deposito, valor_seguro);

        return l;
    }

    public static Locacao recuperarPorId(int id) throws SQLException {
    	Locacao f = new Locacao();
    	LocacaoDAO dao = (LocacaoDAO) DAOFactory.getDAO(f);

        dao.recuperarPorId(id);

        if (f.isPersistente())
            return f;
        else
            return null;
    }

    public static ListaLocacao recuperarTodos() throws SQLException {
    	ListaLocacao lista = new ListaLocacao();
    	ListaLocacaoDAO dao = (ListaLocacaoDAO) DAOFactory.getDAO(lista);
        
        //dao.recuperarTodos();
    	dao.buscarTodasLocacoes();
    	
        return lista;
    }
    
    public static ListaLocacao recuperarTodosRelatorio(Date mes_ref) throws SQLException {
    	ListaLocacao lista = new ListaLocacao();
    	ListaLocacaoDAO dao = (ListaLocacaoDAO) DAOFactory.getDAO(lista);
        
    	dao.recuperarTodosRelatorio(mes_ref);
    	
        return lista;
    }
    
    public static ListaLocacao recuperarTodosLocacaoPagasRelatorio(Date mes_ref) throws SQLException {
    	ListaLocacao lista = new ListaLocacao();
    	ListaLocacaoDAO dao = (ListaLocacaoDAO) DAOFactory.getDAO(lista);
        
    	dao.recuperarTodosLocacaoPagasRelatorio(mes_ref);
    	
        return lista;
    }
    
    public static ListaLocacao recuperarTodosRelatorioAno(Date ano_ref) throws SQLException {
    	ListaLocacao lista = new ListaLocacao();
    	ListaLocacaoDAO dao = (ListaLocacaoDAO) DAOFactory.getDAO(lista);
        
    	dao.recuperarTodosRelatorioAno(ano_ref);
    	
    	
        return lista;
    }
    
    public static ListaLocacao recuperarLocacaoRemessa(Date mes_ref) throws SQLException {
    	ListaLocacao lista = new ListaLocacao();
    	ListaLocacaoDAO dao = (ListaLocacaoDAO) DAOFactory.getDAO(lista);
        
        dao.recuperarLocacaoRemessa(mes_ref);
        
        lista.setItensLocacao(mes_ref);

        return lista;
    }
    
    public static ListaLocacao recuperarLocacoesNaoPagas(Date mes_ref) throws SQLException {
    	ListaLocacao lista = new ListaLocacao();
    	ListaLocacaoDAO dao = (ListaLocacaoDAO) DAOFactory.getDAO(lista);
        
        dao.recuperarLocacoesNaoPagas(mes_ref);
        
        //lista.setItensLocacao(mes_ref);

        return lista;
    }
    
    
    public static Integer countTodosPorLocadorMes(int id) throws SQLException {
    	Locacao obj = new Locacao();
    	LocacaoDAO dao = (LocacaoDAO) DAOFactory.getDAO(obj);
        
    	Integer c = dao.countTodosPorLocadorMes(id);
		
		return c;
    }
    
    public static ListaLocacao recuperarTodosPorLocador(int id) throws SQLException {
    	ListaLocacao lista = new ListaLocacao();
    	ListaLocacaoDAO dao = (ListaLocacaoDAO) DAOFactory.getDAO(lista);
        
        dao.recuperarTodosPorLocador(id);

        return lista;
    }
    
    public static ListaLocacao recuperarTodosPorLocador(Integer id, ArrayList<Integer> array) throws SQLException {
    	ListaLocacao lista = new ListaLocacao();
    	ListaLocacaoDAO dao = (ListaLocacaoDAO) DAOFactory.getDAO(lista);
    	
        dao.recuperarTodosPorLocador(id, array);

        return lista;
    }
    
    public static ListaLocacao aVencer() throws SQLException {
    	ListaLocacao lista = new ListaLocacao();
    	ListaLocacaoDAO dao = (ListaLocacaoDAO) DAOFactory.getDAO(lista);
        
        dao.aVencer();

        return lista;
    }
    
    public static ListaLocacao ctVencidoDash() throws SQLException {
    	ListaLocacao lista = new ListaLocacao();
    	ListaLocacaoDAO dao = (ListaLocacaoDAO) DAOFactory.getDAO(lista);
        
        dao.ctVencidoDash();

        return lista;
    }
    
    public static ListaLocacao ctVencido() throws SQLException {
    	ListaLocacao lista = new ListaLocacao();
    	ListaLocacaoDAO dao = (ListaLocacaoDAO) DAOFactory.getDAO(lista);
        
        dao.ctVencido();

        return lista;
    }
}
