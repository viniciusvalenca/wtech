package dominio.factory;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.SQLException;

import dao.DAOFactory;
import dominio.bean.entity.ItensLocacao;
import dominio.bean.list.ListaItensLocacao;
import dominio.dao.ListaItensLocacaoDAO;
import utilitarios.ConverterData;

public class ItensLocacaoFactory {

    public static ItensLocacao criar() {
        return new ItensLocacao();
    }

    public static ItensLocacao criar(String tipo, Integer ordem, String descricao, Date mes_ref, Integer parcela,
			Integer tot_parcela, BigDecimal valor, String rd,String status, Boolean flg_processado, String mes_refBoleto) throws SQLException {
    	
    	ItensLocacao obj = new ItensLocacao(tipo, ordem, descricao, mes_ref, parcela,
    			tot_parcela, valor, rd,status, flg_processado,mes_refBoleto);

        return obj;
    }
    
    public static ListaItensLocacao recuperarDoMes(Date mes_ref, int id) throws SQLException {
    	ListaItensLocacao lista = new ListaItensLocacao();
    	ListaItensLocacaoDAO dao = (ListaItensLocacaoDAO) DAOFactory.getDAO(lista);
        
        dao.recuperarDoMes(mes_ref, id);

        return lista;
    }

    public static ListaItensLocacao recuperarTodos() throws SQLException {
    	ListaItensLocacao lista = new ListaItensLocacao();
    	ListaItensLocacaoDAO dao = (ListaItensLocacaoDAO) DAOFactory.getDAO(lista);
        
        dao.recuperarTodos();

        return lista;
    }
    
    public static Boolean temItensLocacao(Integer idLocacao) throws SQLException {
    	ListaItensLocacao lista = new ListaItensLocacao();
    	ListaItensLocacaoDAO dao = (ListaItensLocacaoDAO) DAOFactory.getDAO(lista);
        
        boolean retorno = dao.temItensLocacao(idLocacao);

        return retorno;
    }
    
}
