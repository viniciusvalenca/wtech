package dominio.factory;

import java.sql.SQLException;
import java.sql.Timestamp;

import dao.DAOFactory;
import dominio.bean.entity.Usuario;
import dominio.bean.list.ListaUsuario;
import dominio.dao.ListaUsuarioDAO;
import dominio.dao.UsuarioDAO;

public class UsuarioFactory {

	public static Usuario criar() {
		return new Usuario();
		
	}

	public static Usuario criar(String nome, Timestamp dt_acesso, String senha, String login) throws SQLException {

		Usuario u = new Usuario(nome, dt_acesso, senha, login);
		return u;
	}
	
	public static Usuario recuperarPorLogin(String login) throws SQLException {
		Usuario p = new Usuario();
		UsuarioDAO usuarioDao = (UsuarioDAO) DAOFactory.getDAO(p);

		usuarioDao.recuperarPorLogin(login);

		if (p.isPersistente())
			return p;
		else
			return null;
	}

	public static Usuario recuperarPorId(Integer fk_id_usuario) throws SQLException {
		Usuario p = new Usuario();
		UsuarioDAO usuarioDao = (UsuarioDAO) DAOFactory.getDAO(p);

		usuarioDao.recuperarPorId(fk_id_usuario);

		if (p.isPersistente())
			return p;
		else
			return null;
	}

	public static Integer proximoId() throws SQLException {
		Usuario p = new Usuario();
		UsuarioDAO usuarioDao = (UsuarioDAO) DAOFactory.getDAO(p);

		return usuarioDao.proximoId();
	}

	public static ListaUsuario recuperarTodos() throws SQLException {		
		ListaUsuario lista = new ListaUsuario();
		ListaUsuarioDAO dao = (ListaUsuarioDAO) DAOFactory.getDAO(lista);

		dao.recuperarTodos();

		return lista;
	}
	
	
}
