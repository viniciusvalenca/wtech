package dominio.factory;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.SQLException;

import dao.DAOFactory;
import dominio.bean.entity.ImpostoRenda;
import dominio.bean.entity.Pessoa;
import dominio.dao.ImpostoRendaDAO;

public class ImpostoRendaFactory {
	public static ImpostoRenda criar() {
		return new ImpostoRenda();
	}

	public static ImpostoRenda criar(Date mes_ref, BigDecimal valor_desconto, Pessoa locador) throws SQLException {

		ImpostoRenda tel = new ImpostoRenda(mes_ref, valor_desconto, locador);
		return tel;
	}
	
	public static ImpostoRenda recuperarPorLocador(int idlocador, Date mes_ref) throws SQLException{
		ImpostoRenda ir = new ImpostoRenda();
		ImpostoRendaDAO dao = (ImpostoRendaDAO) DAOFactory.getDAO(ir);

        dao.recuperarPorLocador(idlocador, mes_ref);

        if (ir.isPersistente())
            return ir;
        else
            return null;
	}
	
	public static ImpostoRenda recuperarPorLocadorSemLocacao(int idlocador, Date mes_ref) throws SQLException{
		ImpostoRenda ir = new ImpostoRenda();
		ImpostoRendaDAO dao = (ImpostoRendaDAO) DAOFactory.getDAO(ir);

        dao.recuperarPorLocadorSemLocacao(idlocador, mes_ref);

        if (ir.isPersistente())
            return ir;
        else
            return null;
	}
	
	public static ImpostoRenda recuperarPorLocadorLocacao(int idlocador, Date mes_ref, int idlocacao) throws SQLException{
		ImpostoRenda ir = new ImpostoRenda();
		ImpostoRendaDAO dao = (ImpostoRendaDAO) DAOFactory.getDAO(ir);

        dao.recuperarPorLocadorLocacao(idlocador, mes_ref, idlocacao);

        if (ir.isPersistente())
            return ir;
        else
            return null;
	}
}
