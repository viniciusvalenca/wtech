package dominio.factory;

import java.sql.SQLException;
import java.sql.Timestamp;

import dao.DAOFactory;
import dominio.bean.entity.HistoricoAcesso;
import dominio.bean.entity.Usuario;
import dominio.bean.list.ListaHistoricoAcesso;
import dominio.dao.ListaHistoricoAcessoDAO;

public class HistoricoAcessoFactory {

	public static HistoricoAcesso criar() {
		return new HistoricoAcesso();
	}

	public static HistoricoAcesso criar(Timestamp dt_modificacao, String nom_tela, String descricao, Usuario usuario) throws SQLException {
		HistoricoAcesso h = new HistoricoAcesso(dt_modificacao, nom_tela, descricao, usuario);
		return h;
	}
	
	public static ListaHistoricoAcesso recuperarTodos() throws SQLException {
		ListaHistoricoAcesso lista = new ListaHistoricoAcesso();
		ListaHistoricoAcessoDAO dao = (ListaHistoricoAcessoDAO) DAOFactory.getDAO(lista);

		dao.recuperarTodos();

		return lista;
	}
	
	public static ListaHistoricoAcesso recuperarTodosDash() throws SQLException {
		ListaHistoricoAcesso lista = new ListaHistoricoAcesso();
		ListaHistoricoAcessoDAO dao = (ListaHistoricoAcessoDAO) DAOFactory.getDAO(lista);

		dao.recuperarTodosDash();

		return lista;
	}

}
