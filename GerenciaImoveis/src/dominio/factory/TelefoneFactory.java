package dominio.factory;

import java.sql.SQLException;

import dao.DAOFactory;
import dominio.bean.entity.Telefone;
import dominio.bean.list.ListaTelefone;
import dominio.dao.ListaTelefoneDAO;

public class TelefoneFactory {
	public static Telefone criar() {
		return new Telefone();
	}
	
	public static Telefone criar(Integer num_telefone, Integer ddd)
			throws SQLException {
		
		Telefone tel = new Telefone( ddd, num_telefone);
		return tel;
	}
	
	
	 public static ListaTelefone  recuperarPorPessoaId(int id)throws SQLException {
	        ListaTelefone lista = new ListaTelefone();
	        ListaTelefoneDAO dao = (ListaTelefoneDAO) DAOFactory.getDAO(lista);
	        
	        dao.recuperarPorPessoaId(id);

	        return lista;
	    }
}
