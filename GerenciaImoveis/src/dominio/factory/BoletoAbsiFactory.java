package dominio.factory;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Calendar;
import java.sql.Date;

import dao.DAOFactory;
import dominio.bean.entity.BoletoAbsi;
import dominio.bean.entity.Locacao;
import dominio.bean.list.ListaBoletoAbsi;
import dominio.dao.BoletoAbsiDAO;
import dominio.dao.ListaBoletoAbsiDAO;

public class BoletoAbsiFactory {

	public static BoletoAbsi criar() {
		return new BoletoAbsi();
	}

	public static BoletoAbsi criar(String numero_documento, BigDecimal valor, Date mes_ref, Date dt_documento,
			Date dt_vencimento, String seu_numero, Boolean flg_enviado, Locacao locacao) throws SQLException {

		BoletoAbsi obj = new BoletoAbsi(numero_documento, valor, mes_ref, dt_documento, dt_vencimento, seu_numero,
				flg_enviado, locacao);
		return obj;
	}

	public static BoletoAbsi recuperarPorID(int id) throws SQLException {
		BoletoAbsi obj = new BoletoAbsi();
		BoletoAbsiDAO dao = (BoletoAbsiDAO) DAOFactory.getDAO(obj);

		dao.recuperarPorID(id);

		if (obj.isPersistente())
			return obj;
		else
			return null;
	}

	public static BoletoAbsi recuperarPorIdLocacao(int id) throws SQLException {
		BoletoAbsi obj = new BoletoAbsi();
		BoletoAbsiDAO dao = (BoletoAbsiDAO) DAOFactory.getDAO(obj);

		dao.recuperarPorIdLocacao(id);

		if (obj.isPersistente())
			return obj;
		else
			return null;
	}

	public static BoletoAbsi recuperarPorNumDocumento(String numDoc) throws SQLException {
		BoletoAbsi obj = new BoletoAbsi();
		BoletoAbsiDAO dao = (BoletoAbsiDAO) DAOFactory.getDAO(obj);

		// dao.recuperarPorNumDocumento(numDoc);

		if (obj.isPersistente())
			return obj;
		else
			return null;
	}

	public static BoletoAbsi recuperarPorSeuNumero(Integer seuNumero) throws SQLException {
		BoletoAbsi obj = new BoletoAbsi();
		BoletoAbsiDAO dao = (BoletoAbsiDAO) DAOFactory.getDAO(obj);

		dao.recuperarPorSeuNumero(seuNumero);

		if (obj.isPersistente())
			return obj;
		else
			return null;
	}
	
	public static BoletoAbsi buscarBoletoLocacaoMes(Integer idlocacao, Date mes_ref) throws SQLException {
		BoletoAbsi obj = new BoletoAbsi();
		BoletoAbsiDAO dao = (BoletoAbsiDAO) DAOFactory.getDAO(obj);

		dao.buscarBoletoLocacaoMes(idlocacao, mes_ref);

		if (obj.isPersistente())
			return obj;
		else
			return null;
	}

	public static ListaBoletoAbsi recuperarBoletosNovos() throws SQLException {
		ListaBoletoAbsi lista = new ListaBoletoAbsi();
		ListaBoletoAbsiDAO dao = (ListaBoletoAbsiDAO) DAOFactory.getDAO(lista);

		dao.recuperarBoletosNovos();

		return lista;
	}
	
	public static ListaBoletoAbsi listarBoletosNovosPorMes(Date mes_ref) throws SQLException {
		ListaBoletoAbsi lista = new ListaBoletoAbsi();
		ListaBoletoAbsiDAO dao = (ListaBoletoAbsiDAO) DAOFactory.getDAO(lista);

		dao.listarBoletosNovosPorMes(mes_ref);

		return lista;
	}
	
	public static Integer proximoId() throws SQLException {
		BoletoAbsi obj = new BoletoAbsi();
		BoletoAbsiDAO dao = (BoletoAbsiDAO) DAOFactory.getDAO(obj);

		return dao.proximoId();
	}
	
	public static String recuperarLucroMes() throws SQLException {
		BoletoAbsi obj = new BoletoAbsi();
		BoletoAbsiDAO dao = (BoletoAbsiDAO) DAOFactory.getDAO(obj);

		return dao.recuperarLucroMes();
	}

	public static ListaBoletoAbsi recuperarBoletosNaoPagos(Date mes_ref) throws SQLException {
		ListaBoletoAbsi lista = new ListaBoletoAbsi();
		ListaBoletoAbsiDAO dao = (ListaBoletoAbsiDAO) DAOFactory.getDAO(lista);

		dao.recuperarBoletosNaoPagos(mes_ref);

		return lista;
	}
	
    
    public static ListaBoletoAbsi recuperarLocacaoRemessa(Date mes_ref) throws SQLException {
    	ListaBoletoAbsi lista = new ListaBoletoAbsi();
    	ListaBoletoAbsiDAO dao = (ListaBoletoAbsiDAO) DAOFactory.getDAO(lista);
        
        dao.recuperarLocacaoRemessa(mes_ref);

        return lista;
    }
    
    public static ListaBoletoAbsi recuperarPagamento(Date dt) throws SQLException {
    	ListaBoletoAbsi lista = new ListaBoletoAbsi();
    	ListaBoletoAbsiDAO dao = (ListaBoletoAbsiDAO) DAOFactory.getDAO(lista);
    
    	dao.recuperarPagamentoDia(dt);
    	 
        return lista;
    }
    
    public static ListaBoletoAbsi recuperarPagamentoMes(Date mes_ref) throws SQLException {
    	ListaBoletoAbsi lista = new ListaBoletoAbsi();
    	ListaBoletoAbsiDAO dao = (ListaBoletoAbsiDAO) DAOFactory.getDAO(lista);
    	
    	dao.recuperarPagamentoMes(mes_ref);
    	
        return lista;
    }
    
    public static ListaBoletoAbsi recuperarPagamentoAno(Integer idlocacao,Date ano ) throws SQLException {
    	ListaBoletoAbsi lista = new ListaBoletoAbsi();
    	ListaBoletoAbsiDAO dao = (ListaBoletoAbsiDAO) DAOFactory.getDAO(lista);
    	
    	dao.recuperarPagamentoAno(idlocacao,ano);

        return lista;
    }

}
