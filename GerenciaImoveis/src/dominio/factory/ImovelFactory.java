package dominio.factory;

import java.math.BigDecimal;
import java.sql.SQLException;

import dao.DAOFactory;
import dominio.bean.entity.Imovel;
import dominio.bean.list.ListaImovel;
import dominio.dao.ImovelDAO;
import dominio.dao.ListaImovelDAO;

public class ImovelFactory {

    public static Imovel criar() {
        return new Imovel();
    }

    public static Imovel criar(BigDecimal valor, String comissao, String tipo, String modalidade, String qtd_quartos, String qtd_suites,
    		String area, String insc_iptu, String cod_logradouro, String mat_cedae, String mat_light, String mat_ceg, String cbmerj,
			Integer id_locador, Boolean flg_disponivel) throws SQLException {
        Imovel i = new Imovel(valor, comissao, tipo, modalidade, qtd_quartos, qtd_suites,
    			area, insc_iptu, cod_logradouro, mat_cedae, mat_light, mat_ceg, cbmerj, id_locador, flg_disponivel);

        return i;
    }

    public static Imovel recuperarPorId(int id) throws SQLException {
        Imovel f = new Imovel();
        ImovelDAO dao = (ImovelDAO) DAOFactory.getDAO(f);

        dao.recuperarPorId(id);

        if (f.isPersistente())
            return f;
        else
            return null;
    }

    public static ListaImovel  recuperarTodos()throws SQLException {
        ListaImovel lista = new ListaImovel();
        ListaImovelDAO dao = (ListaImovelDAO) DAOFactory.getDAO(lista);
        
        dao.recuperarTodos();

        return lista;
    }
    
    public static ListaImovel  recuperarTodosDisponiveis(Boolean flag)throws SQLException {
        ListaImovel lista = new ListaImovel();
        ListaImovelDAO dao = (ListaImovelDAO) DAOFactory.getDAO(lista);
        
        dao.recuperarTodosDisponiveis(flag);

        return lista;
    }
    
    public static Integer proximoId() throws SQLException {
    	 Imovel f = new Imovel();
         ImovelDAO dao = (ImovelDAO) DAOFactory.getDAO(f);

		return dao.proximoId();
	}
    
    public static Integer countTodos() throws SQLException {
   		Imovel f = new Imovel();
        ImovelDAO dao = (ImovelDAO) DAOFactory.getDAO(f);

		return dao.countTodos();
	}
}
