package dominio.factory;

import java.util.ArrayList;
import java.util.List;

import org.jrimum.bopepo.BancosSuportados;
import org.jrimum.bopepo.Boleto;
import org.jrimum.domkee.comum.pessoa.endereco.CEP;
import org.jrimum.domkee.comum.pessoa.endereco.Endereco;
import org.jrimum.domkee.comum.pessoa.endereco.UnidadeFederativa;
import org.jrimum.domkee.financeiro.banco.febraban.Agencia;
import org.jrimum.domkee.financeiro.banco.febraban.TipoDeMoeda;
import org.jrimum.domkee.financeiro.banco.febraban.Carteira;
import org.jrimum.domkee.financeiro.banco.febraban.Cedente;
import org.jrimum.domkee.financeiro.banco.febraban.ContaBancaria;
import org.jrimum.domkee.financeiro.banco.febraban.NumeroDaConta;
import org.jrimum.domkee.financeiro.banco.febraban.Sacado;
import org.jrimum.domkee.financeiro.banco.febraban.SacadorAvalista;
import org.jrimum.domkee.financeiro.banco.febraban.TipoDeTitulo;
import org.jrimum.domkee.financeiro.banco.febraban.Titulo;
import org.jrimum.domkee.financeiro.banco.febraban.Titulo.EnumAceite;

import dominio.bean.entity.BoletoAbsi;
import dominio.bean.list.ListaBoletoAbsi;
import dominio.bean.list.ListaItensLocacao;

public class BoletoFactory {

	private Boleto boleto;
	private ListaItensLocacao lil;

	public void receiveList(ListaBoletoAbsi lista) {
		for (BoletoAbsi bol : lista) {
			createDefaultBoleto(bol);
		}
	}

	public Boleto createDefaultBoleto(BoletoAbsi bol) {

		Cedente cedente = new Cedente(BoletoAbsi.CEDENTE, BoletoAbsi.CPF_CNPJ);

		Sacado sacado = new Sacado(bol.getLocacao().getLocatario().getNome_completo(),
				bol.getDocumentoLocatarioBoleto());

		String _estado, _cidade, _cep, _bairro, _endereco, _numero, _complemento;
		UnidadeFederativa uFederativa = UnidadeFederativa.RJ;

		_estado = bol.getLocacao().getImovel().getEndereco().getEstado();
		_cidade = bol.getLocacao().getImovel().getEndereco().getCidade();
		_cep = bol.getLocacao().getImovel().getEndereco().getCep();
		_bairro = bol.getLocacao().getImovel().getEndereco().getBairro();
		_endereco = bol.getLocacao().getImovel().getEndereco().getEndereco();
		_numero = bol.getLocacao().getImovel().getEndereco().getNumero();
		_complemento = bol.getLocacao().getImovel().getEndereco().getComplemento();
		
		if(_complemento!=null)
			_numero+= ", "+_complemento;
		
		System.out.println(_numero);

		switch (_estado) {
		case "RJ":
			uFederativa = UnidadeFederativa.RJ;
			break;
		}

		// Endereço do sacado
		Endereco endereco = new Endereco();
		endereco.setUF(uFederativa);
		endereco.setLocalidade(_cidade);
		endereco.setCep(new CEP(_cep));
		endereco.setBairro(_bairro);
		endereco.setLogradouro(_endereco);
		endereco.setNumero(_numero);
		

		sacado.addEndereco(endereco);

		SacadorAvalista sacadorAvalista = new SacadorAvalista(
				bol.getLocacao().getImovel().getLocador().getNome_completo(), bol.getDocumentoLocadorBoleto());

		// Criando o título
		ContaBancaria contaBancaria = new ContaBancaria(BancosSuportados.BANCO_ITAU.create());

		Agencia ag = new Agencia(305);

		// contaBancaria.setAgencia(new Agencia(BoletoAbsi.AGENCIA));
		contaBancaria.setAgencia(ag);

		// Código Benificiário ou Cedente
		contaBancaria.setNumeroDaConta(new NumeroDaConta(13281, BoletoAbsi.DIGITO_VERIFICADOR_CONTA));
		contaBancaria.setCarteira(new Carteira(BoletoAbsi.CARTEIRA));

		Titulo titulo = new Titulo(contaBancaria, sacado, cedente, sacadorAvalista);

		titulo.setNumeroDoDocumento(bol.getNumero_documento());
		titulo.setNossoNumero(bol.getSeu_numero());
		titulo.setDigitoDoNossoNumero(bol.getDigitoDoNossoNumero());

		titulo.setTipoDeMoeda(TipoDeMoeda.REAL);
		titulo.setValor(bol.getValor());
		titulo.setDataDoDocumento(bol.getDt_documento());
		titulo.setDataDoVencimento(bol.getDt_vencimento());

		titulo.setTipoDeDocumento(TipoDeTitulo.RA_RAMO_ATIVIDADE);
		titulo.setAceite(EnumAceite.N);

		boleto = new Boleto(titulo);
		
		boleto.addTextosExtras("txtRsAgenciaCodigoCedente","0305 / 13281-3");
		boleto.addTextosExtras("txtFcAgenciaCodigoCedente","0305 / 13281-3");
		
		boleto.addTextosExtras("txtRsEnvNome",sacado.getNome());
		boleto.addTextosExtras("txtRsEnvEndereco",endereco.getLogradouro()+" - "+endereco.getNumero());
		boleto.addTextosExtras("txtRsEnvBairroEstado",endereco.getBairro()+" / "+endereco.getUF());
		boleto.addTextosExtras("txtRsEnvCep",endereco.getCEP().getCep());

		boleto.setLocalPagamento(BoletoAbsi.LOCAL_PAGAMENTO);
		boleto.setInstrucaoAoSacado(BoletoAbsi.INSTRUCAO_AO_SACADO);

		boleto.setInstrucao1(BoletoAbsi.INSTRUCAO1);
		boleto.setInstrucao2(BoletoAbsi.INSTRUCAO2);
		boleto.setInstrucao3(BoletoAbsi.INSTRUCAO3);

		Integer size = bol.getLocacao().getListaItensLocacao().size();
		lil = bol.getLocacao().getListaItensLocacao();
		
		HistoricoDespesa historicoDespesa = new HistoricoDespesa();
		
		System.out.println("ListaItensLocacao");
		System.out.println(size);
		
		for (int i = 0; i < size; i++) {
			if(!lil.get(i).getRd().equals("N")){				
			
				historicoDespesa.add(lil.get(i).setNomeItem()+(lil.get(i).getDescricao() == null ? "" : " - " + lil.get(i).getDescricao())," R$" + lil.get(i).getValorString());
					
			}
		}

		boleto.addTextosExtras("txtRsHistoricoDespesaCabecalho", "HISTÓRICO DE DESPESAS");
		boleto.addTextosExtras("txtRsHistoricoDespesaDescricao", historicoDespesa.getDetalhamentoDescricao());
		boleto.addTextosExtras("txtRsHistoricoDespesaValor", historicoDespesa.getDetalhamentoValor());

		

		return boleto;
	}
}

	

/**
 * <p>
 * <code>HistoricoDespesa</code> agrupa as despesas a serem exibidas no boleto.
 * Sua principal idéia neste cenário é simplesmente facilitar o repasse dessas
 * informações para a área de textos extras do boleto.
 * </p>
 */
class HistoricoDespesa {

	private List<ItemHistoricoDespesa> itens;
	private final String NOVA_LINHA = "\n";
	
	HistoricoDespesa() {
		super();
		itens = new ArrayList<ItemHistoricoDespesa>();
	}
	
	void add(ItemHistoricoDespesa item) {
		itens.add(item);
	}
	
	void add(String descricao, String valor) {
		add(new ItemHistoricoDespesa(descricao, valor));
	}
	
	String getDetalhamentoDescricao() {
		return getDetalhamento("DESCRICAO", NOVA_LINHA);
	}

	String getDetalhamentoValor() {
		return getDetalhamento("VALOR", NOVA_LINHA);
	}
	
	private String getDetalhamento(String campo, String separador) {
		StringBuilder sb = new StringBuilder();
		for (ItemHistoricoDespesa item : itens) {
			if (campo.equals("DESCRICAO")) {
				sb.append(item.descricao + separador);				
			} else if (campo.equals("VALOR")) {
			sb.append(item.valor + separador);				
			}
		}
		return sb.toString();
		
	}
}

/**
 * <p>
 * <code>ItemHistoricoDespesa</code> representa cada registro de despesa que
 * será apresentado no boleto. Como a idéia é apenas repassar tais informações
 * para a área de textos extras do Boleto, todos os campos foram definidos como
 * <code>String</code>. Desta forma a formatação da informação fica por conta do
 * usuário desenvolvedor.
 * </p>
 */
class ItemHistoricoDespesa {

	
	final String descricao;
	final String valor;
	
	ItemHistoricoDespesa(String descricao, String valor) {
		super();		
		this.descricao = descricao;
		this.valor = valor;
	}
	
}

