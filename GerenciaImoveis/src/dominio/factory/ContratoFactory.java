package dominio.factory;

import java.sql.Date;
import java.sql.SQLException;

import dao.DAOFactory;
import dominio.bean.entity.Contrato;
import dominio.bean.list.ListaContrato;
import dominio.dao.ContratoDAO;
import dominio.dao.ListaContratoDAO;

public class ContratoFactory {

    public static Contrato criar() {
        return new Contrato();
    }

    public static Contrato criar(Date dt_inicio, Date dt_termino, Date dt_reajuste, Date dt_entrega_chave, Integer dia_vencimento) throws SQLException {
    	Contrato c = new Contrato(dt_inicio, dt_termino, dt_reajuste, dt_entrega_chave, dia_vencimento);

        return c;
    }

    public static Contrato recuperarPorId(int id) throws SQLException {
    	Contrato f = new Contrato();
    	ContratoDAO dao = (ContratoDAO) DAOFactory.getDAO(f);

        dao.recuperarPorId(id);

        if (f.isPersistente())
            return f;
        else
            return null;
    }

    public static ListaContrato  recuperarTodos()throws SQLException {
        ListaContrato lista = new ListaContrato();
        ListaContratoDAO dao = (ListaContratoDAO) DAOFactory.getDAO(lista);
        
        dao.recuperarTodos();

        return lista;
    }
    
    public static ListaContrato recuperarPorLocacaoId(int id) throws SQLException {
    	ListaContrato lista = new ListaContrato();
    	ListaContratoDAO dao = (ListaContratoDAO) DAOFactory.getDAO(lista);

		dao.recuperarPorLocacaoId(id);

		return lista;
	}
    
    public static void renovarContratos() throws SQLException {
    	ListaContrato lista = new ListaContrato();
    	ListaContratoDAO dao = (ListaContratoDAO) DAOFactory.getDAO(lista);

		dao.renovarContratos();

	}
    
    public static void aplicarIndice() throws SQLException {
    	ListaContrato lista = new ListaContrato();
    	ListaContratoDAO dao = (ListaContratoDAO) DAOFactory.getDAO(lista);

		dao.aplicarIndice();

	}
     
}
