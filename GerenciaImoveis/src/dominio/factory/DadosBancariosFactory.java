package dominio.factory;

import java.sql.SQLException;

import dao.DAOFactory;
import dominio.bean.entity.DadosBancarios;
import dominio.dao.DadosBancariosDAO;

public class DadosBancariosFactory {
	
	public static DadosBancarios criar() {
		return new DadosBancarios();
	}
	
	public static DadosBancarios criar(String nome_banco, String agencia, String conta, String tp_conta, String titular)
			throws SQLException {
		
		DadosBancarios db = new DadosBancarios(nome_banco, agencia, conta, tp_conta, titular);
		return db;
	}
	
	public static DadosBancarios recuperarPorId(int id) throws SQLException {
		DadosBancarios p = new DadosBancarios();
		DadosBancariosDAO DadosBancariosDao = (DadosBancariosDAO) DAOFactory.getDAO(p);

		DadosBancariosDao.recuperarPorId(id);

		if (p.isPersistente())
			return p;
		else
			return null;
	}
	

}
