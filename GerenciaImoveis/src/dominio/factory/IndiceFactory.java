package dominio.factory;

import java.math.BigDecimal;
import java.sql.SQLException;

import dao.DAOFactory;
import dominio.bean.entity.Indice;
import dominio.dao.IndiceDAO;

public class IndiceFactory {
	public static Indice criar() {
		return new Indice();
	}

	public static Indice criar(BigDecimal Indice) throws SQLException {

		Indice ind = new Indice(Indice);
		return ind;
	}
	
	public static Indice recuperarPorId(int id) throws SQLException {
		Indice obj = new Indice();
		IndiceDAO dao = (IndiceDAO) DAOFactory.getDAO(obj);

        dao.recuperarPorId(id);
		System.out.println("recuperarPorId = " + obj.getIndice());

        if (obj.isPersistente())
            return obj;
        else
            return null;
    }

}
