package dominio.factory;

import java.sql.SQLException;

import dao.DAOFactory;
import dominio.bean.entity.Celular;
import dominio.bean.list.ListaCelular;
import dominio.dao.ListaCelularDAO;

public class CelularFactory {
	public static Celular criar() {
		return new Celular();
	}

	public static Celular criar(Integer num_Celular, Integer ddd) throws SQLException {

		Celular tel = new Celular(ddd, num_Celular);
		return tel;
	}

	public static ListaCelular recuperarPorPessoaId(int id) throws SQLException {
		ListaCelular lista = new ListaCelular();
		ListaCelularDAO dao = (ListaCelularDAO) DAOFactory.getDAO(lista);

		dao.recuperarPorPessoaId(id);

		return lista;
	}
}
