package dominio.factory;

import java.sql.SQLException;

import dao.DAOFactory;
import dominio.bean.entity.Endereco;
import dominio.dao.EnderecoDAO;

public class EnderecoFactory {

    public static Endereco criar() {
        return new Endereco();
    }
    
    public static Endereco criar(String endereco, String bairro, String cep, String num_banco, String estado, String cidade, String complemento, String nom_condominio) throws SQLException {
        Endereco e = new Endereco(endereco, bairro, cep, num_banco, estado, cidade, complemento, nom_condominio);

        return e;
    }

    public static Endereco recuperarPorId(int id) throws SQLException {
        Endereco f = new Endereco();
        EnderecoDAO dao = (EnderecoDAO) DAOFactory.getDAO(f);

        dao.recuperarPorId(id);

        if (f.isPersistente())
            return f;
        else
            return null;
    }


}
