package dominio.dao;

import dao.ConexaoBD;
import dao.DAO;
import dominio.bean.entity.DadosBancarios;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.mysql.jdbc.Statement;

public class DadosBancariosDAO extends DAO<DadosBancarios> {

	public DadosBancariosDAO(DadosBancarios persistente ) {
		super(persistente);
	}

	@Override
	public Integer inserir(Connection conn) throws SQLException {
		String sql = "INSERT INTO Dados_Bancarios (`id`, `nome_banco`, `agencia`, `conta`, `tp_conta`, `titular`) "
				+ "VALUES (NULL, ?, ?, ?, ?, ?)";


		if (conn != null) {
			PreparedStatement stm = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

			stm.setString(1, persistente.getNome_banco());
			stm.setString(2, persistente.getAgencia());
			stm.setString(3, persistente.getConta());
			stm.setString(4, persistente.getTp_conta());
			stm.setString(5, persistente.getTitular());

			stm.execute();

			ResultSet rs = stm.getGeneratedKeys();

			Integer id = null;

			if (rs.next())
				id = rs.getInt(1);

			rs.close();
			stm.close();

			return id;
		}

		return null;
	}

	@Override
	public void atualizar(Connection conn) throws SQLException {

		String sql = "update Dados_Bancarios set nome_banco=?, agencia=?, conta=?, tp_conta=?, titular=? where id=?";


		if (conn != null) {
			PreparedStatement stm = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

			stm.setString(1, persistente.getNome_banco());
			stm.setString(2, persistente.getAgencia());
			stm.setString(3, persistente.getConta());
			stm.setString(4, persistente.getTp_conta());
			stm.setString(5, persistente.getTitular());
			stm.setInt(6, persistente.getId());

			stm.execute();

			stm.close();
		}
	}

	@Override
	public void excluir(Connection conn) throws SQLException {
		String sql = "delete from Dados_Bancarios where id=?";


		if (conn != null) {
			PreparedStatement stm = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

			stm.setInt(1, persistente.getId());

			stm.execute();

			stm.close();
		}
	}

	/*
	 * public void recuperarPorFuncionario(Funcionario f) throws SQLException {
	 * String sql = "select d.* " + "from DadosBancarios d " +
	 * "inner join funcionario f on d.id = f.id_DadosBancarios " +
	 * "where f.id = ?";
	 * 
	 * Connection conn = ConexaoBD.getConnection();
	 * 
	 * if (conn != null) { PreparedStatement stm = conn.prepareStatement(sql);
	 * 
	 * stm.setInt(1, f.getId());
	 * 
	 * ResultSet rs = stm.executeQuery();
	 * 
	 * if (rs.next()) { persistente.setId(rs.getInt("id"));
	 * persistente.setDados(rs); persistente.setPersistente(true); }
	 * 
	 * rs.close(); stm.close(); conn.close(); } }
	 * 
	 */

	public void recuperarPorId(int id) throws SQLException {
		String sql = "select d.* " + "from Dados_Bancarios d " + "where d.id = ?";
        Connection conn = ConexaoBD.getConnection();


		if (conn != null) {
			PreparedStatement stm = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

			stm.setInt(1, id);

			ResultSet rs = stm.executeQuery();

			if (rs.next()) {
				persistente.setId(rs.getInt("id"));
				persistente.setDados(rs);
				persistente.setPersistente(true);
			}

			rs.close();
			stm.close();
			conn.close();
		}
	}
}
