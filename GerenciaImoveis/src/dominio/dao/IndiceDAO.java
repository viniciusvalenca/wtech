package dominio.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import dao.ConexaoBD;
import dao.DAO;
import dominio.bean.entity.Indice;

public class IndiceDAO extends DAO<Indice> {

	public IndiceDAO(Indice persistente) {
		super(persistente);
	}

	@Override
	public Integer inserir(Connection conn) throws SQLException {

		return null;
	}

	@Override
	public void atualizar(Connection conn) throws SQLException {
		String sql = "update Indice set indice_reajuste=? where id=?";

		if (conn != null) {
			PreparedStatement stm = conn.prepareStatement(sql);

			stm.setBigDecimal(1, persistente.getIndice1());
			stm.setInt(2, persistente.getId());

			stm.execute();

			stm.close();
		}
	}

	@Override
	public void excluir(Connection conn) throws SQLException {

	}

	public void recuperarPorId(int id) throws SQLException {
		String sql = "select d.* " + "from Indice d " + "where d.id = ?";
		Connection conn = ConexaoBD.getConnection();

		if (conn != null) {
			PreparedStatement stm = conn.prepareStatement(sql);

			stm.setInt(1, id);

			ResultSet rs = stm.executeQuery();

			if (rs.next()) {
				persistente.setId(rs.getInt("id"));
				persistente.setDados(rs);
				persistente.setPersistente(true);
			}

			rs.close();
			stm.close();
			conn.close();
		}
	}

}
