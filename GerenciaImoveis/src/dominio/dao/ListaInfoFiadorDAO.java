package dominio.dao;

import dao.Persistente;
import dominio.bean.entity.InfoFiador;
import dominio.bean.list.ListaInfoFiador;
import dao.ConexaoBD;
import dao.DAOLista;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ListaInfoFiadorDAO extends DAOLista<ListaInfoFiador> {

	public ListaInfoFiadorDAO(ListaInfoFiador lista) {
		super(lista);
	}

	public void recuperarTodos() throws SQLException {

		String sql = "select * from fiadores_locacao";

		Connection conn = ConexaoBD.getConnection();

		if (conn != null) {
			PreparedStatement stm = conn.prepareStatement(sql);

			ResultSet rs = stm.executeQuery();

			while (rs.next()) {
				Persistente i = listaPersistente.getPersistente();
				i.setId(rs.getInt("id"));
				i.setDados(rs);
				i.setPersistente(true);
				listaPersistente.addPersistente((InfoFiador) i);
			}

			rs.close();
			stm.close();
			conn.close();
		}
	}

	public void recuperarPorLocacaoId(int id) throws SQLException {

		String sql = "select * from fiadores_locacao where fk_id_locacao = ?";

		Connection conn = ConexaoBD.getConnection();

		if (conn != null) {
			PreparedStatement stm = conn.prepareStatement(sql);
			
			stm.setInt(1, id);

			ResultSet rs = stm.executeQuery();

			while (rs.next()) {
				Persistente i = listaPersistente.getPersistente();
				i.setId(rs.getInt("id"));
				i.setDados(rs);
				i.setPersistente(true);
				listaPersistente.addPersistente((InfoFiador) i);
			}

			rs.close();
			stm.close();
			conn.close();
		}
	}

}
