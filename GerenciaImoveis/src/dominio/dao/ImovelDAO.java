package dominio.dao;

import dao.ConexaoBD;
import dao.DAO;
import dominio.bean.entity.Imovel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.mysql.jdbc.Statement;

public class ImovelDAO extends DAO<Imovel> {

    public ImovelDAO(Imovel persistente) {
        super(persistente);
    }

    @Override
    public Integer inserir( Connection conn) throws SQLException {
        String sql = "INSERT INTO Imovel (`id`, `valor`, `comissao`, `tipo`, `modalidade`, `qtd_quartos`," 
		+"`qtd_suites`, `area`, `insc_iptu`, `cod_logradouro`, `mat_cedae`, `mat_light`, `cbmerj`, `fk_id_locador`," 
        +"`fk_id_endereco`, `flg_disponivel`, `mat_ceg`) "
        + "VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";


        if (conn != null) {
        	PreparedStatement stm = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        	
            stm.setBigDecimal(1, persistente.getValor());
            stm.setString(2, persistente.getComissao());
            stm.setString(3, persistente.getTipo());
            stm.setString(4, persistente.getModalidade());
            stm.setString(5, persistente.getQtd_quartos());
            stm.setString(6, persistente.getQtd_suites());
            stm.setString(7, persistente.getArea());
            stm.setString(8, persistente.getInsc_iptu());
            stm.setString(9, persistente.getCod_logradouro());
            stm.setString(10, persistente.getMat_cedae());
            stm.setString(11, persistente.getMat_light());
            stm.setString(12, persistente.getCbmerj());
            stm.setInt(13, persistente.getId_locador());
            stm.setInt(14, persistente.getEndereco().getId());
            stm.setBoolean(15, persistente.getFlg_disponivel());
            stm.setString(16, persistente.getMat_ceg());
            
            stm.execute();
            
            ResultSet rs = stm.getGeneratedKeys();
            
            Integer id = null;
            
            if (rs.next())
                id = rs.getInt(1);
            
            rs.close();
            stm.close();
            
            return id;
        }

        return null;
    }

    @Override
    public void atualizar( Connection conn) throws SQLException {
        
        String sql = "update Imovel set valor=?, comissao=?, tipo=?, modalidade=?, qtd_quartos=?, " 
        		+"qtd_suites=?, area=?, insc_iptu=?, cod_logradouro=?, mat_cedae=?, mat_light=?, cbmerj=?, "
        		+ "fk_id_locador=?, fk_id_endereco=?, flg_disponivel=?, mat_ceg=? where id=?";
        

        if (conn != null) {
        	PreparedStatement stm = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            stm.setBigDecimal(1, persistente.getValor());
            stm.setString(2, persistente.getComissao());
            stm.setString(3, persistente.getTipo());
            stm.setString(4, persistente.getModalidade());
            stm.setString(5, persistente.getQtd_quartos());
            stm.setString(6, persistente.getQtd_suites());
            stm.setString(7, persistente.getArea());
            stm.setString(8, persistente.getInsc_iptu());
            stm.setString(9, persistente.getCod_logradouro());
            stm.setString(10, persistente.getMat_cedae());
            stm.setString(11, persistente.getMat_light());
            stm.setString(12, persistente.getCbmerj());
            stm.setInt(13, persistente.getId_locador());
            stm.setInt(14, persistente.getEndereco().getId());
            stm.setBoolean(15, persistente.getFlg_disponivel());
            stm.setString(16, persistente.getMat_ceg());
            stm.setInt(17, persistente.getId());
            
            stm.execute();
            
            stm.close();
        }
    }

    @Override
    public void excluir( Connection conn) throws SQLException {
        String sql = "delete Imovel where id=?";
        

        if (conn != null) {
            PreparedStatement stm = conn.prepareStatement(sql);

            stm.setInt(1, persistente.getId());
            
            stm.execute();
            
            stm.close();
        }
    }

   /* public void recuperarPorFuncionario(Funcionario f) throws SQLException {
        String sql = "select d.* "
                   + "from Imovel d "
                   + "inner join funcionario f on d.id = f.id_Imovel "
                   + "where f.id = ?";
        
        Connection conn = ConexaoBD.getConnection();

        if (conn != null) {
            PreparedStatement stm = conn.prepareStatement(sql);

            stm.setInt(1, f.getId());
            
            ResultSet rs = stm.executeQuery();
            
            if (rs.next()) {
                persistente.setId(rs.getInt("id"));
                persistente.setDados(rs);
                persistente.setPersistente(true);
            }

            rs.close();
            stm.close();
            conn.close();
        }
    }
     */
    
    public void recuperarPorId(int codigo) throws SQLException {
        String sql = "select d.* "
                   + "from Imovel d "
                   + "where d.id = ?";
        Connection conn = ConexaoBD.getConnection();

        if (conn != null) {
            PreparedStatement stm = conn.prepareStatement(sql);

            stm.setInt(1, codigo);
            
            ResultSet rs = stm.executeQuery();
            
            if (rs.next()) {
                persistente.setId(rs.getInt("id"));
                persistente.setDados(rs);
                persistente.setPersistente(true);
            }
            
            rs.close();
            stm.close();
            conn.close();
        }
    }
    
    
	public Integer proximoId() throws SQLException {
        String sql = "SHOW TABLE STATUS WHERE `Name` = 'imovel'";
        Integer id=0;
        Connection conn = ConexaoBD.getConnection();

        if (conn != null) {
        	PreparedStatement stm = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            ResultSet rs = stm.executeQuery();
            
            if (rs.next()) {
            	id = rs.getInt("Auto_increment");
            }
            
            rs.close();
            stm.close();
            conn.close();
        }
        return id;
    }
	
	public Integer countTodos() throws SQLException {
    	String sql = "SELECT count(id) AS count FROM imovel";
    	Connection conn = ConexaoBD.getConnection();

        if (conn != null) {
            PreparedStatement stm = conn.prepareStatement(sql);
            
            ResultSet rs = stm.executeQuery();
            
            Integer count = null;
            
            if (rs.next()){
                count = rs.getInt("count");
            }
            
            rs.close();
            stm.close();
            conn.close();
            
            return count;
        }

        return null;
    }
}
