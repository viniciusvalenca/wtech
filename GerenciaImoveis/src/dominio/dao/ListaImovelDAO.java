package dominio.dao;

import dao.Persistente;
import dominio.bean.entity.Imovel;
import dominio.bean.list.ListaImovel;
import dao.ConexaoBD;
import dao.DAOLista;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ListaImovelDAO extends DAOLista<ListaImovel> {

    public ListaImovelDAO(ListaImovel imovel) {
        super(imovel);
    }

    public void recuperarTodos() throws SQLException {
    	
        String sql = "select * from Imovel";
        Connection conn = ConexaoBD.getConnection();

        if (conn != null) {
            PreparedStatement stm = conn.prepareStatement(sql);
            
            ResultSet rs = stm.executeQuery();
            
            while (rs.next()) {
                Persistente i = listaPersistente.getPersistente();
                i.setId(rs.getInt("id"));
                i.setDados(rs);
                i.setPersistente(true);
                listaPersistente.addPersistente((Imovel) i);
            }
            
            rs.close();
            stm.close();
            conn.close();
        }
    }

    public void recuperarTodosDisponiveis(Boolean flag) throws SQLException {
    	
        String sql = "select i.* from Imovel i where i.flg_disponivel = ?";
        Connection conn = ConexaoBD.getConnection();

        if (conn != null) {
            PreparedStatement stm = conn.prepareStatement(sql);
            
            stm.setBoolean(1, flag);
            
            ResultSet rs = stm.executeQuery();
            
            while (rs.next()) {
                Persistente i = listaPersistente.getPersistente();
                i.setId(rs.getInt("id"));
                i.setDados(rs);
                i.setPersistente(true);
                listaPersistente.addPersistente((Imovel) i);
            }
            
            rs.close();
            stm.close();
            conn.close();
        }
    }
   
}
