package dominio.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import dao.ConexaoBD;
import dao.DAOLista;
import dao.Persistente;
import dominio.bean.entity.HistoricoAcesso;
import dominio.bean.list.ListaHistoricoAcesso;

public class ListaHistoricoAcessoDAO extends DAOLista<ListaHistoricoAcesso> {
	
	public ListaHistoricoAcessoDAO(ListaHistoricoAcesso historico) {
        super(historico);
    }

	public void recuperarTodos() throws SQLException {

		String sql = "SELECT h.* from historico_acesso h order by h.id desc";

		Connection conn = ConexaoBD.getConnection();
		if (conn != null) {
			PreparedStatement stm = conn.prepareStatement(sql);

			ResultSet rs = stm.executeQuery();

			while (rs.next()) {
				Persistente i = listaPersistente.getPersistente();
				i.setId(rs.getInt("id"));
				i.setDados(rs);
				i.setPersistente(true);
				listaPersistente.addPersistente((HistoricoAcesso) i);
			}

			rs.close();
			stm.close();
			conn.close();
		}
	}
	
	public void recuperarTodosDash() throws SQLException {

		String sql = "SELECT ha.* from historico_acesso ha ORDER BY ha.id DESC LIMIT 6";
		Connection conn = ConexaoBD.getConnection();
		if (conn != null) {
			PreparedStatement stm = conn.prepareStatement(sql);

			ResultSet rs = stm.executeQuery();

			while (rs.next()) {
				Persistente i = listaPersistente.getPersistente();
				i.setId(rs.getInt("id"));
				i.setDados(rs);
				i.setPersistente(true);
				listaPersistente.addPersistente((HistoricoAcesso) i);
			}

			rs.close();
			stm.close();
			conn.close();
		}
	}
}
