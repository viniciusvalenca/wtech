package dominio.dao;

import dao.DAO;
import dominio.bean.entity.ItensLocacao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.mysql.jdbc.Statement;

public class ItensLocacaoDAO extends DAO<ItensLocacao> {

	public ItensLocacaoDAO(ItensLocacao persistente) {
		super(persistente);
	}

	@Override
	public Integer inserir(Connection conn) throws SQLException {
		String sql = "INSERT INTO item (`id`,`tipo`,`ordem`,`descricao`,"
				+ "`mes_ref`,`parcela`,`tot_parcela`,`valor`,`rd`,`status`,`flg_processado`,`mes_refBoleto`, `fk_id_locacao`) " 
				+ "VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

		if (conn != null) {
			PreparedStatement stm = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

			stm.setString(1, persistente.getTipo());
			stm.setInt(2, persistente.getOrdem());
			stm.setString(3, persistente.getDescricao());
			stm.setDate(4, persistente.getMes_ref());
			stm.setInt(5, persistente.getParcela());
			stm.setInt(6, persistente.getTot_parcela());
			stm.setBigDecimal(7, persistente.getValor());
			stm.setString(8, persistente.getRd());
			stm.setString(9, persistente.getStatus());
			stm.setBoolean(10, persistente.getFlg_processado());
			stm.setString(11, persistente.getMes_refBoleto());
			stm.setInt(12, persistente.getFk_id_locacao());
			
			stm.execute();

			ResultSet rs = stm.getGeneratedKeys();

			Integer id = null;

			if (rs.next())
				id = rs.getInt(1);

			rs.close();
			stm.close();

			return id;
		}

		return null;
	}

	@Override
	public void atualizar(Connection conn) throws SQLException {

		String sql = "update item set tipo=?, ordem=?, "
				+ "descricao=?, mes_ref=?, parcela=?, tot_parcela=?, "
				+ "valor=?, rd=?, status=?, flg_processado=?, mes_refBoleto=?, fk_id_locacao=?  where id=?";
System.out.println(persistente.getMes_refBoleto());
		if (conn != null) {
			PreparedStatement stm = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

			stm.setString(1, persistente.getTipo());
			stm.setInt(2, persistente.getOrdem());
			stm.setString(3, persistente.getDescricao());
			stm.setDate(4, persistente.getMes_ref());
			stm.setInt(5, persistente.getParcela());
			stm.setInt(6, persistente.getTot_parcela());
			stm.setBigDecimal(7, persistente.getValor());
			stm.setString(8, persistente.getRd());
			stm.setString(9, persistente.getStatus());
			stm.setBoolean(10, persistente.getFlg_processado());
			stm.setString(11, persistente.getMes_refBoleto());
			stm.setInt(12, persistente.getFk_id_locacao());
			stm.setInt(13, persistente.getId());

			stm.execute();

			stm.close();
		}
	}

	@Override
	public void excluir(Connection conn) throws SQLException {
		String sql = "delete from item where id=?";

		if (conn != null) {
			PreparedStatement stm = conn.prepareStatement(sql);

			stm.setInt(1, persistente.getId());

			stm.execute();

			stm.close();
		}
	}
}
