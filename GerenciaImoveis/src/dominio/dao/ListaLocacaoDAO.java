package dominio.dao;

import dao.Persistente;
import dominio.bean.entity.Locacao;
import dominio.bean.list.ListaLocacao;
import dao.ConexaoBD;
import dao.DAOLista;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ListaLocacaoDAO extends DAOLista<ListaLocacao> {

	public ListaLocacaoDAO(ListaLocacao locacao) {
		super(locacao);
	}

	public void recuperarTodos() throws SQLException {

		String sql = "select * from Locacao";

		Connection conn = ConexaoBD.getConnection();

		if (conn != null) {
			PreparedStatement stm = conn.prepareStatement(sql);

			ResultSet rs = stm.executeQuery();

			while (rs.next()) {
				Persistente i = listaPersistente.getPersistente();
				i.setId(rs.getInt("id"));
				i.setDados(rs);
				i.setPersistente(true);
				listaPersistente.addPersistente((Locacao) i);
			}

			rs.close();
			stm.close();
			conn.close();
		}
	}

	public void buscarTodasLocacoes() throws SQLException {

		String sql = "select l.* from Locacao l " + "INNER JOIN Contrato c ON l.fk_id_contrato = c.id "
				+ "WHERE c.dt_entrega_chave is null";

		Connection conn = ConexaoBD.getConnection();

		if (conn != null) {
			PreparedStatement stm = conn.prepareStatement(sql);

			ResultSet rs = stm.executeQuery();

			while (rs.next()) {
				Persistente i = listaPersistente.getPersistente();
				i.setId(rs.getInt("id"));
				i.setDados(rs);
				i.setPersistente(true);
				listaPersistente.addPersistente((Locacao) i);
			}

			rs.close();
			stm.close();
			conn.close();
		}
	}
	
	public void recuperarTodosRelatorio(Date mes_ref) throws SQLException {

		String sql = "SELECT l.* FROM locacao l where "
				+ "EXISTS(SELECT * FROM boleto b "
				+ "WHERE b.fk_id_locacao = l.id and MONTH(b.mes_ref) = MONTH(?) AND YEAR(b.mes_ref) = YEAR(?) )" 
				+ " AND EXISTS(SELECT * FROM item i "
				+ "WHERE i.fk_id_locacao = l.id and MONTH(i.mes_ref) = MONTH(?) AND YEAR(i.mes_ref) = YEAR(?) )";

		Connection conn = ConexaoBD.getConnection();

		if (conn != null) {
			PreparedStatement stm = conn.prepareStatement(sql);

			stm.setDate(1, mes_ref);
			stm.setDate(2, mes_ref);
			stm.setDate(3, mes_ref);
			stm.setDate(4, mes_ref);
			
			ResultSet rs = stm.executeQuery();
			
			while (rs.next()) {
				Persistente i = listaPersistente.getPersistente();
				i.setId(rs.getInt("id"));
				i.setDados(rs);
				i.setPersistente(true);
				listaPersistente.addPersistente((Locacao) i);
			}

			rs.close();
			stm.close();
			conn.close();
		}
	}
	
	public void recuperarTodosLocacaoPagasRelatorio(Date mes_ref) throws SQLException {

		String sql = "SELECT l.* FROM locacao l where "
				+ "EXISTS(SELECT * FROM boleto b "
				+ "WHERE b.fk_id_locacao = l.id and MONTH(b.mes_ref) = MONTH(?) AND YEAR(b.mes_ref) = YEAR(?) AND b.dt_pagamento is NOT NULL )" 
				+ " AND EXISTS(SELECT * FROM item i "
				+ "WHERE i.fk_id_locacao = l.id and MONTH(i.mes_ref) = MONTH(?) AND YEAR(i.mes_ref) = YEAR(?) )";

		Connection conn = ConexaoBD.getConnection();

		if (conn != null) {
			PreparedStatement stm = conn.prepareStatement(sql);

			stm.setDate(1, mes_ref);
			stm.setDate(2, mes_ref);
			stm.setDate(3, mes_ref);
			stm.setDate(4, mes_ref);
			
			ResultSet rs = stm.executeQuery();
			
			while (rs.next()) {
				Persistente i = listaPersistente.getPersistente();
				i.setId(rs.getInt("id"));
				i.setDados(rs);
				i.setPersistente(true);
				listaPersistente.addPersistente((Locacao) i);
			}

			rs.close();
			stm.close();
			conn.close();
		}
	}
	
	public void recuperarTodosRelatorioAno(Date ano_ref) throws SQLException {

		String sql = "SELECT l.* FROM locacao l where "
				+ "EXISTS(SELECT * FROM boleto b "
				+ "WHERE b.fk_id_locacao = l.id and year(b.dt_pagamento) = year(?) )";

		Connection conn = ConexaoBD.getConnection();

		if (conn != null) {
			PreparedStatement stm = conn.prepareStatement(sql);

			stm.setDate(1, ano_ref);
			
			ResultSet rs = stm.executeQuery();
			
			while (rs.next()) {
				Persistente i = listaPersistente.getPersistente();
				i.setId(rs.getInt("id"));
				i.setDados(rs);
				i.setPersistente(true);
				listaPersistente.addPersistente((Locacao) i);
			}

			rs.close();
			stm.close();
			conn.close();
		}
	}
	
	public void recuperarTodosRelatorioAno2(Date ano_ref) throws SQLException {

		String sql = "SELECT l.* FROM locacao l where "
				+ "EXISTS(SELECT * FROM boleto b "
				+ "WHERE b.fk_id_locacao = l.id and year(b.dt_pagamento) = year(?) )";

		Connection conn = ConexaoBD.getConnection();

		if (conn != null) {
			PreparedStatement stm = conn.prepareStatement(sql);

			stm.setDate(1, ano_ref);
			stm.setDate(2, ano_ref);
			
			ResultSet rs = stm.executeQuery();
			
			while (rs.next()) {
				Persistente i = listaPersistente.getPersistente();
				i.setId(rs.getInt("id"));
				i.setDados(rs);
				i.setPersistente(true);
				listaPersistente.addPersistente((Locacao) i);
			}

			rs.close();
			stm.close();
			conn.close();
		}
	}

	public void recuperarTodosPorLocador(int id) throws SQLException {

		String sql = "SELECT l.* FROM locacao l INNER JOIN imovel i "
				+ "ON l.fk_id_imovel = i.id WHERE i.fk_id_locador=?";

		Connection conn = ConexaoBD.getConnection();

		if (conn != null) {
			PreparedStatement stm = conn.prepareStatement(sql);

			stm.setInt(1, id);

			ResultSet rs = stm.executeQuery();

			while (rs.next()) {
				Persistente i = listaPersistente.getPersistente();
				i.setId(rs.getInt("id"));
				i.setDados(rs);
				i.setPersistente(true);
				listaPersistente.addPersistente((Locacao) i);
			}

			rs.close();
			stm.close();
			conn.close();
		}
	}
	
	public void recuperarTodosPorLocador(Integer id, ArrayList<Integer> array) throws SQLException {

		String sql = "SELECT l.* FROM locacao l INNER JOIN imovel i "
				+ "ON l.fk_id_imovel = i.id WHERE i.fk_id_locador = ? AND l.id IN ( ";

		StringBuilder queryBuilder = new StringBuilder(sql);
		queryBuilder.append("?");
		for (int i = 1; i < array.size(); i++) {
			queryBuilder.append(", ?");
		}
		queryBuilder.append(") ");
		sql = queryBuilder.toString();

		Connection conn = ConexaoBD.getConnection();

		if (conn != null) {

			PreparedStatement stm = conn.prepareStatement(sql);

			stm.setInt(1, id);

			int ifo = 2;
			for (Object param : array) {
				stm.setObject(ifo++, param);
			}

			ResultSet rs = stm.executeQuery();

			while (rs.next()) {
				Persistente i = listaPersistente.getPersistente();
				i.setId(rs.getInt("id"));
				i.setDados(rs);
				i.setPersistente(true);
				listaPersistente.addPersistente((Locacao) i);
			}

			rs.close();
			stm.close();
			conn.close();
		}
	}

	public void recuperarLocacaoRemessa(Date mes_ref) throws SQLException {

		String sql = "SELECT distinct l.* FROM locacao l " + "INNER JOIN item i ON l.id = i.fk_id_locacao "
				+ "WHERE MONTH(i.mes_ref) = MONTH(?) AND YEAR(i.mes_ref) = YEAR(?) AND l.id not in (SELECT b.fk_id_locacao from Boleto b WHERE MONTH(b.mes_ref) = MONTH(?) AND YEAR(b.mes_ref) = YEAR(?))"
				+ "ORDER BY l.id";

		Connection conn = ConexaoBD.getConnection();

		if (conn != null) {
			PreparedStatement stm = conn.prepareStatement(sql);

			stm.setDate(1, mes_ref);
			stm.setDate(2, mes_ref);
			stm.setDate(3, mes_ref);
			stm.setDate(4, mes_ref);
			
			ResultSet rs = stm.executeQuery();

			while (rs.next()) {
				Persistente i = listaPersistente.getPersistente();
				i.setId(rs.getInt("id"));
				i.setDados(rs);
				i.setPersistente(true);
				listaPersistente.addPersistente((Locacao) i);
			}

			rs.close();
			stm.close();
			conn.close();
		}
	}

	public void recuperarLocacoesNaoPagas(Date mes_ref) throws SQLException {

		String sql = "SELECT l.*, b.* FROM locacao l INNER JOIN boleto b ON l.id = b.fk_id_locacao "
				+ "WHERE MONTH(b.mes_ref) = MONTH(?) AND YEAR(b.mes_ref) = YEAR(?) ORDER BY l.id";

		Connection conn = ConexaoBD.getConnection();

		if (conn != null) {
			PreparedStatement stm = conn.prepareStatement(sql);
			
			

			stm.setDate(1, mes_ref);
			stm.setDate(2, mes_ref);
			
			
			

			ResultSet rs = stm.executeQuery();

			while (rs.next()) {
				Persistente i = listaPersistente.getPersistente();
				i.setId(rs.getInt("id"));
				i.setDados(rs);
				i.setPersistente(true);
				listaPersistente.addPersistente((Locacao) i);
			}

			rs.close();
			stm.close();
			conn.close();
		}
	}

	public void aVencer() throws SQLException {
		String sql = "SELECT l.* FROM locacao l " + "INNER JOIN contrato c on c.id = l.fk_id_contrato "
				+ "WHERE c.dt_termino >= curdate() " + "AND c.dt_termino <= DATE_ADD(curdate(), INTERVAL 15 DAY)";

		Connection conn = ConexaoBD.getConnection();
		if (conn != null) {
			PreparedStatement stm = conn.prepareStatement(sql);

			ResultSet rs = stm.executeQuery();

			while (rs.next()) {
				Persistente i = listaPersistente.getPersistente();
				i.setId(rs.getInt("id"));
				i.setDados(rs);
				i.setPersistente(true);
				listaPersistente.addPersistente((Locacao) i);
			}

			rs.close();
			stm.close();
			conn.close();
		}
	}
	
	public void ctVencidoDash() throws SQLException {
		String sql = "SELECT l.* FROM locacao l " + "INNER JOIN contrato c on c.id = l.fk_id_contrato "
				+ "WHERE c.dt_termino < curdate() ORDER BY c.dt_termino DESC LIMIT 9";

		Connection conn = ConexaoBD.getConnection();
		if (conn != null) {
			PreparedStatement stm = conn.prepareStatement(sql);

			ResultSet rs = stm.executeQuery();

			while (rs.next()) {
				Persistente i = listaPersistente.getPersistente();
				i.setId(rs.getInt("id"));
				i.setDados(rs);
				i.setPersistente(true);
				listaPersistente.addPersistente((Locacao) i);
			}

			rs.close();
			stm.close();
			conn.close();
		}
	}
	
	public void ctVencido() throws SQLException {
		String sql = "SELECT l.* FROM locacao l " + "INNER JOIN contrato c on c.id = l.fk_id_contrato "
				+ "WHERE c.dt_termino < curdate() ORDER BY c.dt_termino DESC";

		Connection conn = ConexaoBD.getConnection();
		if (conn != null) {
			PreparedStatement stm = conn.prepareStatement(sql);

			ResultSet rs = stm.executeQuery();

			while (rs.next()) {
				Persistente i = listaPersistente.getPersistente();
				i.setId(rs.getInt("id"));
				i.setDados(rs);
				i.setPersistente(true);
				listaPersistente.addPersistente((Locacao) i);
			}

			rs.close();
			stm.close();
			conn.close();
		}
	}

}
