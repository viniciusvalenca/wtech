package dominio.dao;

import dao.DAO;
import dao.DAOFactory;
import dao.DAOLista;
import dao.IDAOFactory;
import dao.ListaPersistente;
import dao.Persistente;
import dominio.bean.entity.BoletoAbsi;
import dominio.bean.entity.Celular;
import dominio.bean.entity.Contrato;
import dominio.bean.entity.Locacao;
import dominio.bean.entity.DadosBancarios;
import dominio.bean.entity.Endereco;
import dominio.bean.entity.HistoricoAcesso;
import dominio.bean.entity.Imovel;
import dominio.bean.entity.ImpostoRenda;
import dominio.bean.entity.Indice;
import dominio.bean.entity.InfoFiador;
import dominio.bean.entity.ItensLocacao;
import dominio.bean.entity.Pessoa;
import dominio.bean.entity.Telefone;
import dominio.bean.entity.Usuario;
import dominio.bean.list.ListaBoletoAbsi;
import dominio.bean.list.ListaCelular;
import dominio.bean.list.ListaContrato;
import dominio.bean.list.ListaHistoricoAcesso;
import dominio.bean.list.ListaImovel;
import dominio.bean.list.ListaInfoFiador;
import dominio.bean.list.ListaItensLocacao;
import dominio.bean.list.ListaLocacao;
import dominio.bean.list.ListaPessoa;
import dominio.bean.list.ListaTelefone;
import dominio.bean.list.ListaUsuario;

public class FabricaDAO implements IDAOFactory {

    public FabricaDAO() {
        DAOFactory.setDaoFactory(this);
    }

    @SuppressWarnings("rawtypes")
    @Override
    public DAO getDAO(Persistente persistente) {
    	if (persistente instanceof Pessoa)
            return new PessoaDAO((Pessoa) persistente );
        else if (persistente instanceof Imovel)
            return new ImovelDAO((Imovel) persistente);
        else if (persistente instanceof Celular)
            return new CelularDAO((Celular) persistente);
        else if (persistente instanceof DadosBancarios)
            return new DadosBancariosDAO((DadosBancarios) persistente);
        else if (persistente instanceof Endereco)
            return new EnderecoDAO((Endereco) persistente);
        else if (persistente instanceof Telefone)
            return new TelefoneDAO((Telefone) persistente);
        else if (persistente instanceof Usuario)
            return new UsuarioDAO((Usuario) persistente);
        else if (persistente instanceof Contrato)
            return new ContratoDAO((Contrato) persistente);
        else if (persistente instanceof Locacao)
            return new LocacaoDAO((Locacao) persistente);
        else if (persistente instanceof HistoricoAcesso)
            return new HistoricoAcessoDAO((HistoricoAcesso) persistente);
        else if (persistente instanceof BoletoAbsi)
            return new BoletoAbsiDAO((BoletoAbsi) persistente);
        else if (persistente instanceof InfoFiador)
            return new InfoFiadorDAO((InfoFiador) persistente);
        else if (persistente instanceof ItensLocacao)
            return new ItensLocacaoDAO((ItensLocacao) persistente);
        else if (persistente instanceof Indice)
            return new IndiceDAO((Indice) persistente);
        else if (persistente instanceof ImpostoRenda)
            return new ImpostoRendaDAO((ImpostoRenda) persistente);
        else 
            return null;
    }

    @SuppressWarnings("rawtypes")
    @Override
    public DAOLista getDAO(ListaPersistente lista) {
    	if (lista instanceof ListaPessoa)
            return new ListaPessoaDAO((ListaPessoa) lista);
    	else if (lista instanceof ListaImovel)
            return new ListaImovelDAO((ListaImovel) lista);
    	else if (lista instanceof ListaCelular)
            return new ListaCelularDAO((ListaCelular) lista);
    	else if (lista instanceof ListaTelefone)
            return new ListaTelefoneDAO((ListaTelefone) lista);
    	else if (lista instanceof ListaUsuario)
            return new ListaUsuarioDAO((ListaUsuario) lista);
    	else if (lista instanceof ListaContrato)
            return new ListaContratoDAO((ListaContrato) lista);
    	else if (lista instanceof ListaLocacao)
            return new ListaLocacaoDAO((ListaLocacao) lista);
    	else if (lista instanceof ListaBoletoAbsi)
            return new ListaBoletoAbsiDAO((ListaBoletoAbsi) lista);
    	else if (lista instanceof ListaHistoricoAcesso)
            return new ListaHistoricoAcessoDAO((ListaHistoricoAcesso) lista);
    	else if (lista instanceof ListaInfoFiador)
            return new ListaInfoFiadorDAO((ListaInfoFiador) lista);
    	else if (lista instanceof ListaItensLocacao)
            return new ListaItensLocacaoDAO((ListaItensLocacao) lista);
    	else
    		return null;
    }
}
