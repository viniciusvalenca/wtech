package dominio.dao;

import dao.Persistente;
import dominio.bean.entity.Usuario;
import dominio.bean.list.ListaUsuario;
import dao.ConexaoBD;
import dao.DAOLista;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ListaUsuarioDAO extends DAOLista<ListaUsuario> {

	public ListaUsuarioDAO(ListaUsuario usuarios) {
		super(usuarios);
	}

	public void recuperarTodos() throws SQLException {

		String sql = "select * from Usuario";
		Connection conn = ConexaoBD.getConnection();

		if (conn != null) {
			PreparedStatement stm = conn.prepareStatement(sql);

			ResultSet rs = stm.executeQuery();

			while (rs.next()) {
				Persistente p = listaPersistente.getPersistente();
				p.setId(rs.getInt("id"));
				p.setDados(rs);
				p.setPersistente(true);
				listaPersistente.addPersistente((Usuario) p);
			}

			rs.close();
			stm.close();
			conn.close();
		}
	}
}
