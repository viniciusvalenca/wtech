package dominio.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import dao.ConexaoBD;
import dao.DAOLista;
import dao.Persistente;
import dominio.bean.entity.Telefone;
import dominio.bean.list.ListaTelefone;

public class ListaTelefoneDAO extends DAOLista<ListaTelefone> {
	public ListaTelefoneDAO(ListaTelefone Telefone) {
        super(Telefone);
    }

	public void recuperarPorPessoaId(int id) throws SQLException {

		String sql = "SELECT t.* FROM Telefone t WHERE t.fk_id_pessoa = ?";

		Connection conn = ConexaoBD.getConnection();
		if (conn != null) {
			PreparedStatement stm = conn.prepareStatement(sql);
			stm.setInt(1, id);

			ResultSet rs = stm.executeQuery();

			while (rs.next()) {
				Persistente i = listaPersistente.getPersistente();
				i.setId(rs.getInt("id"));
				i.setDados(rs);
				i.setPersistente(true);
				listaPersistente.addPersistente((Telefone) i);
			}

			rs.close();
			stm.close();
			conn.close();
		}
	}
}
