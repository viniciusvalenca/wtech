package dominio.dao;


import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.mysql.jdbc.Statement;

import dao.ConexaoBD;
import dao.DAO;
import dominio.bean.entity.Pessoa;

public class PessoaDAO extends DAO<Pessoa> {
	
	public PessoaDAO(Pessoa persistente) {
        super(persistente);
    }

	@Override
	public Integer inserir(Connection conn) throws SQLException {
		String sql = "INSERT INTO Pessoa (`id`,`nome_completo`,`email`,`email_opcional`,`cpf`, `identidade`," 
				+ "`dt_emissao`,`emissor`,`estado_civil`,`dt_nascimento`,`nacionalidade`,`naturalidade`,`profissao`, `telefones_extras`,"
				+ "`flg_locatario`,`flg_locador`,`flg_fiador`,`fk_id_dadosbancario`,`fk_id_endereco`, `cnpj`) "
				+ "VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	

		        if (conn != null) {
		        	PreparedStatement stm = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

		            stm.setString(1, persistente.getNome_completo());
		            stm.setString(2, persistente.getEmail());
		            stm.setString(3, persistente.getEmail_opcional());
		            stm.setString(4, persistente.getCpf());
		            stm.setString(5, persistente.getIdentidade());
		            stm.setDate(6, (Date) persistente.getDt_emissao());
		            stm.setString(7, persistente.getEmissor());
		            stm.setString(8, persistente.getEstado_civil());
		            stm.setDate(9, (Date) persistente.getDt_nascimento());
		            stm.setString(10, persistente.getNacionalidade());
		            stm.setString(11, persistente.getNaturalidade());
		            stm.setString(12, persistente.getProfissao());
		            stm.setNString(13, persistente.getTelefones_extras());
		            stm.setBoolean(14, persistente.getFlg_locatario());
		            stm.setBoolean(15, persistente.getFlg_locador());
		            stm.setBoolean(16, persistente.getFlg_fiador());
		            if(persistente.getDadosBancarios() == null){
		            	stm.setNull(17, java.sql.Types.NULL);
		            }else{
		            	stm.setInt(17, persistente.getDadosBancarios().getId());
		            }
		            stm.setInt(18, persistente.getEndereco().getId());
		            stm.setString(19, persistente.getCnpj());
		            
		            stm.execute();
		            
		            ResultSet rs = stm.getGeneratedKeys();
		            
		            Integer id = null;
		            
		            if (rs.next())
		                id = rs.getInt(1);
		            
		            rs.close();
		            stm.close();
		            
		            return id;
		        }

		        return null;
	}

	@Override
	public void atualizar(Connection conn) throws SQLException {
		String sql = "update Pessoa set nome_completo=?, email=?, email_opcional=?, cpf=?, identidade=?, " 
				+ "dt_emissao=?, emissor=?, estado_civil=?, dt_nascimento=?, nacionalidade=?, naturalidade=?, profissao=?, telefones_extras=?, "
				+ "flg_locatario=?, flg_locador=?, flg_fiador=?, fk_id_dadosbancario=?, fk_id_endereco=?, cnpj=? where id=?";
        

        if (conn != null) {
        	PreparedStatement stm = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

        	stm.setString(1, persistente.getNome_completo());
            stm.setString(2, persistente.getEmail());
            stm.setString(3, persistente.getEmail_opcional());
            stm.setString(4, persistente.getCpf());
            stm.setString(5, persistente.getIdentidade());
            stm.setDate(6, (Date) persistente.getDt_emissao());
            stm.setString(7, persistente.getEmissor());
            stm.setString(8, persistente.getEstado_civil());
            stm.setDate(9, (Date) persistente.getDt_nascimento());
            stm.setString(10, persistente.getNacionalidade());
            stm.setString(11, persistente.getNaturalidade());
            stm.setString(12, persistente.getProfissao());
            stm.setNString(13, persistente.getTelefones_extras());
            stm.setBoolean(14, persistente.getFlg_locatario());
            stm.setBoolean(15, persistente.getFlg_locador());
            stm.setBoolean(16, persistente.getFlg_fiador());
            if(persistente.getDadosBancarios() == null){
            	stm.setNull(17, java.sql.Types.NULL);
            }else{
            	stm.setInt(17, persistente.getDadosBancarios().getId());
            }
            stm.setInt(18, persistente.getEndereco().getId());
            stm.setString(19, persistente.getCnpj());
            stm.setInt(20, persistente.getId());
            
            stm.execute();
            
            stm.close();
        }
		
	}

	@Override
	public void excluir(Connection conn) throws SQLException {
		String sql = "delete from Pessoa where id=?";


		if (conn != null) {
			PreparedStatement stm = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

			stm.setInt(1, persistente.getId());

			stm.execute();

			stm.close();
		}
		
	}
	
	public void recuperarPorID(int id) throws SQLException {
        String sql = "select p.* "
                   + "from Pessoa p "
                   + "where p.id = ?";
        Connection conn = ConexaoBD.getConnection();

        if (conn != null) {
        	PreparedStatement stm = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            stm.setInt(1, id);
            
            ResultSet rs = stm.executeQuery();
            
            if (rs.next()) {
                persistente.setId(rs.getInt("id"));
                persistente.setDados(rs);
                persistente.setPersistente(true);
            }
            
            rs.close();
            stm.close();
            conn.close();
        }
    }
	
	public void recuperarPorNome(String nome) throws SQLException {
        String sql = "select p.* "
                   + "from Pessoa p "
                   + "where p.nome = ?";
        
        Connection conn = ConexaoBD.getConnection();
        if (conn != null) {
        	PreparedStatement stm = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            stm.setString(1, nome);
            
            ResultSet rs = stm.executeQuery();
            
            if (rs.next()) {
                persistente.setId(rs.getInt("id"));
                persistente.setDados(rs);
                persistente.setPersistente(true);
            }
            
            rs.close();
            stm.close();
            conn.close();
        }
    }
	
	public void recuperarPorRg(String rg) throws SQLException {
        String sql = "select p.* "
                   + "from Pessoa p "
                   + "where p.identidade = ?";
        
        Connection conn = ConexaoBD.getConnection();
        if (conn != null) {
        	PreparedStatement stm = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            stm.setString(1, rg);
            
            ResultSet rs = stm.executeQuery();
            
            if (rs.next()) {
                persistente.setId(rs.getInt("id"));
                persistente.setDados(rs);
                persistente.setPersistente(true);
            }
            
            rs.close();
            stm.close();
            conn.close();
        }
    }
	
	
	public void recuperarPorCpf(String cpf) throws SQLException {
        String sql = "select p.* "
                   + "from Pessoa p "
                   + "where p.cpf = ?";
        Connection conn = ConexaoBD.getConnection();

        if (conn != null) {
        	PreparedStatement stm = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            stm.setString(1, cpf);
            
            ResultSet rs = stm.executeQuery();
            
            if (rs.next()) {
                persistente.setId(rs.getInt("id"));
                persistente.setDados(rs);
                persistente.setPersistente(true);
            }
            
            rs.close();
            stm.close();
            conn.close();
        }
    }
	
	public void recuperarPorCnpj(String cnpj) throws SQLException {
        String sql = "select p.* "
                   + "from Pessoa p "
                   + "where p.cnpj = ?";
        Connection conn = ConexaoBD.getConnection();

        if (conn != null) {
        	PreparedStatement stm = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            stm.setString(1, cnpj);
            
            ResultSet rs = stm.executeQuery();
            
            if (rs.next()) {
                persistente.setId(rs.getInt("id"));
                persistente.setDados(rs);
                persistente.setPersistente(true);
            }
            
            rs.close();
            stm.close();
            conn.close();
        }
    }
	
	public Integer proximoId() throws SQLException {
        String sql = "SHOW TABLE STATUS WHERE `Name` = 'pessoa'";
        Integer id=0;
        
        Connection conn = ConexaoBD.getConnection();
        if (conn != null) {
        	PreparedStatement stm = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            ResultSet rs = stm.executeQuery();
            
            if (rs.next()) {
            	id = rs.getInt("Auto_increment");
            }
            
            rs.close();
            stm.close();
            conn.close();
        }
        return id;
    }
	
	public Integer countLocador() throws SQLException {
    	String sql = "SELECT count(id) AS count FROM pessoa p WHERE p.flg_locador = true";

    	Connection conn = ConexaoBD.getConnection();
        if (conn != null) {
            PreparedStatement stm = conn.prepareStatement(sql);
            
            ResultSet rs = stm.executeQuery();
            
            Integer count = null;
            
            if (rs.next()){
                count = rs.getInt("count");
            }
            
            rs.close();
            stm.close();
            conn.close();
            
            return count;
        }

        return null;
    }
	public Integer countLocatario() throws SQLException {
    	String sql = "SELECT count(id) AS count FROM pessoa p WHERE p.flg_locatario = true";
    	Connection conn = ConexaoBD.getConnection();

        if (conn != null) {
            PreparedStatement stm = conn.prepareStatement(sql);
            
            ResultSet rs = stm.executeQuery();
            
            Integer count = null;
            
            if (rs.next()){
                count = rs.getInt("count");
            }
            
            rs.close();
            stm.close();
            conn.close();
            
            return count;
        }

        return null;
    }
	
	public Integer countFiador() throws SQLException {
    	String sql = "SELECT count(id) AS count FROM pessoa p WHERE p.flg_fiador = true";

    	Connection conn = ConexaoBD.getConnection();
        if (conn != null) {
            PreparedStatement stm = conn.prepareStatement(sql);
            
            ResultSet rs = stm.executeQuery();
            
            Integer count = null;
            
            if (rs.next()){
                count = rs.getInt("count");
            }
            
            rs.close();
            stm.close();
            conn.close();
            
            return count;
        }

        return null;
    }
	
	public Integer countTodos() throws SQLException {
    	String sql = "SELECT count(id) AS count FROM pessoa p";

    	Connection conn = ConexaoBD.getConnection();
        if (conn != null) {
            PreparedStatement stm = conn.prepareStatement(sql);
            
            ResultSet rs = stm.executeQuery();
            
            Integer count = null;
            
            if (rs.next()){
                count = rs.getInt("count");
            }
            
            rs.close();
            stm.close();
            conn.close();
            
            return count;
        }

        return null;
    }
}
