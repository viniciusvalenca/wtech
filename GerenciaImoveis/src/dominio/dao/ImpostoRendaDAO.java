package dominio.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.mysql.jdbc.Statement;

import dao.ConexaoBD;
import dao.DAO;
import dominio.bean.entity.ImpostoRenda;

public class ImpostoRendaDAO extends DAO<ImpostoRenda> {

	public ImpostoRendaDAO(ImpostoRenda persistente) {
		super(persistente);
	}

	@Override
	public Integer inserir(Connection conn) throws SQLException {
		String sql = "INSERT INTO `absi`.`imposto_renda` (`id`,`fk_id_locador`,`mes_ref`, `valor_desconto`,`ajuste_aluguel`, `ajuste_txadm`,`fk_id_locacao`) values (NULL, ?, ?, ?, ?, ?, ?)";


		if (conn != null) {
			PreparedStatement stm = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

			stm.setInt(1, persistente.getLocador().getId());
			stm.setDate(2, persistente.getMes_ref());
			stm.setBigDecimal(3, persistente.getValor_desconto());
			stm.setBigDecimal(4, persistente.getAjuste_aluguel());
			stm.setBigDecimal(5, persistente.getAjuste_txadm());
			if(persistente.getFk_id_locacao()!=null){

				stm.setInt(6, persistente.getFk_id_locacao());
			}else

				stm.setNull(6, java.sql.Types.NULL);
			stm.execute();

			ResultSet rs = stm.getGeneratedKeys();

			Integer id = null;

			if (rs.next())
				id = rs.getInt(1);

			rs.close();
			stm.close();
			
			return id;
		}

		return null;
	}

	@Override
	public void atualizar(Connection conn) throws SQLException {
		String sql = "update imposto_renda set fk_id_locador=?, mes_ref=?, valor_desconto=?, fk_id_locacao=? , ajuste_aluguel=?, ajuste_txadm=? where id=?";
        

        if (conn != null) {
            PreparedStatement stm = conn.prepareStatement(sql);

            stm.setInt(1, persistente.getLocador().getId());
			stm.setDate(2, persistente.getMes_ref());
			stm.setBigDecimal(3, persistente.getValor_desconto());
			if(persistente.getFk_id_locacao() == null){
            	stm.setNull(4, java.sql.Types.NULL);
            }else{
            	stm.setInt(4, persistente.getFk_id_locacao());
            }
			
			stm.setBigDecimal(5, persistente.getAjuste_aluguel());
			stm.setBigDecimal(6, persistente.getAjuste_txadm());
			stm.setInt(7, persistente.getId());
			System.out.println(stm);
			stm.execute();
            
            stm.close();
        }
	}

	@Override
	public void excluir(Connection conn) throws SQLException {
		String sql = "delete from imposto_renda where id=?";


		if (conn != null) {
			PreparedStatement stm = conn.prepareStatement(sql);

			stm.setInt(1, persistente.getId());

			stm.execute();

			stm.close();
		}
	}
	
	public void recuperarPorLocador(int idlocador, Date mes_ref) throws SQLException{
		String sql = "select * from Imposto_renda ir where ir.fk_id_locador = ? and ir.mes_ref = ?";
     
		Connection conn = ConexaoBD.getConnection();
		
        if (conn != null) {
            PreparedStatement stm = conn.prepareStatement(sql);

            stm.setInt(1, idlocador);
            stm.setDate(2, mes_ref);
            
            ResultSet rs = stm.executeQuery();
            
            if (rs.next()) {
                persistente.setId(rs.getInt("id"));
                persistente.setDados(rs);
                persistente.setPersistente(true);
            }
            
            rs.close();
            stm.close();
            conn.close();
        }
	}
	
	public void recuperarPorLocadorSemLocacao(int idlocador, Date mes_ref) throws SQLException{
		String sql = "select * from Imposto_renda ir where ir.fk_id_locador = ? and ir.mes_ref = ? and fk_id_locacao is null";
     
		Connection conn = ConexaoBD.getConnection();
		
        if (conn != null) {
            PreparedStatement stm = conn.prepareStatement(sql);

            stm.setInt(1, idlocador);
            stm.setDate(2, mes_ref);
            
            ResultSet rs = stm.executeQuery();
            
            if (rs.next()) {
                persistente.setId(rs.getInt("id"));
                persistente.setDados(rs);
                persistente.setPersistente(true);
            }
            
            rs.close();
            stm.close();
            conn.close();
        }
	}
	
	public void recuperarPorLocadorLocacao(int idlocador, Date mes_ref, int idlocacao) throws SQLException{
		String sql = "select * from Imposto_renda ir where ir.fk_id_locador = ? and MONTH(ir.mes_ref) = MONTH(?) and YEAR(ir.mes_ref) = YEAR(?)and ir.fk_id_locacao = ?";
     
		Connection conn = ConexaoBD.getConnection();
		
        if (conn != null) {
            PreparedStatement stm = conn.prepareStatement(sql);

            stm.setInt(1, idlocador);
            stm.setDate(2, mes_ref);
            stm.setDate(3, mes_ref);
            stm.setInt(4, idlocacao);
            
            ResultSet rs = stm.executeQuery();
            
            if (rs.next()) {
                persistente.setId(rs.getInt("id"));
                persistente.setDados(rs);
                persistente.setPersistente(true);
            }
            
            rs.close();
            stm.close();
            conn.close();
        }
	}
}
