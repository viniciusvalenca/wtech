package dominio.dao;

import dao.ConexaoBD;
import dao.DAO;
import dominio.bean.entity.Contrato;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.mysql.jdbc.Statement;

public class ContratoDAO extends DAO<Contrato> {

    public ContratoDAO(Contrato persistente) {
        super(persistente);        
    }

    @Override
    public Integer inserir(Connection conn) throws SQLException {
        String sql = "INSERT INTO Contrato (`id` , `dt_inicio` , `dt_termino`, `dt_reajuste`, "
        		+ "`dt_entrega_chave`, `dia_vencimento`) "
        		+ "VALUES (NULL, ?, ?, ?, ?, ?)";


        if (conn != null) {
        	PreparedStatement stm = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        	
            stm.setDate(1,(Date) persistente.getDt_inicio());
            stm.setDate(2,(Date) persistente.getDt_termino());
            stm.setDate(3,(Date) persistente.getDt_reajuste());
            stm.setDate(4,(Date) persistente.getDt_entrega_chave());
            stm.setInt(5,persistente.getDia_vencimento());
            
            stm.execute();
            
            ResultSet rs = stm.getGeneratedKeys();
            
            Integer id = null;
            
            if (rs.next())
                id = rs.getInt(1);
            
            rs.close();
            stm.close();
            
            return id;
        }

        return null;
    }

    @Override
    public void atualizar(Connection conn) throws SQLException {
        
        String sql = "update Contrato set dt_inicio=?, dt_termino=?,"
        		+ " dt_reajuste=?, dt_entrega_chave=?, dia_vencimento=?, flg_renovar=? where id=?";
        
        if (conn != null) {
        	PreparedStatement stm = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

        	stm.setDate(1,(Date) persistente.getDt_inicio());
            stm.setDate(2,(Date) persistente.getDt_termino());
            stm.setDate(3,(Date) persistente.getDt_reajuste());
            stm.setDate(4,(Date) persistente.getDt_entrega_chave());
            stm.setInt(5,persistente.getDia_vencimento());
            stm.setBoolean(6, persistente.getFlg_renovar());
            stm.setInt(7, persistente.getId());
            
            stm.execute();
            
            stm.close();
        }
    }

    @Override
    public void excluir(Connection conn) throws SQLException {
        String sql = "delete Contrato where id=?";
        

        if (conn != null) {
            PreparedStatement stm = conn.prepareStatement(sql);

            stm.setInt(1, persistente.getId());
            
            stm.execute();
            
            stm.close();
        }
    }
    
    public void recuperarPorId(int codigo) throws SQLException {
        String sql = "select c.* "
                   + "from Contrato c "
                   + "where c.id = ?";
        
        Connection conn = ConexaoBD.getConnection();
        if (conn != null) {
            PreparedStatement stm = conn.prepareStatement(sql);

            stm.setInt(1, codigo);
            
            ResultSet rs = stm.executeQuery();
            
            if (rs.next()) {
                persistente.setId(rs.getInt("id"));
                persistente.setDados(rs);
                persistente.setPersistente(true);
            }
            
            rs.close();
            stm.close();
            conn.close();
        }
    }
    
    
}
