package dominio.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import dao.ConexaoBD;
import dao.DAOLista;
import dao.Persistente;
import dominio.bean.entity.Celular;
import dominio.bean.list.ListaCelular;

public class ListaCelularDAO extends DAOLista<ListaCelular> {
	public ListaCelularDAO(ListaCelular Celular) {
        super(Celular);
    }

	public void recuperarPorPessoaId(int id) throws SQLException {

		String sql = "SELECT c.* FROM Celular c WHERE c.fk_id_pessoa = ?";
		Connection conn = ConexaoBD.getConnection();

		if (conn != null) {
			PreparedStatement stm = conn.prepareStatement(sql);
			stm.setInt(1, id);

			ResultSet rs = stm.executeQuery();

			while (rs.next()) {
				Persistente i = listaPersistente.getPersistente();
				i.setId(rs.getInt("id"));
				i.setDados(rs);
				i.setPersistente(true);
				listaPersistente.addPersistente((Celular) i);
			}

			rs.close();
			stm.close();
			conn.close();
		}
	}
}
