package dominio.dao;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.mysql.jdbc.Statement;

import dao.ConexaoBD;
import dao.DAO;
import dominio.bean.entity.Telefone;

public class TelefoneDAO extends DAO<Telefone> {

	public TelefoneDAO(Telefone persistente) {
		super(persistente);
	}

	@Override
	public Integer inserir(Connection conn) throws SQLException {
		String sql = "INSERT INTO `absi`.`Telefone` (`id`,`num_Telefone`,`ddd`, `fk_id_pessoa`) values (NULL, ?, ?, ?)";


		if (conn != null) {
			PreparedStatement stm = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

			stm.setInt(1, persistente.getNum_telefone());
			stm.setInt(2, persistente.getDdd());
			stm.setInt(3, persistente.getId_pessoa());

			stm.execute();

			ResultSet rs = stm.getGeneratedKeys();

			Integer id = null;

			if (rs.next())
				id = rs.getInt(1);

			rs.close();
			stm.close();

			return id;
		}

		return null;
	}

	@Override
	public void atualizar(Connection conn) throws SQLException {
		String sql = "update Telefone set num_telefone=?, ddd=?, fk_id_pessoa=? where id=?";
        

        if (conn != null) {
            PreparedStatement stm = conn.prepareStatement(sql);

            stm.setInt(1, persistente.getNum_telefone());
			stm.setInt(2, persistente.getDdd());
			stm.setInt(3, persistente.getId_pessoa());
			stm.setInt(4, persistente.getId());
            
			stm.execute();
            
            stm.close();
        }
	}

	@Override
	public void excluir(Connection conn) throws SQLException {
		String sql = "delete from Telefone where id=?";


		if (conn != null) {
			PreparedStatement stm = conn.prepareStatement(sql);

			stm.setInt(1, persistente.getId());

			stm.execute();

			stm.close();
		}
	}

	public void recuperarPorID(int id) throws SQLException {
		String sql = "select e.* " + "from Telefone e " + "where e.id = ?";

		Connection conn = ConexaoBD.getConnection();
		if (conn != null) {
			PreparedStatement stm = conn.prepareStatement(sql);

			stm.setInt(1, id);

			ResultSet rs = stm.executeQuery();

			if (rs.next()) {
				persistente.setId(rs.getInt("id"));
				persistente.setDados(rs);
				persistente.setPersistente(true);
			}

			rs.close();
			stm.close();
			conn.close();
		}
	}

}
