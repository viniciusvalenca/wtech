package dominio.dao;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.mysql.jdbc.Statement;

import dao.ConexaoBD;
import dao.DAO;
import dominio.bean.entity.Usuario;

public class UsuarioDAO extends DAO<Usuario> {
	
	public UsuarioDAO(Usuario persistente) {
        super(persistente);
    }

	@Override
	public Integer inserir(Connection conn) throws SQLException {
		String sql = "INSERT INTO Usuario (`id`,`nome`,`dt_acesso`,`senha`,`login`) "
				+ "VALUES (NULL, ?, ?, ?, ?)";
	

		        if (conn != null) {
		        	PreparedStatement stm = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

		            stm.setString(1, persistente.getNome());
		            stm.setTimestamp(2, persistente.getDt_acesso());
		            stm.setString(3, persistente.getSenha());
		            stm.setString(4, persistente.getLogin());
		            
		            stm.execute();
		            
		            ResultSet rs = stm.getGeneratedKeys();
		            
		            Integer id = null;
		            
		            if (rs.next())
		                id = rs.getInt(1);
		            
		            rs.close();
		            stm.close();
		            
		            return id;
		        }

		        return null;
	}

	@Override
	public void atualizar(Connection conn) throws SQLException {
		String sql = "update Usuario set nome=?, dt_acesso=?, senha=?, login=? where id=?";
        

        if (conn != null) {
        	PreparedStatement stm = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

        	stm.setString(1, persistente.getNome());
            stm.setTimestamp(2, persistente.getDt_acesso());
            stm.setString(3, persistente.getSenha());
            stm.setString(4, persistente.getLogin());
            stm.setInt(5, persistente.getId());
            
            stm.execute();
            
            stm.close();
        }
		
	}

	@Override
	public void excluir(Connection conn) throws SQLException {
		String sql = "delete from Usuario where id=?";


		if (conn != null) {
			PreparedStatement stm = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

			stm.setInt(1, persistente.getId());

			stm.execute();

			stm.close();
		}
		
	}
	
	public void recuperarPorLogin(String login) throws SQLException {
        String sql = "select p.* from Usuario p "
                   + "where p.login = ?";
        Connection conn = ConexaoBD.getConnection();
        if (conn != null) {
        	PreparedStatement stm = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            stm.setString(1, login);
            
            ResultSet rs = stm.executeQuery();
            
            if (rs.next()) {
                persistente.setId(rs.getInt("id"));
                persistente.setDados(rs);
                persistente.setPersistente(true);
            }
            
            rs.close();
            stm.close();
            conn.close();
        }
    }
	
	public void recuperarPorId(int id) throws SQLException {
        String sql = "select p.* "
                   + "from Usuario p "
                   + "where p.id = ?";
        
        Connection conn = ConexaoBD.getConnection();
        if (conn != null) {
        	PreparedStatement stm = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            stm.setInt(1, id);
            
            ResultSet rs = stm.executeQuery();
            
            if (rs.next()) {
                persistente.setId(rs.getInt("id"));
                persistente.setDados(rs);
                persistente.setPersistente(true);
            }
            
            rs.close();
            stm.close();
            conn.close();
        }
    }
	
	public Integer proximoId() throws SQLException {
        String sql = "SHOW TABLE STATUS WHERE `Name` = 'usuario'";
        Integer id=0;
        
        Connection conn = ConexaoBD.getConnection();
        if (conn != null) {
        	PreparedStatement stm = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            ResultSet rs = stm.executeQuery();
            
            if (rs.next()) {
            	id = rs.getInt("Auto_increment");
            }
            
            rs.close();
            stm.close();
            conn.close();
        }
        return id;
    }
	
}
