package dominio.dao;

import dao.ConexaoBD;
import dao.DAO;
import dominio.bean.entity.Endereco;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.mysql.jdbc.Statement;


public class EnderecoDAO extends DAO<Endereco> {

  
    public EnderecoDAO(Endereco persistente) {
        super(persistente);
    }
 
    @Override
    public Integer inserir(Connection conn) throws SQLException {
        String sql = "INSERT INTO Endereco (`id`, `endereco`, `bairro`, `cep`, `numero`, `estado`,"
        		+ " `cidade`, `complemento`, `nom_condominio`) " 
		+"VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?)";
        

        if (conn != null) {
        	PreparedStatement stm = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
           
            stm.setString(1, persistente.getEndereco());
            stm.setString(2, persistente.getBairro());
            stm.setString(3, persistente.getCep());
            stm.setString(4, persistente.getNumero());
            stm.setString(5, persistente.getEstado());
            stm.setString(6, persistente.getCidade());
            stm.setString(7, persistente.getComplemento());
            stm.setString(8, persistente.getNom_condominio());
            
            stm.execute();
            
            ResultSet rs = stm.getGeneratedKeys();
            
            Integer id = null;
            
            if (rs.next())
                id = rs.getInt(1);
            
            rs.close();
            stm.close();
            
            return id;
        }

        return null;
    }    
    
    @Override
    public void atualizar(Connection conn) throws SQLException {
    	String sql = "update Endereco set endereco=?, bairro=?, cep=?, numero=?, "
    			+ "estado=?, cidade=?, complemento=?, nom_condominio=? where id=?";
        

        if (conn != null) {
        	PreparedStatement stm = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            stm.setString(1, persistente.getEndereco());
            stm.setString(2, persistente.getBairro());
            stm.setString(3, persistente.getCep());
            stm.setString(4, persistente.getNumero());
            stm.setString(5, persistente.getEstado());
            stm.setString(6, persistente.getCidade());
            stm.setString(7, persistente.getComplemento());
            stm.setString(8, persistente.getNom_condominio());
            stm.setInt(9, persistente.getId());
            
            stm.execute();
            
            stm.close();
        }
    }

    @Override
    public void excluir(Connection conn) throws SQLException {
        String sql = "delete from Endereco where id=?";
        

        if (conn != null) {
        	PreparedStatement stm = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            stm.setInt(1, persistente.getId());
            
            stm.execute();
            
            stm.close();
        }
    }
    

    public void recuperarPorId(int id) throws SQLException {
        String sql = "select e.* "
                   + "from Endereco e "
                   + "where e.id = ?";
        Connection conn = ConexaoBD.getConnection();
        if (conn != null) {
        	PreparedStatement stm = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            stm.setInt(1, id);
            
            ResultSet rs = stm.executeQuery();
            
            if (rs.next()) {
                persistente.setId(rs.getInt("id"));
                persistente.setDados(rs);
                persistente.setPersistente(true);
            }
            
            rs.close();
            stm.close();
            conn.close();
        }
    }
}
