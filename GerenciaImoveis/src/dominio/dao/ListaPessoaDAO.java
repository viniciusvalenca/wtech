package dominio.dao;

import dao.Persistente;
import dominio.bean.entity.Pessoa;
import dominio.bean.list.ListaPessoa;
import dao.ConexaoBD;
import dao.DAOLista;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ListaPessoaDAO extends DAOLista<ListaPessoa> {

	public ListaPessoaDAO(ListaPessoa pessoa) {
		super(pessoa);
	}

	public void recuperarTodos() throws SQLException {

		String sql = "select * from Pessoa";
		Connection conn = ConexaoBD.getConnection();

		if (conn != null) {
			PreparedStatement stm = conn.prepareStatement(sql);

			ResultSet rs = stm.executeQuery();

			while (rs.next()) {
				Persistente p = listaPersistente.getPersistente();
				p.setId(rs.getInt("id"));
				p.setDados(rs);
				p.setPersistente(true);
				listaPersistente.addPersistente((Pessoa) p);
			}

			rs.close();
			stm.close();
			conn.close();
		}
	}

	public void recuperarTodosLocador() throws SQLException {

		String sql = "select * from Pessoa where flg_locador=true";

		Connection conn = ConexaoBD.getConnection();
		if (conn != null) {
			PreparedStatement stm = conn.prepareStatement(sql);

			ResultSet rs = stm.executeQuery();

			while (rs.next()) {
				Persistente p = listaPersistente.getPersistente();
				p.setId(rs.getInt("id"));
				p.setDados(rs);
				p.setPersistente(true);
				listaPersistente.addPersistente((Pessoa) p);
			}

			rs.close();
			stm.close();
			conn.close();
		}
	}

	public void recuperarTodosLocatario() throws SQLException {

		String sql = "select * from Pessoa where flg_locatario=true";

		Connection conn = ConexaoBD.getConnection();
		if (conn != null) {
			PreparedStatement stm = conn.prepareStatement(sql);

			ResultSet rs = stm.executeQuery();

			while (rs.next()) {
				Persistente p = listaPersistente.getPersistente();
				p.setId(rs.getInt("id"));
				p.setDados(rs);
				p.setPersistente(true);
				listaPersistente.addPersistente((Pessoa) p);
			}

			rs.close();
			stm.close();
			conn.close();
		}
	}

	public void recuperarTodosFiador() throws SQLException {

		String sql = "select * from Pessoa where flg_fiador=true";

		Connection conn = ConexaoBD.getConnection();
		if (conn != null) {
			PreparedStatement stm = conn.prepareStatement(sql);

			ResultSet rs = stm.executeQuery();

			while (rs.next()) {
				Persistente p = listaPersistente.getPersistente();
				p.setId(rs.getInt("id"));
				p.setDados(rs);
				p.setPersistente(true);
				listaPersistente.addPersistente((Pessoa) p);
			}

			rs.close();
			stm.close();
			conn.close();
		}
	}

	public void recuperar(Boolean flg_locatario, Boolean flg_locador, Boolean flg_fiador) throws SQLException {

		String sql = "select * from Pessoa where flg_locatario =? or flg_locador = ? or flg_fiador = ?";
		Connection conn = ConexaoBD.getConnection();

		if (conn != null) {
			PreparedStatement stm = conn.prepareStatement(sql);
			if (!flg_locatario)
				stm.setNull(1, java.sql.Types.NULL);
			else
				stm.setBoolean(1, flg_locatario);

			if (!flg_locador)
				stm.setNull(2, java.sql.Types.NULL);
			else
				stm.setBoolean(2, flg_locador);

			if (!flg_fiador)
				stm.setNull(3, java.sql.Types.NULL);
			else
				stm.setBoolean(3, flg_fiador);

			ResultSet rs = stm.executeQuery();

			while (rs.next()) {
				Persistente p = listaPersistente.getPersistente();
				p.setId(rs.getInt("id"));
				p.setDados(rs);
				p.setPersistente(true);
				listaPersistente.addPersistente((Pessoa) p);
			}

			rs.close();
			stm.close();
			conn.close();
		}
	}
	
	public void recuperarAniversariantes() throws SQLException {

		String sql = "SELECT * FROM Pessoa where flg_locador=true and DATE_FORMAT(dt_nascimento, '%m-%d') = DATE_FORMAT(CURDATE(), '%m-%d')";

		Connection conn = ConexaoBD.getConnection();
		if (conn != null) {
			PreparedStatement stm = conn.prepareStatement(sql);

			ResultSet rs = stm.executeQuery();

			while (rs.next()) {
				Persistente p = listaPersistente.getPersistente();
				p.setId(rs.getInt("id"));
				p.setDados(rs);
				p.setPersistente(true);
				listaPersistente.addPersistente((Pessoa) p);
			}

			rs.close();
			stm.close();
			conn.close();
		}
	}

}
