package dominio.dao;

import dao.Persistente;
import dominio.bean.entity.Contrato;
import dominio.bean.list.ListaContrato;
import dao.ConexaoBD;
import dao.DAOLista;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.mysql.jdbc.CallableStatement;

public class ListaContratoDAO extends DAOLista<ListaContrato> {

    public ListaContratoDAO(ListaContrato contrato) {
        super(contrato);
    }

	public void recuperarTodos() throws SQLException {
        String sql = "select * from Contrato";
        
        Connection conn = ConexaoBD.getConnection();
        if (conn != null) {
            PreparedStatement stm = conn.prepareStatement(sql);
            
            ResultSet rs = stm.executeQuery();
            
            while (rs.next()) {
                Persistente i = listaPersistente.getPersistente();
                i.setId(rs.getInt("id"));
                i.setDados(rs);
                i.setPersistente(true);
                listaPersistente.addPersistente((Contrato) i);
            }
            
            rs.close();
            stm.close();
            conn.close();
        }
    }

	public void recuperarPorLocacaoId(int id) throws SQLException {
        String sql = "select c.* from Contrato c WHERE c.fk_id_locacao = ?";
        
        Connection conn = ConexaoBD.getConnection();
        if (conn != null) {
        	PreparedStatement stm = conn.prepareStatement(sql);
			stm.setInt(1, id);
            
            ResultSet rs = stm.executeQuery();
            
            while (rs.next()) {
                Persistente i = listaPersistente.getPersistente();
                i.setId(rs.getInt("id"));
                i.setDados(rs);
                i.setPersistente(true);
                listaPersistente.addPersistente((Contrato) i);
            }
            
            rs.close();
            stm.close();
            conn.close();
        }
    }
	
	public void renovarContratos() throws SQLException {
		
		        java.sql.CallableStatement cs = null;
		        Connection conn = ConexaoBD.getConnection();
		           
		                 
        if (conn != null) {
        	 cs = conn.prepareCall("{call renovarContrato()}");		            
	            cs.execute();
            
            cs.close();
            conn.close();
        }
    }
	
	public void aplicarIndice() throws SQLException {
		
        java.sql.CallableStatement cs = null;
        Connection conn = ConexaoBD.getConnection();
           
                 
if (conn != null) {
	 cs = conn.prepareCall("{call aplicarIndice()}");		            
        cs.execute();
    
    cs.close();
    conn.close();
}
}
	
	
}
