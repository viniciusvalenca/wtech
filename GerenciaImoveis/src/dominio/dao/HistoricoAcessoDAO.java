package dominio.dao;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.mysql.jdbc.Statement;

import dao.DAO;
import dominio.bean.entity.HistoricoAcesso;

public class HistoricoAcessoDAO extends DAO<HistoricoAcesso> {

	public HistoricoAcessoDAO(HistoricoAcesso persistente) {
		super(persistente);
	}

	@Override
	public Integer inserir(Connection conn) throws SQLException {
		String sql = "INSERT INTO `absi`.`historico_acesso` (`id`,`dt_modificacao`,`nom_tela`, `descricao`, `fk_id_usuario`) values (NULL, ?, ?, ?, ?)";


		if (conn != null) {
			PreparedStatement stm = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

			stm.setTimestamp(1, persistente.getDt_modificacao());
			stm.setString(2, persistente.getNom_tela());
			stm.setString(3, persistente.getDescricao());
			stm.setInt(4, persistente.getUsuario().getId());

			stm.executeUpdate();

			ResultSet rs = stm.getGeneratedKeys();

			Integer id = null;

			if (rs.next())
				id = rs.getInt(1);

			rs.close();
			stm.close();

			return id;
		}

		return null;
	}

	@Override
	public void atualizar(Connection conn) throws SQLException {}

	@Override
	public void excluir(Connection conn) throws SQLException {}
}
