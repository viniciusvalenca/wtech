package dominio.dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.NumberFormat;

import com.mysql.jdbc.Statement;

import dao.ConexaoBD;
import dao.DAO;
import dominio.bean.entity.BoletoAbsi;

public class BoletoAbsiDAO extends DAO<BoletoAbsi> {

	public BoletoAbsiDAO(BoletoAbsi persistente) {
		super(persistente);
	}

	@Override
	public Integer inserir(Connection conn) throws SQLException {
		String sql = "INSERT INTO Boleto (`id`,`numero_documento`,`seu_numero`, `valor`,`mes_ref`,`dt_documento`,"
				+ "`dt_vencimento`,`dt_pagamento`,`flg_enviado`,`arq_remessa`,`arq_retorno`,`valor_comissao`,"
				+ "`fk_id_locacao`, `digitoDoNossoNumero`, `valor_aluguel`, `lucro_txbancaria`) "
				+ "VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

		if (conn != null) {
			PreparedStatement stm = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

			stm.setString(1, persistente.getNumero_documento());
			stm.setString(2, persistente.getSeu_numero());
			stm.setBigDecimal(3, persistente.getValor());
			stm.setDate(4, persistente.getMes_ref());
			stm.setDate(5,  persistente.getDt_documento());
			stm.setDate(6, persistente.getDt_vencimento());
		
			if (persistente.getDt_pagamento() == null) {
				stm.setNull(7, java.sql.Types.NULL);
			} else {
				stm.setDate(7, persistente.getDt_pagamento());
			}
			
			stm.setBoolean(8, persistente.getFlg_enviado());
			
			if (persistente.getArq_remessa() == null) {
				stm.setNull(9, java.sql.Types.NULL);
			} else {
				stm.setString(9, persistente.getArq_remessa());
			}
			if (persistente.getArq_retorno() == null) {
				stm.setNull(10, java.sql.Types.NULL);
			} else {
				stm.setString(10, persistente.getArq_retorno());
			}
			stm.setBigDecimal(11, persistente.getValor_comissao());
			stm.setInt(12, persistente.getLocacao().getId());
			stm.setString(13, persistente.getDigitoDoNossoNumero());
			stm.setBigDecimal(14, persistente.getvalor_aluguel());
			stm.setBigDecimal(15, persistente.getLucro_txbancaria());
			
			stm.execute();

			ResultSet rs = stm.getGeneratedKeys();

			Integer id = null;

			if (rs.next())
				id = rs.getInt(1);

			rs.close();
			stm.close();

			return id;
		}

		return null;
	}

	@Override
	public void atualizar(Connection conn) throws SQLException {
		String sql = "update Boleto set numero_documento=?, seu_numero=?, valor=?, mes_ref=?, dt_documento=?,"
				+ "dt_vencimento=?, dt_pagamento=?, flg_enviado=?, arq_remessa=?, arq_retorno=?, valor_comissao=?, "
				+ "fk_id_locacao=?, digitoDoNossoNumero=?, valor_juros=?, valor_despesa_cobranca=?, valor_pago=? ,"
				+ " lucro_txbancaria=?, dt_formulario_transf=?, dt_pag_to_locador=?, valor_pag_to_locador=?, cheque=? , valor_aluguel=? where id=?";
		
		if (conn != null) {
			PreparedStatement stm = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

			stm.setString(1, persistente.getNumero_documento());
			stm.setString(2, persistente.getSeu_numero());
			stm.setBigDecimal(3, persistente.getValor());
			stm.setDate(4, persistente.getMes_ref());
			
			stm.setDate(5,  persistente.getDt_documento());
			stm.setDate(6, persistente.getDt_vencimento());
		
			if (persistente.getDt_pagamento() == null) {
				stm.setNull(7, java.sql.Types.NULL);
			} else {
				stm.setDate(7, persistente.getDt_pagamento());
			}
			
			stm.setBoolean(8, persistente.getFlg_enviado());
			
			if (persistente.getArq_remessa() == null) {
				stm.setNull(9, java.sql.Types.NULL);
			} else {
				stm.setString(9, persistente.getArq_remessa());
			}
			if (persistente.getArq_retorno() == null) {
				stm.setNull(10, java.sql.Types.NULL);
			} else {
				stm.setString(10, persistente.getArq_retorno());
			}
			stm.setBigDecimal(11, persistente.getValor_comissao());
			stm.setInt(12, persistente.getLocacao().getId());
			stm.setString(13, persistente.getDigitoDoNossoNumero());
			stm.setBigDecimal(14, persistente.getValor_juros());
			stm.setBigDecimal(15, persistente.getValor_despesa_cobranca());
			stm.setBigDecimal(16, persistente.getValor_pago());
			stm.setBigDecimal(17, persistente.getLucro_txbancaria());
			if (persistente.getDt_formulario_transf() == null) {
				stm.setNull(18, java.sql.Types.NULL);
			} else {
				stm.setDate(18, persistente.getDt_formulario_transf());
			}
			
			if (persistente.getDt_pag_to_locador() == null) {
				stm.setNull(19, java.sql.Types.NULL);
			} else {
				stm.setDate(19, persistente.getDt_pag_to_locador());
			}
			
			if (persistente.getValor_pag_to_locador() == null) {
				stm.setNull(20, java.sql.Types.NULL);
			} else {
				stm.setBigDecimal(20, persistente.getValor_pag_to_locador());
			}
			
			if (persistente.getCheque() == null) {
				stm.setNull(21, java.sql.Types.NULL);
			} else {
				stm.setInt(21, persistente.getCheque());
			}
			stm.setBigDecimal(22, persistente.getvalor_aluguel());
			stm.setInt(23, persistente.getId());

			stm.execute();

			stm.close();
		}

	}

	@Override
	public void excluir(Connection conn) throws SQLException {
		String sql = "delete from Boleto where id=?";

		if (conn != null) {
			PreparedStatement stm = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

			stm.setInt(1, persistente.getId());

			stm.execute();

			stm.close();
		}

	}

	public void recuperarPorID(int id) throws SQLException {
		String sql = "select b.* from Boleto b where b.id = ?";
		Connection conn = ConexaoBD.getConnection();

		if (conn != null) {
			PreparedStatement stm = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

			stm.setInt(1, id);

			ResultSet rs = stm.executeQuery();

			if (rs.next()) {
				persistente.setId(rs.getInt("id"));
				persistente.setDados(rs);
				persistente.setPersistente(true);
			}

			rs.close();
			stm.close();
			conn.close();
		}
	}
	
	public void recuperarPorIdLocacao(int id) throws SQLException {
		String sql = "select b.* from Boleto b where b.fk_id_locacao = ?";
		Connection conn = ConexaoBD.getConnection();

		if (conn != null) {
			PreparedStatement stm = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

			stm.setInt(1, id);

			ResultSet rs = stm.executeQuery();

			if (rs.next()) {
				persistente.setId(rs.getInt("id"));
				persistente.setDados(rs);
				persistente.setPersistente(true);
			}

			rs.close();
			stm.close();
			conn.close();
		}
	}
	
	public void recuperarPorSeuNumero(Integer seuNumero) throws SQLException {
		String sql = "select b.* from Boleto b where b.seu_numero = ?";
		Connection conn = ConexaoBD.getConnection();

		if (conn != null) {
			PreparedStatement stm = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

			stm.setInt(1, seuNumero);

			ResultSet rs = stm.executeQuery();

			if (rs.next()) {
				persistente.setId(rs.getInt("id"));
				persistente.setDados(rs);
				persistente.setPersistente(true);
			}

			rs.close();
			stm.close();
			conn.close();
		}
	}
	
	public void buscarBoletoLocacaoMes(Integer idlocacao, Date mes_ref) throws SQLException{
		
		String sql = "SELECT distinct b.* FROM absi.boleto b where b.fk_id_locacao = ? " 
		+ "and MONTH(b.mes_ref) = MONTH(?) AND YEAR(b.mes_ref) = YEAR(?)"
		+ "order by b.id desc limit 1";

		Connection conn = ConexaoBD.getConnection();

		if (conn != null) {
			PreparedStatement stm = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

			stm.setInt(1, idlocacao);
			stm.setDate(2, mes_ref);
			stm.setDate(3, mes_ref);
			
			ResultSet rs = stm.executeQuery();

			if (rs.next()) {
				persistente.setId(rs.getInt("id"));
				persistente.setDados(rs);
				persistente.setPersistente(true);
			}

			rs.close();
			stm.close();
			conn.close();
		}
}
	
	public Integer proximoId() throws SQLException {
        String sql = "SHOW TABLE STATUS WHERE `Name` = 'boleto'";
        Integer id=0;
        
        Connection conn = ConexaoBD.getConnection();
        if (conn != null) {
        	PreparedStatement stm = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            ResultSet rs = stm.executeQuery();
            
            if (rs.next()) {
            	id = rs.getInt("Auto_increment");
            }
            
            rs.close();
            stm.close();
            conn.close();
        }
        return id;
    }
	
public void buscarBoletosLocacaoMes(Integer idlocacao, Date mes_ref) throws SQLException{
		
		String sql = "SELECT distinct b.* FROM absi.boleto b where b.fk_id_locacao = ? " 
		+ "and MONTH(b.mes_ref) = MONTH(?) and YEAR(b.mes_ref) = YEAR(?)"
		+ "order by b.id desc limit 1";

		Connection conn = ConexaoBD.getConnection();

		if (conn != null) {
			PreparedStatement stm = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

			stm.setInt(1, idlocacao);
			stm.setDate(2, mes_ref);
			stm.setDate(3, mes_ref);
			
			ResultSet rs = stm.executeQuery();

			if (rs.next()) {
				persistente.setId(rs.getInt("id"));
				persistente.setDados(rs);
				persistente.setPersistente(true);
			}

			rs.close();
			stm.close();
			conn.close();
		}
}
	public String recuperarLucroMes() throws SQLException {
        String sql = "SELECT SUM(b.lucro_txbancaria + b.valor_comissao) lucroMes FROM boleto b WHERE MONTH(b.dt_pagamento) = MONTH(curdate()) AND YEAR(b.dt_pagamento) = YEAR(curdate());";
        BigDecimal lucroMes = new BigDecimal(0);
        
        Connection conn = ConexaoBD.getConnection();
        if (conn != null) {
        	PreparedStatement stm = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            ResultSet rs = stm.executeQuery();
            
            if (rs.next()) {
            	lucroMes = rs.getBigDecimal("lucroMes");
            }
            
            rs.close();
            stm.close();
            conn.close();
        }
        if(lucroMes != null){
        	return NumberFormat.getCurrencyInstance().format(lucroMes);
        }else{
        	return NumberFormat.getCurrencyInstance().format(0);
        }
        
    }
}