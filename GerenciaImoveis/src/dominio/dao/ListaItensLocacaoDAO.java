package dominio.dao;

import dao.Persistente;
import dominio.bean.entity.ItensLocacao;
import dominio.bean.list.ListaItensLocacao;
import dao.ConexaoBD;
import dao.DAOLista;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ListaItensLocacaoDAO extends DAOLista<ListaItensLocacao> {

	public ListaItensLocacaoDAO(ListaItensLocacao lista) {
		super(lista);
	}

	public void recuperarTodos() throws SQLException {

		String sql = "select * from item";

		Connection conn = ConexaoBD.getConnection();

		if (conn != null) {
			PreparedStatement stm = conn.prepareStatement(sql);

			ResultSet rs = stm.executeQuery();

			while (rs.next()) {
				Persistente i = listaPersistente.getPersistente();
				i.setId(rs.getInt("id"));
				i.setDados(rs);
				i.setPersistente(true);
				listaPersistente.addPersistente((ItensLocacao) i);
			}

			rs.close();
			stm.close();
			conn.close();
		}
	}
	
	public boolean temItensLocacao(Integer idLocacao) throws SQLException {

		boolean retorno=false;
		String sql ="SELECT(" + 
				"CASE WHEN EXISTS(SELECT * FROM absi.item where fk_id_locacao =?) THEN 1 " + 
				"ELSE 0 " + 
				"END " + 
				") as existe";

		Connection conn = ConexaoBD.getConnection();

		if (conn != null) {
			PreparedStatement stm = conn.prepareStatement(sql);

			stm.setInt(1, idLocacao);
			ResultSet rs = stm.executeQuery();

			 if (rs.next()) {
				 retorno= rs.getBoolean("existe");
	            }

			rs.close();
			stm.close();
			conn.close();
		}
		return retorno;
	}

	public void recuperarDoMes(Date mes_ref, int id) throws SQLException {
		
		String sql = "SELECT * FROM absi.item WHERE MONTH(mes_ref) = MONTH(?) AND YEAR(mes_ref) = YEAR(?) AND fk_id_locacao = ?";
		
		Connection conn = ConexaoBD.getConnection();

		if (conn != null) {
			PreparedStatement stm = conn.prepareStatement(sql);
			
			stm.setDate(1, mes_ref);
			stm.setDate(2, mes_ref);
			stm.setInt(3, id);

			ResultSet rs = stm.executeQuery();

			while (rs.next()) {
				Persistente i = listaPersistente.getPersistente();
				i.setId(rs.getInt("id"));
				i.setDados(rs);
				i.setPersistente(true);
				listaPersistente.addPersistente((ItensLocacao) i);
			}

			rs.close();
			stm.close();
			conn.close();
		}
	}

}
