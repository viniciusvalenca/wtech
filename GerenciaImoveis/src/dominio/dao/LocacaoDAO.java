package dominio.dao;

import dao.ConexaoBD;
import dao.DAO;
import dao.Persistente;
import dominio.bean.entity.Locacao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.mysql.jdbc.Statement;

public class LocacaoDAO extends DAO<Locacao> {

	public LocacaoDAO(Locacao persistente) {
		super(persistente);
	}

	@Override
	public Integer inserir(Connection conn) throws SQLException {
		String sql = "INSERT INTO Locacao (`id`,  `modalidade`, `valor_deposito`, `valor_seguro`, `fk_id_locatario`, `fk_id_imovel`, `fk_id_contrato` )"
				+ "VALUES (NULL, ?, ?, ?, ?, ?, ?)";

		if (conn != null) {
			PreparedStatement stm = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

			stm.setString(1, persistente.getModalidade());
			stm.setString(2, persistente.getValor_deposito());
			stm.setString(3, persistente.getValor_seguro());
			stm.setInt(4, persistente.getLocatario().getId());
			stm.setInt(5, persistente.getImovel().getId());
			stm.setInt(6, persistente.getContrato().getId());
			stm.execute();

			ResultSet rs = stm.getGeneratedKeys();

			Integer id = null;

			if (rs.next())
				id = rs.getInt(1);

			rs.close();
			stm.close();

			return id;
		}

		return null;
	}

	@Override
	public void atualizar(Connection conn) throws SQLException {

		String sql = "update Locacao set modalidade=?, valor_deposito=?, valor_seguro=?, "
				+ "fk_id_locatario=?, fk_id_imovel=?, fk_id_contrato=? where id=?";

		if (conn != null) {
			PreparedStatement stm = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

			stm.setString(1, persistente.getModalidade());
			stm.setString(2, persistente.getValor_deposito());
			stm.setString(3, persistente.getValor_seguro());
			stm.setInt(4, persistente.getLocatario().getId());
			stm.setInt(5, persistente.getImovel().getId());
			stm.setInt(6, persistente.getContrato().getId());
			stm.setInt(7, persistente.getId());

			stm.execute();

			stm.close();
		}
	}

	@Override
	public void excluir(Connection conn) throws SQLException {
		String sql = "delete Locacao where id=?";

		if (conn != null) {
			PreparedStatement stm = conn.prepareStatement(sql);

			stm.setInt(1, persistente.getId());

			stm.execute();

			stm.close();
		}
	}

	public void recuperarPorId(int codigo) throws SQLException {
		String sql = "select d.* " + "from Locacao d " + "where d.id = ?";

		Connection conn = ConexaoBD.getConnection();
		if (conn != null) {
			PreparedStatement stm = conn.prepareStatement(sql);

			stm.setInt(1, codigo);

			ResultSet rs = stm.executeQuery();

			if (rs.next()) {
				persistente.setId(rs.getInt("id"));
				persistente.setDados(rs);
				persistente.setPersistente(true);
			}

			rs.close();
			stm.close();
			conn.close();
		}
	}
	
	public int countTodosPorLocadorMes(int id) throws SQLException {

		String sql = "SELECT count(*) as count FROM locacao l " +
				"INNER JOIN imovel i ON l.fk_id_imovel = i.id " +
				"INNER JOIN contrato c ON c.id = l.fk_id_contrato " +
				"WHERE i.fk_id_locador = ? AND c.dt_entrega_chave is null";

		Connection conn = ConexaoBD.getConnection();

		if (conn != null) {
			PreparedStatement stm = conn.prepareStatement(sql);

			stm.setInt(1, id);
            
            ResultSet rs = stm.executeQuery();
            
            Integer count = null;
            
            if (rs.next()){
                count = rs.getInt("count");
            }
            
            rs.close();
            stm.close();
            conn.close();
            
            return count;
		}
		return id;
	}

}
