package dominio.dao;

import dao.ConexaoBD;
import dao.DAO;
import dominio.bean.entity.InfoFiador;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.mysql.jdbc.Statement;

public class InfoFiadorDAO extends DAO<InfoFiador> {

    public InfoFiadorDAO(InfoFiador persistente) {
        super(persistente);
    }

    @Override
    public Integer inserir(Connection conn) throws SQLException {
        String sql = "INSERT INTO fiadores_locacao (`id`, `cartorio`, `mat_imovel`, "
        		+ "`endereco_imovel`, `fk_id_locacao`, `fk_id_fiador`) " 
        		+ "VALUES (NULL, ?, ?, ?, ?, ?)";

        if (conn != null) {
        	PreparedStatement stm = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        	
            stm.setString(1, persistente.getCartorio());
            stm.setString(2, persistente.getMat_imovel());
            stm.setString(3, persistente.getEndereco_imovel());
            stm.setInt(4, persistente.getFk_id_locacao());
            stm.setInt(5, persistente.getFk_id_fiador());
            
            stm.execute();
            
            ResultSet rs = stm.getGeneratedKeys();
            
            Integer id = null;
            
            if (rs.next())
                id = rs.getInt(1);
            
            rs.close();
            stm.close();
            
            return id;
        }

        return null;
    }

    @Override
    public void atualizar(Connection conn) throws SQLException {
        
        String sql = "update fiadores_locacao set cartorio=?, mat_imovel=?, " 
        		+"endereco_imovel=?, fk_id_locacao=?, fk_id_fiador=? where id=?";
        

        if (conn != null) {
        	PreparedStatement stm = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        	
        	stm.setString(1, persistente.getCartorio());
            stm.setString(2, persistente.getMat_imovel());
            stm.setString(3, persistente.getEndereco_imovel());
            stm.setInt(4, persistente.getFk_id_locacao());
            stm.setInt(5, persistente.getFk_id_fiador());
            stm.setInt(6, persistente.getId());
            
            stm.execute();
            
            stm.close();
        }
    }

    @Override
    public void excluir(Connection conn) throws SQLException {
        String sql = "delete from fiadores_locacao where id=?";
        

        if (conn != null) {
            PreparedStatement stm = conn.prepareStatement(sql);

            stm.setInt(1, persistente.getId());
            
            stm.execute();
            
            stm.close();
        }
    }
    
    public void recuperarPorId(int codigo) throws SQLException {
        String sql = "select d.* "
                   + "from fiadores_locacao d "
                   + "where d.id = ?";
        
        Connection conn = ConexaoBD.getConnection();
        if (conn != null) {
            PreparedStatement stm = conn.prepareStatement(sql);

            stm.setInt(1, codigo);
            
            ResultSet rs = stm.executeQuery();
            
            if (rs.next()) {
                persistente.setId(rs.getInt("id"));
                persistente.setDados(rs);
                persistente.setPersistente(true);
            }
            
            rs.close();
            stm.close();
            conn.close();
        }
    }
    
	

}
