package dominio.dao;

import dao.Persistente;
import dominio.bean.entity.BoletoAbsi;
import dominio.bean.list.ListaBoletoAbsi;
import dao.ConexaoBD;
import dao.DAOLista;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.mysql.jdbc.Statement;

public class ListaBoletoAbsiDAO extends DAOLista<ListaBoletoAbsi> {
	public ListaBoletoAbsiDAO(ListaBoletoAbsi lista) {
        super(lista);
    }

	public void recuperarBoletosNovos() throws SQLException {
		String sql = "SELECT * FROM boleto WHERE id IN (SELECT MAX(id) FROM boleto GROUP BY numero_documento)";

		Connection conn = ConexaoBD.getConnection();

		if (conn != null) {
			PreparedStatement stm = conn.prepareStatement(sql);

			ResultSet rs = stm.executeQuery();

			while (rs.next()) {
				Persistente i = listaPersistente.getPersistente();
				i.setId(rs.getInt("id"));
				i.setDados(rs);
				i.setPersistente(true);
				listaPersistente.addPersistente((BoletoAbsi) i);
			}

			rs.close();
			stm.close();
			conn.close();
		}
	}
	
	public void listarBoletosNovosPorMes(Date mes_ref) throws SQLException {
		String sql = "SELECT * FROM boleto WHERE MONTH(mes_ref) = MONTH(?) AND YEAR(mes_ref) = YEAR(?) GROUP BY numero_documento";

		Connection conn = ConexaoBD.getConnection();

		if (conn != null) {
			PreparedStatement stm = conn.prepareStatement(sql);
			stm.setDate(1, mes_ref);
			stm.setDate(2, mes_ref);
			ResultSet rs = stm.executeQuery();

			while (rs.next()) {
				Persistente i = listaPersistente.getPersistente();
				i.setId(rs.getInt("id"));
				i.setDados(rs);
				i.setPersistente(true);
				listaPersistente.addPersistente((BoletoAbsi) i);
			}

			rs.close();
			stm.close();
			conn.close();
		}
	}
	
	public void recuperarBoletosNaoPagos(Date mes_ref) throws SQLException {

		String sql = "SELECT  b.* FROM boleto b "				
				+ "WHERE MONTH(b.mes_ref)= MONTH(?) AND YEAR(b.mes_ref)= YEAR(?) "
				+ "AND b.dt_pagamento is NULL ";

		Connection conn = ConexaoBD.getConnection();

		if (conn != null) {
			PreparedStatement stm = conn.prepareStatement(sql);
			
			stm.setDate(1, mes_ref);
			stm.setDate(2, mes_ref);
			
			ResultSet rs = stm.executeQuery();

			while (rs.next()) {
				Persistente i = listaPersistente.getPersistente();
				i.setId(rs.getInt("id"));
				i.setDados(rs);
				i.setPersistente(true);
				listaPersistente.addPersistente((BoletoAbsi) i);
			}

			rs.close();
			stm.close();
			conn.close();
		}
	}
	
	public void recuperarLocacaoRemessa(Date mes_ref) throws SQLException {

		String sql = "SELECT distinct l.*, b.* FROM locacao l "
				+ "INNER JOIN item i ON l.id = i.fk_id_locacao "
				+ "INNER JOIN boleto b ON l.id = b.fk_id_locacao "
				+ "WHERE MONTH(i.mes_ref) MONTH(?) AND YEAR(i.mes_ref) YEAR(?) "
				+ "AND i.flg_processado = 0 "
				+ "ORDER BY l.id";

		Connection conn = ConexaoBD.getConnection();

		if (conn != null) {
			PreparedStatement stm = conn.prepareStatement(sql);
			
			stm.setDate(1, mes_ref);
			stm.setDate(2, mes_ref);
			
			ResultSet rs = stm.executeQuery();

			while (rs.next()) {
				Persistente i = listaPersistente.getPersistente();
				i.setId(rs.getInt("id"));
				i.setDados(rs);
				i.setPersistente(true);
				listaPersistente.addPersistente((BoletoAbsi) i);
			}

			rs.close();
			stm.close();
			conn.close();
		}
	}
	
	
	public void recuperarPagamentoDia(Date dt) throws SQLException {

		String sql = "SELECT b.* FROM absi.boleto b INNER JOIN locacao l ON l.id = b.fk_id_locacao "
				+ "INNER JOIN imovel i ON i.id = l.fk_id_imovel WHERE b.dt_pagamento <=? and "
				+ "(b.dt_formulario_transf =? OR b.dt_formulario_transf is null)ORDER BY i.fk_id_locador;";

		Connection conn = ConexaoBD.getConnection();

		if (conn != null) {
			PreparedStatement stm = conn.prepareStatement(sql);
			stm.setDate(1, dt);
			stm.setDate(2, dt);
			
			ResultSet rs = stm.executeQuery();

			while (rs.next()) {
				Persistente i = listaPersistente.getPersistente();
				i.setId(rs.getInt("id"));
				i.setDados(rs);
				i.setPersistente(true);
				listaPersistente.addPersistente((BoletoAbsi) i);
			}

			rs.close();
			stm.close();
			conn.close();
		}
	}
	
	public void recuperarPagamentoMes(Date mes_ref) throws SQLException {
		
		String sql = "SELECT b.* FROM absi.boleto b INNER JOIN locacao l ON l.id = b.fk_id_locacao "
				+ "INNER JOIN imovel i ON i.id = l.fk_id_imovel WHERE MONTH(b.dt_pagamento) = MONTH(?) AND YEAR(b.dt_pagamento) = YEAR(?) "
				+ "ORDER BY i.fk_id_locador;";

		Connection conn = ConexaoBD.getConnection();

		if (conn != null) {
			PreparedStatement stm = conn.prepareStatement(sql);
			
			stm.setDate(1, mes_ref);
			stm.setDate(2, mes_ref);
			
			ResultSet rs = stm.executeQuery();

			while (rs.next()) {
				Persistente i = listaPersistente.getPersistente();
				i.setId(rs.getInt("id"));
				i.setDados(rs);
				i.setPersistente(true);
				listaPersistente.addPersistente((BoletoAbsi) i);
			}

			rs.close();
			stm.close();
			conn.close();
		}
	}
	
	
public void recuperarPagamentoAno(Integer idlocacao,Date ano) throws SQLException {
		
		String sql = "SELECT b.* FROM absi.boleto b WHERE year(dt_pagamento) = year(?) and fk_id_locacao =?";

		Connection conn = ConexaoBD.getConnection();

		if (conn != null) {
			PreparedStatement stm = conn.prepareStatement(sql);
			
			stm.setDate(1, ano);
			stm.setInt(2, idlocacao);
			
			ResultSet rs = stm.executeQuery();

			while (rs.next()) {
				Persistente i = listaPersistente.getPersistente();
				i.setId(rs.getInt("id"));
				i.setDados(rs);
				i.setPersistente(true);
				listaPersistente.addPersistente((BoletoAbsi) i);
			}

			rs.close();
			stm.close();
			conn.close();
		}
	}
	
	
}
