package dominio.bean.list;

import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import dao.ListaPersistente;
import dominio.bean.entity.BoletoAbsi;
import dominio.bean.entity.ItensLocacao;
import dominio.bean.entity.Locacao;
import dominio.factory.BoletoAbsiFactory;

@SuppressWarnings("serial")
public class ListaBoletoAbsi extends ListaPersistente<BoletoAbsi> {

	@Override
	public BoletoAbsi getPersistente() {
		return BoletoAbsiFactory.criar();
	}

	@Override
	public void addPersistente(BoletoAbsi persistente) {
		add(persistente);
	}
	
	public void setId(Date dt) {
		for (BoletoAbsi bol : this){
			bol.setDt_formulario_transf(dt);
		}
	}
	
	public boolean isUpdated() {
		for (BoletoAbsi bol : this){
			if(!(bol.isUpdated() || bol.isPersistente()))
				return false;
		}
		return true;
	}
	
	public void sort(){
		Collections.sort(this, new Comparator<BoletoAbsi>() {
	        @Override
	        public int compare(BoletoAbsi o1, BoletoAbsi o2) {
	            return o1.getLocacao().getImovel().getLocador().getNome_completo()
	            		.compareTo(o2.getLocacao().getImovel().getLocador().getNome_completo());
	        }
	    });
	}
	
	public void atualizarValorTxAdm() throws SQLException{
		for (BoletoAbsi bol : this){
			bol.getLocacao().setListaItensLocacao(bol.getMes_ref());
			for (ItensLocacao item : bol.getLocacao().getListaItensLocacao()) {
		if (item.getTipo().equals("tabT")) {
			//Taxa de Administração Acordo Extrajudicial
			//tx
			bol.setValor_comissao(bol.getValor_comissao().add(item.getValor()));
		}else if (item.getTipo().equals("tabV")) {
			//Taxa de Administração Rescisão Contratual
			//tx
			bol.setValor_comissao(bol.getValor_comissao().add(item.getValor()));
		}
		}
	}
}
}
