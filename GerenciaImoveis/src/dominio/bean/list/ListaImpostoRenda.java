package dominio.bean.list;

import dao.ListaPersistente;
import dominio.bean.entity.ImpostoRenda;
import dominio.factory.ImpostoRendaFactory;

@SuppressWarnings("serial")
public class ListaImpostoRenda extends ListaPersistente<ImpostoRenda> {
    @Override
    public ImpostoRenda getPersistente() {
        return ImpostoRendaFactory.criar();
    }
    
    @Override
    public void addPersistente(ImpostoRenda persistente) {
        add(persistente);
    }
}
