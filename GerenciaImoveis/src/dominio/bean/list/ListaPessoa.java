package dominio.bean.list;

import dao.ListaPersistente;
import dominio.bean.entity.Pessoa;
import dominio.factory.PessoaFactory;

@SuppressWarnings("serial")
public class ListaPessoa extends ListaPersistente<Pessoa>  {

	@Override
	public Pessoa getPersistente() {
		return PessoaFactory.criar();
	}

	@Override
	public void addPersistente(Pessoa persistente) {
		add(persistente);
	}

}
