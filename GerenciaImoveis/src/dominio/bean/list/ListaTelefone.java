package dominio.bean.list;



import dao.ListaPersistente;
import dominio.bean.entity.Telefone;
import dominio.factory.TelefoneFactory;

@SuppressWarnings("serial")
public class ListaTelefone extends ListaPersistente<Telefone> {

	@Override
	public Telefone getPersistente() {
		return TelefoneFactory.criar();
	}

	@Override
	public void addPersistente(Telefone persistente) {
		add(persistente);
	}
	
	public void setId(int id){
		for (Telefone tel : this)
            tel.setId_pessoa(id);
	}
	
	
	public boolean find (String tel){
		
		for(int i=0; i<this.size();i++){
			if(this.get(i).getDddTel().equals(tel))				
				return true;
		}
		return false;
	}
	
	
}
