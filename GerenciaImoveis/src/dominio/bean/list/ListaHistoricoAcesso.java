package dominio.bean.list;

import dao.ListaPersistente;
import dominio.bean.entity.HistoricoAcesso;
import dominio.factory.HistoricoAcessoFactory;

@SuppressWarnings("serial")
public class ListaHistoricoAcesso extends ListaPersistente<HistoricoAcesso>  {

	@Override
	public HistoricoAcesso getPersistente() {
		return HistoricoAcessoFactory.criar();
	}

	@Override
	public void addPersistente(HistoricoAcesso persistente) {
		add(persistente);
	}

}
