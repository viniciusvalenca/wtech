package dominio.bean.list;

import java.math.BigDecimal;

import dao.ListaPersistente;
import dominio.bean.entity.ItensLocacao;
import dominio.factory.ItensLocacaoFactory;

@SuppressWarnings("serial")
public class ListaItensLocacao extends ListaPersistente<ItensLocacao> {

	@Override
	public ItensLocacao getPersistente() {
		return ItensLocacaoFactory.criar();
	}

	@Override
	public void addPersistente(ItensLocacao persistente) {
		add(persistente);
	}

	public void setId(int id) {
		for (ItensLocacao itens : this)
			itens.setFk_id_locacao(id);
	}
	
	public void setProcessado() {
		for (ItensLocacao itens : this)
			itens.setFlg_processado(true);
	}
	
	public boolean isUpdated() {
		for (ItensLocacao itens : this){
			if(!(itens.isUpdated() || itens.isPersistente()))
				return false;
		}
		return true;
	}

	public void setItensLocacao(ListaItensLocacao lista) {
		
		for (ItensLocacao item : lista) {
			int tmp = this.find(item.getTipo());
			
			if (tmp != -1) {
				this.get(tmp).setItensLocacao(item);
			} else {
				this.add(item);
			}
		}

		for (ItensLocacao item : this) {
			int tmp = lista.find(item.getTipo());
			if (tmp == -1) {
				item.setErase(true);
			}
		}
		

	}

	public int find(String tipo) {

		for (int i = 0; i < this.size(); i++) {
			if (this.get(i).getTipo().equals(tipo))
				return i;
		}
		return -1;
	}
	
	public BigDecimal valorLocatario() {
		BigDecimal valor = new BigDecimal(0);

		for (ItensLocacao itens : this){
			if(itens.getRd().equals("R"))
				valor = valor.subtract(itens.getValor());
			else if(itens.getRd().equals("D"))
				valor = valor.add(itens.getValor());
		}
		return valor;
	}
	
	public BigDecimal valorLocador() {
		BigDecimal valor = new BigDecimal(0);

		for (ItensLocacao itens : this){
			if(itens.getStatus().equals("2"))
				valor = valor.subtract(itens.getValor());
			else if(itens.getStatus().equals("1"))
				valor = valor.add(itens.getValor());
		}
		return valor;
	}
	
	public BigDecimal valorTxBancaria() {
		BigDecimal valor = new BigDecimal(0);

		for (ItensLocacao itens : this){
			if(itens.getTipo().equals("tabB"))
				valor = itens.getValor();
		}
		return valor;
	}
	
	public BigDecimal valorSoma() {
		BigDecimal valor = new BigDecimal(0);

		for (ItensLocacao itens : this){
			if(itens.getStatus().equals("1"))
				valor = valor.add(itens.getValor());
		}
		return valor;
	}
	
	public BigDecimal valorSub() {
		BigDecimal valor = new BigDecimal(0);

		for (ItensLocacao itens : this){
			if(itens.getStatus().equals("2"))
				valor = valor.add(itens.getValor());
		}
		return valor;
	}
	
	public BigDecimal valorFica() {
		BigDecimal valor = new BigDecimal(0);

		for (ItensLocacao itens : this){
			if(itens.getStatus().equals("3"))
				valor = valor.add(itens.getValor());
		}
		return valor;
	}



}
