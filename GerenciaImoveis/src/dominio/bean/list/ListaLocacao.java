package dominio.bean.list;

import java.sql.Date;
import java.sql.SQLException;
import java.text.ParseException;

import dao.ListaPersistente;
import dominio.bean.entity.ItensLocacao;
import dominio.bean.entity.Locacao;
import dominio.factory.LocacaoFactory;

@SuppressWarnings("serial")
public class ListaLocacao extends ListaPersistente<Locacao>  {

	@Override
	public Locacao getPersistente() {
		return LocacaoFactory.criar();
	}

	@Override
	public void addPersistente(Locacao persistente) {
		add(persistente);
	}
	
	public void setItensLocacao(Date mes_ref) throws SQLException {
		for (Locacao lo : this)
			lo.setListaItensLocacao(mes_ref);
	}
	
	public void setBoleto(Date mes_ref) throws SQLException {
		for (Locacao l : this)
			l.setBoleto(mes_ref);
	}
	
	public void setBoletos(Date ano) throws SQLException, ParseException {
		for (Locacao l : this)
			l.setLboleto(ano);
	}

}
