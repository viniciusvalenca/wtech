package dominio.bean.list;





import dao.ListaPersistente;
import dominio.bean.entity.Celular;
import dominio.factory.CelularFactory;

@SuppressWarnings("serial")
public class ListaCelular extends ListaPersistente<Celular> {

	@Override
	public Celular getPersistente() {
		return CelularFactory.criar();
	}

	@Override
	public void addPersistente(Celular persistente) {
		add(persistente);
	}

	public void setId(int id) {
		for (Celular cel : this)
			cel.setId_pessoa(id);
	}
	
	
	public boolean find (String cel){
		
		for(int i=0; i<this.size();i++){
			if(this.get(i).getDddTel().equals(cel))				
				return true;
		}
		return false;
	}


}
