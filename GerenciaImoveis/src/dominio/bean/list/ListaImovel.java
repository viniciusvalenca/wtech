package dominio.bean.list;


import dao.ListaPersistente;
import dominio.bean.entity.Imovel;
import dominio.factory.ImovelFactory;

/**
 * Classe que representa uma lista de atracoes ({@link Imovel}).
 */

@SuppressWarnings("serial")
public class ListaImovel  extends ListaPersistente<Imovel> {

    /**
     * Retorna um novo {@link Imovel}.
     */
    @Override
    public Imovel getPersistente() {
        return ImovelFactory.criar();
    }

    /**
     * Adiciona um {@link Imovel} à lista de Imovels.
     */
    @Override
    public void addPersistente(Imovel persistente) {
        add(persistente);
    }
}
