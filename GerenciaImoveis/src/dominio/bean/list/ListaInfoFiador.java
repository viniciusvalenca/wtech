package dominio.bean.list;

import dao.ListaPersistente;
import dominio.bean.entity.InfoFiador;
import dominio.factory.InfoFiadorFactory;

@SuppressWarnings("serial")
public class ListaInfoFiador extends ListaPersistente<InfoFiador>  {
	
	@Override
	public InfoFiador getPersistente() {
		return InfoFiadorFactory.criar();
	}

	@Override
	public void addPersistente(InfoFiador persistente) {
		add(persistente);
	}
	
	public void setId(int id) {
		for (InfoFiador info : this)
			info.setFk_id_locacao(id);
	}
	
	public void setErasable() {
		for (InfoFiador itens : this)
			itens.setErase(true);
	}

}
