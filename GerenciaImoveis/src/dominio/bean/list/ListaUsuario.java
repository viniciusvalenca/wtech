package dominio.bean.list;

import dao.ListaPersistente;
import dominio.bean.entity.Usuario;
import dominio.factory.UsuarioFactory;

@SuppressWarnings("serial")
public class ListaUsuario extends ListaPersistente<Usuario>  {

	@Override
	public Usuario getPersistente() {
		return UsuarioFactory.criar();
	}

	@Override
	public void addPersistente(Usuario persistente) {
		add(persistente);
	}

}
