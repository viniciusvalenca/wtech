package dominio.bean.list;


import dao.ListaPersistente;
import dominio.bean.entity.Contrato;
import dominio.factory.ContratoFactory;


@SuppressWarnings("serial")
public class ListaContrato  extends ListaPersistente<Contrato> {

    
    @Override
    public Contrato getPersistente() {
        return ContratoFactory.criar();
    }

    
    @Override
    public void addPersistente(Contrato persistente) {
        add(persistente);
    }
}
