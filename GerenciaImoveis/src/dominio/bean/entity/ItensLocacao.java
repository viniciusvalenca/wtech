package dominio.bean.entity;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

import dao.DAO;
import dao.DAOFactory;
import dao.Persistente;
import utilitarios.ConverterData;
import utilitarios.util;

public class ItensLocacao extends Persistente {
	
	private String tipo;
	private Integer ordem;
	private String descricao;
	private Date mes_ref;
	private String mes_refFormat;
	private String mes_refBoleto;
	private Integer parcela;
	private Integer tot_parcela;	
	private BigDecimal valor;
	private BigDecimal valorRelatorio;
	private String rd;
	private String status;
	private Boolean flg_processado;
	private Integer fk_id_locacao;
	private boolean erase;
	private String nome;
	
	public ItensLocacao() {
		super();
	}

	public ItensLocacao(String tipo, Integer ordem, String descricao, Date mes_ref, Integer parcela,
			Integer tot_parcela, BigDecimal valor, String rd,String status, Boolean flg_processado, String mes_refBoleto) {
		super();
		this.tipo = tipo;
		this.ordem = ordem;
		this.descricao = descricao;
		this.mes_ref = mes_ref;
		this.parcela = parcela;
		this.tot_parcela = tot_parcela;
		this.valor = valor;
		this.rd = rd;
		this.status=status;
		this.flg_processado = flg_processado;
		this.mes_refBoleto = mes_refBoleto;
		
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
		
	}

	public Integer getOrdem() {
		return ordem;
	}

	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Date getMes_ref() {
		return mes_ref;
	}

	public void setMes_ref(Date mes_ref) {
		this.mes_ref = mes_ref;
	}
	
	public String getMes_refFormat() {
		return mes_refFormat;
	}

	public void setMes_refFormat() {
		this.mes_refFormat = ConverterData.getMes_refString(mes_ref);
	}

	public Boolean getFlg_processado() {
		return flg_processado;
	}

	public void setFlg_processado(Boolean flg_processado) {
		this.flg_processado = flg_processado;
	}

	public Integer getParcela() {
		return parcela;
	}

	public void setParcela(Integer parcela) {
		this.parcela = parcela;
	}

	public Integer getTot_parcela() {
		return tot_parcela;
	}

	public void setTot_parcela(Integer tot_parcela) {
		this.tot_parcela = tot_parcela;
	}

	public BigDecimal getValor() {
		return valor;
	}
	
	public void setValorRelatorio() {
		//System.out.println("status"+status);
		if(status.equals("2"))
			valorRelatorio = valor.negate();
		else
			valorRelatorio = valor;
			
	}
	
	public BigDecimal getValorRelatorio() {
			return valorRelatorio;			
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
	
	public String getValorString() {
		return util.dotToComma(valor.toString());
	}

	public String getRd() {
		return rd;
	}

	public void setRd(String rd) {
		this.rd = rd;
	}

	public Integer getFk_id_locacao() {
		return fk_id_locacao;
	}

	public void setFk_id_locacao(Integer fk_id_locacao) {
		this.fk_id_locacao = fk_id_locacao;
	}
	
	public boolean isErase() {
		return erase;
	}

	public void setErase(boolean erase) {
		this.erase = erase;
	}	

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setMes_refFormat(String mes_refFormat) {
		this.mes_refFormat = mes_refFormat;
	}
	
	
	public String getStatus() {
		return status;
	}
	
	public String getMes_refBoleto() {
		return mes_refBoleto;
	}

	public void setMes_refBoleto(String mes_refBoleto) {
		this.mes_refBoleto = mes_refBoleto;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String setNomeItem() {

		switch (ordem) {
		case 1:
			nome = "Aluguel " + mes_refBoleto;
			break;
		case 2:
			nome = "Despesa Bancária";
			break;
		case 3:
			nome = "Cedae " + mes_refBoleto;
			break;
		case 4:
			nome = "Luz de Serviço " + mes_refBoleto;
			break;
		case 5:
			nome = "IPTU " +(mes_refBoleto.split("/"))[1]+" "+ parcela + "/" + tot_parcela ;
			break;
		case 6:
			nome = "Condomínio " + mes_refBoleto;
			break;
		case 7:
			nome = "Faxina " + mes_refBoleto;
			break;
		case 8:
			nome = "Seguro Incêndio Anual "+ parcela + "/" + tot_parcela;
			break;
		case 9:
			nome = "Prevenção e Extinção de Incêndios";
			break;
		case 10:
			nome = "Desconto "+parcela + "/" + tot_parcela + " parcela";
			break;
		case 11:
			nome = "Acordo Extrajudicical "+parcela + "/" + tot_parcela;
			break;
		case 12:
			nome = "Cobrança";
			break;
		case 13:
			nome = "Parcela de depósito da garantia da locação"+parcela + "/" + tot_parcela;
			break;
		case 14:
			nome = "Devolução";
			break;
		case 15:
			nome = "Cobrança";
			break;
		case 16:
			nome = "Multa";
			break;
		case 17:
			nome = "Juros";
			break;
		case 18:
			nome = "Desconto "+parcela + "/" + tot_parcela + " parcela";
			break;
		case 19:
			nome = "Desconto "+parcela + "/" + tot_parcela + " parcela";
			break;
		case 20:
			nome = "Taxa de Administração Acordo Extrajudicial "+parcela + "/" + tot_parcela + " parcela";
			break;
		case 21:
			nome = "Rescisão Contratual "+parcela + "/" + tot_parcela + " parcela";
			break;
		case 22:
			nome = "Taxa de Administração Rescisão Contratual"+parcela + "/" + tot_parcela + " parcela";
			break;
		default:
			break;
		}
		return nome;
	}


	public void setItensLocacao(ItensLocacao item) {
		this.tipo = item.getTipo();
		this.ordem = item.getOrdem();
		this.descricao = item.getDescricao();
		this.mes_ref = item.getMes_ref();
		this.parcela = item.getParcela();
		this.tot_parcela = item.getTot_parcela();
		this.valor = item.getValor();
		this.rd = item.getRd();
		this.status = item.getStatus();
		this.mes_refBoleto = item.getMes_refBoleto();
	}
	
	@Override
	@SuppressWarnings("rawtypes")
    public void salvar(Connection conn) throws SQLException {
        DAO dao = DAOFactory.getDAO(this);

        if (isPersistente()){
        	if(isErase())
        		dao.excluir(conn);
        	else{
        	dao.atualizar(conn);
        	setUpdated(true);
        	}
        }
        else {
          this.setId(dao.inserir(conn)); 
            setPersistente(true);
        }
        
    }

	@Override
	public void setDados(ResultSet rs) throws SQLException {
		this.tipo = rs.getString("tipo");
		this.ordem = rs.getInt("ordem");
		this.descricao = rs.getString("descricao");
		this.mes_ref = rs.getDate("mes_ref");
		this.mes_refBoleto = rs.getString("mes_refBoleto");
		this.parcela = rs.getInt("parcela");
		this.tot_parcela = rs.getInt("tot_parcela");
		this.valor = rs.getBigDecimal("valor");
		this.rd = rs.getString("rd");
		this.status = rs.getString("status");
		this.fk_id_locacao = rs.getInt("fk_id_locacao");
		this.flg_processado = rs.getBoolean("flg_processado");
		
		
		setMes_refFormat();
		setNomeItem();
		if(status!=null)
			setValorRelatorio();
		
	}

	
	public String toString2() {
		return "ItensLocacao [tipo=" + tipo + ", ordem=" + ordem + ", descricao=" + descricao + ", mes_ref=" + mes_ref
				+ ", parcela=" + parcela + ", tot_parcela=" + tot_parcela + ", valor=" + valor + ", rd=" + rd
				+ ", fk_id_locacao=" + fk_id_locacao + ", erase=" + erase + ", getId()=" + getId()
				+ ", isPersistente()=" + isPersistente() + ", isUpdated()=" + isUpdated() + "]";
	}

	@Override
	public String toString() {
		return "ItensLocacao [tipo=" + tipo + ", ordem=" + ordem + ", descricao=" + descricao + ", mes_ref=" + mes_ref
				+ ", mes_refFormat=" + mes_refFormat + ", mes_refBoleto=" + mes_refBoleto + ", parcela=" + parcela
				+ ", tot_parcela=" + tot_parcela + ", valor=" + valor + ", valorRelatorio=" + valorRelatorio + ", rd="
				+ rd + ", status=" + status + ", flg_processado=" + flg_processado + ", fk_id_locacao=" + fk_id_locacao
				+ ", erase=" + erase + ", nome=" + nome + "]";
	}
	
	

	
	

	
}

