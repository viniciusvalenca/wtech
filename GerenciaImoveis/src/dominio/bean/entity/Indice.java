package dominio.bean.entity;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;

import dao.Persistente;
import utilitarios.util;

public class Indice extends Persistente {
	private BigDecimal indice;

	public Indice() {
		super();
	}

	public Indice(BigDecimal indice) {
		super();
		this.indice = indice;
	}

	public String getIndice() {
		return util.bigdecimalToPageINDICE(indice);
	}
	public BigDecimal getIndice1() {
		return indice;
	}

	public void setIndice(BigDecimal indice) {
		this.indice = indice;
	}

	@Override
	public void setDados(ResultSet rs) throws SQLException {
		indice = rs.getBigDecimal("indice_reajuste");
		System.out.println("setDados = " +indice);
		this.setPersistente(true);
		this.setId(1);
	}

	
}
