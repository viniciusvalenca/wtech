package dominio.bean.entity;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import dao.DAO;
import dao.DAOFactory;
import dao.Persistente;
import utilitarios.util;

public class Telefone extends Persistente {
	private Integer num_telefone;
	private Integer ddd;
	private Integer id_pessoa;
	private boolean erase;

	public Telefone() {
		super();
	}

	public Telefone(Integer num_telefone, Integer ddd) {
		super();
		this.num_telefone = num_telefone;
		this.ddd = ddd;
	}
	
	public String getNum_telefoneFormat() {
		return "(" + String.valueOf(ddd) + ") " + util.FormatTelefone(String.valueOf(num_telefone));
	}

	public void setNum_telefoneFormat(Integer num_telefone) {
		this.num_telefone = num_telefone;
	}

	public Integer getNum_telefone() {
		return num_telefone;
	}

	public void setNum_telefone(Integer num_telefone) {
		this.num_telefone = num_telefone;
	}

	public Integer getDdd() {
		return ddd;
	}

	public void setDdd(Integer ddd) {
		this.ddd = ddd;
	}

	public Integer getId_pessoa() {
		return id_pessoa;
	}

	public void setId_pessoa(Integer id_pessoa) {
		this.id_pessoa = id_pessoa;
	}

	public boolean isErase() {
		return erase;
	}

	public void setErase(boolean erase) {
		this.erase = erase;
	}
	
	public String getDddTel() {
		return  String.valueOf(ddd) +String.valueOf(num_telefone) ;
	}

	@Override
	public void setDados(ResultSet rs) throws SQLException {
		num_telefone = rs.getInt("num_telefone");
		ddd = rs.getInt("ddd");
		id_pessoa = rs.getInt("fk_id_pessoa");
	}

	@Override
	public String toString() {
		return "Telefone [num_telefone=" + num_telefone + ", ddd=" + ddd + ", id_pessoa=" + id_pessoa + "]";
	}
	
	@Override
	@SuppressWarnings("rawtypes")
    public void salvar(Connection conn) throws SQLException {
        DAO dao = DAOFactory.getDAO(this);
        
        if (isPersistente()){
        	if(isErase())
        		dao.excluir(conn);
        	else{
        	dao.atualizar(conn);
        	setUpdated(true);
        	}
        }
            
        else {
          this.setId(dao.inserir(conn)); 
            setPersistente(true);
        }
    }

}
