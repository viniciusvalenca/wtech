package dominio.bean.entity;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.SQLException;

import dominio.factory.ImpostoRendaFactory;
import utilitarios.ConverterData;

public class IR {
	
	private String mes;
	private BigDecimal valor_aluguel;
	private BigDecimal juros_aluguel;
	private BigDecimal multa_aluguel;
	private BigDecimal valor_txadm;
	private BigDecimal juros_txadm;
	private BigDecimal multa_txadm;
	private BigDecimal ajuste_aluguel;
	private BigDecimal ajuste_txadm;
	private BigDecimal resultado_aluguel;
	private BigDecimal resultado_txadm;
	private Date mes_ref;
	private String mes_refFormat;

	private Integer fk_id_locacao;
	private Integer fk_id_locacador;
	private ImpostoRenda impostoRenda;

	public void setMes_refFormat(String mes_refFormat) {
		this.mes_refFormat = ConverterData.getOnlyDateString(mes_ref);
	}
	
	public String getMes_refFormat() {
		this.mes_refFormat = ConverterData.getOnlyDateString(mes_ref);
		return mes_refFormat;
	}
	
	public void setMes_refFormat() {
		this.mes_refFormat = ConverterData.getOnlyDateString(mes_ref);
		
	}



	@Override
	public String toString() {
		return "IR [mes=" + mes + ", valor_aluguel=" + valor_aluguel + ", valor_txadm=" + valor_txadm
				+ ", ajuste_aluguel=" + ajuste_aluguel + ", ajuste_txadm=" + ajuste_txadm + ", resultado_aluguel="
				+ resultado_aluguel + ", resultado_txadm=" + resultado_txadm + ", mes_ref=" + mes_ref
				+ ", mes_refFormat=" + mes_refFormat + ", fk_id_locacao=" + fk_id_locacao + ", fk_id_locacador="
				+ fk_id_locacador + ", impostoRenda=" + impostoRenda + "]";
	}

	public BigDecimal getValor_txadm() {
		return valor_txadm;
	}

	public void setValor_txadm(BigDecimal valor_txadm) {
		this.valor_txadm = this.valor_txadm .add(valor_txadm);
	}

	public IR() {
		super();
		this.valor_aluguel = new BigDecimal(0);
				this.valor_txadm= new BigDecimal(0);
				this.ajuste_aluguel = new BigDecimal(0);
				this.ajuste_txadm= new BigDecimal(0);
				this.resultado_aluguel = new BigDecimal(0);
				this.resultado_txadm= new BigDecimal(0);
				this.multa_aluguel = new BigDecimal(0);
				this.multa_txadm= new BigDecimal(0);
				this.juros_aluguel = new BigDecimal(0);
				this.juros_txadm= new BigDecimal(0);
	}

	public String getMes() {
		return mes;
	}

	public void setMes(String mes) {
		this.mes = mes;
	}

	public BigDecimal getValor_aluguel() {
		return valor_aluguel;
	}

	public void setValor_aluguel(BigDecimal valor_aluguel) {
		this.valor_aluguel=this.valor_aluguel.add(valor_aluguel);
	}

	public Date getMes_ref() {
		return mes_ref;
	}

	public void setMes_ref(Date mes_ref) {
		this.mes_ref = mes_ref;
	}

	public Integer getFk_id_locacao() {
		return fk_id_locacao;
	}

	public void setFk_id_locacao(Integer fk_id_locacao) {
		this.fk_id_locacao = fk_id_locacao;
	}

	public Integer getFk_id_locacador() {
		return fk_id_locacador;
	}

	public void setFk_id_locacador(Integer fk_id_locacador) {
		this.fk_id_locacador = fk_id_locacador;
	}

	public BigDecimal getAjuste_aluguel() {
		return ajuste_aluguel;
	}

	public void setAjuste_aluguel(BigDecimal ajuste_aluguel) {
		this.ajuste_aluguel = ajuste_aluguel;
	}

	public BigDecimal getAjuste_txadm() {
		return ajuste_txadm;
	}

	public void setAjuste_txadm(BigDecimal ajuste_txadm) {
		this.ajuste_txadm = ajuste_txadm;
	}
	
	
	
	public BigDecimal getResultado_aluguel() {
		return resultado_aluguel;
	}

	public void setResultado_aluguel(BigDecimal resultado_aluguel) {
		this.resultado_aluguel = resultado_aluguel;
	}

	public BigDecimal getResultado_txadm() {
		return resultado_txadm;
	}

	public void setResultado_txadm(BigDecimal resultado_txadm) {
		this.resultado_txadm = resultado_txadm;
	}
	
	

	public BigDecimal getJuros_aluguel() {
		return juros_aluguel;
	}

	public void setJuros_aluguel(BigDecimal juros_aluguel) {
		this.juros_aluguel = juros_aluguel;
	}

	public BigDecimal getMulta_aluguel() {
		return multa_aluguel;
	}

	public void setMulta_aluguel(BigDecimal multa_aluguel) {
		this.multa_aluguel = multa_aluguel;
	}

	public BigDecimal getJuros_txadm() {
		return juros_txadm;
	}

	public void setJuros_txadm(BigDecimal juros_txadm) {
		this.juros_txadm = juros_txadm;
	}

	public BigDecimal getMulta_txadm() {
		return multa_txadm;
	}

	public void setMulta_txadm(BigDecimal multa_txadm) {
		this.multa_txadm = multa_txadm;
	}
	
	public void setAjustes(){
		
		
		this.resultado_aluguel=this.resultado_aluguel.add(this.valor_aluguel);
	
		this.resultado_txadm=this.resultado_txadm.add(this.valor_txadm);
	}

	
		
		public void setResultados() throws SQLException {
			
			//this.setAjustes();
			
			if(this.ajuste_aluguel!=null) {
				this.resultado_aluguel=this.resultado_aluguel.add(this.ajuste_aluguel);
			}
			if(this.ajuste_txadm!=null) {
				this.resultado_txadm=this.resultado_txadm.add(this.ajuste_txadm);
			}
			if(this.juros_aluguel!=null) {
				this.resultado_aluguel=this.resultado_aluguel.add(this.juros_aluguel);
			}
			if(this.juros_txadm!=null) {
				this.resultado_txadm=this.resultado_txadm.add(this.juros_txadm);
			}
			if(this.multa_aluguel!=null) {
				this.resultado_aluguel=this.resultado_aluguel.add(this.multa_aluguel);
			}
			if(this.multa_txadm!=null) {
				this.resultado_txadm=this.resultado_txadm.add(this.multa_txadm);
			}
					
					//System.out.println("ajuste_aluguel: " + this.ajuste_aluguel);
					//System.out.println("ajuste_txadm: " + this.ajuste_txadm);			
					//System.out.println("resultado_aluguel: " + this.resultado_aluguel);
					//System.out.println("resultado_txadm: " + this.resultado_txadm);
			}
	}

