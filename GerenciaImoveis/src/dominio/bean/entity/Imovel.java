package dominio.bean.entity;

import dao.DAO;
import dao.DAOFactory;
import dao.Persistente;
import dominio.factory.EnderecoFactory;
import dominio.factory.PessoaFactory;
import utilitarios.util;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Imovel extends Persistente {

	private BigDecimal valor;
	private String comissao;
	private String tipo;
	private String modalidade;
	private String qtd_quartos;
	private String qtd_suites;
	private String area;
	private String insc_iptu;
	private String cod_logradouro;
	private String mat_cedae;
	private String mat_light;
	private String mat_ceg;
	private String cbmerj;
	private Integer id_locador;
	private Boolean flg_disponivel;
	private Endereco endereco;
	private Pessoa locador;
	
	private String valorString;

	public Imovel() {
		super();
	}

	public Imovel(BigDecimal valor, String comissao, String tipo, String modalidade, String qtd_quartos, String qtd_suites,
			String area, String insc_iptu, String cod_logradouro, String mat_cedae, String mat_light, String mat_ceg,
			String cbmerj, Integer id_locador, Boolean flg_disponivel) {
		super();
		this.valor = valor;
		this.comissao = comissao;
		this.tipo = tipo;
		this.modalidade = modalidade;
		this.qtd_quartos = qtd_quartos;
		this.qtd_suites = qtd_suites;
		this.area = area;
		setInsc_iptu(insc_iptu);
		setCod_logradouro(cod_logradouro);
		setMat_ceg(mat_ceg);
		setMat_cedae(mat_cedae);
		setMat_ligth(mat_light);
		setCbmerj(cbmerj);
		this.id_locador = id_locador;
		this.flg_disponivel = flg_disponivel;
	}

	public BigDecimal getValor() {
		return valor;
	}
	
	public BigDecimal getValorComTxAdm() {
		
		BigDecimal valorComTxAdm = valor.add(getValorTxAdm());
		
		return valorComTxAdm;
		
	}
	
	public BigDecimal getValorTxAdm() {
		BigDecimal cem = new BigDecimal(".01");
		
		BigDecimal porcentagem = cem.multiply(new BigDecimal(comissao));
		
		BigDecimal txAdm = valor.multiply(porcentagem);
		
		return txAdm;
		
	}
	
	public String getCurrencyFormat() {
		valorString =  util.bigdecimalToPage(valor);
		return valorString;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public String getComissao() {
		return comissao;
	}

	public void setComissao(String comissao) {
		this.comissao = comissao;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getModalidade() {
		return modalidade;
	}

	public void setModalidade(String modalidade) {
		this.modalidade = modalidade;
	}

	public String getQtd_quartos() {
		return qtd_quartos;
	}

	public void setQtd_quartos(String qtd_quartos) {
		this.qtd_quartos = qtd_quartos;
	}

	public String getQtd_suites() {
		return qtd_suites;
	}

	public void setQtd_suites(String qtd_suites) {
		this.qtd_suites = qtd_suites;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getInsc_iptu() {
		return insc_iptu;
	}

	public void setInsc_iptu(String insc_iptu) {
		if (insc_iptu.length() > 0)
			this.insc_iptu = insc_iptu;
		else
			this.insc_iptu = null;
	}

	public String getCod_logradouro() {
		return cod_logradouro;
	}

	public void setCod_logradouro(String cod_logradouro) {
		if (cod_logradouro.length() > 0)
			this.cod_logradouro = cod_logradouro;
		else
			this.cod_logradouro = null;
	}

	public String getMat_cedae() {
		return mat_cedae;
	}

	public void setMat_cedae(String mat_cedae) {
		if (mat_cedae.length() > 0)
			this.mat_cedae = mat_cedae;
		else
			this.mat_cedae = null;
	}

	public String getMat_light() {
		return mat_light;
	}

	public void setMat_ligth(String mat_light) {
		if (mat_light.length() > 0)
			this.mat_light = mat_light;
		else
			this.mat_light = null;
	}

	public String getMat_ceg() {
		return mat_ceg;
	}

	public void setMat_ceg(String mat_ceg) {
		if (mat_ceg.length() > 0)
			this.mat_ceg = mat_ceg;
		else
			this.mat_ceg = null;
	}

	public String getCbmerj() {
		return cbmerj;
	}

	public void setCbmerj(String cbmerj) {
		if (cbmerj.length() > 0)
			this.cbmerj = cbmerj;
		else
			this.cbmerj = null;
	}

	public Integer getId_locador() {
		return id_locador;
	}

	public void setId_locador(Integer id_locador) {
		this.id_locador = id_locador;
	}

	public Pessoa getLocador() {
		return locador;
	}

	public void setLocador(Pessoa locador) {
		this.locador = locador;
	}

	public Boolean getFlg_disponivel() {
		return flg_disponivel;
	}

	public void setFlg_disponivel(Boolean flg_disponivel) {
		this.flg_disponivel = flg_disponivel;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	@Override
	public void setDados(ResultSet rs) throws SQLException {
		valor = rs.getBigDecimal("valor");
		comissao = rs.getString("comissao");
		tipo = rs.getString("tipo");
		modalidade = rs.getString("modalidade");
		qtd_quartos = rs.getString("qtd_quartos");
		qtd_suites = rs.getString("qtd_suites");
		area = rs.getString("area");
		insc_iptu = rs.getString("insc_iptu");
		cod_logradouro = rs.getString("cod_logradouro");
		mat_cedae = rs.getString("mat_cedae");
		mat_light = rs.getString("mat_light");
		mat_ceg = rs.getString("mat_ceg");
		cbmerj = rs.getString("cbmerj");
		id_locador = rs.getInt("fk_id_locador");
		locador = PessoaFactory.recuperarPorID(id_locador);
		flg_disponivel = rs.getBoolean("flg_disponivel");
		endereco = EnderecoFactory.recuperarPorId(rs.getInt("fk_id_endereco"));

		
		getCurrencyFormat();
	}

	@Override
	@SuppressWarnings("rawtypes")
	public void salvar(Connection conn) throws SQLException {
		DAO dao = DAOFactory.getDAO(this);

		System.out.println("------------------------------------  salvar Imóvel");

		this.getEndereco().salvar(conn);

		if (isPersistente()) {
			dao.atualizar(conn);
			setUpdated(true);

		} else {
			Integer id = dao.inserir(conn);
			this.setId(id);
			setPersistente(true);
		}
	}

	@Override
	public String toString() {
		return "Imovel [valor=" + valor + ", Currency=" + getCurrencyFormat() + ", comissao=" + comissao + ", tipo=" + tipo + ", modalidade=" + modalidade
				+ ", qtd_quartos=" + qtd_quartos + ", qtd_suites=" + qtd_suites + ", area=" + area + ", insc_iptu="
				+ insc_iptu + ", cod_logradouro=" + cod_logradouro + ", mat_cedae=" + mat_cedae + ", mat_light="
				+ mat_light + ", mat_ceg=" + mat_ceg + ", cbmerj=" + cbmerj + ", id_locador=" + id_locador
				+ ", flg_disponivel=" + flg_disponivel + ", endereco=" + endereco + ", locador=" + locador
				+ "]";
	}

}
