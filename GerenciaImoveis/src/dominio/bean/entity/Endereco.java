package dominio.bean.entity;

import java.sql.ResultSet;
import java.sql.SQLException;

import dao.Persistente;

public class Endereco extends Persistente {
	private String endereco;
	private String bairro;
	private String cep;
	private String numero;
	private String estado;
	private String cidade;
	private String complemento;
	private String nom_condominio;
	
	private String enderecoCompleto;

	public Endereco() {
		super();
	}

	public Endereco(String endereco, String bairro, String cep, String numero, String estado, String cidade,
			String complemento, String nom_condominio) {
		super();
		this.endereco = endereco;
		this.bairro = bairro;
		this.cep = cep;
		this.numero = numero;
		this.estado = estado;
		this.cidade = cidade;
		setComplemento(complemento);
		setNom_condominio(nom_condominio);
	}

	public String getEndereco() {
		return endereco;
	}
	
	public String getEnderecoFormatado() {
		return endereco + bairro + cep + numero
				+ estado  + cidade;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String num_banco) {
		this.numero = num_banco;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		if (complemento.length() > 0)
			this.complemento = complemento;
		else
			this.complemento = null;
	}

	public String getNom_condominio() {
		return nom_condominio;
	}

	public void setNom_condominio(String nom_condominio) {
		if (nom_condominio.length() > 0)
			this.nom_condominio = nom_condominio;
		else
			this.nom_condominio = null;
	}

	@Override
	public void setDados(ResultSet rs) throws SQLException {
		endereco = rs.getString("endereco");
		bairro = rs.getString("bairro");
		cep = rs.getString("cep");
		numero = rs.getString("numero");
		estado = rs.getString("estado");
		cidade = rs.getString("cidade");
		complemento = rs.getString("complemento");
		nom_condominio = rs.getString("nom_condominio");
		
		setEnderecoCompleto();
	}
	
	public void setEnderecoCompleto(){
		
		String temp;
		temp = endereco + ", " + numero + ", ";
		
		if(complemento != null){
			temp += complemento + ", ";
		}
		
		temp += bairro + ", "
		+ cidade + " - "
		+ estado;
		
		this.enderecoCompleto = temp;
				
				
	}

	public String getEnderecoCompleto() {
		return enderecoCompleto;
	}

	@Override
	public String toString() {
		return "Endereco [endereco=" + endereco + ", bairro=" + bairro + ", cep=" + cep + ", numero=" + numero
				+ ", estado=" + estado + ", cidade=" + cidade + "]";
	}

}
