package dominio.bean.entity;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

import dao.Persistente;
import utilitarios.ConverterData;

public class Contrato extends Persistente {

	private Date dt_inicio;
	private Date dt_termino;
	private Date dt_reajuste;
	private Date dt_entrega_chave;
	private Integer dia_vencimento;
	private Boolean flg_renovar;

	private String dt_inicioFormat;
	private String dt_terminoFormat;
	private String dt_renovacaoFormat;
	private String dt_reajusteFormat;
	private String dt_entrega_chaveFormat;
	
	public Contrato() {
		super();
	}

	public Contrato(Date dt_inicio, Date dt_termino, Date dt_reajuste, Date dt_entrega_chave, Integer dia_vencimento) {
		super();
		this.dt_inicio = dt_inicio;
		this.dt_termino = dt_termino;
		this.dt_reajuste = dt_reajuste;
		this.dt_entrega_chave = dt_entrega_chave;
		this.dia_vencimento = dia_vencimento;
	}

	public Integer getDia_vencimento() {
		return dia_vencimento;
	}

	public void setDia_vencimento(Integer dia_vencimento) {
		this.dia_vencimento = dia_vencimento;
	}

	public Date getDt_inicio() {
		return dt_inicio;
	}

	public void setDt_inicio(Date dt_inicio) {
		this.dt_inicio = dt_inicio;
	}
	
	public String getDt_inicioFormat() {
		return dt_inicioFormat;
	}

	public void setDt_inicioFormat() {
		this.dt_inicioFormat = ConverterData.getOnlyDateString(dt_inicio);
	}

	public Date getDt_termino() {
		return dt_termino;
	}

	public void setDt_termino(Date dt_termino) {
		this.dt_termino = dt_termino;
	}
	
	public String getDt_terminoFormat() {
		return dt_terminoFormat;
	}

	public void setDt_terminoFormat() {
		this.dt_terminoFormat = ConverterData.getOnlyDateString(dt_termino);
	}
	
	public String getDt_renovacaoFormat() {
		return dt_renovacaoFormat;
	}
	
	public Date getDt_reajuste() {
		return dt_reajuste;
	}

	public void setDt_reajuste(Date dt_reajuste) {
		this.dt_reajuste = dt_reajuste;
	}
	
	public String getDt_reajusteFormat() {
		return dt_reajusteFormat;
	}

	public void setDt_reajusteFormat() {
		this.dt_reajusteFormat = ConverterData.getOnlyDateString(dt_reajuste);
	}

	public Date getDt_entrega_chave() {
		return dt_entrega_chave;
	}

	public void setDt_entrega_chave(Date dt_entrega_chave) {
		this.dt_entrega_chave = dt_entrega_chave;
	}
	
	public String getDt_entrega_chaveFormat() {
		return dt_entrega_chaveFormat;
	}

	public void setDt_entrega_chaveFormat() {
		this.dt_entrega_chaveFormat = ConverterData.getOnlyDateString(dt_entrega_chave);
	}

	public Boolean getFlg_renovar() {
		return flg_renovar;
	}

	public void setFlg_renovar(Boolean flg_renovar) {
		this.flg_renovar = flg_renovar;
	}

	@Override
	public void setDados(ResultSet rs) throws SQLException {
		dt_inicio = rs.getDate("dt_inicio");
		dt_termino = rs.getDate("dt_termino");
		dt_reajuste = rs.getDate("dt_reajuste");
		dt_entrega_chave = rs.getDate("dt_entrega_chave");
		dia_vencimento = rs.getInt("dia_vencimento");
		flg_renovar = rs.getBoolean("flg_renovar");

		setDt_inicioFormat();
		setDt_terminoFormat();
		setDt_reajusteFormat();
		setDt_entrega_chaveFormat();
	}

}
