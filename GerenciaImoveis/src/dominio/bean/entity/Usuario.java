package dominio.bean.entity;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import dao.Persistente;
import dao.TransactionController;
import dominio.factory.HistoricoAcessoFactory;
import utilitarios.ConverterData;

public class Usuario extends Persistente {
	
	private String nome;
	private Timestamp dt_acesso;
	private String senha;
	private String login;
	
	private String dt_acessoFormat;

	public Usuario() {
		super();
	}

	public Usuario(String nome, Timestamp dt_acesso, String senha, String login) {
		super();
		this.nome = nome;
		this.dt_acesso = dt_acesso;
		this.senha = senha;
		this.login = login;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Timestamp getDt_acesso() {
		return dt_acesso;
	}

	public void setDt_acesso() {
		this.dt_acesso = new Timestamp(System.currentTimeMillis());
	}
	
	public String getDt_acessoFormat() {
		return dt_acessoFormat;
	}

	public void setDt_acessoFormat(){
		this.dt_acessoFormat = ConverterData.timestampToString(dt_acesso);
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}
	
	public void atualizarAcesso(String nom_tela, String descricao) throws SQLException{
		HistoricoAcesso h = HistoricoAcessoFactory.criar(null, nom_tela, descricao, this);
			
		System.out.println("Historico: "+ descricao);
		
		TransactionController tc = new TransactionController();
		tc.add(h);		
		tc.salvar();
	}

	@Override
	public void setDados(ResultSet rs) throws SQLException {
		nome = rs.getString("nome");
		dt_acesso = rs.getTimestamp("dt_acesso");
		senha = rs.getString("senha");
		login = rs.getString("login");

		setDt_acessoFormat();
	}

	@Override
	public String toString() {
		return "Usuario [nome=" + nome + ", dt_acesso=" + dt_acesso + ", senha=" + senha + ", login=" + login
				+ ", dt_acessoFormat=" + dt_acessoFormat + "]";
	}

	
}
