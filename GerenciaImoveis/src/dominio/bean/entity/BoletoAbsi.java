package dominio.bean.entity;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Date;

import dao.DAO;
import dao.DAOFactory;
import dao.Persistente;
import dominio.factory.LocacaoFactory;
import utilitarios.ConverterData;
import utilitarios.util;

import org.jrimum.utilix.text.Filler;
import org.jrimum.vallia.digitoverificador.Modulo;

public class BoletoAbsi extends Persistente {

	private String numero_documento;
	private BigDecimal valor;
	private BigDecimal valor_aluguel;
	private BigDecimal valor_pago;
	private BigDecimal valor_juros;
	private BigDecimal valor_despesa_cobranca;
	private BigDecimal valor_comissao;
	private BigDecimal lucro_txbancaria;
	
	private Date dt_pag_to_locador;
	private BigDecimal valor_pag_to_locador;
	private Integer cheque;
	
	private BigDecimal ir;
	
	private String dt_pag_to_locadorFormat;
	private String valor_pag_to_locadorFormat;
	private String chequeString;

	private String valorString;
	private String valor_aluguelString;
	private String valor_pagoString;
	private String valor_jurosString;
	private String valor_despesa_cobrancaString;
	private String valor_comissaoString;
	private String valor_diferencaString;

	private Date mes_ref;
	private Date dt_documento;
	private Date dt_formulario_transf;
	private Date dt_vencimento;
	private Date dt_pagamento;
	private String seu_numero;
	private String arq_remessa;
	private String arq_retorno;
	private Locacao locacao;
	private Boolean flg_enviado;
	private String digitoDoNossoNumero;

	private String dt_documentoFormat;
	private String dt_vencimentoFormat;
	private String dt_pagamentoFormat;

	// INFO BOLETO
	public static String CEDENTE = "ABSI NEGOCIOS IMOBILIARIOS"; // Benefici�rio
	public static String CPF_CNPJ = "34.267.664/0001-33";
	public static String LOCAL_PAGAMENTO = "PAGÁVEL EM QUALQUER BANCO ATÉ O VENCIMENTO";
	public static String INSTRUCAO_AO_SACADO = "Evite multas, pague em dias suas contas."; // VERIFICAR

	// CONTA
	public static String NOME_BANCO = "BANCO ITAÚ S.A.";
	public static Integer AGENCIA = 0305;
	public static Integer NUMERO_DA_CONTA = 13281;
	public static String DIGITO_VERIFICADOR_CONTA = "3";
	public static Integer CARTEIRA = 109;

	public static String agencia_string = "0305";
	public static String numero_conta_string = "13281";
	public static String carteira_string = "109";
	public static String cpf_cnpg_string = "34267664000133";

	public static String INSTRUCAO1 = "#APÓS VENCIMENTO MULTA DE 10% SOBRE O ALUGUEL";
	public static String INSTRUCAO2 = "#ACRESCER JUROS DE 1% AO MÊS SOBRE O ALUGUEL";
	public static String INSTRUCAO3 = "#NÃO RECEBER APÓS 15 DIAS DO VENCIMENTO";

	
	public BoletoAbsi() {
		super();
	}

	public BoletoAbsi(String numero_documento, BigDecimal valor, Date mes_ref, Date dt_documento, Date dt_vencimento,
			String seu_numero, Boolean flg_enviado, Locacao locacao) {
		super();
		this.numero_documento = numero_documento;
		this.valor = valor;
		this.mes_ref = mes_ref;
		this.dt_documento = dt_documento;
		this.dt_vencimento = dt_vencimento;
		this.seu_numero = Filler.ZERO_LEFT.fill(seu_numero, 8);
		this.locacao = locacao;
		this.flg_enviado = flg_enviado;
		valor_comissao = locacao.getImovel().getValorTxAdm();
		digitoDoNossoNumero = String.valueOf(calculeDvMod10());
	}

	public BigDecimal getIr() {
		return ir;
	}

	public void setIr(BigDecimal ir) {
		this.ir = ir;
	}

	public Date getDt_pag_to_locador() {
		return dt_pag_to_locador;
	}

	public void setDt_pag_to_locador(Date dt_pag_to_locador) {
		this.dt_pag_to_locador = dt_pag_to_locador;
	}

	public BigDecimal getValor_pag_to_locador() {
		return valor_pag_to_locador;
	}

	public void setValor_pag_to_locador(BigDecimal valor_pag_to_locador) {
		this.valor_pag_to_locador = valor_pag_to_locador;
	}

	public Integer getCheque() {
		return cheque;
	}

	public void setCheque(Integer cheque) {
		this.cheque = cheque;
	}

	public String getValorDiferencaStringFormat() {
		BigDecimal temp;
		
		if(valor_pago != null)
			temp = valor.subtract(valor_pago);
		else
			temp = new BigDecimal(0);
		
		valor_diferencaString = util.bigdecimalToPage(temp);
		return valor_diferencaString;
	}

	public String getValorStringFormat() {
		valorString = util.bigdecimalToPage(valor);
		return valorString;
	}

	public String getvalor_aluguelStringFormat() {
		valor_aluguelString = util.bigdecimalToPage(valor_aluguel);
		return valor_aluguelString;
	}

	public String getValorPagoStringFormat() {
		valor_pagoString = util.bigdecimalToPage(valor_pago);
		return valor_pagoString;
	}
	
	public String getValor_pag_to_locadorFormat() {
		valor_pag_to_locadorFormat = util.bigdecimalToPage(valor_pag_to_locador);
		return valor_pag_to_locadorFormat;
	}

	public BigDecimal getValor_comissao() {
		return valor_comissao;
	}

	public void setValor_comissao(BigDecimal valor_comissao) {
		this.valor_comissao = valor_comissao;
	}

	public String getSeu_numero() {
		return seu_numero;
	}

	public void setSeu_numero(String seu_numero) {
		this.seu_numero = seu_numero;
	}

	public Boolean getFlg_enviado() {
		return flg_enviado;
	}

	public void setFlg_enviado(Boolean flg_enviado) {
		this.flg_enviado = flg_enviado;
	}

	public Date getDt_pagamento() {
		return dt_pagamento;
	}

	public void setDt_pagamento(Date dt_pagamento) {
		this.dt_pagamento = dt_pagamento;
	}

	public String getNumero_documento() {
		return numero_documento;
	}

	public void setNumero_documento(String numero_documento) {
		this.numero_documento = numero_documento;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public BigDecimal getvalor_aluguel() {
		return valor_aluguel;
	}

	public void setvalor_aluguel(BigDecimal valor) {
		this.valor_aluguel = valor;
	}

	public BigDecimal getValor_juros() {
		return valor_juros;
	}
	
	public Boolean hasValor_juros() {
		if(valor_juros != null)
			return true;
		else
		return false;
	}

	public void setValor_juros(BigDecimal valor_juros) {
		this.valor_juros = valor_juros;
	}

	public String getValor_jurosFormat() {
		valor_jurosString = util.bigdecimalToPage(valor_juros);
		return valor_jurosString;
	}

	public BigDecimal getValor_despesa_cobranca() {
		return valor_despesa_cobranca;
	}

	public void setValor_despesa_cobranca(BigDecimal valor_despesa_cobranca) {
		this.valor_despesa_cobranca = valor_despesa_cobranca;
	}

	public BigDecimal getValor_pago() {
		return valor_pago;
	}
	
	public BigDecimal getLucro_txbancaria() {
		return lucro_txbancaria;
	}

	public void setLucro_txbancaria(BigDecimal lucro_txbancaria) {
		this.lucro_txbancaria = lucro_txbancaria;
	}

	public void setValor_pago(BigDecimal valor_pago) {
		this.valor_pago = valor_pago;
	}

	public Date getMes_ref() {
		return mes_ref;
	}

	public void setMes_ref(Date mes_ref) {
		this.mes_ref = mes_ref;
	}

	public String getDt_diaProtesto() {
		// return ConverterData.incrementDiaDeProtesto(dt_vencimento,
		// dt_vencimento.getDay());

		setDt_vencimentoFormat();

		String temp = dt_vencimentoFormat;

		String[] v = temp.split("/");
		String dia = v[0];

		return dia;
	}

	public Date getDt_documento() {
		return dt_documento;
	}

	public void setDt_documento(Date dt_documento) {
		this.dt_documento = dt_documento;
	}

	public Date getDt_vencimento() {
		return dt_vencimento;
	}

	public void setDt_vencimento(Date dt_vencimento) {
		this.dt_vencimento = dt_vencimento;
	}
	
	public String getDt_pag_to_locadorFormat() {
		return dt_pag_to_locadorFormat;
	}

	public void setDt_pag_to_locadorFormat() {
		this.dt_pag_to_locadorFormat = ConverterData.getOnlyDateString(dt_pag_to_locador);
	}

	public String getDt_documentoFormat() {
		return dt_documentoFormat;
	}

	public void setDt_documentoFormat() {
		this.dt_documentoFormat = ConverterData.getOnlyDateString(dt_documento);
	}

	public String getDt_vencimentoFormat() {
		return dt_vencimentoFormat;
	}

	public void setDt_vencimentoFormat() {
		this.dt_vencimentoFormat = ConverterData.getOnlyDateString(dt_vencimento);
	}

	public Integer getDt_vencimentoBoleto() {
		setDt_vencimentoFormat();

		String temp = dt_vencimentoFormat;

		String[] v = temp.split("/");
		String dia = v[0];
		String mes = v[1];
		String ano = v[2];
		ano = ano.substring(2, 4);

		temp = dia + mes + ano;

		return Integer.valueOf(temp);
	}

	public String getDt_pagamentoFormat() {
		return dt_pagamentoFormat;
	}

	public void setDt_pagamentoFormat() {
		this.dt_pagamentoFormat = ConverterData.getOnlyDateString(dt_pagamento);
	}

	public String getArq_remessa() {
		return arq_remessa;
	}

	public void setArq_remessa(String arq_remessa) {
		this.arq_remessa = arq_remessa;
	}

	public String getArq_retorno() {
		return arq_retorno;
	}

	public void setArq_retorno(String arq_retorno) {
		this.arq_retorno = arq_retorno;
	}

	public Locacao getLocacao() {
		return locacao;
	}

	public void setLocacao(Locacao locacao) {
		this.locacao = locacao;
	}

	public String getDigitoDoNossoNumero() {
		return digitoDoNossoNumero;
	}

	public void setDigitoDoNossoNumero(String digitoDoNossoNumero) {
		this.digitoDoNossoNumero = digitoDoNossoNumero;
	}

	public Integer getTipoSacado() {
		if (locacao.getLocatario().getCpf() != null) {
			return 01;
		}
		return 02;
	}

	public String getDocumentoLocatarioBoleto() {
		if (locacao.getLocatario().getCpf() != null) {
			return locacao.getLocatario().getCpf();
		}

		return locacao.getLocatario().getCnpj();
	}

	public String getDocumentoLocadorBoleto() {
		if (locacao.getImovel().getLocador().getCpf() != null) {
			return locacao.getImovel().getLocador().getCpf();
		}

		return locacao.getImovel().getLocador().getCnpj();
	}

	public String getDocumentoSacado() {
		if (getTipoSacado() == 01) {
			return ((locacao.getLocatario().getCpf()).replace("-", "")).replace(".", "");
		}

		return (((locacao.getLocatario().getCnpj()).replace("-", "")).replace(".", "")).replace("/", "");
	}
	
	public String getSeuNumero(){
		String temp = util.unaccent(numero_documento);
		if (temp.length() > 8) {
			temp = temp.substring(0, 8);
		}
		return temp;
	}

	public String getNomeSacado() {
		String temp = util.unaccent(locacao.getLocatario().getNome_completo().toUpperCase());
		if (temp.length() > 30) {
			temp = temp.substring(0, 30);
		}
		return temp;
	}

	public String getEnderecoSacado() {
		String temp = util.unaccent(locacao.getImovel().getEndereco().getEnderecoCompleto().toUpperCase());
		if (temp.length() > 40) {
			temp = temp.substring(0, 40);
		}
		return temp;
	}

	public String getBairroSacado() {
		String temp = util.unaccent(locacao.getLocatario().getEndereco().getBairro().toUpperCase());
		if (temp.length() > 12) {
			temp = temp.substring(0, 12);
		}
		return temp;
	}

	public String getCepSacado() {
		String temp = (locacao.getLocatario().getEndereco().getCep()).replace("-", "").toUpperCase();
		return temp;
	}

	public String getCidadeSacado() {
		String temp = util.unaccent(locacao.getLocatario().getEndereco().getCidade().toUpperCase());
		if (temp.length() > 15) {
			temp = temp.substring(0, 15);
		}
		return temp;
	}

	public String getSacadorAvalista() {
		String temp = util.unaccent(locacao.getImovel().getLocador().getNome_completo().toUpperCase());
		if (temp.length() > 30) {
			temp = temp.substring(0, 30);
		}
		return temp;
	}
	
	
	public Date getDt_formulario_transf() {
		return dt_formulario_transf;
	}

	public void setDt_formulario_transf(Date dt_formulario_transf) {
		this.dt_formulario_transf = dt_formulario_transf;
	}

	@Override
	public void setDados(ResultSet rs) throws SQLException {
		numero_documento = rs.getString("numero_documento");
		valor = rs.getBigDecimal("valor");
		valor_aluguel = rs.getBigDecimal("valor_aluguel");
		mes_ref = rs.getDate("mes_ref");
		seu_numero = rs.getString("seu_numero");
		digitoDoNossoNumero = rs.getString("digitoDoNossoNumero");
		dt_documento = rs.getDate("dt_documento");
		dt_pagamento = rs.getDate("dt_pagamento");
		dt_vencimento = rs.getDate("dt_vencimento");
		dt_formulario_transf = rs.getDate("dt_formulario_transf");
		arq_remessa = rs.getString("arq_remessa");
		arq_retorno = rs.getString("arq_retorno");
		flg_enviado = rs.getBoolean("flg_enviado");
		valor_pago = rs.getBigDecimal("valor_pago");
		valor_comissao = rs.getBigDecimal("valor_comissao");
		valor_despesa_cobranca = rs.getBigDecimal("valor_despesa_cobranca");
		valor_juros = rs.getBigDecimal("valor_juros");
		lucro_txbancaria = rs.getBigDecimal("lucro_txbancaria");
		locacao = LocacaoFactory.recuperarPorId(rs.getInt("fk_id_locacao"));
		
		dt_pag_to_locador = rs.getDate("dt_pag_to_locador");
		cheque = rs.getInt("cheque");
		valor_pag_to_locador = rs.getBigDecimal("valor_pag_to_locador");

		setDt_pag_to_locadorFormat();
		
		setDt_documentoFormat();
		setDt_pagamentoFormat();
		setDt_vencimentoFormat();

		getValor_jurosFormat();
		getValor_pag_to_locadorFormat();
		getValorPagoStringFormat();
		getValorStringFormat();
		getvalor_aluguelStringFormat();
		getValorDiferencaStringFormat();
	}

	@Override
	public String toString() {
		return "BoletoAbsi [numero_documento=" + numero_documento + ", valor=" + valor + ", valor_pago=" + valor_pago
				+ ", valor_juros=" + valor_juros + ", valor_despesa_cobranca=" + valor_despesa_cobranca
				+ ", valor_comissao=" + valor_comissao + ", mes_ref=" + mes_ref + ", dt_documento=" + dt_documento
				+ ", dt_vencimento=" + dt_vencimento + ", dt_pagamento=" + dt_pagamento + ", seu_numero=" + seu_numero
				+ ", arq_remessa=" + arq_remessa + ", arq_retorno=" + arq_retorno + ", locacao=" + locacao
				+ ", flg_enviado=" + flg_enviado + ", digitoDoNossoNumero=" + digitoDoNossoNumero
				+ ", dt_documentoFormat=" + dt_documentoFormat + ", dt_vencimentoFormat=" + dt_vencimentoFormat
				+ ", dt_pagamentoFormat=" + dt_pagamentoFormat + "]";
	}

	private Integer calculeDvMod10() {

		String temp = agencia_string + numero_conta_string + carteira_string + seu_numero;

		int somatorio = Modulo.calculeSomaSequencialMod10(temp, 1, 2);
		int restoDivisao;

		if (somatorio < 10) {
			restoDivisao = somatorio;
		} else {
			restoDivisao = somatorio % 10;
		}

		int restoSubtracao = (10 - restoDivisao);

		if (restoDivisao == 0) {
			return 0;
		} else {
			return restoSubtracao;
		}
	}

	@Override
	@SuppressWarnings("rawtypes")
	public void salvar(Connection conn) throws SQLException {
		DAO dao = DAOFactory.getDAO(this);

		System.out.println("------------------------------------  salvar Boleto");

		if (isPersistente()) {
			dao.atualizar(conn);
			setUpdated(true);

		} else {
			Integer id = dao.inserir(conn);
			this.setId(id);
			setPersistente(true);
		}

		System.out.println("------------------------------------  salvar Itens da locacao");

		if (this.getLocacao().getListaItensLocacao() != null) {
			this.getLocacao().getListaItensLocacao().salvar(conn);
		}

	}
}
