package dominio.bean.entity;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import dao.DAO;
import dao.DAOFactory;
import dao.Persistente;
import utilitarios.util;

public class Celular extends Persistente {
	private Integer num_celular;
	private Integer ddd;
	private Integer id_pessoa;
	private boolean erase;
	
	public Celular() {
		super();
	}

	public Celular(Integer num_celular, Integer ddd) {
		super();
		this.num_celular = num_celular;
		this.ddd = ddd;
	}
	
	public String getNum_celularFormat() {
		return "(" + String.valueOf(ddd) + ") " + util.FormatCelular(String.valueOf(num_celular));
	}

	public void setNum_celularFormat(Integer num_celular) {
		this.num_celular = num_celular;
	}

	public Integer getNum_celular() {
		return num_celular;
	}

	public void setNum_celular(Integer num_celular) {
		this.num_celular = num_celular;
	}

	public Integer getDdd() {
		return ddd;
	}

	public void setDdd(Integer ddd) {
		this.ddd = ddd;
	}
	
	
	public Integer getId_pessoa() {
		return id_pessoa;
	}

	public void setId_pessoa(Integer id_pessoa) {
		this.id_pessoa = id_pessoa;
	}
	
	public boolean isErase() {
		return erase;
	}

	public void setErase(boolean erase) {
		this.erase = erase;
	}
	
	public String getDddTel() {
		return  String.valueOf(ddd) +String.valueOf(num_celular) ;
	}

	@Override
	public void setDados(ResultSet rs) throws SQLException {
		num_celular = rs.getInt("num_celular");
		ddd = rs.getInt("ddd");
		id_pessoa = rs.getInt("fk_id_pessoa");
	}

	
	
	@Override
	public String toString() {
		return "Celular [num_celular=" + num_celular + ", ddd=" + ddd + ", erase=" + erase + ", getNum_celularFormat()="
				+ getNum_celularFormat() + "]";
	}

	@Override
	@SuppressWarnings("rawtypes")
    public void salvar(Connection conn) throws SQLException {
        DAO dao = DAOFactory.getDAO(this);
        
        if (isPersistente()){
        	if(isErase())
        		dao.excluir(conn);
        	else{
        	dao.atualizar(conn);
        	setUpdated(true);
        	}
        }
            
        else {
          this.setId(dao.inserir(conn)); 
            setPersistente(true);
        }
    }
	
	
}

