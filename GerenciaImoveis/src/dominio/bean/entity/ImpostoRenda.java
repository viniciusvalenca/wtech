package dominio.bean.entity;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

import dao.Persistente;
import dominio.factory.PessoaFactory;
import utilitarios.util;

public class ImpostoRenda extends Persistente {
	
	private Date mes_ref;
	private BigDecimal valor_desconto;
	private String valor_descontoString;
	private Pessoa locador;
	private Integer fk_id_locacao;
	private BigDecimal ajuste_aluguel;
	private BigDecimal ajuste_txadm;
	
	public BigDecimal getAjuste_aluguel() {
		return ajuste_aluguel;
	}

	public void setAjuste_aluguel(BigDecimal ajuste_aluguel) {
		this.ajuste_aluguel = ajuste_aluguel;
	}

	public BigDecimal getAjuste_txadm() {
		return ajuste_txadm;
	}

	public void setAjuste_txadm(BigDecimal ajuste_txadm) {
		this.ajuste_txadm = ajuste_txadm;
	}

	public ImpostoRenda() {
		super();
	}

	public ImpostoRenda(Date mes_ref, BigDecimal valor_desconto, Pessoa locador) {
		super();
		this.mes_ref = mes_ref;
		this.valor_desconto = valor_desconto;
		this.locador = locador;
	}

	public Integer getFk_id_locacao() {
		return fk_id_locacao;
	}

	public void setFk_id_locacao(Integer fk_id_locacao) {
		this.fk_id_locacao = fk_id_locacao;
	}

	public Date getMes_ref() {
		return mes_ref;
	}

	public void setMes_ref(Date mes_ref) {
		this.mes_ref = mes_ref;
	}

	public BigDecimal getValor_desconto() {
		return valor_desconto;
	}

	public void setValor_desconto(BigDecimal valor_desconto) {
		this.valor_desconto = valor_desconto;
	}

	public String getValorDescontoStringFormat() {
		valor_descontoString =  util.bigdecimalToPage(valor_desconto);
		return valor_descontoString;
	}

	public Pessoa getLocador() {
		return locador;
	}

	public void setLocador(Pessoa locador) {
		this.locador = locador;
	}
	
	@Override
	public void setDados(ResultSet rs) throws SQLException {
		valor_desconto = rs.getBigDecimal("valor_desconto");
		ajuste_aluguel = rs.getBigDecimal("ajuste_aluguel");
		ajuste_txadm = rs.getBigDecimal("ajuste_txadm");
		mes_ref = rs.getDate("mes_ref");
		locador = PessoaFactory.recuperarPorID(rs.getInt("fk_id_locador"));
		fk_id_locacao = rs.getInt("fk_id_locacao") == 0 ? null : rs.getInt("fk_id_locacao");
		System.out.println(ajuste_aluguel);

		getValorDescontoStringFormat();
	}

	@Override
	public String toString() {
		return "ImpostoRenda [mes_ref=" + mes_ref + ", valor_desconto=" + valor_desconto + ", valor_descontoString="
				+ valor_descontoString + ", locador=" + locador + "]";
	}
	
}
