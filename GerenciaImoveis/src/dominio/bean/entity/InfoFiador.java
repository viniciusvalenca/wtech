package dominio.bean.entity;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import dao.DAO;
import dao.DAOFactory;
import dao.Persistente;

public class InfoFiador extends Persistente {

	private String mat_imovel;
	private String cartorio;
	private String endereco_imovel;
	
	private Integer fk_id_locacao;	
	private Integer fk_id_fiador;
	
	private boolean erase;

	public InfoFiador() {
		super();
	}
	
	public InfoFiador(String mat_imovel, String cartorio, String endereco_imovel, Integer fk_id_fiador) {
		super();
		this.mat_imovel = mat_imovel;
		this.cartorio = cartorio;
		this.endereco_imovel = endereco_imovel;
		this.fk_id_fiador = fk_id_fiador;
	}

	public String getMat_imovel() {
		return mat_imovel;
	}

	public void setMat_imovel(String mat_imovel) {
		if (mat_imovel.length() > 0)
			this.mat_imovel = mat_imovel;
		else
			this.mat_imovel = null;
	}

	public String getCartorio() {
		return cartorio;
	}

	public void setCartorio(String cartorio) {
		if (cartorio.length() > 0)
			this.cartorio = cartorio;
		else
			this.cartorio = null;
	}

	public String getEndereco_imovel() {
		return endereco_imovel;
	}

	public void setEndereco_imovel(String endereco_imovel) {
		if (endereco_imovel.length() > 0)
			this.endereco_imovel = endereco_imovel;
		else
			this.endereco_imovel = null;
	}
	
	public Integer getFk_id_locacao() {
		return fk_id_locacao;
	}

	public void setFk_id_locacao(Integer fk_id_locacao) {
		this.fk_id_locacao = fk_id_locacao;
	}

	public Integer getFk_id_fiador() {
		return fk_id_fiador;
	}

	public void setFk_id_fiador(Integer fk_id_fiador) {
		this.fk_id_fiador = fk_id_fiador;
	}
	
	public boolean isErase() {
		return erase;
	}

	public void setErase(boolean erase) {
		this.erase = erase;
	}
	
	public void setInfoFiador(InfoFiador inf) {
		setFk_id_fiador(inf.getFk_id_fiador());
		setMat_imovel(inf.getMat_imovel());
		setCartorio(inf.getCartorio());
		setEndereco_imovel(inf.getEndereco_imovel());
	}

	@Override
	public void setDados(ResultSet rs) throws SQLException {
		mat_imovel = rs.getString("mat_imovel");
		cartorio = rs.getString("cartorio");
		endereco_imovel = rs.getString("endereco_imovel");
		fk_id_locacao = rs.getInt("fk_id_locacao");
		fk_id_fiador = rs.getInt("fk_id_fiador");
	}
	
	@Override
	@SuppressWarnings("rawtypes")
    public void salvar(Connection conn) throws SQLException {
        DAO dao = DAOFactory.getDAO(this);
        
        if (isPersistente()){
        	if(isErase())
        		dao.excluir(conn);
        	else{
        	dao.atualizar(conn);
        	setUpdated(true);
        	}
        }
            
        else {
          this.setId(dao.inserir(conn)); 
            setPersistente(true);
        }
    }

	@Override
	public String toString() {
		return "InfoFiador [mat_imovel=" + mat_imovel + ", cartorio=" + cartorio + ", endereco_imovel="
				+ endereco_imovel + ", fk_id_locacao=" + fk_id_locacao + ", fk_id_fiador=" + fk_id_fiador + "]";
	}

}
