package dominio.bean.entity;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Connection;
import java.sql.Date;

import dao.DAO;
import dao.DAOFactory;
import dao.Persistente;
import dominio.bean.list.ListaCelular;
import dominio.bean.list.ListaTelefone;
import dominio.factory.CelularFactory;
import dominio.factory.DadosBancariosFactory;
import dominio.factory.EnderecoFactory;
import dominio.factory.TelefoneFactory;
import utilitarios.ConverterData;

public class Pessoa extends Persistente {

	private String nome_completo;
	private String email;
	private String email_opcional;
	private String cpf;
	private String cnpj;
	private String cpfcnpj;
	private String identidade;
	private Date dt_emissao;
	private String emissor;
	private String estado_civil;
	private Date dt_nascimento;
	private String nacionalidade;
	private String naturalidade;
	private String profissao;
	private String telefones_extras;
	private Boolean flg_locatario;
	private Boolean flg_locador;
	private Boolean flg_fiador;
	private DadosBancarios dadosBancarios;
	private Endereco endereco;

	private String dt_nascimentoFormat;
	private String dt_emissaoFormat;

	private ListaTelefone telefones;
	private ListaCelular celulares;

	public Pessoa() {
		super();
	}

	public Pessoa(String nome_completo, String email, String email_opcional, String cpf, String cnpj, String identidade,
			Date dt_emissao, String emissor, String estado_civil, Date dt_nascimento, String nacionalidade,
			String naturalidade, String profissao, String telefones_extras, Boolean flg_locatario, Boolean flg_locador, Boolean flg_fiador) {
		super();
		this.nome_completo = nome_completo;
		this.email = email;
		setEmail_opcional(email_opcional);
		setCpf(cpf);
		setCnpj(cnpj);
		setCpfcnpj();
		this.identidade = identidade;
		this.dt_emissao = dt_emissao;
		this.emissor = emissor;
		this.estado_civil = estado_civil;
		this.dt_nascimento = dt_nascimento;
		this.nacionalidade = nacionalidade;
		this.naturalidade = naturalidade;
		this.profissao = profissao;
		setTelefones_extras(telefones_extras);
		this.flg_locatario = flg_locatario;
		this.flg_locador = flg_locador;
		this.flg_fiador = flg_fiador;

	}

	public String getNome_completo() {
		return nome_completo;
	}

	public void setNome_completo(String nome_completo) {
		this.nome_completo = nome_completo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmail_opcional() {
		return email_opcional;
	}

	public void setEmail_opcional(String email_opcional) {
		if (email_opcional.length() > 0)
			this.email_opcional = email_opcional;
		else
			this.email_opcional = null;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		if (cpf.length() > 0)
			this.cpf = cpf;
		else
			this.cpf = null;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		if (cnpj.length() > 0)
			this.cnpj = cnpj;
		else
			this.cnpj = null;
	}
	
	public void setCpfcnpj() {
		if(cpf!=null)
			cpfcnpj= "CPF: "+cpf;	
		else			
			cpfcnpj= "CNPJ: "+cnpj;		
	}
	
	public String getCpfcnpj() {
		return cpfcnpj;
	}

	public String getIdentidade() {
		return identidade;
	}

	public void setIdentidade(String identidade) {
		this.identidade = identidade;
	}

	public Date getDt_emissao() {
		return dt_emissao;
	}

	public void setDt_emissao(Date dt_emissao) {
		this.dt_emissao = dt_emissao;
	}

	public String getDt_emissaoFormat() {
		return dt_emissaoFormat;
	}

	public void setDt_emissaoFormat() {
		if(dt_emissao != null){
			this.dt_emissaoFormat = ConverterData.getOnlyDateString(dt_emissao);
		}else{
			this.dt_emissaoFormat = "";
		}
	}

	public String getEmissor() {
		return emissor;
	}

	public void setEmissor(String emissor) {
		this.emissor = emissor;
	}

	public String getEstado_civil() {
		return estado_civil;
	}

	public void setEstado_civil(String estado_civil) {
		this.estado_civil = estado_civil;
	}

	public Date getDt_nascimento() {
		return dt_nascimento;
	}

	public void setDt_nascimento(Date dt_nascimento) {
		this.dt_nascimento = dt_nascimento;
	}

	public String getDt_nascimentoFormat() {
		return dt_nascimentoFormat;
	}

	public void setDt_nascimentoFormat() {
		this.dt_nascimentoFormat = ConverterData.getOnlyDateString(dt_nascimento);
	}

	public String getNacionalidade() {
		return nacionalidade;
	}

	public void setNacionalidade(String nacionalidade) {
		this.nacionalidade = nacionalidade;
	}

	public String getNaturalidade() {
		return naturalidade;
	}

	public void setNaturalidade(String naturalidade) {
		this.naturalidade = naturalidade;
	}

	public String getProfissao() {
		return profissao;
	}

	public void setProfissao(String profissao) {
		this.profissao = profissao;
	}

	public String getTelefones_extras() {
		return telefones_extras;
	}

	public void setTelefones_extras(String telefones_extras) {
		if (telefones_extras.length() > 0)
			this.telefones_extras = telefones_extras;
		else
			this.telefones_extras = null;
	}

	public Boolean getFlg_locatario() {
		return flg_locatario;
	}

	public void setFlg_locatario(Boolean flg_locatario) {
		this.flg_locatario = flg_locatario;
	}

	public Boolean getFlg_locador() {
		return flg_locador;
	}

	public void setFlg_locador(Boolean flg_locador) {
		this.flg_locador = flg_locador;
	}

	public Boolean getFlg_fiador() {
		return flg_fiador;
	}

	public void setFlg_fiador(Boolean flg_fiador) {
		this.flg_fiador = flg_fiador;
	}

	public DadosBancarios getDadosBancarios() {
		return dadosBancarios;
	}

	public void setDadosBancarios(DadosBancarios dadosBancarios) {
		this.dadosBancarios = dadosBancarios;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public ListaTelefone getTelefones() {
		return telefones;
	}

	public ListaCelular getCelulares()  {
		return celulares;
	}

	public void setTelefones(ListaTelefone telefones) {
		if(isPersistente()){
			telefones.setId(this.getId());	
		}
		if (this.telefones != null) {
			for (int i = 0; i < this.telefones.size(); i++) {
				Telefone t = this.telefones.get(i);
				if (!telefones.find(t.getDddTel())) {
					t.setErase(true);
				}
			}
			for (int i = 0; i < telefones.size(); i++) {
				Telefone t = telefones.get(i);
				if (!this.telefones.find(t.getDddTel())) {
					this.telefones.add(t);
				}
			}
		} else
			this.telefones = telefones;
	}

	public void setCelulares(ListaCelular celulares) {
		if(isPersistente()){
			celulares.setId(this.getId());
		}
		if (this.celulares != null) {
			for (int i = 0; i < this.celulares.size(); i++) {
				Celular t = this.celulares.get(i);
				if (!celulares.find(t.getDddTel())) {
					t.setErase(true);
				}
			}
			for (int i = 0; i < celulares.size(); i++) {
				Celular t = celulares.get(i);
				if (!this.celulares.find(t.getDddTel())) {
					this.celulares.add(t);
				}
			}
		} else
			this.celulares = celulares;
	}

	@Override
	public void setDados(ResultSet rs) throws SQLException {
		nome_completo = rs.getString("nome_completo");
		email = rs.getString("email");
		email_opcional = rs.getString("email_opcional");
		cpf = rs.getString("cpf");
		cnpj = rs.getString("cnpj");
		identidade = rs.getString("identidade");
		dt_emissao = rs.getDate("dt_emissao");
		emissor = rs.getString("emissor");
		estado_civil = rs.getString("estado_civil");
		dt_nascimento = rs.getDate("dt_nascimento");
		nacionalidade = rs.getString("nacionalidade");
		naturalidade = rs.getString("naturalidade");
		profissao = rs.getString("profissao");
		telefones_extras = rs.getString("telefones_extras");
		flg_locatario = rs.getBoolean("flg_locatario");
		flg_locador = rs.getBoolean("flg_locador");
		flg_fiador = rs.getBoolean("flg_fiador");
		celulares = CelularFactory.recuperarPorPessoaId(getId());
		telefones = TelefoneFactory.recuperarPorPessoaId(getId());
		endereco = EnderecoFactory.recuperarPorId(rs.getInt("fk_id_endereco"));
		if (flg_locador == true) {
			dadosBancarios = DadosBancariosFactory.recuperarPorId(rs.getInt("fk_id_dadosbancario"));
		}
		setDt_emissaoFormat();
		setDt_nascimentoFormat();
		setCpfcnpj();
	}

	@Override
	@SuppressWarnings("rawtypes")
	public void salvar(Connection conn) throws SQLException {
		DAO dao = DAOFactory.getDAO(this);

		System.out.println("------------------------------------  salvar()");

		this.getEndereco().salvar(conn);

		if (flg_locador == true && this.getDadosBancarios() != null)
			this.getDadosBancarios().salvar(conn);

		if (isPersistente()) {
			dao.atualizar(conn);
			setUpdated(true);
		} else {
			int id = dao.inserir(conn);
			this.setId(id);
			
			setPersistente(true);
			this.getTelefones().setId(id);
			this.getCelulares().setId(id);
		}


		if (this.getTelefones().size() > 0) {
			getTelefones().salvar(conn);
		}

		if (this.getCelulares().size() > 0) {
			getCelulares().salvar(conn);
		}

	}

	@Override
	public String toString() {
		return "Pessoa [nome Completo=" + nome_completo + ", email=" + email + ", email_opcional=" + email_opcional
				+ ", cpf=" + cpf + ", identidade=" + identidade + ", dt_emissao=" + dt_emissao + ", emissor=" + emissor
				+ ", estado_civil=" + estado_civil + ", dt_nascimento=" + dt_nascimento + ", dt_nascimentoFormat="
				+ getDt_nascimentoFormat() + ", dt_emissaoFormat=" + getDt_emissaoFormat() + ", nacionalidade="
				+ nacionalidade + ", naturalidade=" + naturalidade + ", profissao=" + profissao +", telefones_extras=" + telefones_extras + ", flg_locatario="
				+ flg_locatario + ", flg_locador=" + flg_locador + ", flg_fiador=" + flg_fiador + ", dadosBancarios="
				+ dadosBancarios + ", endereco=" + endereco + ", cpfcnpj=" + cpfcnpj + "]";
	}

}
