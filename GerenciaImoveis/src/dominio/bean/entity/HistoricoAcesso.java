package dominio.bean.entity;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import dao.Persistente;
import dominio.factory.UsuarioFactory;
import utilitarios.ConverterData;

public class HistoricoAcesso extends Persistente {

	private Timestamp dt_modificacao;
	private String nom_tela;
	private String descricao;
	private Usuario usuario;
	
	private String dt_modificacaoFormat;

	public HistoricoAcesso() {
		super();
	}

	public HistoricoAcesso(Timestamp dt_modificacao, String nom_tela, String descricao, Usuario usuario) {
		super();
		this.dt_modificacao = dt_modificacao;
		this.nom_tela = nom_tela;
		this.descricao = descricao;
		this.usuario = usuario;
	}

	public String getDt_modificacaoFormat() {
		return dt_modificacaoFormat;
	}

	public void setDt_modificacaoFormat(Timestamp dt) {
		this.dt_modificacaoFormat = ConverterData.timestampToString(dt);
	}

	public Timestamp getDt_modificacao() {
		return dt_modificacao;
	}

	public void setDt_modificacao(Timestamp dt_modificacao) {
		this.dt_modificacao = dt_modificacao;
	}

	public String getNom_tela() {
		return nom_tela;
	}

	public void setNom_tela(String nom_tela) {
		this.nom_tela = nom_tela;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Integer fk_id_usuario) throws SQLException {
		this.usuario = UsuarioFactory.recuperarPorId(fk_id_usuario);
	}

	@Override
	public void setDados(ResultSet rs) throws SQLException {
		dt_modificacao = rs.getTimestamp("dt_modificacao");
		nom_tela = rs.getString("nom_tela");
		descricao = rs.getString("descricao");
		setUsuario(rs.getInt("fk_id_usuario"));
		setDt_modificacaoFormat(dt_modificacao);
	}

	@Override
	public String toString() {
		return "HistoricoAcesso [dt_modificacao=" + dt_modificacao + ", nom_tela=" + nom_tela + ", descricao="
				+ descricao + "]";
	}
	
}
