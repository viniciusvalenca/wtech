package dominio.bean.entity;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import dao.DAO;
import dao.DAOFactory;
import dao.Persistente;
import dominio.bean.list.ListaBoletoAbsi;
import dominio.bean.list.ListaInfoFiador;
import dominio.bean.list.ListaItensLocacao;
import dominio.factory.BoletoAbsiFactory;
import dominio.factory.ContratoFactory;
import dominio.factory.ImovelFactory;
import dominio.factory.ImpostoRendaFactory;
import dominio.factory.InfoFiadorFactory;
import dominio.factory.ItensLocacaoFactory;
import dominio.factory.PessoaFactory;

public class Locacao extends Persistente {

	private String modalidade;
	private String valor_deposito;
	private String valor_seguro;
	private Pessoa locatario;
	private Imovel imovel;

	private BoletoAbsi boleto;
	private ListaBoletoAbsi lboleto;
	private ArrayList<IR> irs;
	private Contrato contrato;
	private ListaInfoFiador listaInfoFiador;
	private ListaItensLocacao listaItensLocacao;

	public Locacao() {
		super();
	}

	public Locacao(String modalidade, String valor_deposito, String valor_seguro) throws SQLException {
		super();
		this.modalidade = modalidade;
		setValor_deposito(valor_deposito);
		setValor_seguro(valor_seguro);

	}

	public BoletoAbsi getBoleto() {
		return boleto;
	}

	public void setBoleto(Date mes_ref) throws SQLException {
		this.boleto = BoletoAbsiFactory.buscarBoletoLocacaoMes(getId(), mes_ref);
	}

	public String getModalidade() {
		return modalidade;
	}

	public void setModalidade(String modalidade) {

		if (!modalidade.equals(this.modalidade)) {

			if (this.modalidade.equals("Seguro Fianca")) {
				valor_seguro = null;
			} else if (this.modalidade.equals("Deposito")) {
				valor_deposito = null;
			} else if (this.modalidade.equals("Fiador")) {
				listaInfoFiador.setErasable();
			}
		}

		this.modalidade = modalidade;
	}

	public String getValor_deposito() {
		return valor_deposito;
	}

	public void setValor_deposito(String valor_deposito) {
		if (valor_deposito.length() > 0)
			this.valor_deposito = valor_deposito;
		else
			this.valor_deposito = null;
	}

	public String getValor_seguro() {
		return valor_seguro;
	}

	public void setValor_seguro(String valor_seguro) {
		if (valor_seguro.length() > 0)
			this.valor_seguro = valor_seguro;
		else
			this.valor_seguro = null;
	}

	public Pessoa getLocatario() {
		return locatario;
	}

	public void setLocatario(Pessoa locatario) {
		this.locatario = locatario;
	}

	public Imovel getImovel() {
		return imovel;
	}

	public void setImovel(Imovel imovel) {
		this.imovel = imovel;
		imovel.setFlg_disponivel(false);
	}

	public Contrato getContrato() {
		return contrato;
	}

	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}

	public ListaInfoFiador getListaInfoFiador() {
		return listaInfoFiador;
	}

	public void setListaInfoFiador(ListaInfoFiador listaInfoFiador) {

		if (this.listaInfoFiador != null) {
			int size = this.listaInfoFiador.size() - listaInfoFiador.size();

			System.out.println(size);

			if (size == 0) {
				for (int i = 0; i < this.listaInfoFiador.size(); i++) {

					this.getListaInfoFiador().get(i).setInfoFiador(listaInfoFiador.get(i));

				}
			} else if (size < 0) {
				for (int i = 0; i < listaInfoFiador.size(); i++) {

					if (this.listaInfoFiador.size() >= i + 1) {
						this.getListaInfoFiador().get(i).setInfoFiador(listaInfoFiador.get(i));
					} else {
						this.listaInfoFiador.add(listaInfoFiador.get(i));
					}
				}
			} else if (size > 0) {
				for (int i = 0; i < this.listaInfoFiador.size(); i++) {

					if (listaInfoFiador.size() >= i + 1) {
						this.getListaInfoFiador().get(i).setInfoFiador(listaInfoFiador.get(i));
					} else {
						this.listaInfoFiador.get(i).setErase(true);
					}
				}
			}
		} else {
			this.listaInfoFiador = listaInfoFiador;
		}
	}

	public ListaItensLocacao getListaItensLocacao() {
		return listaItensLocacao;
	}

	public void setListaItensLocacao(ListaItensLocacao listaItensLocacao) {
		this.listaItensLocacao = listaItensLocacao;
	}

	public void setListaItensLocacao(Date mes_ref) throws SQLException {
		this.listaItensLocacao = ItensLocacaoFactory.recuperarDoMes(mes_ref, getId());
	}

	public BigDecimal getValorBoleto() {
		BigDecimal valor = listaItensLocacao.valorLocatario();

		return valor;
	}

	public BigDecimal getValorSoma() {
		BigDecimal valor = listaItensLocacao.valorSoma();
		System.out.println(boleto);

		if (boleto.hasValor_juros()) {
			valor = valor.add(boleto.getValor_juros());
		}
		return valor;
	}

	public BigDecimal getValorSub() {
		BigDecimal valor = listaItensLocacao.valorSub();
		valor = valor.add(boleto.getValor_comissao());

		return valor;
	}

	public BigDecimal getValorFica() {
		BigDecimal valor = listaItensLocacao.valorFica();

		return valor;
	}

	public ListaBoletoAbsi getLboleto() {
		return lboleto;
	}

	public void setLboleto(Date ano) throws SQLException, ParseException {
		this.lboleto = BoletoAbsiFactory.recuperarPagamentoAno(getId(), ano);
		completarMesesLboleto(ano);
	}

	////////////////////////////////////////////////////
	public void completarMesesLboleto(Date ano) throws SQLException, ParseException {
		ArrayList<String> todosMeses = new ArrayList<String>();
		irs = new ArrayList<IR>();
		todosMeses.add("Janeiro");
		todosMeses.add("Fevereiro");
		todosMeses.add("Março");
		todosMeses.add("Abril");
		todosMeses.add("Maio");
		todosMeses.add("Junho");
		todosMeses.add("Julho");
		todosMeses.add("Agosto");
		todosMeses.add("Setembro");
		todosMeses.add("Outubro");
		todosMeses.add("Novembro");
		todosMeses.add("Dezembro");
			
    	Calendar cal = Calendar.getInstance();
    	cal.setTime(ano);
  
    	int year = cal.get(Calendar.YEAR);
		for (int i = 1; i <= 12; i++) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");			
			java.sql.Date sqlDate = new java.sql.Date(sdf.parse("01/"+String.valueOf(i)+"/"+String.valueOf(year)).getTime());
			IR ir = new IR();
			ir.setMes(todosMeses.get(i - 1));
			ir.setMes_ref(sqlDate);
			ir.setFk_id_locacador(getImovel().getLocador().getId());
			ir.setFk_id_locacao(getId());
			ir.setMes_refFormat();
			
			ImpostoRenda ir2 = ImpostoRendaFactory.recuperarPorLocadorLocacao(ir.getFk_id_locacador(), sqlDate,getId());
			
			if(ir2 != null ) {					
				if(ir2.getAjuste_aluguel()!=null) {
					ir.setAjuste_aluguel(ir2.getAjuste_aluguel());
					
				}
				if(ir2.getAjuste_txadm()!=null) {
					ir.setAjuste_txadm(ir2.getAjuste_txadm());
					
				}
				
			}
			
			//ir.setResultados();
			
			irs.add(ir);
			
		}

		for (BoletoAbsi boleto : lboleto) {
			irs.get(boleto.getDt_pagamento().getMonth()).setValor_aluguel(boleto.getvalor_aluguel());
			irs.get(boleto.getDt_pagamento().getMonth()).setValor_txadm(boleto.getValor_comissao());
			irs.get(boleto.getDt_pagamento().getMonth()).setMes_ref(boleto.getDt_pagamento());
			irs.get(boleto.getDt_pagamento().getMonth()).setMes_refFormat();
			irs.get(boleto.getDt_pagamento().getMonth()).setMulta_aluguel(boleto.getValor_juros());
			
			
			setListaItensLocacao(boleto.getMes_ref());
			
			for (ItensLocacao item : listaItensLocacao) {

				if (item.getTipo().equals("tabK")) {
					//Acordo Extrajudicial
					//aluguel
					irs.get(boleto.getDt_pagamento().getMonth()).setValor_aluguel(item.getValor());
				} else if (item.getTipo().equals("tabT")) {
					//Taxa de Administração Acordo Extrajudicial
					//tx
					irs.get(boleto.getDt_pagamento().getMonth()).setValor_txadm(item.getValor());
				} else if (item.getTipo().equals("tabU")) {
					//Rescisão Contratual
					//aluguel
					irs.get(boleto.getDt_pagamento().getMonth()).setValor_aluguel(item.getValor());
				} else if (item.getTipo().equals("tabV")) {
					//Taxa de Administração Rescisão Contratual
					//tx
					irs.get(boleto.getDt_pagamento().getMonth()).setValor_txadm(item.getValor());
				}

			}
			//irs.get(boleto.getDt_pagamento().getMonth()).setAjustes();
			//irs.get(boleto.getDt_pagamento().getMonth()).setResultados();
		}
		for (int i = 1; i <= 12; i++) {
			System.out.println("valor ajuste aluguel1: " + irs.get(i-1).getResultado_aluguel());
			irs.get(i-1).setAjustes();
			System.out.println("valor ajuste aluguel2: " + irs.get(i-1).getResultado_aluguel());
			irs.get(i-1).setResultados();
			System.out.println("valor ajuste aluguel3: " + irs.get(i-1).getResultado_aluguel());
			
			
			
			
			
		}

	}

	public ArrayList<IR> getIrs() {
		return irs;
	}

	public void setIrs(ArrayList<IR> irs) {
		this.irs = irs;
	}

	public void setBoleto(BoletoAbsi boleto) {
		this.boleto = boleto;
	}

	public void setLboleto(ListaBoletoAbsi lboleto) {
		this.lboleto = lboleto;
	}

	@Override
	public void setDados(ResultSet rs) throws SQLException {

		modalidade = rs.getString("modalidade");
		valor_deposito = rs.getString("valor_deposito");
		valor_seguro = rs.getString("valor_seguro");

		contrato = ContratoFactory.recuperarPorId(rs.getInt("fk_id_contrato"));
		locatario = PessoaFactory.recuperarPorID(rs.getInt("fk_id_locatario"));
		imovel = ImovelFactory.recuperarPorId(rs.getInt("fk_id_imovel"));

		if (modalidade.equalsIgnoreCase("fiador")) {
			listaInfoFiador = InfoFiadorFactory.recuperarPorLocacaoId(getId());
		}
	}

	@Override
	@SuppressWarnings("rawtypes")
	public void salvar(Connection conn) throws SQLException {
		DAO dao = DAOFactory.getDAO(this);

		System.out.println("------------------------------------  salvar Locacao");

		this.getContrato().salvar(conn);

		this.getImovel().salvar(conn);

		if (isPersistente()) {
			dao.atualizar(conn);
			setUpdated(true);

		} else {
			int id = dao.inserir(conn);
			this.setId(id);
			setPersistente(true);

			if (modalidade.equals("Fiador"))
				this.getListaInfoFiador().setId(id);

			if (this.getListaItensLocacao() != null)
				this.getListaItensLocacao().setId(id);
		}

		if (this.getListaInfoFiador() != null)
			getListaInfoFiador().salvar(conn);

		if (this.getListaItensLocacao() != null)
			getListaItensLocacao().salvar(conn);

	}

	@Override
	public String toString() {
		return "Locacao [modalidade=" + modalidade + ", valor_deposito=" + valor_deposito + ", valor_seguro="
				+ valor_seguro + ", locatario=" + locatario + ", imovel=" + imovel + ", contrato=" + contrato
				+ ", listaInfoFiador=" + listaInfoFiador + ", listaItensLocacao=" + listaItensLocacao + "]";
	}

}
