package dominio.bean.entity;

import java.sql.ResultSet;
import java.sql.SQLException;

import dao.Persistente;

public class DadosBancarios extends Persistente {
	private String nome_banco;
	private String agencia;
	private String conta;
	private String tp_conta;
	private String titular;

	public DadosBancarios() {
		super();
	}

	public DadosBancarios(String nome_banco, String agencia, String conta, String tp_conta, String titular) {
		super();
		this.nome_banco = nome_banco;
		this.agencia = agencia;
		this.conta = conta;
		this.tp_conta = tp_conta;
		this.titular = titular;
	}

	public String getNome_banco() {
		return nome_banco;
	}

	public void setNome_banco(String nome_banco) {
		this.nome_banco = nome_banco;
	}

	public String getAgencia() {
		return agencia;
	}

	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}

	public String getConta() {
		return conta;
	}

	public void setConta(String conta) {
		this.conta = conta;
	}

	public String getTp_conta() {
		return tp_conta;
	}

	public void setTp_conta(String tp_conta) {
		this.tp_conta = tp_conta;
	}

	public String getTitular() {
		return titular;
	}

	public void setTitular(String titular) {
		this.titular = titular;
	}

	@Override
	public void setDados(ResultSet rs) throws SQLException {
		nome_banco = rs.getString("nome_banco");
		agencia = rs.getString("agencia");
		conta = rs.getString("conta");
		tp_conta = rs.getString("tp_conta");
		titular = rs.getString("titular");
	}

	@Override
	public String toString() {
		return "DadosBancarios [nome_banco=" + nome_banco + ", agencia=" + agencia
				+ ", conta=" + conta + ", tp_conta=" + tp_conta + ", titular=" + titular + "]";
	}

}
