package dao;

public class DAOFactory {

    private static IDAOFactory daoFactory;

    public static void setDaoFactory(IDAOFactory daoFactory) {
        DAOFactory.daoFactory = daoFactory;
    }

    @SuppressWarnings("rawtypes")
	public static DAO getDAO(Persistente persistente) {
        return daoFactory.getDAO(persistente);
    }

    @SuppressWarnings("rawtypes")
	public static DAOLista getDAO(ListaPersistente lista) {
        return daoFactory.getDAO(lista);
    }
}
