package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@XmlRootElement
public abstract class Persistente {
    
    private Integer id;
    private boolean persistente;
    private boolean updated;

    public Persistente() {
        persistente = false;
        updated = false;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
    @XmlTransient
    public boolean isPersistente() {
        return persistente;
    }

    public void setPersistente(boolean persistente) {
        this.persistente = persistente;
    }
    
        
    public boolean isUpdated() {
		return updated;
	}

	public void setUpdated(boolean updated) {
		this.updated = updated;
	}

	@SuppressWarnings("rawtypes")
    public void salvar(Connection conn) throws SQLException {
        DAO dao = DAOFactory.getDAO(this);
        
        if (isPersistente()){
            dao.atualizar(conn);
            setUpdated(true);
        }
        else {
            id = dao.inserir(conn);
            setPersistente(true);
        }
    }

    @SuppressWarnings("rawtypes")
    public void excluir(Connection conn) throws SQLException {
        if (isPersistente()) {
            DAO dao = DAOFactory.getDAO(this);

            dao.excluir(conn);
            
            this.setPersistente(false);
        }
    }

    public abstract void setDados(ResultSet rs) throws SQLException;
}
