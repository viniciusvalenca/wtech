package dao;

public interface IDAOFactory {

    @SuppressWarnings("rawtypes")
	public DAO getDAO(Persistente persistente);

    @SuppressWarnings("rawtypes")
    public DAOLista getDAO(ListaPersistente lista);
}
