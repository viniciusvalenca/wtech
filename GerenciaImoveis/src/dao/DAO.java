package dao;

import java.sql.Connection;
import java.sql.SQLException;

public abstract class DAO<T> {

    protected T persistente;
    
    public DAO(T persistente) {
        this.persistente = persistente;
    }

    public abstract Integer inserir(Connection conn) throws SQLException;

    public abstract void atualizar(Connection conn) throws SQLException;

    public abstract void excluir(Connection conn) throws SQLException;

	
}
