package dao;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

public class ConexaoBD {

	public static DataSource dataSource = null;

	public static void iniciar() {
		try {

			System.out.println("-----------------------------------------------------");
			System.out.println("init method has been called and servlet is initialized");

			/*
			 * Using JDNI lookup get the DataSource.
			 */

			Context initContext = new InitialContext();
			Context envContext = (Context) initContext.lookup("java:/comp/env");
			dataSource = (DataSource) envContext.lookup("jdbc/absi");

			System.out.println("Using JDNI lookup got the DataSource : " + dataSource);

			System.out.println("-----------------------------------------------------");

		}

		catch (Exception exe) {
			exe.printStackTrace();
		}

	}

	public static DataSource getPool() {

		return dataSource;

	}

	public static Connection getConnection() throws SQLException {

		return dataSource.getConnection();
	}

}
