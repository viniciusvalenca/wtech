package dao;


import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class ListaPersistente<T extends Persistente> extends ArrayList<T> {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void salvar(Connection conn) throws SQLException {
        for (T obj : this)
            obj.salvar(conn);
    }

    public void excluir(Connection conn) throws SQLException {
        for (T obj : this)
            obj.excluir(conn);
    }

    public abstract T getPersistente();

    public abstract void addPersistente(T persistente);
}
