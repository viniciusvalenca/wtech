package dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

public class TransactionController {

	private ArrayList<Persistente> list;

	public TransactionController() {
		super();
		list = new ArrayList<>();
	}

	public void add(Persistente obj) {
		list.add(obj);

	}
	
	public void add(ListaPersistente obj) {
		list.addAll(obj);


	}

	public void salvar() throws SQLException {
		Connection conn = ConexaoBD.getConnection();
		conn.setAutoCommit(false);
		
		try {

			for (Persistente obj : list) {
				obj.salvar(conn);
			}
			
			conn.commit();

		} catch (Exception ex) {
			ex.printStackTrace();
			
			for (Persistente obj : list) {
				obj.setPersistente(false);
				obj.setUpdated(false);
			}
			
			conn.rollback();

		}
		finally{
			conn.close();
		}

	}

}
