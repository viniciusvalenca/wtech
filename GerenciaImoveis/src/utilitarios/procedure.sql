

delimiter //
CREATE PROCEDURE renovarContrato() 
BEGIN
DECLARE _dt_termino date;
DECLARE _dt_entrega_chave date;
  DECLARE done BOOLEAN DEFAULT FALSE;
  DECLARE _id BIGINT UNSIGNED;
  DECLARE cur CURSOR FOR SELECT id FROM contrato; 
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done := TRUE;


  OPEN cur;
  testLoop: LOOP
    FETCH cur INTO _id;
    IF done THEN
      LEAVE testLoop;
    END IF;

SET _dt_termino = (SELECT dt_termino FROM contrato where id=_id);
SET _dt_entrega_chave = (SELECT dt_entrega_chave FROM contrato where id=_id);

 IF (_dt_termino = curdate() AND _dt_entrega_chave IS NULL AND flg_renovar==1)THEN
	UPDATE contrato SET dt_termino= DATE_ADD(_dt_termino, INTERVAL 30 MONTH) WHERE id=_id;
	UPDATE contrato SET flg_renovar= 0 WHERE id=_id;
END IF;

  END LOOP testLoop;

  CLOSE cur;
END//
delimiter ;

delimiter //
CREATE PROCEDURE aplicarIndice() 
BEGIN
DECLARE _dt_reajuste date;
DECLARE _dt_entrega_chave date;
DECLARE _id_locacao INT;
DECLARE _valor DECIMAL(10,2);
DECLARE _indice DECIMAL(10,3);

  DECLARE done BOOLEAN DEFAULT FALSE;
  DECLARE _id BIGINT UNSIGNED;
  DECLARE cur CURSOR FOR SELECT id FROM contrato; 
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done := TRUE;


  OPEN cur;
  testLoop: LOOP
    FETCH cur INTO _id;
    IF done THEN
      LEAVE testLoop;
    END IF;

SET _dt_entrega_chave = (SELECT dt_entrega_chave FROM contrato where id=_id);
SET _dt_reajuste = (SELECT dt_reajuste FROM contrato where id=_id);
SET _indice = (SELECT indice_reajuste FROM indice where id=1);

 IF (_dt_reajuste = curdate() AND _dt_entrega_chave IS NULL )THEN
 
 SET _id_locacao = (SELECT l.id FROM locacao l inner join contrato c where c.id=_id and c.id = l.fk_id_contrato);
	SET _valor = (SELECT valor from imovel i inner join locacao l  WHERE l.id=_id_locacao and l.fk_id_imovel=i.id);
        
    UPDATE imovel i inner join locacao l SET valor= _valor *ROUND(1+(_indice/100),4)  WHERE l.id=_id_locacao and l.fk_id_imovel=i.id;
    
END IF;

  END LOOP testLoop;

  CLOSE cur;
END//
delimiter ;