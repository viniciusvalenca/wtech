package utilitarios;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.Normalizer;
import java.util.Calendar;

public abstract class util {

	//funcionando
	public static String removerCaracter(String str){
		String temp;
		temp = str.replace(".", "");
		temp = temp.replaceAll("-", "");
		return temp;
	}
	
	public static String removerCaracterCNPJ(String str){
		String temp;
		temp = str.replace(".", "");
		temp = temp.replaceAll("-", "");
		temp = temp.replaceAll("/", "");
		return temp;
	}
	
	//funcionando
	public static Integer getDDD(String str){
		String temp;
		temp = str.substring(1, 3);
		return Integer.valueOf(temp);
	}
	
	//funcionando
	public static Integer getNumero(String str){
		String temp;
		temp = str.substring(5, str.length());
		temp = removerCaracter(temp);
		return Integer.valueOf(temp);
	}
	
	//funcionando
	
	public static String FormatCelular(String str){
		String temp1, temp2;
		temp1 = str.substring(0, 5);
		temp2 = str.substring(5, 9);
		return temp1 + "-" + temp2;
	}
	
	//funcionando
	public static String FormatTelefone(String str){
		String temp1, temp2;
		temp1 = str.substring(0, 4);
		temp2 = str.substring(4, 8);
		return temp1 + "-" + temp2;
	}
	
	public static String formatData(String dt){
		String temp, dia, mes;
		dia = dt.substring(0, 2);
		mes = dt.substring(2, 4);
		
		Calendar now = Calendar.getInstance();
		int year = now.get(Calendar.YEAR);
		String yearInString = String.valueOf(year);
		
		temp = yearInString + "-" + mes + "-" + dia;
		
		return temp;
	}
	
	public static Integer getAno(String dt){
		String[] v = dt.split("/");
		String temp = v[1];
		return Integer.valueOf(temp);
	}
	
	public static Integer getMes(String dt){
		String[] v = dt.split("/");
		String temp = v[0];
		return Integer.valueOf(temp);
	}
	//funcionando
	public static String converterMesRef(String dt){
		String[] v = dt.split("/");
		String temp = v[1] + "-" + v[0] + "-01";
		return temp;
	}
	
	//funcionando
		public static String converterMesRef2(String dt){
			String[] v = dt.split("/");
			String temp = v[2] + "-" + v[1] + "-" + v[0];
			return temp;
		}
	
	public static String converterData(String dt){
		String[] v = dt.split("/");
		String temp = v[2] + "-" + v[1] + "-" + v[0];
		return temp;
	}
	
	public static String getNumBanco(String str){
		str = str.replaceAll(" ", "");
		String[] v = str.split("-");
		return v[0];
	}
	
	public static String stringToBigDecimal(String str){
		String[] v = str.split(",");
		String temp = v[0];
		temp = temp.replace(".", "");
		return temp + "." + v[1];
	}
	
	public static String stringToBigDecimalRS(String str){		
		str=str.replace("R", "").replace("$", "").replace(" ", "");
		String[] v = str.split(",");
		int size = v.length;
		String temp = v[0];
		temp = temp.replace(".", "");
		if(size>1) {
		return temp + "." + v[1];
	}else {
		return temp + ".00";
	}
	}

	//funcionando
	public static String bigdecimalToPage(BigDecimal big){
		String str = "";
		
		if(big == null || big.toString() == "0"){
			str = "0.00";
		}else{
			big = big.setScale(2, RoundingMode.CEILING);
			str = big.toString();
		}
	
		return str.replace(".", ",");
	}
	
	public static String bigdecimalToPageINDICE(BigDecimal big){
			String str = "";
			
			if(big == null || big.toString() == "0"){
				str = "0.000";
			}else{
				big = big.setScale(3, RoundingMode.CEILING);
				str = big.toString();
			}
		
			return str.replace(".", ",");
		}
	
	public static BigDecimal bigdecimalToBD(String str){
		str = str.replace(",", ".");
		BigDecimal big = new BigDecimal(str); 
		return big;
	}
	
	//funcionando
	public static String dotToComma(String str) {
		String temp = str.toString();

		String[] v = temp.split("\\.");
		
		temp = v[0];
		temp = temp.replace(",", ".");
		temp = temp + "," + v[1];

		return temp;
	}
	
	public static String unaccent(String src) {
		return Normalizer
				.normalize(src, Normalizer.Form.NFD)
				.replaceAll("[^\\p{ASCII}]", "");
	}
	
	public static int getHoursUntilTarget(int targetHour) {
	    Calendar calendar = Calendar.getInstance();
	    int hour = calendar.get(Calendar.HOUR_OF_DAY);
	    return hour < targetHour ? targetHour - hour : targetHour - hour + 24;
	}
	
	public static boolean validarCPF(String cpf){
		return ValidarCpfCnpj.isValidCPF(removerCaracter(cpf));
	}
	
	public static boolean validarCNPJ(String cnpj){
		return ValidarCpfCnpj.isValidCNPJ(removerCaracterCNPJ(cnpj));
	}
}
