package utilitarios;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

public class Upload {

	private static InputStream inputStream;

	public Upload() {
		super();
	}

	public static InputStream uploadImagem(HttpServletRequest request, Part filePart)
			throws IllegalStateException, IOException, ServletException {
		inputStream = filePart.getInputStream();
		System.out.println(filePart.getSize());
		if(filePart.getSize() > 0){
			return inputStream;
		}
		           
		return null;
	}

	public void mostrar(HttpServletResponse response, InputStream imagem) throws IOException {
		inputStream = new BufferedInputStream(imagem);
		BufferedImage bufferedImage = ImageIO.read(inputStream);
		response.setContentType("image/png");
		ImageIO.write(bufferedImage, "png", response.getOutputStream());
	}

}