package utilitarios;

import java.awt.Desktop;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

public class DownloadFile {

	public static void downloadToPage(HttpServletResponse response, String reportLocation, String caminho, String nameFile) throws IOException {
		ServletOutputStream out = response.getOutputStream();
		
		response.setContentType("application/octet-stream");
		response.setHeader("Content-Disposition", "attachment;filename=" + nameFile);
		
		File file = new File(reportLocation + caminho + nameFile);

		FileInputStream fileIn = new FileInputStream(file);

		int tamanho = (int) file.length();
		
		byte[] outputByte = new byte[tamanho];
		
		while(fileIn.read(outputByte, 0, tamanho) != -1){
			out.write(outputByte, 0, tamanho);
		}
		
		fileIn.close();
		out.flush();
	}
	
	public static void downloadPDF(HttpServletResponse response, byte[] b, String nameFile) throws IOException {
		ServletOutputStream out = response.getOutputStream();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		
		baos = new ByteArrayOutputStream(b.length);
		baos.write(b, 0, b.length);
		
		response.setHeader("Content-Disposition", "attachment; filename=" + nameFile);
		response.setContentType("application/pdf");
		response.setContentType("application/download");
		response.setHeader("Pragma", "no-cache");
		response.setContentLength(baos.size());
		
		baos.writeTo(out);
		out.flush();
	}
	
	public static void openFile(File file) throws IOException{
		Desktop desktop = Desktop.getDesktop();
        if(file.exists()) desktop.open(file);
	}
}
