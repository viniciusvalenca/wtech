package utilitarios;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public abstract class ConverterData {

	public static java.sql.Date incrementDiaDeProtesto(java.sql.Date mes_ref, int dia){
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(mes_ref);
		gc.set(gc.get(Calendar.YEAR), gc.get(Calendar.MONTH), dia);
		return new java.sql.Date((gc.getTime()).getTime());
	}
	
	public static java.sql.Date incrementDiaVencimento(int ano, int dia, int mes){
		GregorianCalendar gc = new GregorianCalendar();
		System.out.println(ano + " - " + dia + " - " + mes); 
		
		gc.set(ano, mes, dia);
		
		return new java.sql.Date((gc.getTime()).getTime());
	}
	
	public static java.sql.Date incrementMesRef(java.sql.Date mes_ref){
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(mes_ref);
		gc.add(Calendar.MONTH, 1);
		return new java.sql.Date((gc.getTime()).getTime());
	}
	
	public static java.sql.Date decrementMesRef(java.sql.Date mes_ref){
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(mes_ref);
		gc.add(Calendar.MONTH, -1);
		return new java.sql.Date((gc.getTime()).getTime());
	}
	
	public static java.sql.Date createDateFromMesRef(String mes_ref) throws ParseException{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return new java.sql.Date(sdf.parse(util.converterMesRef(mes_ref)).getTime());
	}
	
	public static java.sql.Date createDateFromMesRef2(String mes_ref) throws ParseException{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return new java.sql.Date(sdf.parse(util.converterMesRef2(mes_ref)).getTime());
	}
	
	public static Date stringToDate(String dt) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date startDatestart = sdf.parse(dt);
		return startDatestart;
	}

	public static java.sql.Date stringToDateSqlBD(String dt) throws ParseException {
		DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
		java.sql.Date data = new java.sql.Date(fmt.parse(dt).getTime());
		return data;
	}
	
	public static java.sql.Date getDateTimeNow() throws ParseException {
		Date date = new Date();
		String dt = formatBanco(date);
		DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		java.sql.Date data2 = new java.sql.Date(fmt.parse(dt).getTime());
		return data2;
	}
	
	public static String getMesExtenso() throws ParseException {
        return new SimpleDateFormat("MMMM").format(new Date());
	}
	
	public static String getDateStringRemessa() throws ParseException {
        return new SimpleDateFormat("dd-MM-yyyy").format(new Date());
	}
	
	public static String formatBancoRetorno(Date data) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		return sdf.format(data);
	}

	public static java.sql.Date getDateNow() throws ParseException {
		Date date = new Date();
		String dt = formatBanco(date);
		DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
		java.sql.Date data2 = new java.sql.Date(fmt.parse(dt).getTime());
		return data2;
	}
	
	public static String formatBanco(Date data) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		return sdf.format(data);
	}
	
	public static String getOnlyDateString(Date data) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		return data != null ? sdf.format(data) : null;
	}
	
	public static String getMes_refString(Date data) {
		SimpleDateFormat sdf = new SimpleDateFormat("MM/yyyy");
		return data != null ? sdf.format(data) : null;
	}
	
	public static String getDateHourString(Date data) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
		return sdf.format(data);
	}

	public static String horaToString(Date data) {
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		return sdf.format(data);
	}
	
	public static String timestampToString(Timestamp dt) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:MM:ss");
		return sdf.format(dt);
	}

}
