package utilitarios;

import java.io.IOException;
import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import java.text.SimpleDateFormat;

import org.apache.commons.mail.ByteArrayDataSource;
import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.MultiPartEmail;
import org.apache.commons.mail.SimpleEmail;

public abstract class Email {

	public static String EMAIL_REMETENTE = "absinegociosimobiliarios@gmail.com";
	public static String NOME_REMETENTE = "Absi Negócios Imobiliários Ltda";
	public static String PASSWORD = "SAFerraz1961";
	public static String HOST_NAME = "smtp.gmail.com";

	public static void emailBoleto(byte[] boleto, String emailDestinatario, String nomeDestinatario, String cc) throws MessagingException, EmailException {
		try {

			ByteArrayDataSource dataSource = new ByteArrayDataSource(boleto, "application/pdf");
			// DataHandler dataHandler = new DataHandler(dataSource);

			Properties props = new Properties();

			props.put("mail.smtp.host", HOST_NAME);
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.port", "587");
			props.put("mail.smtp.starttls.enable",true);
			props.put("mail.smtp.ssl.protocols", "TLSv1.2");

			// get Session
			Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(EMAIL_REMETENTE, PASSWORD);
				}
			});
			MimeMessage message = new MimeMessage(session);

			if (emailDestinatario != null && !emailDestinatario.equals(""))
				message.addRecipient(Message.RecipientType.TO,
						new InternetAddress(emailDestinatario, nomeDestinatario));
			if (cc != null && !cc.equals(""))
				message.addRecipient(Message.RecipientType.TO, new InternetAddress(cc, nomeDestinatario));

			message.setFrom(new InternetAddress(EMAIL_REMETENTE));
			message.setReplyTo(InternetAddress.parse(emailDestinatario, false));
			message.setSubject("Boleto Bancário", "UTF-8");

			String nomeArquivo = nomeDestinatario + "_" + new SimpleDateFormat("dd-MM-yyyy").format(new Date());
			

			// creates message part
			MimeBodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setContent("Segue, em anexo, boleto bancário.", "text/html; charset=UTF-8");

			messageBodyPart.setDataHandler(new DataHandler(dataSource));
			messageBodyPart.setFileName(nomeArquivo + ".pdf");
			
			// creates multi-part
			Multipart multipart = new MimeMultipart("related");
			multipart.addBodyPart(messageBodyPart);
			
			

			message.setContent(multipart);
			Transport.send(message);

			
			

		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public static void emailLocador(byte[] boleto, String emailDestinatario, String nomeDestinatario, String cc) throws MessagingException, EmailException {
		try {

			ByteArrayDataSource dataSource = new ByteArrayDataSource(boleto, "application/pdf");
			// DataHandler dataHandler = new DataHandler(dataSource);


			Properties props = new Properties();

			props.put("mail.smtp.host", HOST_NAME);
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.port", "587");
			props.put("mail.smtp.starttls.enable",true);
			props.put("mail.smtp.ssl.protocols", "TLSv1.2");

			// get Session
			Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(EMAIL_REMETENTE, PASSWORD);
				}
			});
			MimeMessage message = new MimeMessage(session);

			if (emailDestinatario != null && !emailDestinatario.equals(""))
				message.addRecipient(Message.RecipientType.TO,
						new InternetAddress(emailDestinatario, nomeDestinatario));
			if (cc != null && !cc.equals(""))
				message.addRecipient(Message.RecipientType.TO, new InternetAddress(cc, nomeDestinatario));

			message.setFrom(new InternetAddress(EMAIL_REMETENTE));
			message.setReplyTo(InternetAddress.parse(emailDestinatario, false));
			message.setSubject("Relatório Mensal", "UTF-8");
			
			

			String nomeArquivo = nomeDestinatario + "_" + new SimpleDateFormat("dd-MM-yyyy").format(new Date());
			
			// creates message part
						MimeBodyPart messageBodyPart = new MimeBodyPart();
						messageBodyPart.setContent("Segue, em anexo, relatório mensal.", "text/html; charset=UTF-8");

						messageBodyPart.setDataHandler(new DataHandler(dataSource));
						messageBodyPart.setFileName(nomeArquivo + ".pdf");
						
						// creates multi-part
						Multipart multipart = new MimeMultipart("related");
						multipart.addBodyPart(messageBodyPart);
						
						

						message.setContent(multipart);
						Transport.send(message);

			

		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public static void emailIr(byte[] boleto, String emailDestinatario, String nomeDestinatario, String cc) throws MessagingException, EmailException {
		try {

			ByteArrayDataSource dataSource = new ByteArrayDataSource(boleto, "application/pdf");
			// DataHandler dataHandler = new DataHandler(dataSource);


			Properties props = new Properties();

			props.put("mail.smtp.host", HOST_NAME);
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.port", "587");
			props.put("mail.smtp.starttls.enable",true);
			props.put("mail.smtp.ssl.protocols", "TLSv1.2");

			// get Session
			Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(EMAIL_REMETENTE, PASSWORD);
				}
			});
			MimeMessage message = new MimeMessage(session);

			if (emailDestinatario != null && !emailDestinatario.equals(""))
				message.addRecipient(Message.RecipientType.TO,
						new InternetAddress(emailDestinatario, nomeDestinatario));
			if (cc != null && !cc.equals(""))
				message.addRecipient(Message.RecipientType.TO, new InternetAddress(cc, nomeDestinatario));

			message.setFrom(new InternetAddress(EMAIL_REMETENTE));
			message.setReplyTo(InternetAddress.parse(emailDestinatario, false));
			message.setSubject("Informe de rendimentos", "UTF-8");
			

			String nomeArquivo = nomeDestinatario + "_" + new SimpleDateFormat("dd-MM-yyyy").format(new Date());

			// creates message part
						MimeBodyPart messageBodyPart = new MimeBodyPart();
						messageBodyPart.setContent("Segue, em anexo, informe de rendimentos.", "text/html; charset=UTF-8");

						messageBodyPart.setDataHandler(new DataHandler(dataSource));
						messageBodyPart.setFileName(nomeArquivo + ".pdf");
						
						// creates multi-part
						Multipart multipart = new MimeMultipart("related");
						multipart.addBodyPart(messageBodyPart);
						
						

						message.setContent(multipart);
						Transport.send(message);


		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

}
